package ru.ml.core.metadata.handler.custom;

import com.google.inject.Inject;
import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.handler.DefaultMlClassHandler;
import ru.ml.core.model.MlDynamicEntityImpl;

import java.util.GregorianCalendar;

@Deprecated
public class VCardHandler
        extends DefaultMlClassHandler {
    @Inject
    CommonDao commonDao;

    public void beforeCreate(DynamicEntityImpl entity) {
        if (entity.get("type") == null) {
            throw new MlApplicationException("��� ����� �� ������, ���������� ���������");
        }
        String cardNum = "";

        String cardType = (String) entity.get("type");
        if (cardType.equals("PERM_CARD")) {
            DynamicEntityImpl holder = (DynamicEntityImpl) entity.get("holder");
            String holderId = (String) holder.get("holderid");

            String cardsQuery = String.format("SELECT count(c.id) FROM Card c WHERE c.holder.id = %s AND c.type='PERM_CARD'", new Object[]{holder.get("id")});
            long cardCount = ((Long) this.commonDao.getQuery(cardsQuery).getSingleResult()).longValue();

            String cardnum = String.format("%02d", new Object[]{Long.valueOf(cardCount + 1L)}) + holderId;

            int checkSum = generateCheckSum(cardnum);

            cardnum = cardnum + String.format("%02d", new Object[]{Integer.valueOf(checkSum)});
            entity.set("num", cardnum);
            cardNum = cardnum;
        } else if (cardType.equals("TEMP_CARD")) {
            DynamicEntityImpl settings = (MlDynamicEntityImpl) this.commonDao.findById(Long.valueOf(1L), "Settings");

            int lastTempCardId = 1;
            int nowYear = new GregorianCalendar().get(1) - 2000;

            String lastTempCard = (String) settings.get("lasttempcardnum");
            if (lastTempCard != null) {
                int year = Integer.parseInt(lastTempCard.substring(0, 2));
                if (year == nowYear) {
                    lastTempCardId = Integer.parseInt(lastTempCard.substring(2));
                    lastTempCardId++;
                }
            }
            String orgcode = (String) settings.get("orgcode");

            String tempCardId = nowYear + String.format("%06d", new Object[]{Integer.valueOf(lastTempCardId)});
            String fullTempCardId = "00" + tempCardId + orgcode;

            int checkSum = generateCheckSum(fullTempCardId);

            fullTempCardId = fullTempCardId + checkSum;
            entity.set("num", fullTempCardId);

            settings.set("lasttempcardnum", tempCardId);
            cardNum = fullTempCardId;
        }
        String personum = createHexValue(cardNum);
        entity.set("personum", personum);
    }

    private int generateCheckSum(String cardnum) {
        int checkSum = 0;
        for (int i = cardnum.length() - 1; i >= 0; i--) {
            checkSum += (14 - i) * Integer.parseInt(cardnum.charAt(i) + "");
        }
        checkSum %= 101;
        if (checkSum == 100) {
            checkSum = 0;
        }
        return checkSum;
    }

    private static String createHexValue(String cardNumber) {
        if (cardNumber.length() != 16) {
            throw new MlApplicationException("Card number length not equals 16 symbols. Length = " + cardNumber.length());
        }
        String firstPart = codeFirstPart(cardNumber.substring(0, 10));
        while (firstPart.length() != 8) {
            firstPart = "0" + firstPart;
        }
        String secondPart = Long.toHexString(Long.parseLong(cardNumber.substring(10, 14)));
        while (secondPart.length() != 4) {
            secondPart = "0" + secondPart;
        }
        String thirdPart = Long.toHexString(Long.parseLong(cardNumber.substring(14, 16)));
        if (thirdPart.length() % 2 != 0) {
            thirdPart = "0" + thirdPart;
        }
        return (firstPart + secondPart + thirdPart).toUpperCase();
    }

    private static String codeFirstPart(String firstPart) {
        int issueNumber = Integer.parseInt(firstPart.substring(0, 2));
        int year = Integer.parseInt(firstPart.substring(2, 4));
        int number = Integer.parseInt(firstPart.substring(4));

        int result = number | year << 19 | issueNumber << 25;
        return Integer.toHexString(result);
    }

    public static void main(String[] args) {
        VCardHandler v = new VCardHandler();
        String num = "0014000001300172";
        System.out.println(createHexValue(num));
    }
}
