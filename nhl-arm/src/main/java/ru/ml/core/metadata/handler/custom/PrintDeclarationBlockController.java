package ru.ml.core.metadata.handler.custom;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.metadata.api.MetaDataHelper;
import ru.ml.core.model.MlDynamicEntityImpl;
import ru.ml.core.model.page.MlPageBlockBase;
import ru.ml.core.model.system.MlClass;
import ru.ml.web.block.controller.BlockController;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

@Deprecated
public class PrintDeclarationBlockController
  implements BlockController
{
  @Inject
  private CommonDao commonDao;
  
  public void serve(MlHttpServletRequest request, MlPageBlockBase mlInstance, MlHttpServletResponse resp)
    throws MlApplicationException, MlServerException
  {
    MlDynamicEntityImpl settings = (MlDynamicEntityImpl)this.commonDao.getQuery("SELECT s FROM Settings s").getSingleResult();
    String template = (String)settings.get("decltemp");
    
    Long holderId = Long.valueOf(Long.parseLong(request.getString("objectId")));
    MlDynamicEntityImpl holder = (MlDynamicEntityImpl)this.commonDao.findById(holderId, "Holder");
    
    template = "<body onload=window.print()>" + template + "</body>";
    
    String filename = "declaration.html";
      Map<String, Object> data;
    FileOutputStream fos = null;
    String filePath = request.getRequest().getServletContext().getRealPath("/WEB-INF/templates/");
    File dir = new File(filePath);
    try
    {
      if (!dir.exists()) {
        dir.mkdirs();
      }
      File f = new File(filePath, filename);
      if (!f.exists()) {
        f.createNewFile();
      }
      fos = new FileOutputStream(f);
      fos.write(template.getBytes());
      if (fos != null) {
        try
        {
          fos.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
      data = new HashMap();
    }
    catch (IOException ioe)
    {
      throw new MlServerException(ioe.getMessage());
    }
    finally
    {
      if (fos != null) {
        try
        {
          fos.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }

    MlClass mlClass = MetaDataHelper.getMlClassByName("Card");
    MlDynamicEntityImpl card = (MlDynamicEntityImpl)this.commonDao.findById(request.getLong("objectId"), "Card");
    data.put("mlClass", mlClass);
    data.put("currentPageBlock", mlInstance);
    data.put("object", card);
    resp.addDataToJson("className", new JsonPrimitive(mlClass.getEntityName()));
    resp.setDataType(MlHttpServletResponse.DataType.JSON);
    resp.renderTemplateToJson(filename, data);
    
    holder.set("declprinted", Boolean.valueOf(true));
    this.commonDao.mergeTransactional(holder);
  }
}
