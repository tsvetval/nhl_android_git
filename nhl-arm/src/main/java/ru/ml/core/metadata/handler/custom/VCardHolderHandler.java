package ru.ml.core.metadata.handler.custom;

import com.google.inject.Inject;
import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.handler.DefaultMlClassHandler;
import ru.ml.core.model.MlDynamicEntityImpl;

import java.io.UnsupportedEncodingException;
import java.util.GregorianCalendar;
@Deprecated
public class VCardHolderHandler
        extends DefaultMlClassHandler {
    @Inject
    CommonDao commonDao;

    public void beforeCreate(DynamicEntityImpl entity) {
        if (entity.get("holderid") != null) {
            return;
        }
        DynamicEntityImpl settings = (MlDynamicEntityImpl) this.commonDao.findById(Long.valueOf(1L), "Settings");

        int lastHolderId = 1;
        int nowYear = new GregorianCalendar().get(1) - 2000;

        String lastHolder = (String) settings.get("lastholderid");
        if (lastHolder != null) {
            int year = Integer.parseInt(lastHolder.substring(0, 2));
            if (year == nowYear) {
                lastHolderId = Integer.parseInt(lastHolder.substring(2));
                lastHolderId++;
            }
        }
        String orgcode = (String) settings.get("orgcode");

        String holderId = nowYear + String.format("%06d", new Object[]{Integer.valueOf(lastHolderId)});
        String fullHolderId = holderId + orgcode;
        entity.set("holderid", fullHolderId);

        settings.set("lastholderid", holderId);
    }

    public void afterCreate(DynamicEntityImpl entity) {
        super.afterCreate(entity);
        updateFields(entity);
    }

    public void afterUpdate(DynamicEntityImpl entity, boolean runBeforeHandlers) {
        super.afterUpdate(entity, runBeforeHandlers);
        updateFields(entity);
    }

    private void updateFields(DynamicEntityImpl cardHolder) {
        DynamicEntityImpl settings = (MlDynamicEntityImpl) this.commonDao.findById(Long.valueOf(1L), "Settings");

        String vCard = createVCard(cardHolder, settings);
        String nDef = createPersVCard(convertToHexString(vCard));

        cardHolder.set("vcard", vCard);
        cardHolder.set("persovcard", nDef);
    }

    public String createPersVCard(String vCardHex) {
        StringBuilder nDef0 = new StringBuilder();
        nDef0.append("C20A0000");
        String vCardHexLength = Integer.toHexString(vCardHex.length() / 2).toUpperCase();
        nDef0.append(addZeroBytes(vCardHexLength, 2));
        nDef0.append("746578742f7663617264");
        nDef0.append(vCardHex);

        StringBuilder persoVCard = new StringBuilder();
        persoVCard.append("03");
        String nDef0HexLength = Integer.toHexString(nDef0.length() / 2).toUpperCase();
        persoVCard.append("FF" + addZeroBytes(nDef0HexLength, 2));

        persoVCard.append(nDef0);
        persoVCard.append("FE");

        return persoVCard.toString();
    }

    public String createVCard(DynamicEntityImpl cardHolder, DynamicEntityImpl settings) {
        String divider = "\n";
        String firstName = attributeEscaping(cardHolder.get("firstname").toString());
        String lastName = attributeEscaping(cardHolder.get("lastname").toString());
        String secondName = "";
        if (cardHolder.get("secondname") != null) {
            secondName = attributeEscaping(cardHolder.get("secondname").toString());
        }
        StringBuilder vCardBuilder = new StringBuilder();
        vCardBuilder.append("BEGIN:VCARD").append("\n");
        vCardBuilder.append("VERSION:3.0").append("\n");

//        if (cardHolder.get("email1") != null) {
//            String newLine = "EMAIL:" + attributeEscaping(cardHolder.get("email1").toString()) + "\n";
//            addWithCondition(vCardBuilder, newLine);
//        }
        if (cardHolder.get("workphone") != null) {
            String newLine = "TEL:" + attributeEscaping(cardHolder.get("workphone").toString()) + "\n";
            addWithCondition(vCardBuilder, newLine);
        }
        if (cardHolder.get("pos") != null) {
            String newLine = "TITLE:" + attributeEscaping(cardHolder.get("pos").toString()) + "\n";
            addWithCondition(vCardBuilder, newLine);
        }
        if (settings.get("orgname") != null) {
            String newLine = "ORG:" + attributeEscaping(settings.get("orgname").toString()) + "\n";
            addWithCondition(vCardBuilder, newLine);
        }
        vCardBuilder.append("FN:").append(firstName);
        if (!secondName.isEmpty()) {
            vCardBuilder.append(" ").append(secondName);
        }
        vCardBuilder.append(" ").append(lastName).append("\n");
        if (cardHolder.get("address") != null) {
            String newLine = "ADR:" + cardHolder.get("address").toString() + "\n";
            addWithCondition(vCardBuilder, newLine);
        }
        if (settings.get("orgwebsite") != null) {
            String newLine = "URL:" + attributeEscaping(settings.get("orgwebsite").toString()) + "\n";
            addWithCondition(vCardBuilder, newLine);
        }
        vCardBuilder.append("END:VCARD");

        return vCardBuilder.toString();
    }

    private String addZeroBytes(String value, int totalByteCount) {
        String result = "";
        for (int i = value.length(); i < totalByteCount * 2; i++) {
            result = result + "0";
        }
        result = result + value;
        return result;
    }

    private String convertToHexString(String inputString) {
        String result;
        try {
            result = binaryToHexString(inputString.getBytes("UTF8"));
        } catch (UnsupportedEncodingException e) {
            result = "Error in converting to HEX";
            e.printStackTrace();
        }
        return result;
    }

    private void addWithCondition(StringBuilder vCardBuilder, String newLine) {
        int maxLength = 728;
        if (convertToHexString(vCardBuilder.toString()).length() + convertToHexString(newLine).length() < 728) {
            vCardBuilder.append(newLine);
        }
    }

    private static String attributeEscaping(String attribute) {
        String result = "";
        if ((attribute != null) && (!attribute.isEmpty())) {
            result = attribute;

            result = result.replace("\\", "\\\\");
        }
        return result;
    }

    private static String binaryToHexString(byte[] bytes) {
        char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[(j * 2)] = hexArray[(v >>> 4)];
            hexChars[(j * 2 + 1)] = hexArray[(v & 0xF)];
        }
        return new String(hexChars);
    }
}
