package ru.ml.core.metadata.handler.custom;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.metadata.api.MetaDataHelper;
import ru.ml.core.model.MlDynamicEntityImpl;
import ru.ml.core.model.page.MlPageBlockBase;
import ru.ml.core.model.system.MlClass;
import ru.ml.web.block.controller.BlockController;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class ChangeStatusBlockController
        implements BlockController {
    private static final String SHOW = "show";
    private static final String ACTION_NOT_USED = "notUsed";
    private static final String ACTION_READY_TO_PERSONALIZE = "readyToPersonalized";
    private static final String ACTION_BLOCK = "block";
    private static final String ACTION_ACTIVE = "active";
    private static final String ACTION_CANCEL = "cancel";
    static final String TEMPLATE_PREFIX = "blocks/content/";
    static final String TEMPLATE_EDIT = "blocks/content/edit/object.hml";
    static final String TEMPLATE_VIEW = "blocks/content/view/object.hml";
    static final String TEMPLATE_SHOW = "blocks/status/status.hml";
    @Inject
    CommonDao commonDao;
    private static final Logger log = LoggerFactory.getLogger(ChangeStatusBlockController.class);

    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp)
            throws MlApplicationException, MlServerException {
        String action = params.getString("action");
        MlDynamicEntityImpl card;
        switch (action) {
            case "show":
                if (params.hasString("className")) {
                    showObject(params, mlInstance, resp, "blocks/status/status.hml");
                } else {
                    throw new MlApplicationException("������������ ��������� �������, � ������� ������ �������������� [folderId] ��� [className]");
                }
                break;
            case "readyToPersonalized":
                card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
                card.set("status", "READY_TO_PERSONALIZE");
                this.commonDao.mergeTransactional(card);

                showObject(params, mlInstance, resp);
                break;
            case "block":
                card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
                card.set("status", "BLOCKED");
                this.commonDao.mergeTransactional(card);

                showObject(params, mlInstance, resp);
                break;
            case "notUsed":
                card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
                card.set("status", "NOT_USED");
                this.commonDao.mergeTransactional(card);

                showObject(params, mlInstance, resp);
                break;
            case "active":
                card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
                MlDynamicEntityImpl holder = (MlDynamicEntityImpl) card.get("holder");
                String jpql = String.format("SELECT c FROM Card c WHERE c.holder.id = %s AND c.status = 'ACTIVE'", new Object[]{holder.get("id")});
                List<MlDynamicEntityImpl> activeCardsList = this.commonDao.getQuery(jpql).getResultList();
                for (MlDynamicEntityImpl entity : activeCardsList) {
                    entity.set("status", "BLOCKED");
                    this.commonDao.mergeTransactional(entity);
                }
                card.set("status", "ACTIVE");
                this.commonDao.mergeTransactional(card);

                showObject(params, mlInstance, resp);
                break;
            case "cancel":
                showObject(params, mlInstance, resp);
                break;
            default:
                log.error("Unknown action: " + action);
                throw new MlApplicationException("������ ���������� (" + action + ") ���� �� ����������");
        }
    }

    private void showObject(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) {
        Map<String, Object> data = new HashMap();
        MlClass mlClass = MetaDataHelper.getMlClassByName("Card");
        MlDynamicEntityImpl card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
        data.put("mlClass", mlClass);
        data.put("currentPageBlock", mlInstance);
        data.put("object", card);
        resp.addDataToJson("className", new JsonPrimitive(mlClass.getEntityName()));
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
    }

    private void showObject(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp, String action) {
        Map<String, Object> data = new HashMap();
        MlClass mlClass = MetaDataHelper.getMlClassByName("Card");
        MlDynamicEntityImpl card = (MlDynamicEntityImpl) this.commonDao.findById(params.getLong("objectId"), "Card");
        data.put("mlClass", mlClass);
        data.put("currentPageBlock", mlInstance);
        data.put("object", card);
        resp.addDataToJson("className", new JsonPrimitive(mlClass.getEntityName()));
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.renderTemplateToJson(action, data);
    }
}
