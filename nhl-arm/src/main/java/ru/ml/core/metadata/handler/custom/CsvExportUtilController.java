package ru.ml.core.metadata.handler.custom;

import au.com.bytecode.opencsv.CSVWriter;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.datatype.DatatypeConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.model.MlDynamicEntityImpl;
import ru.ml.core.model.util.MlUtil;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.utils.controller.utils.AbstractUtilController;

@Deprecated
public class CsvExportUtilController
  extends AbstractUtilController
{
  private static final Logger log = LoggerFactory.getLogger(CsvExportUtilController.class);
  @Inject
  private CommonDao commonDao;
  @Inject
  protected EntityManager entityManager;
  
  public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp)
    throws IOException, ParseException, DatatypeConfigurationException
  {
    String sql = String.format("SELECT personum, num, to_char(expiredate, 'DD.MM.YYYY HH-mm'), holderid, holderorgid, lastname, firstname, secondname, active, CASE WHEN holder IS NOT NULL AND active AND status = 'ACTIVE' AND expiredate >= current_date AND  m.\"Service_id\" = %s THEN 1 ELSE 0 END AS access FROM \"Card\" c  LEFT JOIN \"Holder\" h ON c.holder = h.id LEFT JOIN \"MN_Holder_Service\" m ON m.\"Holder_id\" = h.id ", new Object[] { request.getString("objectId") });
    
    EntityTransaction transaction = this.entityManager.getTransaction();
    FileOutputStream fos = null;
    CSVWriter csvWriter = null;
    Long serviceId = Long.valueOf(Long.parseLong(request.getString("objectId")));
    MlDynamicEntityImpl service = (MlDynamicEntityImpl)this.commonDao.findById(serviceId, "Service");
    String code = (String)service.get("code");
    MlDynamicEntityImpl settings = (MlDynamicEntityImpl)this.commonDao.getQuery("SELECT s FROM Settings s").getSingleResult();
    String savePath = (String)settings.get("exportfolder");
    try
    {
      transaction.begin();
      Connection connection = (Connection)this.entityManager.unwrap(Connection.class);
      
      Statement statement = connection.createStatement();
      statement.execute(sql);
      ResultSet resultSet = statement.getResultSet();
      
      ByteArrayOutputStream result = new ByteArrayOutputStream();
      
      char separator = ';';
      
      csvWriter = new CSVWriter(new OutputStreamWriter(result, Charset.forName("windows-1251")), separator);
      csvWriter.writeAll(resultSet, true);
      csvWriter.close();
      
      Calendar c = GregorianCalendar.getInstance();
      c.add(11, -1);
      String day = String.format("%02d", new Object[] { Integer.valueOf(c.get(5)) }) + "." + String.format("%02d", new Object[] { Integer.valueOf(c.get(2)) }) + "." + c.get(1);
      
      String time = c.get(11) + "-" + c.get(12);
      String filename = "export_" + code + "_" + day + "_" + time + ".csv";
      
      File file = new File(savePath, filename);
      fos = new FileOutputStream(file);
      fos.write(result.toByteArray());
      service.set("exportfile", result.toByteArray());
      service.set("exportfile_filename", filename);
      service.set("exporttime", c.getTime());
      this.commonDao.mergeTransactional(service);
    }
    catch (SQLException e)
    {
      ByteArrayOutputStream message = new ByteArrayOutputStream();
      PrintWriter writer = new PrintWriter(message);
      e.printStackTrace(writer);
      writer.close();
      
      JsonObject result = new JsonObject();
      result.add("showType", new JsonPrimitive("modal"));
      result.add("idModal", new JsonPrimitive((Long)mlUtil.get("id")));
      result.add("title", new JsonPrimitive("��������� ��������: ������"));
      result.add("content", new JsonPrimitive("�������� ������: " + e.getMessage()));
      resp.setJsonData(result);
    }
    finally
    {
      if (transaction.isActive()) {
        transaction.commit();
      }
      if (fos != null) {
        fos.close();
      }
      if (csvWriter != null) {
        csvWriter.close();
      }
    }
  }
}
