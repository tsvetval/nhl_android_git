package ru.ml.core.metadata.handler.custom;

import com.google.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.datatype.DatatypeConfigurationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.model.MlDynamicEntityImpl;
import ru.ml.core.model.util.MlUtil;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.utils.controller.utils.AbstractUtilController;

@Deprecated
public class PrintDeclarationUtilController
  extends AbstractUtilController
{
  @Inject
  private CommonDao commonDao;
  
  public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp)
    throws IOException, ParseException, DatatypeConfigurationException
  {
    MlDynamicEntityImpl settings = (MlDynamicEntityImpl)this.commonDao.getQuery("SELECT s FROM Settings s").getSingleResult();
    String template = (String)settings.get("decltemp");
    
    Long holderId = Long.valueOf(Long.parseLong(request.getString("objectId")));
    MlDynamicEntityImpl holder = (MlDynamicEntityImpl)this.commonDao.findById(holderId, "Holder");
    
    int firstBrace = template.indexOf("${");
    while (firstBrace >= 0)
    {
      int firstCloseBrace = template.substring(firstBrace).indexOf("}");
      String region = template.substring(firstBrace + 2, firstBrace + firstCloseBrace);
      Object value = "";
      String[] parts = region.split("\\.");
      if (parts.length == 2)
      {
        String clazz = parts[0];
        String prop = parts[1];
        switch (clazz)
        {
        case "Settings": 
          value = settings.get(prop);
          break;
        case "Holder": 
          value = holder.get(prop);
          if (value == null) {
            value = "";
          }
          if ((value instanceof Date))
          {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            value = simpleDateFormat.format(value);
          }
          break;
        }
      }
      template = template.replaceAll("\\$\\{" + region + "\\}", (String)value);
      firstBrace = template.indexOf("${");
    }

    template = "<body onload=window.print()>" + template + "</body>";
    
    String filename = "declaration.html";
    
    FileOutputStream fos = null;
    String filePath = request.getRequest().getServletContext().getRealPath("/WEB-INF/templates/");
    File dir = new File(filePath);
    try
    {
      if (!dir.exists()) {
        dir.mkdirs();
      }
      File f = new File(filePath, filename);
      if (!f.exists()) {
        f.createNewFile();
      }
      fos = new FileOutputStream(f);
      fos.write(template.getBytes());
    }
    catch (IOException ioe)
    {
      throw new MlServerException(ioe.getMessage());
    }
    finally
    {
      if (fos != null) {
        fos.close();
      }
    }
    resp.addDownloadData(template.getBytes(), filename);
    
    holder.set("declprinted", Boolean.valueOf(true));
    this.commonDao.mergeTransactional(holder);
  }
}
