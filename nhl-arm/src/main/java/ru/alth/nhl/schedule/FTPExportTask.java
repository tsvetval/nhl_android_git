package ru.alth.nhl.schedule;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.reports.exportReports.ExportReport;
import ru.alth.nhl.service.reports.ReportStrategy;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.web.schedule.AbstractScheduleJob;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */


public class FTPExportTask extends AbstractScheduleJob {
    private static final Logger log = LoggerFactory.getLogger(FTPExportTask.class);

    @Override
    public void doBefore() {
    }

    @Override
    public void doAfter() {

    }

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) {
        try {
            exportFileToFTP();
        } catch (Exception e) {
            log.error("Error import file to FTP", e);
        }
    }

    private void exportFileToFTP() throws Exception {
        CardDao cardDao = GuiceConfigSingleton.inject(CardDao.class);
        List<Card.CardStatus> statusList = new ArrayList<>();
        statusList.add(Card.CardStatus.READY_TO_PERSONALIZE);
        statusList.add(Card.CardStatus.READY_TO_BANK_PERSONALIZE);

        for(Card.CardStatus status : statusList){
            List<Card> cardList = cardDao.findCardsWithStatus(status);
            if(!cardList.isEmpty()){
                ExportReport report = ReportStrategy.getExportReport(cardList);
                report.createReportAndChangeStatus();
            }
        }
    }
}
