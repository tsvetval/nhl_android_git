package ru.alth.nhl.schedule;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.PersoTypeDao;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.reports.ReportStrategy;
import ru.alth.nhl.service.reports.importReports.ImportReport;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.web.schedule.AbstractScheduleJob;

import java.util.List;

/**
 * Автоматическая загрузка файла с FTP
 */
public class FTPImportTask extends AbstractScheduleJob {
    private static final Logger log = LoggerFactory.getLogger(FTPImportTask.class);

    @Override
    public void doBefore() {
    }

    @Override
    public void doAfter() {

    }

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) {
        PersoTypeDao persoTypeDao = GuiceConfigSingleton.inject(PersoTypeDao.class);

        // Выполняем импорт отчетов в БД для каждого вида карт
        List<PersoType> persoTypeList = persoTypeDao.getAllPersoTypes();
        for (PersoType type : persoTypeList){
            ImportReport report = ReportStrategy.getImportReport(type);
            report.importReport();
        }
    }
}
