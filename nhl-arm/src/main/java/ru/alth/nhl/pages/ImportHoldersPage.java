package ru.alth.nhl.pages;

import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;

/**
 * Created by i_tovstyonok on 06.04.2016.
 */
@Resource(id = "ImportHoldersPage", url = "/import/holders", description = "Страница импорта данных о держателях")
@Page(
        title = "Страница импорта данных о держателях", template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(id = "importHoldersPB", bootJS = "nhl/page_block/import_holders/importHoldersBoot", description = "Блок импорта данных о держателях")
        }
)
public class ImportHoldersPage implements MlPage{
}
