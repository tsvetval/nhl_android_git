package ru.alth.nhl.pages;

import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;

/**
 *
 */
@Resource(
        id = "ImportPersoPage",
        url = "/import/perso",
        description = "Страница импорта объектов персонализации")
@Page(
        title = "Страница импорта объектов инвест программы из XLS",
        template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(
                        bootJS = "nhl/page_block/import_perso/importPersoBoot",
                        description = "Блок импорта объектов персонализации",
                        id = "importPersoPB",
                        zone = "CENTER"
                )
        }
)
public class ImportPersoPage implements MlPage {
}
