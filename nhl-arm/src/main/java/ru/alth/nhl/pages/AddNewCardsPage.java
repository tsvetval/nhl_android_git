package ru.alth.nhl.pages;

import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;



/**
 * Страница утилиты массового добавления карт
 */

@Resource(
        id = "AddNewCards",
        url = "/pages/addCards",
        description = "Страница утилиты массового добавления карт")
@Page(
        title = "Создать карты",
        template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(
                        bootJS = "nhl/page_block/add_new_cards/AddNewCardsBoot",
                        description = "Блок утилиты массового добавления карт",
                        id = "addNewCardsPB",
                        zone = "CENTER"
                ),
                @PageBlock(
                        bootJS = "cms/page_blocks/user_block/UserBlockBoot",
                        description = "Блок пользователя",
                        id = "40",
                        zone = "TOP"
                )
        }
)

public class AddNewCardsPage implements MlPage {

}
