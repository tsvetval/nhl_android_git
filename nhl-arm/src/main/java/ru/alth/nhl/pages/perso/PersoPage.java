package ru.alth.nhl.pages.perso;

import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;

/**
 * Основная страница арма персонализации
 */
@Resource(
        id = "PersoPage",
        url = "/perso/update",
        description = "Страница обновления карт")
@Page(
        title = "Страница обновления карт",
        template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(
                        bootJS = "nhl/page_block/perso/persoBoot",
                        description = "Блок обновления карт",
                        id = "mainPersoPB",
                        zone = "CENTER"
                ),
                @PageBlock(
                        bootJS = "cms/page_blocks/user_block/UserBlockBoot",
                        description = "Блок пользователя",
                        id = "40",
                        zone = "TOP"
                )
        }
)
public class PersoPage implements MlPage {
}
