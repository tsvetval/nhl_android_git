package ru.alth.nhl.pages;

import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;


/**
 * Страница переключения статуса карты
 */

@Resource(
        id = "ChangeCardStatus",
        url = "/card/changeCardStatus",
        description = "Страница переключения статуса карты")
@Page(
        title = "Страница переключения статуса карты",
        template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(
                        bootJS = "nhl/page_block/card_status/changeCardStatusBoot",
                        description = "Блок переключения статусов",
                        id = "changeCardStatusPB",
                        zone = "CENTER"
                )
        }
)

public class ChangeCardStatusPage implements MlPage {

}
