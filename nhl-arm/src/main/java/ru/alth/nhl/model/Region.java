package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.model.MlDynamicEntityImpl;

/**
 * Created by Sergei on 21.04.2016.
 */
public class Region extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public enum Fields {
        id,
        name,
        bank_branch
    }

    public Long getId() {
        return this.get(Card.Fields.id.name());
    }
    public String getName() {
        return get(Fields.name.name());
    }

    public BankBranch getBankBranch() {
        return this.get(Fields.bank_branch.name());
    }

    public void setBankBranch(BankBranch bankBranch) {
        this.set(Fields.bank_branch.name(), bankBranch);
    }
    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
