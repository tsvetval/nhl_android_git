package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.model.MlDynamicEntityImpl;

/**
 * Created by i_tovstyonok on 28.03.2016.
 */
public class BankBranch extends MlDynamicEntityImpl{

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public static class Properties {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String FILIAL = "filial";
        public static final String ADDRESS = "address";
        public static final String ABS_NUMBER = "ABS_number";
    }


    public Long getId() {
        return get(Properties.ID);
    }

    public String getName() {
        return get(Properties.NAME);
    }

    public String getFilial(){
        return this.get(Properties.FILIAL);
    }

    public String getABSNumber(){
        return get(Properties.ABS_NUMBER);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
