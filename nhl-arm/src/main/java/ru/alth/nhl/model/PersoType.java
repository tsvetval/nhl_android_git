package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.model.MlDynamicEntityImpl;

/**
 * Типы персонализации карт
 */
public class PersoType extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    protected static class Properties{
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CARD_TYPE = "card_type";

        public static final String IMPORT_FTP_URL = "import_ftp_url";
        public static final String IMPORT_FTP_FOLDER = "import_ftp_folder";
        public static final String IMPORT_FTP_USER = "import_ftp_user";
        public static final String IMPORT_FTP_PASSWORD = "import_ftp_password";

        public static final String EXPORT_FTP_URL = "export_ftp_url";
        public static final String EXPORT_FTP_FOLDER = "export_ftp_folder";
        public static final String EXPORT_FTP_USER = "export_ftp_user";
        public static final String EXPORT_FTP_PASSWORD = "export_ftp_password";

        public static final String ARCHIVE = "archive";

        public static final String IMPORT_NHL_FTP_FOLDER = "import_nhl_ftp_folder";
        public static final String EXPORT_NHL_FTP_FOLDER = "export_nhl_ftp_folder";
        public static final String ARCHIVE_NHL = "archive_nhl";

        public static final String STORAGE_TYPE = "storageType";
        public static final String OS_TYPE = "os_type";

    }

    public enum OSType {
        SCONE, JAVA
    }

    public OSType getOSType(){
        return OSType.valueOf(get(Properties.OS_TYPE));
    }

    public enum CardType {
        BANK ("Банковская"),
        NOT_BANK ("Не банковская");

        private String description;
        CardType(String description) {
            this.description = description;
        }
    }

    public CardType getCardType() {
        if (this.get(Properties.CARD_TYPE) != null) {
            return CardType.valueOf(this.get(Properties.CARD_TYPE));
        }
        return null;
    }

    public enum StorageType{
        FTP, SFTP
    }

    public Long getId(){
        return get(Properties.ID);
    }

    public void setImportFTPURL(String value){
        set(Properties.IMPORT_FTP_URL, value);
    }

    public String getImportFTPURL(){
        return get(Properties.IMPORT_FTP_URL);
    }

    public void setExportFTPURL(String value){
        set(Properties.EXPORT_FTP_URL, value);
    }

    public String getExportFTPURL(){
        return get(Properties.EXPORT_FTP_URL);
    }

    public void setImportFTPFolder(String value){
        set(Properties.IMPORT_FTP_FOLDER, value);
    }

    public String getImportFTPFolder(){
        return get(Properties.IMPORT_FTP_FOLDER) + "/";
    }

    public void setExportFTPFolder(String value){
        set(Properties.EXPORT_FTP_FOLDER, value);
    }

    public String getExportFTPFolder(){
        return get(Properties.EXPORT_FTP_FOLDER) + "/";
    }

    public void setImportFTPUser(String value){
        set(Properties.IMPORT_FTP_USER, value);
    }

    public String getImportFTPUser(){
        return get(Properties.IMPORT_FTP_USER);
    }

    public void setImportFTPPassword(String value){
        set(Properties.IMPORT_FTP_PASSWORD, value);
    }

    public String getImportFTPPassword(){
        return get(Properties.IMPORT_FTP_PASSWORD);
    }

    public void setExportFTPUser(String value){
        set(Properties.EXPORT_FTP_USER, value);
    }

    public String getExportFTPUser(){
        return get(Properties.EXPORT_FTP_USER);
    }

    public void setExportFTPPassword(String value){
        set(Properties.EXPORT_FTP_PASSWORD, value);
    }

    public String getExportFTPPassword(){
        return get(Properties.EXPORT_FTP_PASSWORD);
    }

    public String getName(){
        return  get(Properties.NAME);
    }

    public  void setName(String value){
        this.set(Properties.NAME, value);
    }

    public void setArchive(String value){
        set(Properties.ARCHIVE, value);
    }

    public StorageType getStorageType(){
        return StorageType.valueOf(get(Properties.STORAGE_TYPE));
    }
    public String getArchive(){
        return get(Properties.ARCHIVE) + "/";
    }

    public void setImportNHLFTPFolder(String value){
        set(Properties.IMPORT_NHL_FTP_FOLDER, value);
    }

    public String getImportNHLFTPFolder(){
        return get(Properties.IMPORT_NHL_FTP_FOLDER) + "/";
    }

    public void setExportNHLFTPFolder(String value){
        set(Properties.EXPORT_NHL_FTP_FOLDER, value);
    }

    public String getExportNHLFTPFolder(){
        return get(Properties.EXPORT_NHL_FTP_FOLDER) + "/";
    }

    public void setArchiveNHL(String value){
        set(Properties.ARCHIVE_NHL, value);
    }

    public String getArchiveNHL(){
        return get(Properties.ARCHIVE_NHL) + "/";
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
