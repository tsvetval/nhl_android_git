package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.model.MlDynamicEntityImpl;

import java.util.Date;

/**
 * Ответ персонализации
 */
public class PersoResponse extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public enum Fields{
        id("Идентификатор"),
        date("Дата получения"),
        file("Файл отчета"),
        file_filename("Имя файла"),
        description("Комментарий"),
        status("Статус");

        private String desc;
        Fields(String desc) {
            this.desc = desc;
        }
    }

    public enum Status {
        SUCCESS,
        ERROR
    }

    public void setId(Long id) {
        this.set(Fields.id.name(), id);
    }

    public Long getId() {
        return this.get(Fields.id.name());
    }

    public void setDate(Date date) {
        this.set(Fields.date.name(), date);
    }

    public Date getDate() {
        return this.get(Fields.date.name());
    }

    public void setFile(byte[] file) {
        this.set(Fields.file.name(), file);
    }

    public byte[] getFile() {
        return this.get(Fields.file.name());
    }

    public void setFilename(String filename) {
        this.set(Fields.file_filename.name(), filename);
    }

    public String getFilename() {
        return this.get(Fields.file_filename.name());
    }

    public void setDescription(String description) {
        this.set(Fields.description.name(), description);
    }

    public String getDescription() {
        return this.get(Fields.description.name());
    }

    public void setStatus(Status status) {
        this.set(Fields.status.name(), status.name());
    }

    public String getStatus() {
        return this.get(Fields.status.name());
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
