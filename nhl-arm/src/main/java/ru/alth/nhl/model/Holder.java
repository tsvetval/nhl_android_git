package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.model.MlDynamicEntityImpl;

import java.util.Date;

/**
 *
 */
public class Holder extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }


    public enum Fields {
        id("Идентификатор"),
        firstname("Имя"),
        secondname("Отчество"),
        lastname("Фамилия"),
        birthdate("Дата рождения"),
        birthplace("Место рождения"),
        rezus("Резус фактор"),
        gruppa("Группа крови"),
        sex("Пол"),
        documPlace("Место выдачи паспорта"),
        subdivisionCode("Код подразделения"),

        idnhlnumber("Идентификационный номер члена НХЛ"),
        teamname("Название команды"),
        vidchlenstva("Вид членства (игрок, болельщик, судья, сотрудник НХЛ)"),
        statusnhl("Статус члена НХЛ"),
        phone("телефон"),
        email("Адрес электронной почты"),
        mailingAddress("Адрес фактического проживания"),
        residenceAddress("Адрес прописки"),

        dopusk("Допуск на матч"),
        documnum("Серия, номер паспорта"),
        documdate("дата выдачи паспорта"),
        drivernum("Серия, номер водительского удостоверения"),
        driverdate("дата выдачи водительского удостоверения"),
        matchDate("Дата матча"),
        matchPlace("Место проведения матча"),
        team("Команда"),
        playerNumber("Номер игрока"),

        insurcompname("Название страховой компании"),
        insurnum("Номер страхового полиса"),
        medocmotrdate("Дата прохождения мед. осмотра"),
        rezultmedocmotr("Результат мед. Осмотра"),
        height("Рост"),
        weight("Вес"),
        protivopakaz("Противопоказания обезболивающих препаратов (ДА/НЕТ)"),
        allegry("Аллергия на препараты "),
        chronical("Хронические заболевания (ДА/НЕТ)"),

        region("Регион"),
        embossed_name("Имя на карте"),
        embossed_lastname("Фамилия на карте"),
        residency("Резидент"),
        external_id("Идентификатор из внешней системы"),
        data_source("Название фотографии"),
        photo_filename("Название фотографии"),
        photo("Фото"),;
        private String description;

        Fields(String description) {
            this.description = description;
        }
    }

    public Long getId() {
        return this.get(Fields.id.name());
    }

    public byte[] getPhoto() {
        return this.get(Fields.photo.name());
    }

    public void setPhoto(byte[] photo) {
        this.set(Fields.photo.name(), photo);
    }

    public String getPhotoFilename() {
        return this.get(Fields.photo_filename.name());
    }

    public void setPhotoFilename(String photoFilename) {
        this.set(Fields.photo_filename.name(), photoFilename);
    }

    public String getFirstName() {
        return this.get(Fields.firstname.name());
    }

    public void setFirstName(String firstname) {
        this.set(Fields.firstname.name(), firstname);
    }

    public String getSecondName() {
        return this.get(Fields.secondname.name());
    }

    public void setSecondName(String secondName) {
        this.set(Fields.secondname.name(), secondName);
    }

    public String getLastName() {
        return this.get(Fields.lastname.name());
    }

    public void setLastName(String lastName) {
        this.set(Fields.lastname.name(), lastName);
    }

    public Date getBirthdate() {
        return this.get(Fields.birthdate.name());
    }

    public void setBirthdate(Date birthdate) {
        this.set(Fields.birthdate.name(), birthdate);
    }

    public String getIdNhlNumber() {
        return this.get(Fields.idnhlnumber.name());
    }

    public void setIdNhlNumber(String idNhlNumber) {
        this.set(Fields.idnhlnumber.name(), idNhlNumber);
    }

    public String getTeamName() {
        return this.get(Fields.teamname.name());
    }

    public String getTeam() {
        return this.get(Fields.team.name());
    }

    public void setTeamName(String teamName) {
        this.set(Fields.teamname.name(), teamName);
    }

    public String getPhone() {
        return this.get(Fields.phone.name());
    }

    public void setPhone(String phone) {
        this.set(Fields.phone.name(), phone);
    }

    public String getEmail() {
        return this.get(Fields.email.name());
    }

    public void setEmail(String email) {
        this.set(Fields.email.name(), email);
    }

    public String getVidChlenstva() {
        return this.get(Fields.vidchlenstva.name());
    }

    public void setVidChlenstva(String vidChlenstva) {
        this.set(Fields.vidchlenstva.name(), vidChlenstva);
    }

    public String getStatusNhl() {
        return this.get(Fields.statusnhl.name());
    }

    public void setStatusNhl(String statusNhl) {
        this.set(Fields.statusnhl.name(), statusNhl);
    }

    public String getMailingAddress() {
        return this.get(Fields.mailingAddress.name());
    }

    public void setMailingAddress(String mailingAddress) {
        this.set(Fields.mailingAddress.name(), mailingAddress);
    }

    public String getGroup() {
        return this.get(Fields.gruppa.name());
    }

    public void setGroup(String group) {
        this.set(Fields.gruppa.name(), group);
    }

    public String getRezus() {
        return this.get(Fields.rezus.name());
    }

    public void setRezus(String rezus) {
        this.set(Fields.rezus.name(), rezus);
    }

    public String getInsurCompname() {
        return this.get(Fields.insurcompname.name());
    }

    public void setInsurCompname(String insurCompname) {
        this.set(Fields.insurcompname.name(), insurCompname);
    }

    public String getInsurNum() {
        return this.get(Fields.insurnum.name());
    }

    public void setInsurNum(String insurNum) {
        this.set(Fields.insurnum.name(), insurNum);
    }

    public String getDocNum() {
        return this.get(Fields.documnum.name());
    }

    public void setDocNum(String docNum) {
        this.set(Fields.documnum.name(), docNum);
    }

    public Date getDocumDate() {
        return this.get(Fields.documdate.name());
    }

    public void setDocumDate(Date documDate) {
        this.set(Fields.documdate.name(), documDate);
    }

    public String getEmbossedName() {
        return this.get(Fields.embossed_name.name());
    }

    public void setEmbossedName(String embossedName) {
        this.set(Fields.embossed_name.name(), embossedName);
    }

    public String getEmbossedLastName() {
        return this.get(Fields.embossed_lastname.name());
    }

    public void setEmbossedLastName(String embossedLastName) {
        this.set(Fields.embossed_lastname.name(), embossedLastName);
    }

    public String getResidency() {
        return this.get(Fields.residency.name());
    }

    public void setResidency(String residency) {
        this.set(Fields.residency.name(), residency);
    }

    public Long getExternalId() {
        return this.get(Fields.external_id.name());
    }

    public void setExternalId(Long externalId) {
        this.set(Fields.external_id.name(), externalId);
    }

    public Long getDataSource() {
        return this.get(Fields.data_source.name());
    }

    public void setDataSource(String dataSource) {
        this.set(Fields.data_source.name(), dataSource);
    }

    public String getResidenceAddress() {
        return get(Fields.residenceAddress.name());
    }

    public String getSex() {
        return get(Fields.sex.name());
    }

    public String getBirthPlace() {
        return get(Fields.birthplace.name());
    }

    public String getDocumPlace() {
        return get(Fields.documPlace.name());
    }

    public String getSubdivisionCode() {
        return get(Fields.subdivisionCode.name());
    }

    public String getResultMedOsmotr() {
        return get(Fields.rezultmedocmotr.name());
    }

    public String getProtivopakaz() {
        return get(Fields.protivopakaz.name());
    }

    public String getAllegry() {
        return get(Fields.allegry.name());
    }

    public String getChronical() {
        return get(Fields.chronical.name());
    }

    public String getDrivernum() {
        return get(Fields.drivernum.name());
    }

    public String getMatchPlace() {
        return get(Fields.matchPlace.name());
    }

    public Long getHeight() {
        return get(Fields.height.name());
    }

    public Long getWeight() {
        return get(Fields.weight.name());
    }

    public Long getPlayerNumber() {
        return get(Fields.playerNumber.name());
    }

    public Date getMedocmotrDate() {
        return get(Fields.medocmotrdate.name());
    }

    public Date getDriverDate() {
        return get(Fields.driverdate.name());
    }

    public Date getMatchDate() {
        return get(Fields.matchDate.name());
    }

    public void setRegion(Region region) {
        this.set(Fields.region.name(), region);
    }

    public Region getRegion() {
        return this.get(Fields.region.name());
    }
}
