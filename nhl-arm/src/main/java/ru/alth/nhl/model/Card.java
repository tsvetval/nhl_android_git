package ru.alth.nhl.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.alth.nhl.util.TLV;
import ru.ml.core.model.MlDynamicEntityImpl;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 */
public class Card extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    /**
     * Статусы карт
     */
    public enum CardStatus {
        SEND_TO_BANK_PERSONALIZATION("Отправлена на персонализацию в банк"),
        BANK_PERSONALIZED("Персонализирована банком"),
        NEW("Новая заявка на выпуск карты"),
        SEND_TO_PERSONALIZATION("Отправлена на персонализацию"),
        BLOCKED("Блокирована"),
        NOT_USED("Уничтожена/утеряна"),
        PERSONALIZED("Персонализирована"),
        PERSONALIZATION_ERROR("Ошибка при персонализации"),
        READY_TO_PERSONALIZE("Готова к персонализации"),
        READY_TO_BANK_PERSONALIZE("Готова к персонализации в банке"),
        ACTIVE("Активна");
        public String description;

        CardStatus(String description) {
            this.description = description;
        }
    }


    public enum Fields {
        id(""),
        status(""),
        holder(""),
        expiredate(""),
        persotype(""),
        bank_department(""),
        pan_hash_number(""),
        cardstatus(""),
        num(""),
        info(""),
        persoerror("Сообщение об ошибке"),
        flagASCI("Признак ASCI"),
        hashf1("Хэш файл 1"),
        hashf2("Хэш файл 2"),
        hashf3("Хэш файл 3"),
        hashf4("Хэш файл 4");
        private String description;

        Fields(String description) {
            this.description = description;
        }

    }

    public enum CardStatusNhl {
        ACTIVE("00"),
        BLOCKED("01");
        private String code;
        CardStatusNhl(String code){
            this.code = code;
        }
    }

    public Long getId() {
        return this.get(Fields.id.name());
    }

    public Holder getHolder() {
        return this.get(Fields.holder.name());
    }

    public BankBranch getBankBranch(){
        return this.get(Fields.bank_department.name());
    }

    public void setBankBranch(BankBranch bankBranch) {
        this.set(Fields.bank_department.name(), bankBranch);
    }

    public void setHolder(Holder holder) {
        this.set(Fields.holder.name(), holder);
    }

    public CardStatus getStatus() {
        if (this.get(Fields.status.name()) != null) {
            return CardStatus.valueOf(this.get(Fields.status.name()));
        }
        return null;
    }

    public void setStatus(CardStatus status) {
        this.set(Fields.status.name(), status.name());
    }

    public String getPANHashNumber() {
        return this.get(Fields.pan_hash_number.name());
    }

    public void setPANHashNumber(String panHashNumber) {
        this.set(Fields.pan_hash_number.name(), panHashNumber);
    }

    public String getPersoError() {
        return this.get(Fields.persoerror.name());
    }

    public void setPersoError(String persoError) {
        this.set(Fields.persoerror.name(), persoError);
    }

    public Date getExpireDate() {
        return this.get(Fields.expiredate.name());
    }

    public void setExpireDate(Date expiredate) {
        this.set(Fields.expiredate.name(), expiredate);
    }

    public Boolean getFlagASCI() {
        return this.get(Fields.flagASCI.name());
    }

    public void setFlagASCI(Boolean flagASCI) {
        this.set(Fields.flagASCI.name(), flagASCI);
    }

    public void setInfo(String info) {
        this.set(Fields.info.name(), info);
    }

    public String getInfo() {
        return this.get(Fields.info.name());
    }

    public void setNum(String num) {
        this.set(Fields.num.name(), num);
    }

    public String getNum() {
        return this.get(Fields.num.name());
    }

    public void setPersoType(PersoType type) {
        this.set(Fields.persotype.name(), type);
    }

    public PersoType getPersoType() {
        return this.get(Fields.persotype.name());
    }

    public String getCardStatusNhl() {
        return this.get(Fields.cardstatus.name());
    }

    public void setCardStatusNhl(CardStatusNhl cardStatusNhl) {
        this.set(Fields.cardstatus.name(), cardStatusNhl.code);
    }

    private static final DateTimeFormatter shortDateFormat = DateTimeFormatter.ofPattern("MMyy").withZone(ZoneId.systemDefault());


    public TLV serializeTLVFile1WithOutImage() throws UnsupportedEncodingException {
        Holder holder = this.getHolder();
        Map<String, String> fileTags = new TreeMap<>();
        fileTags.put("DF12", printCP1251HexBinary(holder.get(Holder.Fields.lastname.name())));
        fileTags.put("DF13", printCP1251HexBinary(holder.get(Holder.Fields.firstname.name())));
        fileTags.put("DF14", printCP1251HexBinary(holder.get(Holder.Fields.secondname.name())));

        fileTags.put("DF15", printDateOrEmpty(holder.get(Holder.Fields.birthdate.name())));

        if (this.getExpireDate() != null) {
            fileTags.put("DF16", shortDateFormat.format(
                            (this.getExpireDate()).toInstant())
            );
        } else {
            fileTags.put("DF16", "");
        }
        fileTags.put("DF17", printPaddingStringOrEmpty(getNum()));

        fileTags.put("DF51", printStringOrEmpty(this.get(Card.Fields.hashf1.name())));
        return new TLV(fileTags);
    }


    public TLV serializeTLVFile2() throws UnsupportedEncodingException {
        Holder holder = this.getHolder();
        Map<String, String> fileTags = new TreeMap<>();
        fileTags.put("DF21", printPaddingStringOrEmpty(holder.get(Holder.Fields.idnhlnumber.name())));
        fileTags.put("DF22", printCP1251HexBinary(holder.get(Holder.Fields.teamname.name())));
        fileTags.put("DF23", printStringOrEmpty(holder.get(Holder.Fields.vidchlenstva.name())));
        fileTags.put("DF24", printStringOrEmpty(holder.get(Holder.Fields.statusnhl.name())));
        fileTags.put("DF25", printCP1251HexBinary(holder.get(Holder.Fields.phone.name())));
        fileTags.put("DF26", printCP1251HexBinary(holder.get(Holder.Fields.email.name())));
        fileTags.put("DF27", printCP1251HexBinary(holder.get(Holder.Fields.mailingAddress.name())));
        fileTags.put("DF52", printStringOrEmpty(this.get(Card.Fields.hashf2.name())));
        return new TLV(fileTags);
    }


    public TLV serializeTLVFile3() throws UnsupportedEncodingException {
        Holder holder = this.getHolder();
        Map<String, String> fileTags = new TreeMap<>();
        fileTags.put("DF31", printStringOrEmpty(this.get(Fields.cardstatus.name())));
        fileTags.put("DF32", printStringOrEmpty(holder.get(Holder.Fields.dopusk.name())));

        String docnum = holder.get(Holder.Fields.documnum.name());
        /*if (docnum != null) {
            docnum = docnum.replaceAll(" ", "");
        }*/
        fileTags.put("DF33", (printCP1251HexBinary(docnum)
                + printCP1251HexBinary(printDocumDate(holder.get(Holder.Fields.documdate.name())))));
        fileTags.put("DF34", printCP1251HexBinary(holder.get(Holder.Fields.drivernum.name()))
                + printCP1251HexBinary(printDateOrEmpty(holder.get(Holder.Fields.driverdate.name()))));
        fileTags.put("DF35", printDateOrEmpty(holder.get(Holder.Fields.matchDate.name())));
        fileTags.put("DF36", printCP1251HexBinary(holder.get(Holder.Fields.matchPlace.name())));
        fileTags.put("DF37", printCP1251HexBinary(holder.get(Holder.Fields.team.name())));
        fileTags.put("DF38", printLongCP1251HexBinary(holder.get(Holder.Fields.playerNumber.name())));
        fileTags.put("DF39", printStringOrEmpty(convertFlagASCIForTLV(this.get(Card.Fields.flagASCI.name()))));
        fileTags.put("DF53", printStringOrEmpty(this.get(Card.Fields.hashf3.name())));

        return new TLV(fileTags);
    }


    public TLV serializeTLVFile4() throws UnsupportedEncodingException {
        Holder holder = this.getHolder();
        Map<String, String> fileTags = new TreeMap<>();
        fileTags.put("DF18", printStringOrEmpty(holder.get(Holder.Fields.rezus.name())));
        fileTags.put("DF19", printStringOrEmpty(holder.get(Holder.Fields.gruppa.name())));
        fileTags.put("DF41", printCP1251HexBinary(holder.get(Holder.Fields.insurcompname.name())));
        fileTags.put("DF42", printCP1251HexBinary(holder.get(Holder.Fields.insurnum.name())));
        fileTags.put("DF43", printDateOrEmpty(holder.get(Holder.Fields.medocmotrdate.name())));
        fileTags.put("DF44", printStringOrEmpty(holder.get(Holder.Fields.rezultmedocmotr.name())));
        fileTags.put("DF45", printLongCP1251HexBinary(holder.get(Holder.Fields.height.name())));
        fileTags.put("DF46", printLongCP1251HexBinary(holder.get(Holder.Fields.weight.name())));
        fileTags.put("DF47", printStringOrEmpty(holder.get(Holder.Fields.protivopakaz.name())));
        fileTags.put("DF48", printStringOrEmpty(holder.get(Holder.Fields.allegry.name())));
        fileTags.put("DF49", printStringOrEmpty(holder.get(Holder.Fields.chronical.name())));
        fileTags.put("DF54", printStringOrEmpty(this.get(Card.Fields.hashf4.name())));

        return new TLV(fileTags);
    }

    public TLV serializeTLVFile5() {
        Holder holder = this.getHolder();
        Map<String, String> fileTags = new TreeMap<>();
        fileTags.put("DF17", printPaddingStringOrEmpty(getNum()));
        fileTags.put("DF51", printStringOrEmpty(this.get(Card.Fields.hashf1.name())));
        fileTags.put("DF52", printStringOrEmpty(this.get(Card.Fields.hashf2.name())));
        fileTags.put("DF53", printStringOrEmpty(this.get(Card.Fields.hashf3.name())));
        fileTags.put("DF54", printStringOrEmpty(this.get(Card.Fields.hashf4.name())));

        return new TLV(fileTags);
    }

    public String getHashFile1() {
        return this.get(Fields.hashf1.name());
    }

    public void setHashFile1(String value) {
        this.set(Fields.hashf1.name(), value);
    }

    public String getHashFile2() {
        return this.get(Fields.hashf2.name());
    }

    public void setHashFile2(String value) {
        this.set(Fields.hashf2.name(), value);
    }

    public String getHashFile3() {
        return this.get(Fields.hashf3.name());
    }

    public void setHashFile3(String value) {
        this.set(Fields.hashf3.name(), value);
    }

    public String getHashFile4() {
        return this.get(Fields.hashf4.name());
    }

    public void setHashFile4(String value) {
        this.set(Fields.hashf4.name(), value);
    }

    private String printStringOrEmpty(Object value) {
        return value == null ? "" : value.toString();
    }

    private String printPaddingStringOrEmpty(Object value) {
        if (value != null) {
            String valueString = value.toString();
            if (valueString == null) {
                return "";
            } else if (valueString.length() % 2 == 0) {
                return valueString;
            } else {
                return "0" + valueString;
            }
        }
        return "";
    }

    private String printHexStringOrEmpty(Long value) {
        return value == null ? "" : Long.toHexString(value);
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    private String printDateOrEmpty(Date value) {
        return value == null ? "" : dateFormat.format(value);
    }

    private String printCP1251HexBinary(String value) throws UnsupportedEncodingException {
        return value == null ? "" : DatatypeConverter.printHexBinary(value.getBytes("cp1251"));
    }

    private String printLongCP1251HexBinary(Long value) throws UnsupportedEncodingException {
        return value == null ? "" : DatatypeConverter.printHexBinary(value.toString().getBytes("cp1251"));
    }

    private String convertFlagASCIForTLV(boolean flag){
        if(flag){
            return "01";
        } else {
            return "00";
        }
    }

    private String printDocumDate(Date value){
        return value == null ? "00000000" : dateFormat.format(value);
    }
}
