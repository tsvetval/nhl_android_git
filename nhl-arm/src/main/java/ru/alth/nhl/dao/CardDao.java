package ru.alth.nhl.dao;


import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.ml.core.dao.CommonDao;
import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class CardDao extends CommonDao<Card> {

    public Card findCardByNum(String num){
        Query query =  entityManager.createQuery("select o from Card o where o.num = :num");
        query.setParameter("num", num);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Card)resultList.get(0);
        } else {
            return null;
        }
    }

    public List<Card> findCardsByHolder(Holder holder){
        Query query =  entityManager.createQuery("select o from Card o where o.holder = :holder");
        query.setParameter("holder", holder);
        return (List<Card>)query.getResultList();

    }

    public List<Card> findCardsWithStatus(Card.CardStatus cardStatus){
        Query query =  entityManager.createQuery("select c from Card c where c.status =:status");
        query.setParameter("status", cardStatus.name());
        return (List<Card>)query.getResultList();

    }

    public List<Card> findCardsByBankBranchAndStatus(List<BankBranch> bankBranch, Card.CardStatus cardStatus){
        Query query =  entityManager.createQuery("select c from Card c where c.bank_department in :bankBranch and c.status =:status");
        query.setParameter("bankBranch", bankBranch);
        query.setParameter("status", cardStatus.name());
        return (List<Card>)query.getResultList();

    }

    public Card findCardsByPANHashNumber(String panHashNumber){
        Query query =  entityManager.createQuery("select c from Card c where c.pan_hash_number =:panHashNumber");
        query.setParameter("panHashNumber", panHashNumber);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Card)resultList.get(0);
        } else {
            return null;
        }

    }

    public Card findCardByHolderAndStatus(Holder holder, Card.CardStatus cardStatus){
        Query query =  entityManager.createQuery("select c from Card c where c.status =:status and c.holder = :holder");
        query.setParameter("holder", holder);
        query.setParameter("status", cardStatus.name());
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Card)resultList.get(0);
        } else {
            return null;
        }
    }

    public Card findCardByHolderIdNHLNumberAndStatus(String idNHLNumber, Card.CardStatus cardStatus){
        Query query =  entityManager.createQuery("select c from Card c join c.holder h where c.status =:status and h.idnhlnumber = :idNHLNumber");
        query.setParameter("idNHLNumber", idNHLNumber);
        query.setParameter("status", cardStatus.name());
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Card)resultList.get(0);
        } else {
            return null;
        }
    }

    public List<Card> getCardsByHolderWithoutTheseStatuses(Holder holder, List<Card.CardStatus> cardStatusList){
        Query query =  entityManager.createQuery("select c from Card c where c.holder =:holder and c.status not in :statuses order by c.id");
                query.setParameter("holder", holder).setParameter("statuses", cardStatusList);
        return (List<Card>) query.getResultList();
    }

}
