package ru.alth.nhl.dao;

import ru.alth.nhl.model.Region;
import ru.ml.core.dao.CommonDao;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Sergei on 21.04.2016.
 */
public class RegionDao extends CommonDao<Region> {

    public Region findRegionByName(String name){
        Query query =  entityManager.createQuery("select r from Region r where r.name =:name");
        query.setParameter("name", name);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Region)resultList.get(0);
        } else {
            return null;
        }
    }
}
