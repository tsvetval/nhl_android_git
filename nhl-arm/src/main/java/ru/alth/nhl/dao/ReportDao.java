package ru.alth.nhl.dao;

import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.PersoResponse;
import ru.ml.core.dao.CommonDao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by Sergei on 25.03.2016.
 */
public class ReportDao extends CommonDao {

    private static final Logger log = LoggerFactory.getLogger(ReportDao.class);

    public Long getCurrentValueFromSequence(){
        Query query =  entityManager.createNativeQuery("select last_value from \"Report_name_SEQ\"");
        return (Long)query.getSingleResult();
    }

    public Long getNextValueFromSequence(){
        Query query =  entityManager.createNativeQuery("select nextval('\"Report_name_SEQ\"')");
        return (Long)query.getSingleResult();
    }

    public void refreshSequence(){
        entityManager.createNativeQuery("alter sequence \"Report_name_SEQ\" restart").executeUpdate();
        log.debug("Sequence \"Report_name_SEQ\" was restarted.");
    }

    public Date getLastBankPersReportDate(){
        Query query = entityManager
                    .createNativeQuery("select \"createDate\" from \"NHL_PersoTask\" where \"file_filename\" like ?1 order by \"createDate\" desc limit 1");
        query.setParameter(1, "A%");
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Date)resultList.get(0);
        } else {
            return null;
        }
    }

    public PersoResponse findPersoResponseByFilename(String filename){
        Query query = entityManager.createQuery("select p from PersoResponse p where p.file_filename = :filename");
        query.setParameter("filename", filename);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (PersoResponse)resultList.get(0);
        } else {
            return null;
        }
    }

    public byte[] getPersoTaskByName(String fileName){
        try{
            Query query = entityManager
                    .createNativeQuery("select \"file\" from \"NHL_PersoTask\" where \"file_filename\" = ?1");
            query.setParameter(1, fileName);
            return (byte[])query.getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }
}
