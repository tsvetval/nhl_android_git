package ru.alth.nhl.dao;

import ru.alth.nhl.model.Holder;
import ru.ml.core.dao.CommonDao;
import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class HolderDao extends CommonDao<Holder> {

    public List<Holder> getAllHolders(){
        Query query = entityManager.createQuery("select h from Holder h");
        return (List<Holder>)query.getResultList();
    }

    public List<Holder> findHoldersByIds(List<Long> ids){
        Query query = entityManager.createQuery("select h from Holder h where h.id in :ids");
        query.setParameter("ids", ids);
        return (List<Holder>) query.getResultList();
    }

    public Holder findHolderByIdNHLNumber(String idNHLNumber){
        Query query = entityManager.createQuery("select h from Holder h where h.idnhlnumber = :idnhlnumber");
        query.setParameter("idnhlnumber", idNHLNumber);
        return (Holder) query.getSingleResult();
    }

    public Holder checkHolderByDocumNumber(Holder holder){
        Query query;
        if(holder.getIdNhlNumber() == null){
            query = entityManager.createQuery("select h from Holder h where h.documnum = :documNumber");
        } else {
            query = entityManager.createQuery("select h from Holder h where h.documnum = :documNumber and h.idnhlnumber <> :idnhlnumber");
            query.setParameter("idnhlnumber", holder.getIdNhlNumber());
        }
        query.setParameter("documNumber", holder.getDocNum());
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Holder)resultList.get(0);
        } else {
            return null;
        }
    }

    public Holder findHolderByIdExternalId(Long externalId){
        Query query = entityManager.createQuery("select h from Holder h where h.external_id = :externalId");
        query.setParameter("externalId", externalId);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Holder)resultList.get(0);
        } else {
            return null;
        }
    }

    public Holder findHolderByDocumNumber(String documNumber){
        Query query = entityManager.createQuery("select h from Holder h where h.documnum = :documNumber");
        query.setParameter("documNumber", documNumber);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (Holder)resultList.get(0);
        } else {
            return null;
        }
    }
    public boolean checkUniqueExternalId(Holder holder) {
        Query query;
        if(holder.getId()!=null) {
            query = entityManager.createQuery("select h from Holder h where h.external_id = :external and h.id <> :id").setParameter("external", holder.getExternalId()).setParameter("id", holder.getId());
        }else {
            query = entityManager.createQuery("select h from Holder h where h.external_id = :external").setParameter("external", holder.getExternalId());
        }
        return query.getResultList().isEmpty();
    }
}
