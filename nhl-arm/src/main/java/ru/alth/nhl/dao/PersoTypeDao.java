package ru.alth.nhl.dao;

import ru.alth.nhl.model.Holder;
import ru.alth.nhl.model.PersoType;
import ru.ml.core.dao.CommonDao;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Sergei on 18.03.2016.
 */
public class PersoTypeDao extends CommonDao<PersoType> {

    public List<PersoType> getAllPersoTypes(){
        Query query =  entityManager.createQuery("select p from PersoType p");
        return (List<PersoType>)query.getResultList();
    }

}
