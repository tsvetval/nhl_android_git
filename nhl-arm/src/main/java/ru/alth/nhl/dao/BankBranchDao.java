package ru.alth.nhl.dao;

import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Region;
import ru.ml.core.dao.CommonDao;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by i_tovstyonok on 28.03.2016.
 */
public class BankBranchDao extends CommonDao<BankBranch> {

    public List<BankBranch> getAllBranches() {
        Query query =  entityManager.createQuery("select b from BankBranch b");
        return (List<BankBranch>)query.getResultList();
    }

    public List<BankBranch> findBankBranchByFilial(String filial){
        Query query =  entityManager.createQuery("select o from BankBranch o where o.filial = :filial");
        query.setParameter("filial", filial);
        return (List<BankBranch>)query.getResultList();
    }
}
