package ru.alth.nhl.handler;

import com.google.inject.Inject;
import ru.alth.nhl.common.RoleType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.handler.DefaultMlClassHandler;
import ru.ml.core.model.security.MlRole;
import ru.ml.core.model.security.MlUser;
import ru.ml.core.services.AccessService;

import java.util.List;

/**
 * Created by Sergei on 01.04.2016.
 */
public class UserHandler extends DefaultMlClassHandler<MlUser> {

    @Inject
    private AccessService accessService;

    @Override
    public void beforeDelete(MlUser user) {
        checkAdminNHLActions(user);
    }

    @Override
    public void beforeUpdate(MlUser user) {
        checkAdminNHLActions(user);
    }

    @Override
    public void beforeCreate(MlUser user) {
        checkAdminNHLActions(user);
    }

    private void checkAdminNHLActions(MlUser user){
        List<MlRole> roles = accessService.getUserRoles();
        for (MlRole role : roles) {
            if(role.getRoleType().equals(RoleType.ADMIN_NHL)){
                for(MlRole adminRole : user.getRoles()){
                    if(adminRole.getRoleType().equals(ru.ml.core.common.RoleType.MAIN_ADMIN_ROLE)){
                        throw  new MlApplicationException("У вас нет прав на работу с пользователями, имеющими роль Администратор системы");
                    }
                }
            }
        }
    }
}
