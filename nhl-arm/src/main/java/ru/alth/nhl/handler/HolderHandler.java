package ru.alth.nhl.handler;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.RoleType;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.service.HashService;
import ru.alth.nhl.service.ValidateHolders;
import ru.alth.nhl.util.ImageConvertor;
import ru.alth.nhl.util.Transliterator;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.handler.DefaultMlClassHandler;
import ru.ml.core.model.security.MlRole;
import ru.ml.core.services.AccessService;
import ru.ml.prop.MlProperties;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;


/**
 *
 */
public class HolderHandler extends DefaultMlClassHandler<Holder> {
    private static final Logger log = LoggerFactory.getLogger(HolderHandler.class);

    @Inject
    private HolderDao holderDao;
    @Inject
    private AccessService accessService;
    @Inject
    private HashService hashService;
    @Inject
    private CardDao cardDao;
    @Inject
    private Transliterator transliterator;
    @Inject
    private ValidateHolders validateHolders;
    @Inject
    private ImageConvertor imageConvertor;
    @Inject
    private MlProperties mlProperties;

    /**
     * Перед созданием пересчитываем хеш
     *
     * @param holder - держатель
     */
    public void beforeCreate(Holder holder) {
        validateHolders.checkFieldsLength(holder);
        checkResidency(holder);
        checkPhoto(holder, holder.getPhoto());
        generateEmbossedFields(holder);
        checkDocumMaskForResident(holder);
        checkDocumNumber(holder);
        checkUniqueExternalId(holder);
        updateHolderHashes(holder);

    }

    /**
     * Перед созданием пересчитываем хеш
     *
     * @param holder держатель
     */
    public void beforeUpdate(Holder holder) {
        validateHolders.checkFieldsLength(holder);
        checkResidency(holder);
        checkPhoto(holder, holderDao.findHolderByIdNHLNumber(holder.getIdNhlNumber()).getPhoto());
        checkCardsStatuses(holder);
        generateEmbossedFields(holder);
        checkDocumMaskForResident(holder);
        checkDocumNumber(holder);
        checkUniqueExternalId(holder);
        updateHolderHashes(holder);
    }

    private void checkUniqueExternalId(Holder holder) {
        if(!holderDao.checkUniqueExternalId(holder)){
            throw new MlApplicationException("external_id не уникален");
        }
    }

    @Override
    public void afterCreate(Holder holder) {
        generateHolderIdNHLNumber(holder);
    }

    private void updateHolderHashes(Holder holder) {
        hashService.updateHashesForHolderCards(holder);
    }

    private void generateHolderIdNHLNumber(Holder holder){
        Long id = holder.getId();
        if (holder.getIdNhlNumber() == null || holder.getIdNhlNumber().isEmpty()) {
            String idNHLNumber = String.format("%020d", id);
            holder.setIdNhlNumber(idNHLNumber);
        }
    }

    private void generateEmbossedFields(Holder holder){
        holder.setEmbossedName(transliterator.transliterate(holder.getFirstName()));
        holder.setEmbossedLastName(transliterator.transliterate(holder.getLastName()));
    }

    private void checkDocumMaskForResident(Holder holder){
        String pattern = "^\\d{4}\\s{1}\\d{6}$";
        if(holder.getResidency().equals("PR") && !Pattern.matches(pattern, holder.getDocNum())){
            throw new MlApplicationException("Серия и номер паспорта гражданина РФ заполнены некорректно." +
                    " Введите серию и номер через пробел. Например \"1234 567890\"");
        }
    }

    private void checkCardsStatuses(Holder holder) {
        List<Card> cards = cardDao.findCardsByHolder(holder);

        // Проверка роли оператора по приему заявлений
        List<MlRole> roles = accessService.getUserRoles();
        for (MlRole role : roles) {
            if (role.getRoleType().equals(RoleType.DECLARATION_OPERATOR) && !cards.isEmpty()) {
                log.debug("Редактирование запрещено, т.к. у Держателя есть карты или существует заявка на выпуск карты");
                throw new MlApplicationException("Редактирование запрещено, т.к. у Держателя есть карты или существует заявка на выпуск карты");
            }
        }
    }

    // Проверка уникальности номера паспорта
    private void checkDocumNumber(Holder holder){
        if(holderDao.checkHolderByDocumNumber(holder) != null){
            throw new MlApplicationException("Паспортные данные не уникальны");
        }
    }

    private void checkPhoto(Holder holder, byte [] photo){
        boolean autoreduce = Boolean.parseBoolean(mlProperties.getProperty("autoImageReduce"));
        if (photo != null && photo.length > 7620) {
            if(autoreduce){
                byte [] reducedImage = null;
                try{
                    reducedImage = imageConvertor.reduceImage(photo);
                } catch (IOException e){
                    log.debug("Error while reducing holders`s photo", e);
                }
                holder.setPhoto(reducedImage);
            } else {
                throw new MlApplicationException("Размер фотографии не должен превышать 7.5 Кбайт");
            }
        }
    }

    private void checkResidency(Holder holder){
        if(holder.getResidency().equals("PR") && (holder.getSecondName() == null || holder.getSecondName().equals(""))){
            throw new MlApplicationException("У резидента РФ поле \"Отчество\" обязательно для заполнения");
        }
    }
}