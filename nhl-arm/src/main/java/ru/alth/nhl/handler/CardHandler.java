package ru.alth.nhl.handler;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.BankBranchDao;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.CardCreateService;
import ru.alth.nhl.service.HashService;
import ru.ml.core.handler.DefaultMlClassHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Sergei on 02.03.2016.
 */
public class CardHandler extends DefaultMlClassHandler<Card> {
    @Inject
    private HashService hashService;
    @Inject
    private CardCreateService cardCreateService;

    private static final Logger log = LoggerFactory.getLogger(CardHandler.class);

    public void beforeCreate(Card card) {
        cardCreateService.changeCardStatus(card);
        checkRegion(card);
        updateHashes(card);
    }


    public void beforeUpdate(Card card) {
        updateHashes(card);
    }


    public void afterCreate(Card card) {
        generateCardNum(card);
        card.setExpireDate(createExpireDate());
    }

    private void updateHashes(Card card) {
        hashService.updateCardHashes(card);
    }

    private Date createExpireDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 3);
        //TODO java 8
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMyy");
        Date date = new Date();
        try{
            date = dateFormat.parse(dateFormat.format(calendar.getTime()));
        } catch (Exception e){

        }
        return date;
    }

    private void generateCardNum(Card card){
        Long id = card.getId();
        if (card.getNum() == null || card.getNum().isEmpty()) {
            String cardNum = String.format("%08d", id);
            card.setNum(cardNum);
        }
    }

    private void checkRegion(Card card){
        if(card.getHolder().getRegion() != null){
            card.setBankBranch(card.getHolder().getRegion().getBankBranch());
        }
    }
}