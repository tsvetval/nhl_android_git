package ru.alth.nhl.util.crypto;

/**
 *	Base Exception class for packet SMartChipSM and it's subpackets. 
 *  The class contains error code besides a message and gives a possibility to convert error code to corresponding error message.
 */
public class SMartChipSMException extends Exception
{
    private static final long	serialVersionUID	= -5241141167678669568L;
    /**
     * Code of the error
     */
    private int errCode;

    /**
     * Error code that means successful execution
     */
    public static final int SMERR_OK = 0;
    /**
     * Error code that means shortage of resources
     */
    public static final int SMERR_OUT_OF_RESOURCES = 1;
    /**
     * Error code that means that requested functionality isn't supported
     */
    public static final int SMERR_FUNC_NOT_SUPPORTED = 2;
    /**
     * Error code that means that condition of usage of invoked method is not satisfied
     */
    public static final int SMERR_COND_NOT_SATISFIED = 3;
    /**
     * Error code that means that at least one of parameters of invoked method has incorrect value
     */
    public static final int SMERR_WRONG_PARAMETER = 4;
    /**
     * Error code that means that token isn't found in the slot
     */
    public static final int SMERR_TOKEN_NOT_FOUND = 5;
    /**
     * Error code that means that specified token is inapplicable
     */
    public static final int SMERR_TOKEN_INVALID = 6;
    /**
     * Error code that means that specified slot isn't found
     */
    public static final int SMERR_SLOT_NOT_FOUND = 7;
    /**
     * Error code that means that specified token is busy
     */
    public static final int SMERR_TOKEN_BUSY = 8;
    /**
     * Error code that means that specified token ID is incorrect
     */
    public static final int SMERR_WRONG_TOKEN_ID = 9;
    /**
     * Error code that means that the password of the token is blocked
     */
    public static final int SMERR_PASSWORD_BLOCKED = 10;
    /**
     * Error code that means that specified key isn't found
     */
    public static final int SMERR_KEY_NOT_FOUND = 11;
    /**
     * Error code that means that key type is incompatible with invoked method
     */
    public static final int SMERR_INCOMPATIBLE_KEY_TYPE = 12;
    /**
     * Error code that means that attributes of the key are conflicting with each other or with invoked method
     */
    public static final int SMERR_INCOMPATIBLE_KEY_ATTR = 13;
    /**
     * Error code that means that the operation is forbidden for specified key
     */
    public static final int SMERR_OPERATION_FORBIDDEN = 14;
    /**
     * Error code that means that the cryptogram is incorrect
     */
    public static final int SMERR_WRONG_CRYPTOGRAM = 15;
    /**
     * Error code that means that the method can't be executed with given data
     */
    public static final int SMERR_WRONG_DATA = 16;
    /**
     * Error code that means that unsupported cryptography is needed
     */
    public static final int SMERR_UNSUPPORTED_CRYPTO = 17;
    /**
     * Error code that means that specified key already exists
     */
    public static final int SMERR_KEY_ALREADY_EXIST = 18;
    /**
     * Error code that means that specified password is invalid. The second byte contains amount of remaining attempts
     */
    public static final int SMERR_INVALID_PASSWORD = 0x100;
    /**
     * Error code that means fatal error
     */
    public static final int SMERR_FATAL_ERROR = 0x7FFF;
    /**
     * The least value of error code specific for vendors
     */
    public static final int SMERR_VENDOR_SPECIFIC = 0x8000;

    /**
     * Constructor.
     * @param code - Defines code of the error.
     */
    public SMartChipSMException(int code)
    {
        super(GetErrorMessage(code));
        errCode = code;
    }

    /**
     * The method returns a code of the error, that was a cause of the exception
     * @return The unique code of the error
     */
    public int getErrCode()
    {
        return errCode;
    }

    /**
     * The method returns a message which describes specified error code
     * @param code - the code of the error
     * @return Error message
     */
    static public String GetErrorMessage(int code)
    {
        String message = "Undefined error";
        switch (code)
        {
            case SMERR_OK:
                message = "Successful execution";
                break;
            case SMERR_OUT_OF_RESOURCES:
                message = "Shortage of resources";
                break;
            case SMERR_FUNC_NOT_SUPPORTED:
                message = "Requested functionality isn't supported";
                break;
            case SMERR_COND_NOT_SATISFIED:
                message = "Condition of use is not satisfied";
                break;
            case SMERR_WRONG_PARAMETER:
                message = "At least one of parameters of invoked method has incorrect value";
                break;
            case SMERR_TOKEN_NOT_FOUND:
                message = "Token isn't found in the slot";
                break;
            case SMERR_TOKEN_INVALID:
                message = "Token is invalid";
                break;
            case SMERR_SLOT_NOT_FOUND:
                message = "Specified slot isn't found";
                break;
            case SMERR_TOKEN_BUSY:
                message = "Specified token is busy";
                break;
            case SMERR_WRONG_TOKEN_ID:
                message = "Specified token ID is incorrect";
                break;
            case SMERR_PASSWORD_BLOCKED:
                message = "The password of the token is blocked";
                break;
            case SMERR_KEY_NOT_FOUND:
                message = "Specified key isn't found";
                break;
            case SMERR_INCOMPATIBLE_KEY_TYPE:
                message = "Key type is incompatible with invoked method";
                break;
            case SMERR_INCOMPATIBLE_KEY_ATTR:
                message = "Incompatible attributes of the key";
                break;
            case SMERR_OPERATION_FORBIDDEN:
                message = "The operation is forbidden for specified key";
                break;
            case SMERR_WRONG_CRYPTOGRAM:
                message = "The cryptogram is incorrect";
                break;
            case SMERR_WRONG_DATA:
                message = "The method can't be executed with given data";
                break;
            case SMERR_UNSUPPORTED_CRYPTO:
                message = "Unsupported cryptography is needed";
                break;
            case SMERR_KEY_ALREADY_EXIST:
                message = "Specified key already exists";
                break;
            case SMERR_FATAL_ERROR:
                message = "Security Module fatal error";
                break;
            default:
                break;
        }
        // Wrong password presented, password isn't blocked
        if (code >= SMERR_INVALID_PASSWORD && code < (SMERR_INVALID_PASSWORD | 0xFF))
            message = "Specified password is invalid. " + (code & 0x0FF) + " attempts remains";
        // Specified error code belong to range of vendor specific codes
        if (code >= SMERR_VENDOR_SPECIFIC )
            message = "Vendor specific error";
        return message;
    }
}
