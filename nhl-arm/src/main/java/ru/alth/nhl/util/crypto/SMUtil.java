package ru.alth.nhl.util.crypto;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.sql.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Formatter;
import java.util.GregorianCalendar;

import static ru.alth.nhl.util.crypto.EnCipherMode.*;
import static ru.alth.nhl.util.crypto.EnMacMode.*;

/**
 * The class contains various useful utilities formed as static methods
 */
public class SMUtil {
    /**
     * The method converts a number from BCD format to int type
     *
     * @param bcd    - array contains source number (BCD)
     * @param offset - offset inside array where BCD number is located
     * @param len    - length of BCD number in bytes
     * @return int-presentation of BCD number
     * @throws SMartChipSMException if input array doesn't contain BCD number
     */
    public static int Bcd2Int(byte[] bcd, int offset, int len) throws SMartChipSMException {
        int i, ret, finish = offset + len;
        int high, low;
        for (i = offset, ret = 0; i < finish; i++) {
            high = (bcd[i] >> 4) & 0x0F;
            low = bcd[i] & 0x0F;
            if (high > 9 || low > 9)
                throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
            ret = ret * 100 + high * 10 + low;
        }
        return ret;
    }

    /**
     * The method converts fragment of byte array to string (one byte -> two hexadecimal characters)
     *
     * @param arr    - source byte array
     * @param offset - offset in array arr
     * @param len    - length of fragment to convert (in bytes)
     * @return string presentation of binary data
     */
    public static String ByteArrayToHexString(byte arr[], int offset, int len) {
        byte[] array = Arrays.copyOfRange(arr, offset, offset + len);
        return DatatypeConverter.printHexBinary(array);
    }

    /**
     * The method converts byte array to string (one byte -> two hexadecimal characters)
     *
     * @param arr - source byte array
     * @return string presentation of binary data
     */
    public static String ByteArrayToHexString(byte arr[]) {
        return ByteArrayToHexString(arr, 0, arr.length);
    }

    /**
     * The method converts string of hexadecimal characters to byte array (two hexadecimal characters -> one byte)
     *
     * @param s - hexadecimal string
     * @return byte array
     * @throws SMartChipSMException if input string has odd length or contains not hexadecimal characters
     */
    public static byte[] HexStringToByteArray(String s) throws SMartChipSMException {
        if (s == null || s.length() == 0)
            return null;
        if (s.length() % 2 != 0)
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        return DatatypeConverter.parseHexBinary(s);
    }

    /**
     * The method converts string presentation of date to instance of class Date
     *
     * @param str - string presentation of date "DD.MM.YYYY"
     * @return constructed instance of class Date
     * @throws SMartChipSMException - if input string has wrong format
     */
    public static Date StringDateToDate(String str) throws SMartChipSMException {
        Date date;
        Calendar calendar = GregorianCalendar.getInstance();
        try {
            String sArr[] = str.split("\\x2E");
            if (sArr.length != 3 || sArr[0].length() != 2 || sArr[1].length() != 2 || sArr[2].length() != 4)
                throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
            //int year=Integer.parseInt(sArr[2]), month=Integer.parseInt(sArr[1]), day=Integer.parseInt(sArr[0]);
            calendar.set(Integer.parseInt(sArr[2]), Integer.parseInt(sArr[1]) - 1, Integer.parseInt(sArr[0]));
            date = new Date(calendar.getTimeInMillis());
        } catch (Exception ex) {
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        }
        return date;
    }

    /**
     * The method converts internal Java presentation of date/time to string like "DD.MM.YYYY"
     *
     * @param date - Java presentation of date/time
     * @return a string that present the date as string looks like "DD.MM.YYYY"
     */
    public static String DateToStringDate(Date date) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        Formatter fmt = new Formatter();
        fmt.format("%02d.%02d.%04d", calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
        return fmt.toString();
    }

    /**
     * The method extracts value of specified tag from specified XML element
     *
     * @param element - XML element
     * @param sTag    - tag
     * @return value of specified tag
     */
    public static String GetXmlTagValue(Element element, String sTag) {
        NodeList nlList = element.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    /**
     * The method generates random number of specified length
     *
     * @param len - the length of random number to build
     * @return Generated random number
     */
    public static byte[] GetRandom(int len) {
        byte[] rand = new byte[len];
        SecureRandom randomGenerator = new SecureRandom();
        randomGenerator.nextBytes(rand);
        return rand;
    }

    /**
     * The method encrypts/decrypts presented data using simple (8 bytes) DES key
     *
     * @param source   - data to encrypt/decrypt
     * @param keyValue - the value of simple DES-key. The array should be 8 bytes long
     * @param dir      - type of cryptographic transformation. The parameter could be ENCRYPT_MODE or DECRYPT_MODE
     * @param mode     - mode  of cryptographic transformation. The parameter could be ECB or CBC.
     * @return Encrypted/plain text data
     * @throws SMartChipSMException
     */
    public static byte[] encDecSimpleDes(byte[] source, byte[] keyValue, EnCipherDirection dir, EnCipherMode mode) throws SMartChipSMException {
        SecretKeyFactory keyfactory;
        Cipher cipher;
        SecretKey key;
        DESKeySpec specKey;
        byte[] z = {0, 0, 0, 0, 0, 0, 0, 0}, ret;
        IvParameterSpec iv;

        if (keyValue == null || keyValue.length != 8)
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        try {
            specKey = new DESKeySpec(keyValue);
            keyfactory = SecretKeyFactory.getInstance("DES");
            key = keyfactory.generateSecret((KeySpec) specKey);
            switch (mode) {
                case CBC:
                    cipher = Cipher.getInstance("DES/CBC/NoPadding");
                    iv = new IvParameterSpec(z);
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key, iv);
                    break;
                case ECB:
                    cipher = Cipher.getInstance("DES/ECB/NoPadding");
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key);
                    break;
                default:
                    throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
            }
            ret = cipher.doFinal(source);
        } catch (Exception e) {
            throw new SMartChipSMException(SMartChipSMException.SMERR_FATAL_ERROR);
        }
        return ret;
    }

    /**
     * The method encrypts/decrypts presented data using double length (16 bytes) DES key
     *
     * @param keyValue - the value of DES-key. The array should be 16 bytes long
     * @param source   - data to encrypt/decrypt
     * @param mode     - mode  of cryptographic transformation. The parameter could be ECB or CBC.
     * @param dir      - type of cryptographic transformation. The parameter could be ENCRYPT_MODE or DECRYPT_MODE
     * @return Encrypted/plain text data
     * @throws SMartChipSMException
     */
    public static byte[] encDec3DES2(byte[] keyValue, byte[] source, EnCipherMode mode, EnCipherDirection dir) throws SMartChipSMException {
        byte[] buf = new byte[24];
        SecretKeyFactory keyfactory;
        Cipher cipher;
        SecretKey key;
        DESedeKeySpec specKey;
        byte[] z = {0, 0, 0, 0, 0, 0, 0, 0}, ret;
        IvParameterSpec iv;

        if (keyValue == null || keyValue.length != 16)
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        try {
            System.arraycopy(keyValue, 0, buf, 0, 16);
            System.arraycopy(keyValue, 0, buf, 16, 8);
            specKey = new DESedeKeySpec(buf);
            keyfactory = SecretKeyFactory.getInstance("DESede");
            key = keyfactory.generateSecret((KeySpec) specKey);
            switch (mode) {
                case CBC:
                    cipher = Cipher.getInstance("DESede/CBC/NoPadding");
                    iv = new IvParameterSpec(z);
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key, iv);
                    break;
                case ECB:
                    cipher = Cipher.getInstance("DESede/ECB/NoPadding");
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key);
                    break;
                default:
                    throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
            }
            ret = cipher.doFinal(source);
        } catch (Exception e) {
            throw new SMartChipSMException(SMartChipSMException.SMERR_FATAL_ERROR);
        }
        return ret;
    }

    /**
     * The method computes a message createExternalAuthenticationCommand code (MAC) from the specified data using specified key.
     * Before evaluating MAC the library converts the raw data in accordance to method 2 of standard ISO/IEC 9797-1
     * by adding to the original message to the right of compulsory byte '80 'and then the minimum number of bytes of '00',
     * so that the size of the resulting message <b>MSG: = (MSG || '80 '|| '00' || ... || '00')</b> was a multiple of 8 bytes.
     * Mode of MAC computation specifies mode parameter, which can take the following values:
     * <pre>
     * <b>MAC_FULL_TDES</b> - calculation in accordance with ISO / IEC 9797-1 algorithm 1;
     * <b>MAC_RETAIL</b> - calculation in accordance with ISO / IEC 9797-1 algorithm 3.
     * </pre>
     *
     * @param key  - the value of DES-key. The array should be 16 bytes long
     * @param data - an array containing the data that should be used to compute MAC. The length of the data may not be a multiple of 8.
     * @param mode - mode of computing MAC. Possible values are listed above.
     * @return The message createExternalAuthenticationCommand code (MAC) from the specified data
     * @throws SMartChipSMException
     */
    public static byte[] mac(byte[] key, byte[] data, EnMacMode mode) throws SMartChipSMException {
        byte[] ret = new byte[8];
        int srcLen = ((data.length + 8) / 8) * 8;
        byte[] src = new byte[srcLen];
        byte[] lastBlk = new byte[8];
        byte[] keySimple;

        Arrays.fill(src, (byte) 0);
        System.arraycopy(data, 0, src, 0, data.length);
        src[data.length] = (byte) 0x80;
        if (src.length == 8)
            mode = MAC_FULL_TDES;
        switch (mode) {
            case MAC_FULL_TDES:
                src = encDec3DES2(key, src, CBC, EnCipherDirection.ENCRYPT_MODE);
                System.arraycopy(src, srcLen - 8, ret, 0, 8);
                break;
            case MAC_RETAIL:
                keySimple = new byte[8];
                System.arraycopy(key, 0, keySimple, 0, 8);
                ret = SMUtil.encDecSimpleDes(src, keySimple, EnCipherDirection.ENCRYPT_MODE, CBC);
                for (int i = 0; i < 8; i++)
                    lastBlk[i] = (byte) (src[srcLen - 8 + i] ^ ret[srcLen - 16 + i]);
                ret = encDec3DES2(key, lastBlk, ECB, EnCipherDirection.ENCRYPT_MODE);
                break;
            default:
                throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        }
        return ret;
    }

    /**
     * The method computes SHA1 hash function from the specified data.
     *
     * @param data - data to calculate hash
     * @return - calculated hash value
     * @throws Exception
     */
    public static byte[] sha1(byte[] data) throws Exception {
        MessageDigest algorithm = MessageDigest.getInstance("SHA1");
        algorithm.update(data);
        return algorithm.digest();
    }

    /**
     * The method computes the Key Check Value (KCV from specified 3DES2 key.
     *
     * @param keyValue - 3DES2 key to calculate KCV
     * @return - calculated Key Check Value
     * @throws SMartChipSMException
     */
    public static byte[] calcKCV(byte[] keyValue) throws SMartChipSMException {
        byte[] zerros = new byte[]{0, 0, 0, 0, 0, 0, 0, 0}, res, kcv;
        res = encDec3DES2(keyValue, zerros, ECB, EnCipherDirection.ENCRYPT_MODE);
        kcv = new byte[3];
        System.arraycopy(res, 0, kcv, 0, 3);
        return kcv;
    }

    /**
     * The method processes data on the submitted public key. The data length must be equal to modulusLen.
     *
     * @param modulus        - the modulus of RSA key in big endian format
     * @param publicExponent - public exponent of RSA key in big endian format
     * @param data           - data to process. The data length must be equal to modulusLen.
     * @return Resulting cryptogram
     * @throws Exception
     */
    public static byte[] rsaPublic(byte[] modulus, int publicExponent, byte[] data) throws Exception {
        RSAPublicKeySpec pubSpecKey;
        BigInteger biMod, biData;
        RSAPublicKey pubKey;
        byte[] zerro = {0};
        byte[] pubExp = new byte[4];
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Cipher cipher;

        //if ( modulus.length != data.length )
        //	throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        if (modulus[0] < 0)
            modulus = arraysConcat(zerro, modulus);
        biMod = new BigInteger(modulus);
        biData = new BigInteger(data);
        if (biMod.compareTo(biData) <= 0)
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        pubExp[0] = (byte) ((publicExponent >> 24) & 0x00FF);
        pubExp[1] = (byte) ((publicExponent >> 16) & 0x00FF);
        pubExp[2] = (byte) ((publicExponent >> 8) & 0x00FF);
        pubExp[3] = (byte) ((publicExponent) & 0x00FF);
        pubSpecKey = new RSAPublicKeySpec(biMod, new BigInteger(pubExp));
        pubKey = (RSAPublicKey) keyFactory.generatePublic(pubSpecKey);

        cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * The method concatenates two presented arrays
     *
     * @param A - the first array (it will be left part of resulting array)
     * @param B - the second array (it will be right part of resulting array)
     * @return Concatenation of presented arrays
     */
    public static byte[] arraysConcat(byte[] A, byte[] B) {
        byte[] res = new byte[A.length + B.length];
        System.arraycopy(A, 0, res, 0, A.length);
        System.arraycopy(B, 0, res, A.length, B.length);
        return res;
    }

    /**
     * The method searches the specified tag in presented TLV-expression. TLV-expression can contains tags no more than 2 bytes long.
     * The search is recursive (method enters into "constructed" data objects).
     *
     * @param TLV    - an array which contains the TLV-expression
     * @param Offset - offset of the TLV-expression in array TLV
     * @param Len    - the length of the TLV-expression
     * @param sTag   - the tag to search in the TLV-expression
     * @return The value of found tag, or null (if tag isn't found)
     * @throws SMartChipSMException
     */
    public static byte[] TLV_GetValue(byte[] TLV, int Offset, int Len, String sTag) throws SMartChipSMException {
        byte[] Tag = HexStringToByteArray(sTag);
        int posTag, TagLen, ValueLen, posLen, posValue, i;
        Len += Offset;
        for (posTag = Offset; posTag < Len; ) {
            // Parse a tag
            if ((TLV[posTag] & (byte) 0x1F) == 0x1F) {
                TagLen = 2;
                for (; posTag + TagLen - 1 < Len; TagLen++) {
                    if ((TLV[posTag + TagLen - 1] & (byte) 0x80) == 0)
                        break;
                }
            } else
                TagLen = 1;
            posLen = posTag + TagLen;
            // Parse the length of the value
            if ((TLV[posLen] & (byte) 0x80) == 0) {
                ValueLen = TLV[posLen];
                posValue = posLen + 1;
            } else {
                int LenLen = TLV[posLen] & 0x7F;
                if (LenLen > 4)
                    return null;
                for (i = 0, ValueLen = 0; i < LenLen; i++) {
                    ValueLen <<= 8;
                    ValueLen += (TLV[posLen + 1 + i] & 0x000000FF);
                }
                posValue = posLen + 1 + LenLen;
            }
            // Is the tag found?
            if (Tag.length == TagLen && Arrays.equals(Tag, Arrays.copyOfRange(TLV, posTag, posTag + TagLen))) {
                byte[] ret = new byte[ValueLen];
                System.arraycopy(TLV, posValue, ret, 0, ValueLen);
                return ret;
            }
            if ((TLV[posTag] & (byte) 0x20) != 0)    // Constructed data object
            {
                // Enter into the tag
                byte[] ret = TLV_GetValue(TLV, posValue, ValueLen, sTag);
                if (ret != null)
                    return ret;
            }
            // move to a next tag
            posTag = posValue + ValueLen;
        }
        return null;
    }

    /**
     * The method searches the specified tag in presented TLV-expression. TLV-expression can contains tags no more than 2 bytes long.
     * The search is not recursive (method isn't enters into "constructed" data objects).
     *
     * @param TLV      - an array which contains the TLV-expression
     * @param sTag     - the tag to search in the TLV-expression
     * @param tagCount - required sequence number of the occurrence of the tag
     * @return The value of found tag, or null (if tag isn't found)
     * @throws SMartChipSMException
     */
    public static byte[] TLV_GetValuePlane(byte[] TLV, String sTag, int tagCount) throws SMartChipSMException {
        byte[] Tag = HexStringToByteArray(sTag);
        int posTag, TagLen, ValueLen, posLen, posValue, i, counter;
        for (posTag = 0, counter = 1; posTag < TLV.length; ) {
            // Parse a tag
            if ((TLV[posTag] & (byte) 0x1F) == 0x1F) {
                TagLen = 2;
                for (; posTag + TagLen - 1 < TLV.length; TagLen++) {
                    if ((TLV[posTag + TagLen - 1] & (byte) 0x80) == 0)
                        break;
                }
            } else
                TagLen = 1;
            posLen = posTag + TagLen;
            // Parse the length of the value
            if ((TLV[posLen] & (byte) 0x80) == 0) {
                ValueLen = TLV[posLen];
                posValue = posLen + 1;
            } else {
                int LenLen = TLV[posLen] & 0x7F;
                if (LenLen > 4)
                    return null;
                for (i = 0, ValueLen = 0; i < LenLen; i++) {
                    ValueLen <<= 8;
                    ValueLen += (TLV[posLen + 1 + i] & 0x000000FF);
                }
                posValue = posLen + 1 + LenLen;
            }
            // Is the tag found?
            if (Tag.length == TagLen && Arrays.equals(Tag, Arrays.copyOfRange(TLV, posTag, posTag + TagLen))) {
                if (tagCount == counter) // this occurrence is the tagCount occurrence in order
                {
                    byte[] ret = new byte[ValueLen];
                    System.arraycopy(TLV, posValue, ret, 0, ValueLen);
                    return ret;
                } else
                    counter++;
            }
            // move to a next tag
            posTag = posValue + ValueLen;
        }
        return null;
    }


    /**
     * The method converts presented integer to hexadecimal string with even length
     *
     * @param num - integer
     * @return Hexadecimal presentation of integer number
     */
    public static String IntToHexStr(int num) {
        String tStr = Integer.toString(num, 16);
        if (tStr.length() % 2 != 0)
            tStr = "0" + tStr;
        return tStr;
    }


    public static byte[] encDec3DES2WithIV(byte[] keyValue, byte[] source, EnCipherMode mode, EnCipherDirection dir, byte [] z) throws SMartChipSMException {
        byte[] buf = new byte[24];
        SecretKeyFactory keyfactory;
        Cipher cipher;
        SecretKey key;
        DESedeKeySpec specKey;
        byte[] ret;
        IvParameterSpec iv;

        if (keyValue == null || keyValue.length != 16)
            throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
        try {
            System.arraycopy(keyValue, 0, buf, 0, 16);
            System.arraycopy(keyValue, 0, buf, 16, 8);
            specKey = new DESedeKeySpec(buf);
            keyfactory = SecretKeyFactory.getInstance("DESede");
            key = keyfactory.generateSecret((KeySpec) specKey);
            switch (mode) {
                case CBC:
                    cipher = Cipher.getInstance("DESede/CBC/NoPadding");
                    iv = new IvParameterSpec(z);
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key, iv);
                    break;
                case ECB:
                    cipher = Cipher.getInstance("DESede/ECB/NoPadding");
                    if (dir == EnCipherDirection.ENCRYPT_MODE)
                        cipher.init(Cipher.ENCRYPT_MODE, key);
                    else
                        cipher.init(Cipher.DECRYPT_MODE, key);
                    break;
                default:
                    throw new SMartChipSMException(SMartChipSMException.SMERR_WRONG_PARAMETER);
            }
            ret = cipher.doFinal(source);
        } catch (Exception e) {
            throw new SMartChipSMException(SMartChipSMException.SMERR_FATAL_ERROR);
        }
        return ret;
    }

}
