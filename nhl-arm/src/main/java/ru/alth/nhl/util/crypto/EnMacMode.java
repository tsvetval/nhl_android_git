package ru.alth.nhl.util.crypto;

/**
 *
 */
public enum EnMacMode {
    /**
     * MAC in accordance with ISO / IEC 9797-1 algorithm 1
     */
    MAC_FULL_TDES,
    /**
     * MAC in accordance with ISO / IEC 9797-1 algorithm 3
     */
    MAC_RETAIL
}
