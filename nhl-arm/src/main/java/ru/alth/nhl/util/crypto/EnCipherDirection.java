package ru.alth.nhl.util.crypto;

/**
 *
 */
public enum EnCipherDirection {
    ENCRYPT_MODE,
    DECRYPT_MODE
}
