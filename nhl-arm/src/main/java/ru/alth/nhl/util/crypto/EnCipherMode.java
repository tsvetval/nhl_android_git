package ru.alth.nhl.util.crypto;

/**
 *
 */
public enum EnCipherMode {
    ECB,
    CBC
}
