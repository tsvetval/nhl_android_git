package ru.alth.nhl.util;

import com.google.inject.Inject;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.service.ImportHolders;
import ru.ml.core.common.exceptions.MlApplicationException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

/**
 *
 */
public class ZipReader {

    private static final Logger log = LoggerFactory.getLogger(ZipReader.class);
    private static final String XLSX_PATTERN = "^\\d{14}[_]\\d+(.xlsx)$";

    @Inject
    private ImportHolders importHolders;

    public HashMap<String, byte[]> unzipArchive(String filePath) {
        log.debug(String.format("Begin extracting zip with path [%s]", filePath));
        HashMap<String, byte[]> result = new HashMap<>();
        String xlsxFileName = null;
        ArchiveEntry photosEntry;
        boolean directoryHasImages = false;
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(filePath));
            ArchiveInputStream input = new ArchiveStreamFactory().createArchiveInputStream(is);
            ArchiveEntry entry;
            while ((entry = input.getNextEntry()) != null) {
                if (entry.getName().endsWith(".xlsx")) {
                    xlsxFileName = entry.getName();
                    result.put(xlsxFileName.substring(0, xlsxFileName.indexOf(".")), IOUtils.toByteArray(input));
                }
                if (entry.isDirectory()) {
                    while ((photosEntry = input.getNextEntry()) != null) {
                        if(photosEntry.getName().endsWith(".xlsx")){
                            xlsxFileName = photosEntry.getName();
                            result.put(xlsxFileName.substring(0, xlsxFileName.indexOf(".")), IOUtils.toByteArray(input));
                            break;
                        }
                        if(photosEntry.getName().endsWith(".jpg")){
                            directoryHasImages = true;
                            result.put(photosEntry.getName().substring(photosEntry.getName().indexOf("/") + 1, photosEntry.getName().indexOf("_")), IOUtils.toByteArray(input));
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.debug(String.format("Unable to extract zip with path [%s]", filePath));
            throw new MlApplicationException("Невозможно прочитать архив", e);
        }

        if(xlsxFileName == null || !importHolders.isReportNameCorrect(xlsxFileName, XLSX_PATTERN) || !directoryHasImages){
            throw new MlApplicationException("Выбранный файл не поддерживается системой");
        }
        return result;
    }
}
