package ru.alth.nhl.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Перевод русских символов в транслит
 */
public class Transliterator {

    private static final Map<Character, String> charMap = new HashMap<>();

    static {
        charMap.put('А', "A");
        charMap.put('Б', "B");
        charMap.put('В', "V");
        charMap.put('Г', "G");
        charMap.put('Д', "D");
        charMap.put('Е', "E");
        charMap.put('Ё', "E");
        charMap.put('Ж', "ZH");
        charMap.put('З', "Z");
        charMap.put('И', "I");
        charMap.put('Й', "I");
        charMap.put('К', "K");
        charMap.put('Л', "L");
        charMap.put('М', "M");
        charMap.put('Н', "N");
        charMap.put('О', "O");
        charMap.put('П', "P");
        charMap.put('Р', "R");
        charMap.put('С', "S");
        charMap.put('Т', "T");
        charMap.put('У', "U");
        charMap.put('Ф', "F");
        charMap.put('Х', "KH");
        charMap.put('Ц', "TS");
        charMap.put('Ч', "CH");
        charMap.put('Ш', "SH");
        charMap.put('Щ', "SHCH");
        charMap.put('Ъ', "IE");
        charMap.put('Ы', "Y");
        charMap.put('Ь', "");
        charMap.put('Э', "E");
        charMap.put('Ю', "IU");
        charMap.put('Я', "IA");
        charMap.put('а', "A");
        charMap.put('б', "B");
        charMap.put('в', "V");
        charMap.put('г', "G");
        charMap.put('д', "D");
        charMap.put('е', "E");
        charMap.put('ё', "E");
        charMap.put('ж', "ZH");
        charMap.put('з', "Z");
        charMap.put('и', "I");
        charMap.put('й', "I");
        charMap.put('к', "K");
        charMap.put('л', "L");
        charMap.put('м', "M");
        charMap.put('н', "N");
        charMap.put('о', "O");
        charMap.put('п', "P");
        charMap.put('р', "R");
        charMap.put('с', "S");
        charMap.put('т', "T");
        charMap.put('у', "U");
        charMap.put('ф', "F");
        charMap.put('х', "KH");
        charMap.put('ц', "TS");
        charMap.put('ч', "CH");
        charMap.put('ш', "SH");
        charMap.put('щ', "SHCH");
        charMap.put('ъ', "IE");
        charMap.put('ы', "Y");
        charMap.put('ь', "");
        charMap.put('э', "E");
        charMap.put('ю', "IU");
        charMap.put('я', "IA");
        charMap.put('a', "A");
        charMap.put('b', "B");
        charMap.put('c', "C");
        charMap.put('d', "D");
        charMap.put('e', "E");
        charMap.put('f', "F");
        charMap.put('g', "G");
        charMap.put('h', "H");
        charMap.put('i', "I");
        charMap.put('j', "J");
        charMap.put('k', "K");
        charMap.put('l', "L");
        charMap.put('m', "M");
        charMap.put('n', "N");
        charMap.put('o', "O");
        charMap.put('p', "P");
        charMap.put('q', "Q");
        charMap.put('r', "R");
        charMap.put('s', "S");
        charMap.put('t', "T");
        charMap.put('u', "U");
        charMap.put('v', "V");
        charMap.put('w', "W");
        charMap.put('x', "X");
        charMap.put('y', "Y");
        charMap.put('z', "Z");
    }

    public String transliterate(String string) {
        StringBuilder transliteratedString = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            Character ch = string.charAt(i);
            String charFromMap = charMap.get(ch);
            if (charFromMap == null) {
                transliteratedString.append(ch);
            } else {
                transliteratedString.append(charFromMap);
            }
        }
        return transliteratedString.toString();
    }
}
