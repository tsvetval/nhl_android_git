package ru.alth.nhl.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 */
public class ImageConvertor {

    private static final Logger log = LoggerFactory.getLogger(ImageConvertor.class);

    /**
     * Уменьшает размер изображения
     */
    public byte [] reduceImage(byte [] image) throws IOException {
        log.debug(String.format("Start reducing image with size: [%s] bytes", image.length));
        InputStream imageInputStream;
        Image img;
        int width;
        int height;

        while (image.length > 7620) {
            imageInputStream = new ByteArrayInputStream(image);
            img = ImageIO.read(imageInputStream);
            width = (int)Math.round(img.getWidth(null) * 0.9);
            height = (int)Math.round(img.getHeight(null) * 0.9);
            log.debug(String.format("Trying width [%s] and height [%s]", width, height));

            final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            final Graphics2D graphics2D = bufferedImage.createGraphics();
            graphics2D.setComposite(AlphaComposite.Src);
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.drawImage(img, 0, 0, width, height, null);
            graphics2D.dispose();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", baos);
            baos.flush();
            image = baos.toByteArray();
            baos.close();
        }
        log.debug(String.format("End reducing image. Final size is: [%s] bytes", image.length));
        return image;
    }

}
