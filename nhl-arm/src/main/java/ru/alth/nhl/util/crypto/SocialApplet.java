package ru.alth.nhl.util.crypto;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;

import javax.smartcardio.CardException;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.*;

/**
 * Операции для карты с Social Applet`ом
 */
public class SocialApplet {
    private static final Logger log = LoggerFactory.getLogger(SocialApplet.class);

    private static final String writeKey1 = "49C2B5296EF420A4A229E68AD9E543A4";
    private static final String writeKey2 = "16F862073104EA8F0801081697CECE6B";
    private static final String writeKey3 = "49295D9ECB621394A864F880E92C1CBF";
    private static final String writeKey4 = "DCA43E25BA1AA19254C7802361B9D932";
    private static final String writeKey5 = "2F3D68EC2C46B9D9D07A437AEA73F47A";


    public String createExternalAuthenticationCommand(String inputCommand, String randomNumber, String fileNumber, byte [] sessionKey) throws SMartChipSMException {

        log.debug("Creating external authentication command");
        // Счётчик аутентификаций
        String counter = inputCommand.substring(2, 6);

        // Случайное число, генерируемое картой
        String challenge = inputCommand.substring(6, 18);

        // Формирование блока Dhost
        String dHost = counter + challenge + randomNumber + "8000000000000000";

        byte [] dcrHost = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(dHost), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);

        String outputCrypt = DatatypeConverter.printHexBinary(dcrHost)
                .substring(DatatypeConverter.printHexBinary(dcrHost).length() - 16, DatatypeConverter.printHexBinary(dcrHost).length());
        // Команда APDU External Authentication
        return  "808200" + fileNumber + "08" + outputCrypt;
    }


    public String diverKey(String selectRes, String masterKey) throws SMartChipSMException {
        log.debug("Diversification of master key");
        // Серийный номер карты
        String serialNum = selectRes.substring(selectRes.length() - 16);
        log.debug("Card serial number " + serialNum);
        String keyDerivationData = serialNum + inverse(serialNum);
        log.debug("KeyDerivationData " + keyDerivationData);

        log.debug("Master-key " + masterKey);
        byte [] cardKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(masterKey),
                DatatypeConverter.parseHexBinary(keyDerivationData), EnCipherMode.ECB, EnCipherDirection.ENCRYPT_MODE);
        return DatatypeConverter.printHexBinary(cardKey);
    }


    private String inverse(String string){
        Map<Character, Character> map = new HashMap<>();
        map.put('0', 'F');
        map.put('1', 'E');
        map.put('2', 'D');
        map.put('3', 'C');
        map.put('4', 'B');
        map.put('5', 'A');
        map.put('6', '9');
        map.put('7', '8');
        map.put('8', '7');
        map.put('9', '6');
        map.put('A', '5');
        map.put('B', '4');
        map.put('C', '3');
        map.put('D', '2');
        map.put('E', '1');
        map.put('F', '0');
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < string.length(); i++){
            output.append(map.get(string.charAt(i)));
        }
        return output.toString();
    }

    public List<String> generateReadCmdList(int fileSize, byte [] sessionKey, String hostCrypto) throws SMartChipSMException, IOException, CardException {
        log.debug("Generation read commands list");
        List<String> resultList = new ArrayList<>();

        int blockSize = 192;
        String ClaIns = "84B0";
        int offset = 0;

        while (fileSize > offset) {
            int length;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset);

            int cmdLen = length;
            String dataForMac = ClaIns + p1p2 + String.format("%02X", cmdLen) + "800000";
            log.debug("Data for mac " + dataForMac);
            String cmd = ClaIns + p1p2 + "09" + String.format("%02X", cmdLen);

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(sessionKey,
                    DatatypeConverter.parseHexBinary(dataForMac), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE, DatatypeConverter.parseHexBinary(hostCrypto)));

            log.debug("Calculated mac " + calcMacData);
            cmd = cmd + calcMacData;
            log.debug("Command read binary " + cmd);
            resultList.add(cmd);
            offset += length;
        }
        return resultList;
    }


    public List<String> getWriteFileCommands(Long fileNumber, String fileData, byte[] sessionKey, String hostCrypto) throws Exception {
        List<String> resultList = new ArrayList<>();
        log.debug("FileData " + fileData);
        log.debug("Host crypto " + hostCrypto);
        // Select file
        String selectFile =  "80A400" + String.format("%02X", fileNumber) + "04";
        log.debug("Select file " + selectFile);
        resultList.add(selectFile);

//        int blockSize = 234;//192; // 192 -
        int blockSize = 192; // 192 -
        String ClaIns = "84D0";
        int offset = 0;
        int fileSize = fileData.length();
        int length = 0;
        StringBuilder stringBuilder = new StringBuilder(fileData);
        String iv = hostCrypto;
        while (fileSize > offset) {

            if (fileData.length() - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileData.length() - offset;
            }

            String dataStr = stringBuilder.substring(offset, offset + length);
            String len = String.format("%02X", dataStr.length() / 2);
            log.debug("Len " + len);
            dataStr += "80";
            // Построение блока EncryptedData
            String dataForEncrypt = dataStr;

            while (dataForEncrypt.length() % 16 != 0){
                dataForEncrypt += "00";
            }
            log.debug("dataForEncrypt " + dataForEncrypt); // Data + padding кратно 8 байтам

            String encryptedData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2(sessionKey,
                    DatatypeConverter.parseHexBinary(dataForEncrypt), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE));
            log.debug("EncryptedData " + encryptedData);


            // Построение блока MAC
            while ((10 + dataStr.length()) % 16 != 0){
                dataStr += "00";
            }
            log.debug("dataStr " + dataStr); // Data + padding кратно 8 байтам


            String p1p2 = String.format("%04X", offset/2);
            log.debug("p1p2 " + p1p2);

            String dataForMac = ClaIns + p1p2 + len + dataStr;

            log.debug("dataForMac " + dataForMac);

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(sessionKey,
                    DatatypeConverter.parseHexBinary(dataForMac), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE,
                    DatatypeConverter.parseHexBinary(iv)));
            calcMacData = calcMacData.substring(calcMacData.length()-16);
            log.debug("CalcMacData " + calcMacData);

            String blockLength = String.format("%02X", (encryptedData.length() + calcMacData.length()) / 2);
            log.debug("BlockLength " + blockLength);

            String cmd = ClaIns + p1p2 + blockLength + encryptedData + calcMacData;
            log.debug("Cmd " + cmd);

            resultList.add(cmd);
            offset += length;
            iv = calcMacData;
        }
        return resultList;
    }
}
