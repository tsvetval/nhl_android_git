package ru.alth.nhl.common;

/**
 * Роли пользователей
 */
public class RoleType extends ru.ml.core.common.RoleType {

    public static final String DECLARATION_OPERATOR = "DECLARATION_OPERATOR";
    public static final String REGISTRY_OPERATOR = "REGISTRY_OPERATOR";
    public static final String ADMIN_NHL ="ADMIN_NHL";
}
