package ru.alth.nhl.service.reports.importReports;

import ru.alth.nhl.model.PersoType;

import java.util.List;

/**
 *
 */
public interface ImportReport {
    void importReport();
    void setPersoType(PersoType type);
}
