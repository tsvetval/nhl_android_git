package ru.alth.nhl.service;


import au.com.bytecode.opencsv.CSVWriter;
import com.google.inject.Inject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.dto.HolderDto;
import ru.ml.core.common.exceptions.MlApplicationException;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Импорт данных держателей
 */
public class ImportHolders {

    private static final Logger log = LoggerFactory.getLogger(ImportHolders.class);
    private static final SimpleDateFormat birthdayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final Long MAX_PHOTO_SIZE = 12288L;

    @Inject
    private HolderService holderService;
    @Inject
    private ImportPersoServiceImpl importPersoServiceImpl;
    @Inject
    private ValidateHolders validateHolders;


    public List<String[]> saveHolders(List<HolderDto> holderDtoList) throws IOException {
        List<String[]>  errorsList = new ArrayList<>();
        for(HolderDto holderDto: holderDtoList){
            try {
                log.debug(String.format("Start checking holder [%s]", holderDto.getExternalId()));
                if(validateHolders.isCorrectHolderData(holderDto) == null){
                    log.debug("Checking success");
                    holderService.saveHolderViaHolderDto(holderDto);
                } else{
                    log.debug("Checking failed");
                    errorsList.add(validateHolders.isCorrectHolderData(holderDto));
                }
            } catch (ParseException e) {
                log.debug("Error while parsing file");
                throw new MlApplicationException("Ошибка чтения файла", e);
            }
        }
        return errorsList;
    }


    public List<HolderDto> parseXLSXFile(String fileName, HashMap<String, byte[]> unzippedFile) {
        log.debug(String.format("Start parsing XLSX report [%s]", fileName));
        byte[] csvFile = unzippedFile.get(fileName.substring(0, fileName.indexOf(".")));
        InputStream is = new ByteArrayInputStream(csvFile);
        List<HolderDto> holderDtoList = new ArrayList<>();

        try{
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next();
            while (rowIterator.hasNext()){
                ArrayList<String> record = new ArrayList<>();
                Row row = rowIterator.next();
                Cell cell;
                for(int i = 0; i < 9; i ++){
                    cell = row.getCell(i);
                    if(cell !=null){
                        switch (cell.getCellType()){
                            case Cell.CELL_TYPE_STRING:
                                record.add(cell.getStringCellValue());
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                record.add(String.valueOf((long)cell.getNumericCellValue()));
                                break;
                            default:
                                record.add("");
                        }
                    } else {
                        record.add("");
                    }
                }
                if(record.get(0).equals("")) continue;
                log.debug(String.format("Try parsing holder with ID [%s]", record.get(0)));
                HolderDto holderDto = new HolderDto();
                holderDto.setExternalId(record.get(0));
                holderDto.setLastName(record.get(1));
                holderDto.setFirstName(record.get(2));
                holderDto.setSecondName(record.get(3));
                holderDto.setBirthdate(record.get(4));
                holderDto.setDocSer(record.get(5));
                holderDto.setDocNum(record.get(6));
                holderDto.setRegion(record.get(7));
                holderDto.setStatus(record.get(8));
                holderDto.setDataSource(fileName);
                holderDto.setPhoto(unzippedFile.get(record.get(0)));
                holderDtoList.add(holderDto);
                log.debug(String.format("HolderDtoList size is [%s]", holderDtoList.size()));
            }
        } catch (IOException e){
            log.debug(String.format("Error while parsing XLSX report [%s]", fileName));
            throw new MlApplicationException("Ошибка чтения файла", e);
        }

        log.debug(String.format("Parsing XLSX report [%s] complete", fileName));
        return holderDtoList;
    }

    private static final SimpleDateFormat errorReportFormat = new SimpleDateFormat("ddMMYYYYHHmmss");

    public String writeCSVWithErrorHolders(List<String[]> errorList) throws IOException {
        String reportName = "holder-errors_" + errorReportFormat.format(new Date()) + ".csv";
        log.debug(String.format("Start writing error report [%s]", reportName));
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, ';', CSVWriter.NO_QUOTE_CHARACTER);
        csvWriter.writeAll(errorList);
        csvWriter.close();

        byte[] output = writer.toString().getBytes("UTF-8");
        importPersoServiceImpl.createOutputReport(output, reportName);
        log.debug(String.format("Error report [%s] has been stored to db", reportName));
        return reportName;
    }

    public boolean isReportNameCorrect(String reportName, String pattern){
        return Pattern.matches(pattern, reportName);
    }

}
