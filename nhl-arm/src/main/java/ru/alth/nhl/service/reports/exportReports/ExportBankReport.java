package ru.alth.nhl.service.reports.exportReports;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.BankBranchDao;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.PersonalizeServiceImpl;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.holders.EnumHolder;
import ru.ml.core.holders.MetaDataHolder;
import ru.ml.core.model.system.MlAttr;
import ru.ml.core.model.system.MlEnum;

import java.util.List;

/**
 * Отчет для персонализации в Банке банковских карт
 */
public class ExportBankReport implements ExportReport {

    private static final Logger log = LoggerFactory.getLogger(ExportBankReport.class);
    private List<Card> cardList;

    @Inject
    private PersonalizeServiceImpl personalizeService;
    @Inject
    private CardDao cardDao;

    @Override
    public void createReportAndChangeStatus() throws Exception {
        log.debug(String.format("Exporting [%s] cards for bank personalize.", cardList.size()));
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        EnumHolder enumholder = GuiceConfigSingleton.inject(EnumHolder.class);
        BankBranchDao bankBranchDao = GuiceConfigSingleton.inject(BankBranchDao.class);
        MlAttr mlAttr = metaDataHolder.getAttr("BankBranch", "filial");
        for(MlEnum mlEnum: enumholder.getEnumList(mlAttr)){

            List<BankBranch> bankBranchList = bankBranchDao.findBankBranchByFilial(mlEnum.getCode());
            if(!bankBranchList.isEmpty()){
                List<Card> cards = cardDao.findCardsByBankBranchAndStatus(bankBranchList, Card.CardStatus.READY_TO_BANK_PERSONALIZE);
                log.debug(String.format("For filial with code [%s] were found [%s] cards", mlEnum.getCode(), cards.size()));
                if(!cards.isEmpty()){
                    personalizeService.importReportAndChangeStatus(cards, false);
                }
            }
        }
    }

    @Override
    public void setCardsForReport(List<Card> cardList) {
        this.cardList = cardList;
    }
}
