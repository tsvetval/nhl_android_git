package ru.alth.nhl.service;

import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.dto.HolderDto;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.ml.core.common.exceptions.MlApplicationException;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Sergei on 18.03.2016.
 */
public class ValidateHolders {

    private static final Long MAX_PHOTO_SIZE = 7620L;

    @Inject
    CardCreateService cardCreateService;
    @Inject
    CardDao cardDao;
    @Inject
    HolderDao holderDao;

    private static final Logger log = LoggerFactory.getLogger(ValidateHolders.class);

    public void validateHolder(List<Holder> holderList, String info, Long persoTypeId, String bankBranchId){
        List<Card> cardsList;
        List<Holder> holderFieldErrors = new ArrayList<>();
        List<Holder> holderCardErrors = new ArrayList<>();

        for(Holder holder: holderList){
            cardsList = cardDao.findCardsByHolder(holder);
            for (Card card: cardsList){
                if(!isCardNewBlockedActive(card) && !holderCardErrors.contains(holder)){
                    holderCardErrors.add(holder);
                }
            }
            if(holderFieldsNotFilled(holder, bankBranchId)) {
                holderFieldErrors.add(holder);
            }
        }

        //Если все обязательные поля у холдеров заполнены
        if (holderFieldErrors.isEmpty()){
            //И если у них нет заявок на выпуск карт
            if (holderCardErrors.isEmpty()){
                for(Holder holder: holderList){
                    cardCreateService.storeNewCard(holder, info, persoTypeId, bankBranchId);
                }
                log.debug("Cards were created.");
            } else {
                log.debug("Unable to create cards: cards have wrong statuses");
                throw new MlApplicationException("У следующих держателей уже имеются заявки на выпуск карт: " + createErrorHoldersList(holderCardErrors) + ".");
            }
        } else {
            log.debug("Unable to create cards: holders have empty fields");
            throw new MlApplicationException("Из-за отсутствия необходимых полей для следующих держателей" +
                    " не может быть персонализирована карта указанного типа: " + createErrorHoldersList(holderFieldErrors) + ".");
    }
}

    /**
     * Проверка на отсутствие заявок на выпуск карт
     * */
    private boolean isCardNewBlockedActive(Card card){
        return  card.getStatus().equals(Card.CardStatus.NEW) || card.getStatus().equals(Card.CardStatus.ACTIVE)
                || card.getStatus().equals(Card.CardStatus.BLOCKED);
    }

    /**
     * Проверка на заполненность обязательных полей у холдеров
     */
    private boolean holderFieldsNotFilled(Holder holder, String bankBranchId) {
        if (holder.getFirstName() == null || holder.getLastName() == null ||
                (holder.getSecondName() == null && holder.getResidency().equals("PR")) || holder.getBirthdate() == null ||
                holder.getPhoto() == null || holder.getIdNhlNumber() == null ||
                holder.getDocNum() == null || holder.getResidency() == null) {
            return true;
        }
        return !bankBranchId.isEmpty() && (holder.getEmbossedName() == null || holder.getEmbossedLastName() == null);
    }


    private String createErrorHoldersList(List<Holder> holderList){
        return holderList.stream().map(holder -> holder.getLastName() + " " + holder.getFirstName() + " " + holder.getSecondName())
                .collect(Collectors.toList()).toString().replace("[", "").replace("]", "");
    }

    private static final SimpleDateFormat birthdayDateFormat = new SimpleDateFormat("dd.MM.yyyy");


    public String[] isCorrectHolderData(HolderDto holderDto) throws ParseException {
        if (holderDto.getPhoto() == null) {
            return new String[]{holderDto.getExternalId(), "Для данного держателя отсутствует фотография"};
        }
        if (isHolderDtoHasEmptyFields(holderDto)) {
            return new String[]{holderDto.getExternalId(), "У данной записи есть незаполненные поля"};
        }
        if (holderDto.getPhoto().length > MAX_PHOTO_SIZE) {
            return new String[]{holderDto.getExternalId(), "Размер фотографии для данного держателя превышает " + 7.4 + " Kb"};
        }
        try{
            if(DatatypeConverter.printHexBinary(holderDto.getFirstName().getBytes("UTF-8")).length()/2 > 50){
                return new String[]{holderDto.getExternalId(), "Значение в поле 'Имя' превышает максимально допустимый размер"};
            }
            if(DatatypeConverter.printHexBinary(holderDto.getLastName().getBytes("UTF-8")).length()/2 > 50){
                return new String[]{holderDto.getExternalId(), "Значение в поле 'Фамилия' превышает максимально допустимый размер"};
            }
            if(DatatypeConverter.printHexBinary(holderDto.getSecondName().getBytes("UTF-8")).length()/2 > 50){
                return new String[]{holderDto.getExternalId(), "Значение в поле 'Отчество' превышает максимально допустимый размер"};
            }
            if(DatatypeConverter.printHexBinary((holderDto.getDocSer() + holderDto.getDocSer()).getBytes("UTF-8")).length()/2 > 12){
                return new String[]{holderDto.getExternalId(), "Серия и номер документа превышают максимально допустимый размер"};
            }
        } catch (UnsupportedEncodingException e){
            throw new MlApplicationException("Ошибка конвертации из UTF-8");
        }

        if (holderDao.findHolderByIdExternalId(Long.valueOf(holderDto.getExternalId())) != null) {
            return new String[]{holderDto.getExternalId(), "Держатель с данным идентификатором уже существует"};
        }
        if(holderDto.getStatus().equals("1")){
            holderDto.setResidency("PR");
            if (holderDto.getDocSer().length() != 4 || !StringUtils.isNumeric(holderDto.getDocSer())
                    || holderDto.getDocNum().length() != 6 || !StringUtils.isNumeric(holderDto.getDocNum())) {
                return new String[]{holderDto.getExternalId(), "Паспортные данные указаны некорректно"};
            }
            if(holderDto.getSecondName() == null || holderDto.getSecondName().equals("")){
                return new String[]{holderDto.getExternalId(), "Не заполнено отчество"};
            }
        }
        if(holderDto.getStatus().equals("0")){
            holderDto.setResidency("PNR");
            String pattern = "^[а-яА-ЯёЁa-zA-Z0-9\\s]+$";
            if(!(holderDto.getDocSer().length() != 0 || holderDto.getDocNum().length() != 0)
                    || !Pattern.matches(pattern, holderDto.getDocSer() + holderDto.getDocNum())){
                return new String[]{holderDto.getExternalId(), "Паспортные данные указаны некорректно"};
            }
        }

        if (holderDao.findHolderByDocumNumber(holderDto.getDocSer() + " " + holderDto.getDocNum()) != null) {
            return new String[]{holderDto.getExternalId(), "Держатель с данными паспортными данными уже существует"};
        }

        if (birthdayDateFormat.parse(holderDto.getBirthdate()).after(birthdayDateFormat.parse(birthdayDateFormat.format(new Date())))) {
            return new String[]{holderDto.getExternalId(), "Держатель ещё не родился"};
        }
        log.debug("Checking data complete");
        return null;
    }

    private boolean isHolderDtoHasEmptyFields(HolderDto holderDto){
        return holderDto.getBirthdate().equals("") || holderDto.getBirthdate() == null || holderDto.getDataSource().equals("") || holderDto.getDataSource() == null
                || holderDto.getDocNum().equals("") || holderDto.getDocNum() == null || holderDto.getExternalId().equals("") || holderDto.getExternalId() == null || holderDto.getFirstName().equals("")
                || holderDto.getFirstName() == null || holderDto.getLastName().equals("") || holderDto.getLastName() == null || holderDto.getPhoto() == null;
    }


    public Holder checkFieldsLength(Holder holder){
        try {
            if (holder.getFirstName() != null && !holder.getFirstName().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getFirstName().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Имя\" превышает максимально допустимый размер");
            }
            if (holder.getLastName() != null && !holder.getLastName() .equals("") &&
                    DatatypeConverter.printHexBinary(holder.getLastName() .getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Фамилия\" превышает максимально допустимый размер");
            }
            if (holder.getSecondName() != null && !holder.getSecondName().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getSecondName().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Отчество\" превышает максимально допустимый размер");
            }
            if (holder.getTeamName() != null && !holder.getTeamName().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getTeamName().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Название команды\" превышает максимально допустимый размер");
            }
            if (holder.getPhone() != null && !holder.getPhone().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getPhone().getBytes("UTF-8")).length() / 2 > 20) {
                throw new MlApplicationException("Значение в поле \"Телефон\" превышает максимально допустимый размер");
            }
            if (holder.getEmail() != null && !holder.getEmail().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getEmail().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Адрес электронной почты\" превышает максимально допустимый размер");
            }
            if (holder.getMailingAddress() != null && !holder.getMailingAddress().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getMailingAddress().getBytes("UTF-8")).length() / 2 > 270) {
                throw new MlApplicationException("Значение в поле \"Фактический адрес проживния\" превышает максимально допустимый размер");
            }
            if (holder.getDocNum() != null && !holder.getDocNum().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getDocNum().getBytes("UTF-8")).length() / 2 > 26) {
                throw new MlApplicationException("Значение в поле \"Серия и номер паспорта\" превышают максимально допустимый размер");
            }
            if (holder.getDrivernum() != null && !holder.getDrivernum().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getDrivernum().getBytes("UTF-8")).length()/ 2 > 16) {
                throw new MlApplicationException("Значение в поле \"Серия и номер водительского удостоверения\" превышают максимально допустимый размер");
            }
            if (holder.getMatchPlace() != null && !holder.getMatchPlace().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getMatchPlace().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Место проведения матча\" превышает максимально допустимый размер");
            }
            if (holder.getTeam() != null && !holder.getTeam().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getTeam().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Команда\" превышает максимально допустимый размер");
            }
            if (holder.getPlayerNumber() != null &&
                    DatatypeConverter.printHexBinary(holder.getPlayerNumber().toString().getBytes("UTF-8")).length() / 2 > 2) {
                throw new MlApplicationException("Значение в поле \"Номер игрока\" превышает максимально допустимый размер");
            }
            if (holder.getInsurCompname() != null && !holder.getInsurCompname().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getInsurCompname().getBytes("UTF-8")).length() / 2 > 50) {
                throw new MlApplicationException("Значение в поле \"Название страховой компании\" превышает максимально допустимый размер");
            }
            if (holder.getInsurNum() != null && !holder.getInsurNum().equals("") &&
                    DatatypeConverter.printHexBinary(holder.getInsurNum().getBytes("UTF-8")).length() / 2 > 30) {
                throw new MlApplicationException("Значение в поле \"Номер страхового полиса\" превышает максимально допустимый размер");
            }
            if (holder.getHeight() != null &&
                    DatatypeConverter.printHexBinary(holder.getHeight().toString().getBytes("UTF-8")).length() / 2 > 3) {
                throw new MlApplicationException("Значение в поле \"Рост\" превышает максимально допустимый размер");
            }
            if (holder.getWeight() != null &&
                    DatatypeConverter.printHexBinary(holder.getWeight().toString().getBytes("UTF-8")).length() / 2 > 3) {
                throw new MlApplicationException("Значение в поле \"Вес\" превышает максимально допустимый размер");
            }
        } catch (UnsupportedEncodingException e){
            log.error(e.getMessage());
        }
        return holder;
    }

}
