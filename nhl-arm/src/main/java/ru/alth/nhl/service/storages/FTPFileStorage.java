package ru.alth.nhl.service.storages;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.controller.exchange.ExportPersoController;
import ru.ml.core.common.exceptions.MlApplicationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FTPFileStorage implements FileStorage {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ExportPersoController.class);


    private String ftpUrl;
    private String ftpUser;
    private String ftpPassword;
    private String ftpFolder;

    private FTPClient ftpClient;

    @Override
    public List<String> listFiles() throws Exception {
        log.debug(String.format("Start read folder %s", getFtpFolder()));
        try {
            List<String> files = new ArrayList<>();
            FTPFile[] subFiles = getFtpClient().listFiles(getFtpFolder());
            log.debug("List files readed");
            for (FTPFile ftpFile : subFiles) {
                files.add(ftpFile.getName());
            }
            return files;
        } catch (Exception e) {
            log.error("Error while reading list files", e);
            throw e;
        }
    }


    @Override
    public void writeFile(String fileName, byte[] content) throws Exception {
        log.debug(String.format("Start write file %s to folder %s", fileName, getFtpFolder()));
        try {
            InputStream stream = new ByteArrayInputStream(content);
            if (!getFtpClient().storeFile(getFtpFolder() + fileName, stream)) {
                throw new IllegalStateException("Ошибка передачи файла на ftp" +
                        getFtpUrl() +
                        ", ответ сервера: " +
                        getFtpClient().getReplyCode() +
                        ". Возможно отсутствуют права на запись.");
            }
        } catch (Exception e) {
            log.error("Error while writing", e);
            throw e;
        }
    }

    @Override
    public byte[] readFile(String fileName) throws Throwable {
        log.debug(String.format("Start read file %s from folder %s", fileName, getFtpFolder()));
        try {
            InputStream stream = getFtpClient().retrieveFileStream(getFtpFolder() + fileName);
            byte[] content = IOUtils.toByteArray(stream);
            return content;
        } catch (Exception e) {
            log.error("Error while reading", e);
            throw e;
        }
    }

    @Override
    public void deleteFile(String filename) throws Exception {
        log.debug(String.format("Start delete file %s from folder %s", filename, getFtpFolder()));
        try {
            if (!getFtpClient().deleteFile(getFtpFolder() + filename)) {
                log.debug(String.format("Unable to delete file [%s]", filename));
            }
        } catch (Exception e) {
            log.error("Error while deleting", e);
            throw e;
        }
    }

    @Override
    public void renameFile(String oldName, String newName) throws Exception {
        log.debug(String.format("Start rename file %s to %s in folder %s", oldName, newName, getFtpFolder()));
        try {
            if (!getFtpClient().rename(getFtpFolder() + oldName, getFtpFolder() + newName)) {
                throw new MlApplicationException(String.format("Unable to rename file [%s]", oldName));
            }
        } catch (Exception e) {
            log.error("Error while renaming", e);
            throw e;
        }
    }

    @Override
    public void setSettings(String ftpUrl, String ftpFolder, String ftpUser, String ftpPassword) {
        this.ftpUrl = ftpUrl;
        this.ftpUser = ftpUser;
        this.ftpPassword = ftpPassword;
        this.ftpFolder = ftpFolder;
        log.debug(String.format("FTP settings is url=%s, folder=%s, user=%s, passord=%s", ftpUrl, ftpFolder, ftpUser, ftpPassword));
    }

    @Override
    public void changeFolder(String folder) {
        ftpFolder = folder;
        log.debug(String.format("Folder chenged to %s", folder));
    }

    @Override
    public void disconnect() {
        log.debug("Start disconnect");
        try {
            ftpClient.disconnect();
            log.debug("Done disconnect");
        } catch (IOException e) {
            log.debug("Error on disconnect", e);
        }
    }

    public FTPClient getFtpClient() throws IOException {
        if (ftpClient == null || !ftpClient.isConnected()) {
            try {
                log.debug("Start connecting");
                ftpClient = new FTPClient();
                ftpClient.setConnectTimeout(10000);
                ftpClient.setDefaultTimeout(10000);
                ftpClient.connect(getFtpUrl());
                log.debug("Connection with URL completed");
                ftpClient.login(getFtpUser(), getFtpPassword());

                log.debug("Set LocalPassiveMode");
                ftpClient.enterLocalPassiveMode();

                log.debug("Connection with login/pass completed");
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            } catch (IOException e) {
                log.error("Error while connection ", e);
                throw e;
            }
        }
        return ftpClient;
    }

    public String getFtpUrl() {
        return ftpUrl;
    }

    public void setFtpUrl(String ftpUrl) {
        this.ftpUrl = ftpUrl;
    }

    public String getFtpUser() {
        return ftpUser;
    }

    public void setFtpUser(String ftpUser) {
        this.ftpUser = ftpUser;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public String getFtpFolder() {
        return ftpFolder;
    }

    public void setFtpFolder(String ftpFolder) {
        this.ftpFolder = ftpFolder;
    }

    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }
}
