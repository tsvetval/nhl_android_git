package ru.alth.nhl.service;


import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.dao.CommonDao;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Вспомогательный класс для подсчёта хэшей карт
 */
public class HashService {

    private static final Logger log = LoggerFactory.getLogger(HashService.class);
    @Inject
    private CommonDao commonDao;
    @Inject
    private CardDao cardDao;

    /**
     * Update card hashes without Transaction
     * @param holder
     */
    public void updateHashesForHolderCards(Holder holder) {
        try {
            List<Card.CardStatus> statusList = new ArrayList<>();
            statusList.add(Card.CardStatus.NOT_USED);
            List<Card> cards = cardDao.getCardsByHolderWithoutTheseStatuses(holder, statusList);
            for (Card card : cards) {
                card.setHashFile1(calcHashFile1(card));
                card.setHashFile2(calcHashFile2(card));
                card.setHashFile3(calcHashFile3(card));
                card.setHashFile4(calcHashFile4(card));
                commonDao.mergeWithoutSecurityCheck(card, false, false);
            }
        } catch (Exception e) {
            log.error("Calc Hashes Error", e);
            throw new MlApplicationException("Ошибка расчета хешей");
        }
    }

    public void updateCardHashes(Card card) {
        try {
            card.setHashFile1(calcHashFile1(card));
            card.setHashFile2(calcHashFile2(card));
            card.setHashFile3(calcHashFile3(card));
            card.setHashFile4(calcHashFile4(card));
        } catch (Exception e) {
            log.error("Calc Hashes Error", e);
            throw new MlApplicationException("Ошибка расчета хешей");
        }
    }

    private String calcHashFile1(Card card) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Holder holder = card.getHolder();
        String stringForHash = "";
        stringForHash += holder.get(Holder.Fields.firstname.name());
        stringForHash += holder.get(Holder.Fields.secondname.name());
        stringForHash += holder.get(Holder.Fields.lastname.name());
        stringForHash += holder.get(Holder.Fields.birthdate.name());

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(stringForHash.getBytes("UTF-8"));
        byte[] digest = md.digest();

        return DatatypeConverter.printHexBinary(digest);
    }


    private String calcHashFile2(Card card) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Holder holder = card.getHolder();
        String stringForHash = "";
        stringForHash += holder.get(Holder.Fields.idnhlnumber.name());
        stringForHash += holder.get(Holder.Fields.teamname.name());
        stringForHash += holder.get(Holder.Fields.vidchlenstva.name());
        stringForHash += holder.get(Holder.Fields.statusnhl.name());
        stringForHash += holder.get(Holder.Fields.phone.name());
        stringForHash += holder.get(Holder.Fields.email.name());
        stringForHash += holder.get(Holder.Fields.mailingAddress.name());

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(stringForHash.getBytes("UTF-8"));
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);//String.format("%064x", new java.math.BigInteger(1, digest));
    }

    private String calcHashFile3(Card card) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Holder holder = card.getHolder();
        String stringForHash = "";
        stringForHash += card.getCardStatusNhl();
        stringForHash += holder.get(Holder.Fields.dopusk.name());
        stringForHash += holder.get(Holder.Fields.documnum.name());
        stringForHash += holder.get(Holder.Fields.documdate.name());
        stringForHash += holder.get(Holder.Fields.drivernum.name());
        stringForHash += holder.get(Holder.Fields.driverdate.name());
        stringForHash += holder.get(Holder.Fields.matchDate.name());
        stringForHash += holder.get(Holder.Fields.matchPlace.name());
        stringForHash += holder.get(Holder.Fields.team.name());
        stringForHash += holder.get(Holder.Fields.playerNumber.name());

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(stringForHash.getBytes("UTF-8"));
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);
    }


    private String calcHashFile4(Card card) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Holder holder = card.getHolder();
        String stringForHash = "";
        stringForHash += holder.get(Holder.Fields.gruppa.name());
        stringForHash += holder.get(Holder.Fields.rezus.name());
        stringForHash += holder.get(Holder.Fields.insurcompname.name());
        stringForHash += holder.get(Holder.Fields.insurnum.name());
        stringForHash += holder.get(Holder.Fields.medocmotrdate.name());
        stringForHash += holder.get(Holder.Fields.rezultmedocmotr.name());
        stringForHash += holder.get(Holder.Fields.height.name());
        stringForHash += holder.get(Holder.Fields.weight.name());
        stringForHash += holder.get(Holder.Fields.protivopakaz.name());
        stringForHash += holder.get(Holder.Fields.allegry.name());
        stringForHash += holder.get(Holder.Fields.chronical.name());

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(stringForHash.getBytes("UTF-8"));
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);
    }

}
