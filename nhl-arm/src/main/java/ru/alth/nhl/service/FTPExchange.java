package ru.alth.nhl.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.storages.FileStorage;
import ru.alth.nhl.service.storages.FileStorageStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Операции с FTP
 */
public class FTPExchange {

    private static final Logger log = LoggerFactory.getLogger(FTPExchange.class);
    private Pattern pattern;
    private static final String BAD_FILE_SUFFIX = "_err";

    public FTPExchange() {

    }

    /**
     * Получить все файлы из FTP
     */
    public List<String> listFiles(PersoType persoType, boolean isNHLPersonalization) throws Exception {
        List<String> files = new ArrayList<>();
        setPattern(".*_err");
        FileStorage fileStorage = FileStorageStrategy.getImportStorage(persoType, isNHLPersonalization);
        try {
            List<String> storageFiles = fileStorage.listFiles();
            for (String ftpFile : storageFiles) {
                // Загружаем только отчеты без "загрузочного" суффикса
                if (!fileNameIsMatchPattern(ftpFile)) {
                    log.debug("pattern is match for name "+ftpFile);
                    files.add(ftpFile);
                    log.debug("file added");
                }
            }
        } finally {
            fileStorage.disconnect();
        }
        return files;
    }


    /**
     * Запись файла в хранилище FTP
     */
    public void writeFile(String fileName, byte[] content, PersoType persoType, boolean isNHLPersonalization) throws Exception {
        FileStorage fileStorage = FileStorageStrategy.getExportStorage(persoType, isNHLPersonalization);
        try {
            fileName = fileName + BAD_FILE_SUFFIX;
            fileStorage.writeFile(fileName, content);
            renameFile(fileName, persoType, isNHLPersonalization);
        } finally {
            fileStorage.disconnect();
        }
    }

    /**
     * Чтение файла из хранилища FTP
     */
    public byte[] readFile(String fileName, PersoType persoType, boolean isNHLPersonalization) throws Throwable {
        byte[] content = null;
        FileStorage fileStorage = FileStorageStrategy.getImportStorage(persoType, isNHLPersonalization);
        try {
            content = fileStorage.readFile(fileName);
        } finally {
            fileStorage.disconnect();
        }
        return content;
    }

    public void deleteFile(String filename, PersoType persoType, boolean isNHLPersonalization) throws Exception {
        FileStorage fileStorage = FileStorageStrategy.getImportStorage(persoType, isNHLPersonalization);
        try {
            fileStorage.deleteFile(filename);
        } finally {
            fileStorage.disconnect();
        }
    }

    public void moveFileToArchive(String fileName, PersoType persoType, byte[] content, boolean isNHLPersonalization) throws Exception {
        FileStorage fileStorage = FileStorageStrategy.getImportStorage(persoType, isNHLPersonalization);
        try {
            if(isNHLPersonalization){
                fileStorage.changeFolder(persoType.getArchiveNHL());
            } else {
                fileStorage.changeFolder(persoType.getArchive());
            }
            fileStorage.writeFile(fileName, content);
        }catch (Exception e){
            log.error("Can't send file to archive "+persoType.getArchive());
        }finally {
            fileStorage.disconnect();
            deleteFile(fileName, persoType, isNHLPersonalization);
        }
    }

    public String renameFile(String fileName, PersoType persoType, boolean isNHLPersonalization) throws Exception {
        String renamedFile = fileName.substring(0, fileName.lastIndexOf("_"));
        FileStorage fileStorage = FileStorageStrategy.getExportStorage(persoType, isNHLPersonalization);
        try {
            fileStorage.renameFile(fileName, renamedFile);
        } finally {
            fileStorage.disconnect();
        }
        return renamedFile;
    }

    public boolean fileNameIsMatchPattern(String fileName) {
        return getPattern().matcher(fileName).find();
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

}
