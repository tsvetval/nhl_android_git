package ru.alth.nhl.service.storages;

import com.jcraft.jsch.*;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import sun.misc.IOUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * Created by admin on 07.04.2016.
 */
public class SFTPFileStorage implements FileStorage {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SFTPFileStorage.class);

    private static final int SFTP_PORT = 22;

    private String sftpUrl = "10.129.60.192";
    private String sftpUser = "user";
    private String sftpPassword = "123456_usr_P@ssw0rd_MLCMS";
    private String sftpFolder = "/home/user";
    ChannelSftp channelSftp;
    Session session;


    private static Properties config = new java.util.Properties();

    static {
        config.put("StrictHostKeyChecking", "no");
    }

    @Override
    public void setSettings(String sftpUrl, String sftpFolder, String sftpUser, String sftpPassword) {
        this.sftpUrl = sftpUrl;
        this.sftpUser = sftpUser;
        this.sftpPassword = sftpPassword;
        this.sftpFolder = sftpFolder;
        log.debug(String.format("SFTP settings is url=%s, folder=%s, user=%s, passord=%s", sftpUrl, sftpFolder, sftpUser, sftpPassword));
    }

    @Override
    public List<String> listFiles() throws Exception {
        log.debug(String.format("Start read folder %s", getSftpFolder()));
        try {
            List<String> files = new ArrayList<>();
            ChannelSftp channelSftp = getChanel();
            Vector filelist = channelSftp.ls(getSftpFolder());
            log.debug("List files readed");
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry lsFile = (ChannelSftp.LsEntry) filelist.get(i);
                if (lsFile.getFilename().equals(".")
                        || lsFile.getFilename().equals("..")
                        || lsFile.getAttrs().isDir()) {
                    continue;
                }
                files.add(lsFile.getFilename());
            }
            return files;
        } catch (Exception e) {
            log.error("Error while reading list files", e);
            throw e;
        }
    }

    @Override
    public void writeFile(String fileName, byte[] content) throws Exception {
        log.debug(String.format("Start write file %s to folder %s", fileName, getSftpFolder()));
        try {
            ChannelSftp channelSftp = getChanel();
            OutputStream outputStream = channelSftp.put(fileName, ChannelSftp.OVERWRITE);
            outputStream.write(content);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            log.error("Error while writing", e);
            throw e;
        }
    }

    @Override
    public byte[] readFile(String fileName) throws Throwable {
        log.debug(String.format("Start read file %s from folder %s", fileName, getSftpFolder()));
        try {
            ChannelSftp channelSftp = getChanel();
            InputStream inputStream = channelSftp.get(fileName);
            byte[] result = IOUtils.readFully(inputStream, -1, true);
            return result;
        } catch (Exception e) {
            log.error("Error while reading", e);
            throw e;
        }
    }

    @Override
    public void deleteFile(String filename) throws Exception {
        log.debug(String.format("Start delete file %s from folder %s", filename, getSftpFolder()));
        try {
            ChannelSftp channelSftp = getChanel();
            channelSftp.rm(filename);
        } catch (Exception e) {
            log.error("Error while deleting", e);
            throw e;
        }
    }

    @Override
    public void renameFile(String oldName, String newName) throws Exception {
        log.debug(String.format("Start rename file %s to %s in folder %s", oldName, newName, getSftpFolder()));
        try {
            ChannelSftp channelSftp = getChanel();
            channelSftp.rename(oldName, newName);
        } catch (Exception e) {
            log.error("Error while renaming", e);
            throw e;
        }
    }

    @Override
    public void changeFolder(String folder) {
        sftpFolder = folder;
        if (channelSftp != null && channelSftp.isConnected()) {
            try {
                channelSftp.cd(getSftpFolder());
            } catch (SftpException e) {
                throw new MlApplicationException("Can't change folder", e);
            }
        }
        log.debug(String.format("Folder chenged to %s", folder));
    }

    @Override
    public void disconnect() {
        log.debug("Start disconnect");
        try {
            getChanel().disconnect();
            log.debug("Done disconnect");
        } catch (Exception e) {
            log.debug("Error on disconnect", e);
        }finally {
            try {
                getSession().disconnect();
            } catch (JSchException e) {
                log.error("Error on close session ",e);
            }
        }
    }

    public ChannelSftp getChanel() throws JSchException, SftpException {
        if (channelSftp == null || !channelSftp.isConnected()) {
            try {
                log.debug("Start connecting");
                Session session = getSession();
                session.connect();
                log.debug("Connection completed. Try get chanel");
                Channel channel = session.openChannel("sftp");
                channel.connect();
                log.debug("Chanel receved");
                channelSftp = (ChannelSftp) channel;
                log.debug(String.format("Selecting folder %s", getSftpFolder()));
                channelSftp.cd(getSftpFolder());
            } catch (Exception e) {
                log.error("Error while connection ", e);
                throw e;
            }
        }
        return channelSftp;
    }

    private Session getSession() throws JSchException {
        if(session == null || !session.isConnected()) {
            JSch jsch = new JSch();
            session = jsch.getSession(getSftpUser(), getSftpUrl(), SFTP_PORT);
            session.setPassword(getSftpPassword());
            session.setConfig(config);
        }
        return session;
    }

    public String getSftpUrl() {
        return sftpUrl;
    }

    public void setSftpUrl(String sftpUrl) {
        this.sftpUrl = sftpUrl;
    }

    public String getSftpUser() {
        return sftpUser;
    }

    public void setSftpUser(String sftpUser) {
        this.sftpUser = sftpUser;
    }

    public String getSftpPassword() {
        return sftpPassword;
    }

    public void setSftpPassword(String sftpPassword) {
        this.sftpPassword = sftpPassword;
    }

    public String getSftpFolder() {
        return sftpFolder;
    }

    public void setSftpFolder(String sftpFolder) {
        this.sftpFolder = sftpFolder;
    }
}
