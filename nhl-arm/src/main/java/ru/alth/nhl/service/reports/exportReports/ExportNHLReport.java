package ru.alth.nhl.service.reports.exportReports;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.PersonalizeServiceImpl;

import java.util.List;

/**
 * Отчет для персонализации в НХЛ небанковских карт
 */
public class ExportNHLReport implements ExportReport {

    private static final Logger log = LoggerFactory.getLogger(ExportNHLReport.class);
    private List<Card> cardList;

    @Inject
    private PersonalizeServiceImpl personalizeService;

    @Override
    public void createReportAndChangeStatus() throws Exception {
        log.debug(String.format("Exporting [%s] cards for NHL without PAN.", cardList.size()));
        personalizeService.importReportAndChangeStatus(cardList, false);
    }

    public void setCardsForReport(List<Card> cardList) {
        this.cardList = cardList;
    }
}
