package ru.alth.nhl.service.reports.importReports;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.FTPExchange;
import ru.alth.nhl.service.ImportPersoServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Реализация импорта отчета банковских карт
 */
public class ImportBankCardReport implements ImportReport {

    private static final Logger log = LoggerFactory.getLogger(ImportNotBankCardReport.class);
    private PersoType type;

    @Inject
    private ImportPersoServiceImpl importPersoService;
    @Inject
    private FTPExchange ftpExchange;

    @Override
    public void importReport() {
        try {
            List<String> fileList = ftpExchange.listFiles(type, true);
            Map<String, List<Card>> cardMap = new HashMap<>();
            List<Card> cardList = new ArrayList<>();
            List<Card> cardErrorList = new ArrayList<>();
            log.debug(String.format("Filelist size is [%s]", fileList.size()));
            for (String filename : fileList) {
                if (filename.substring(0, 17).equals("ExportReport_Bank")) {
                    byte[] persoFileData = ftpExchange.readFile(filename, type, true);
                    log.debug(String.format("File [%s] size = [%s] bytes", filename, persoFileData.length));
                    log.debug("ExportReport_Bank found");
                    try {
                        Map<String, List<Card>> temp = importPersoService.importBankPersReportAndMoveToArchive(persoFileData, filename, type, true);
                        if (!temp.isEmpty()) {
                            if (temp.get("cardList") != null) {
                                if (temp.get("cardList").get(0).getPANHashNumber() != null) {
                                    cardList.addAll(temp.get("cardList"));
                                    cardMap.put("cardList", cardList);
                                }
                            }
                            if (temp.get("cardErrorList") != null) {
                                if (temp.get("cardErrorList").get(0).getPANHashNumber() != null) {
                                    cardErrorList.addAll(temp.get("cardErrorList"));
                                    cardMap.put("cardErrorList", cardErrorList);
                                }
                            }
                        }
                    } catch (Exception e) {
                        // Skip this error
                        log.error(String.format("Error while importing file [%s]", filename), e);
                        importPersoService.createErrorReport(persoFileData, filename);
                        ftpExchange.moveFileToArchive(filename, type, persoFileData, true);
                    }
                }
            }

            // Создание отчета с PAN персонализированных карт для банка
            if (cardMap.get("cardList") != null) {
                log.debug(String.format("Building personalize report with [%s] cards", cardMap.get("cardList").size()));
                String nameBankReport = importPersoService.generateNameForBankReport();
                importPersoService.makeBankReport(cardMap.get("cardList"), type, nameBankReport, false);
            }
            if (cardMap.get("cardErrorList") != null) {
                log.debug(String.format("Building personalize error report with [%s] cards", cardMap.get("cardErrorList").size()));
                String nameBankReport = importPersoService.generateErrorNameForBankReport();
                importPersoService.makeBankReport(cardMap.get("cardErrorList"), type, nameBankReport, false);
            }

            fileList = ftpExchange.listFiles(type, false);
            log.debug(String.format("Filelist size is [%s]", fileList.size()));
            for (String filename : fileList) {
                byte[] persoFileData = ftpExchange.readFile(filename, type, false);
                log.debug(String.format("File [%s] size = [%s] bytes", filename, persoFileData.length));

                if (filename.substring(0, 3).equals("NHL")) {
                    try {
                        importPersoService.importBankReportAndMoveFileToArchive(persoFileData, filename, type, false);
                    } catch (Exception e) {
                        // Skip this error
                        log.error(String.format("Error while importing file [%s]", filename), e);
                    }
                }

                if (filename.substring(0, 6).equals("active")) {
                    try {
                        importPersoService.importBankReportAndMoveFileToArchive(persoFileData, filename, type, false);
                    } catch (Exception e) {
                        // Skip this error
                        log.error(String.format("Error while importing file [%s]", filename), e);
                        importPersoService.createErrorReport(persoFileData, filename);
                        ftpExchange.moveFileToArchive(filename, type, persoFileData, false);
                    }
                }
            }
        } catch (Throwable e){
            // Skip this error, and continue
            log.error("Fatal ERROR in FTPImportTask on importing not bank report ", e);
        }
    }

    @Override
    public void setPersoType(PersoType type) {
        this.type = type;
    }
}
