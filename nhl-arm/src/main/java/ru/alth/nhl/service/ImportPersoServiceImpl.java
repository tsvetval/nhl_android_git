package ru.alth.nhl.service;

import au.com.bytecode.opencsv.CSVWriter;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.model.PersoResponse;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.util.CustomCSVWriter;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.holders.MetaDataHolder;
import ru.ml.core.model.MlDynamicEntityImpl;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.nio.charset.Charset;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *
 */
public class ImportPersoServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(ImportPersoServiceImpl.class);
    @Inject
    private CommonDao commonDao;
    @Inject
    private CardDao cardDao;
    @Inject
    private HolderDao holderDao;
    @Inject
    FTPExchange ftpExchange;
    @Inject
    PersonalizeServiceImpl personalizeServiceImpl;
    @Inject
    MetaDataHolder metaDataHolder;

    public List<CardPersoResponse> parsePersoAnswerXML(InputSource input) throws ParserConfigurationException, SAXException, IOException {
        log.debug("Start parsing xml file");
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        XMLReader xmlreader = parser.getXMLReader();
        CardHandler cardHandler = new CardHandler();
        xmlreader.setContentHandler(cardHandler);

        xmlreader.parse(input );
        log.debug("End parsing xml file");
        // проставляем картам статус
        List<CardPersoResponse> cardPersoResponseList = cardHandler.getCardList();
        log.debug(String.format("found [%s] cards in file", cardPersoResponseList.size()));
        return cardPersoResponseList;
    }

    public Map<String, List<Card>> changeCardStatus(List<CardPersoResponse> cardPersoResponseList) throws MlApplicationException{
        List<Card> cardList = new ArrayList<>();
        List<Card> cardErrorList = new ArrayList<>();
        for (CardPersoResponse cardPersoResponse : cardPersoResponseList) {
            Card card;
            // Если банковская карта после персонализации
            if(cardPersoResponse.getCardNumber() == null){
                log.debug("Try changing cardStatus for card with PAN hash: " + cardPersoResponse.getCardPAN());
                card = cardDao.findCardsByPANHashNumber(cardPersoResponse.getCardPAN());
                if (card == null) {
                    log.error(String.format("Card with PAN hash not found [%s]", cardPersoResponse.getCardPAN()));
                    throw new MlApplicationException(String.format("Не найдена карта с PAN [%s]", cardPersoResponse.getCardPAN()));
                }
            } else {
                log.debug("Try changing cardStatus for card with num: " + cardPersoResponse.cardNumber);
                card = cardDao.findCardByNum(cardPersoResponse.cardNumber);
                if (card == null) {
                    log.error(String.format("Card with number not found [%s]", cardPersoResponse.cardNumber));
                    throw new MlApplicationException(String.format("Не найдена карта с номером [%s]", cardPersoResponse.cardNumber));
                }
            }

            if (cardPersoResponse.getCardStatus().equals(PersoCardStatus.PERSONALIZED)) {
                card.setStatus(Card.CardStatus.PERSONALIZED);
                cardList.add(card);
            } else if(cardPersoResponse.getCardNumber() != null && cardPersoResponse.getCardStatus().equals(PersoCardStatus.PERSONALIZATION_ERROR)) {
                card.setStatus(Card.CardStatus.READY_TO_PERSONALIZE);
                cardErrorList.add(card);
                card.setPersoError(cardPersoResponse.getErrorMessage());
            } else {
                card.setStatus(Card.CardStatus.PERSONALIZATION_ERROR);
                cardErrorList.add(card);
                card.setPersoError(cardPersoResponse.getErrorMessage());
            }

            log.debug(String.format("Status changed to [%s]", card.getStatus()));
            commonDao.persistWithoutSecurityCheck(card, false, false);
            log.debug("Card stored to db");
        }


        Map<String, List<Card>> map = new HashMap();
        if(!cardList.isEmpty()){
            map.put("cardList", cardList);
        }
        if(!cardErrorList.isEmpty()){
            map.put("cardErrorList", cardErrorList);
        }
        return map;
    }


    @Transactional(rollbackOn = Exception.class)
    public void importBankReportAndMoveFileToArchive(byte[] persoFileData, String reportName, PersoType type, boolean isNHLPersonalization) throws Exception {
        InputStream myInputStream = new ByteArrayInputStream(persoFileData);

        // Чтение файла с PAN от банка для присвоения статуса Готова к персонализации
        if(type.getCardType().equals(PersoType.CardType.BANK) && reportName.substring(0, 3).equals("NHL")){
            byte[] output = readBankReportForPers(myInputStream, reportName);
            if(output != null){
                String errorReportName = generateErrorNameForNHLReport();
                createOutputReport(output, errorReportName);
                createBankErrorReport(persoFileData, reportName, errorReportName);
            } else {
                createReport(persoFileData, reportName);
            }
            ftpExchange.moveFileToArchive(reportName, type, persoFileData, isNHLPersonalization);
        }

        // Чтение файла с PAN от банка для присвоения статуса Активна
        if(reportName.substring(0, 6).equals("active")){
            readBankReportForActive(myInputStream, reportName);
            createReport(persoFileData, reportName);
            ftpExchange.moveFileToArchive(reportName, type, persoFileData, isNHLPersonalization);
        }
    }

    /** Импорт НЕ банковских отчетов на персонализацию*/

    @Transactional(rollbackOn = Exception.class)
    public void importNotBankReportAndMoveFileToArchive(byte[] persoFileData, String reportName, PersoType type, boolean isNHLPersonalization) throws Exception {
        InputStream myInputStream = new ByteArrayInputStream(persoFileData);
        if(reportName.substring(0, 17).equals("ExportReport_Bank")){
            InputSource is = new InputSource(myInputStream);
            List<CardPersoResponse>  cardPersoResponseList = parsePersoAnswerXML(is);
            changeCardStatus(cardPersoResponseList);
            createReport(persoFileData, reportName);
            ftpExchange.moveFileToArchive(reportName, type, persoFileData, isNHLPersonalization);
        }
    }

    /** Импорт банковских отчетов на персонализацию*/

    @Transactional(rollbackOn = Exception.class)
    public Map<String, List<Card>> importBankPersReportAndMoveToArchive(byte[] persoFileData, String reportName, PersoType type, boolean isNHLPersonalization) throws Exception {
        InputStream myInputStream = new ByteArrayInputStream(persoFileData);
        log.debug(String.format("Parsing personalization report [%s]", reportName));
        InputSource is = new InputSource(myInputStream);
        List<CardPersoResponse>  cardPersoResponseList = parsePersoAnswerXML(is);
        Map<String, List<Card>> cardMap = changeCardStatus(cardPersoResponseList);
        createReport(persoFileData, reportName);
        ftpExchange.moveFileToArchive(reportName, type, persoFileData, isNHLPersonalization);
        log.debug(String.format("End importing [%s]", reportName));
        return cardMap;
    }

    /** Создание уведомления о персонализации карт для банка*/

    public void makeBankReport(List<Card> cardList, PersoType type, String nameBankReport, boolean isNHLPersonalization) throws Exception {
        log.debug(String.format("Card Type is [%s]", cardList.get(0).getPersoType().getCardType().name()));
        log.debug("Start creating bank answer for cards");
        byte[] output = personalizeServiceImpl.createReportAnswerToBank(cardList);
        ftpExchange.writeFile(nameBankReport, output, type, isNHLPersonalization);
        createOutputReport(output, nameBankReport);
    }

    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMYYYYHHmm").withZone( ZoneId.systemDefault());

    public String generateNameForBankReport(){
        Date currentDate = new Date();
        return "personalize_" + formatter.format(currentDate.toInstant()) + ".txt";
    }

    public String generateErrorNameForBankReport(){
        Date currentDate = new Date();
        return "personalize-error_" + formatter.format(currentDate.toInstant()) + ".txt";
    }

    public String generateErrorNameForNHLReport(){
        Date currentDate = new Date();
        return "error_" + formatter.format(currentDate.toInstant()) + ".txt";
    }

    private void readBankReportForActive(InputStream is, String reportName){
        log.debug(String.format("Reading bank report [%s]", reportName));
        try(BufferedReader br = new BufferedReader(new InputStreamReader(is))){
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String delims = "[;]";
                String panList [] = sCurrentLine.split(delims);
                for(String pan: panList){
                    Card card = cardDao.findCardsByPANHashNumber(pan);
                    if(card != null){
                        card.setStatus(Card.CardStatus.ACTIVE);
                        cardDao.persistTransactionalWithoutSecurityCheck(card, false, false);
                    } else {
                        log.debug(String.format("Card not found with PAN [%s]", pan));
                    }
                }
            }
        } catch (IOException e) {
            log.error(String.format("Error reading bank report [%s]", reportName), e);
        }
    }

    private byte[] readBankReportForPers(InputStream is, String reportName) throws IOException {
        log.debug(String.format("Reading bank report [%s]", reportName));
        List<String> errorsList = new ArrayList<>();
        List<Card> cardList = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(is))){
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                Card card;
                try{
                    Holder holder = holderDao.findHolderByIdNHLNumber(sCurrentLine.substring(0, 20));
                    card = cardDao.findCardByHolderAndStatus(holder, Card.CardStatus.SEND_TO_BANK_PERSONALIZATION);
                } catch (Exception e){
                    errorsList.add(String.format("Не найден держатель с ID %s", sCurrentLine.substring(0, 20)));
                    log.debug(String.format("Holder with IdNHLNumber [%s] does not exist", sCurrentLine.substring(0, 20)));
                    continue;
                }

                if(card != null){
                    cardDao.detach(card);
                    card.setPANHashNumber(sCurrentLine.substring(21, 61));
                    card.setStatus(Card.CardStatus.READY_TO_PERSONALIZE);
                    cardList.add(card);
                } else {
                    errorsList.add(String.format("У держателя с ID %s не найдена карта в нужном статусе",
                            sCurrentLine.substring(0, 20)));
                    log.debug(String.format("Card not found for holder with IdNHLNumber [%s] and status [%s]",
                            sCurrentLine.substring(0, 20), Card.CardStatus.SEND_TO_BANK_PERSONALIZATION));
                }
            }
            if(!errorsList.isEmpty()){
                return createNHLErrorReport(errorsList);
            }

            for(Card card: cardList){
                log.debug(String.format("Changing card status to [%s] for card [%s]", Card.CardStatus.READY_TO_PERSONALIZE, card.getPANHashNumber()));
                cardDao.mergeWithoutSecurityCheck(card, false, false);
            }
        } catch (IOException e) {
            log.error(String.format("Error reading bank report [%s]", reportName), e);
            errorsList.add("Некорректный формат файла");
            throw e;
        }
        return null;
    }

    private byte[] createNHLErrorReport(List<String> errorsList){
        log.debug("Creating error report of bank personalization");
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        CustomCSVWriter writer = null;
        String noSeparator = "";
        char escapechar = CSVWriter.NO_ESCAPE_CHARACTER;
        String lineEnd = "";
        try {
            writer = new CustomCSVWriter(new OutputStreamWriter(result, Charset.forName("windows-1251")), noSeparator, escapechar, lineEnd);
            for (String string: errorsList){
                writer.writeNext(new String[]{string});
                writer.writeNext(new String[]{"\r\n"});
            }
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    log.error("Can't close report file");
                }
            }
        }
        log.debug("Report complete");
        return result.toByteArray();
    }

    public void createOutputReport(byte[] fileData, String reportName){
        Date currentDate = new Date();
        MlDynamicEntityImpl newObject = commonDao.createNewEntity(metaDataHolder.getEntityClassByName("NHL_PersoTask"));
        newObject.set("file", fileData);
        newObject.set("file_filename", reportName);
        newObject.set("createDate", currentDate);
        commonDao.persistTransactionalWithoutSecurityCheck(newObject);
    }


    public void createReport(byte[] persoFileData, String fileName) {
        log.debug(String.format("Creating report for file [%s]", fileName));
        Date currentDate = new Date();
        PersoResponse persoResponse = (PersoResponse) commonDao.createNewEntity(PersoResponse.class);
        persoResponse.setFile(persoFileData);
        persoResponse.setFilename(fileName);
        persoResponse.setDate(currentDate);
        persoResponse.setStatus(PersoResponse.Status.SUCCESS);
        commonDao.persistWithoutSecurityCheck(persoResponse);
        log.debug(String.format("Report for file [%s] stored to db", fileName));
    }

    @Transactional
    public void createErrorReport(byte[] persoFileData, String fileName) {
        log.debug(String.format("Creating error report for file [%s]", fileName));
        Date currentDate = new Date();
        PersoResponse persoResponse = (PersoResponse) commonDao.createNewEntity(PersoResponse.class);
        persoResponse.setFile(persoFileData);
        persoResponse.setFilename(fileName);
        persoResponse.setDate(currentDate);
        persoResponse.setStatus(PersoResponse.Status.ERROR);
        commonDao.persistWithoutSecurityCheck(persoResponse);
        log.debug(String.format("Report for file [%s] stored to db", fileName));
    }

    @Transactional
    public void createBankErrorReport(byte[] persoFileData, String fileName, String errorReportName) {
        log.debug(String.format("Creating error report for file [%s]", fileName));
        Date currentDate = new Date();
        PersoResponse persoResponse = (PersoResponse) commonDao.createNewEntity(PersoResponse.class);
        persoResponse.setFile(persoFileData);
        persoResponse.setFilename(fileName);
        persoResponse.setDate(currentDate);
        persoResponse.setStatus(PersoResponse.Status.ERROR);
        persoResponse.setDescription(errorReportName);
        commonDao.persistWithoutSecurityCheck(persoResponse);
        log.debug(String.format("Report for file [%s] stored to db", fileName));
    }


    public enum PersoCardStatus {
        PERSONALIZED, PERSONALIZATION_ERROR
    }


    public class CardPersoResponse {


        private String cardNumber;
        private String cardPAN;
        private PersoCardStatus cardStatus;
        private String errorMessage;
        private String status;

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public String getCardPAN() {
            return cardPAN;
        }

        public void setCardPAN(String cardPAN) {
            this.cardPAN = cardPAN;
        }

        public PersoCardStatus getCardStatus() {
            return cardStatus;
        }

        public void setCardStatus(PersoCardStatus cardStatus) {
            this.cardStatus = cardStatus;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class CardHandler extends DefaultHandler {

        private final String CARD = "Card";
        private final String CRN = "CRN";
        private final String STATUS = "Status";
        private final String UIDSCENARIO = "UIDScenario";
        private final String ERROR_MESSAGE ="ErrorMessage";

        private List<CardPersoResponse> cardList;
        private CardPersoResponse currentCard;
        private StringBuilder builder;
        private boolean isBankCard = false;


        public List<CardPersoResponse> getCardList() {
            return this.cardList;
        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {
            super.characters(ch, start, length);
            builder.append(ch, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String name)
                throws SAXException {
            super.endElement(uri, localName, name);

            if(name.equalsIgnoreCase(UIDSCENARIO)){
                isBankCard = "00002".equals(builder.toString());
            }

            //TODO возможно нужно брать не первые элементы STATUS  CRN
            if (this.currentCard != null) {
                if (name.equalsIgnoreCase(CRN) && currentCard.getCardNumber() == null) {
                    if(isBankCard){
                        currentCard.setCardPAN(builder.toString());
                    } else {
                        currentCard.setCardNumber(builder.toString());
                    }
                } else if (name.equalsIgnoreCase(STATUS) && currentCard.getCardStatus() == null) {
                    if ("2".equals(builder.toString())) {
                        currentCard.setCardStatus(PersoCardStatus.PERSONALIZED);
                    } else {
                        currentCard.setCardStatus(PersoCardStatus.PERSONALIZATION_ERROR);
                    }
                } else if(currentCard.getCardStatus() != null && (name.equals(ERROR_MESSAGE) && currentCard.getCardStatus().equals(PersoCardStatus.PERSONALIZATION_ERROR))){
                    currentCard.setErrorMessage(builder.toString());
                } else if (name.equalsIgnoreCase(CARD)) {
                    cardList.add(currentCard);
                }
                builder.setLength(0);
            }
        }

        @Override
        public void startDocument() throws SAXException {
            super.startDocument();
            cardList = new ArrayList<>();
        }

        @Override
        public void startElement(String uri, String localName, String name,
                                 Attributes attributes) throws SAXException {

            builder = new StringBuilder();
            super.startElement(uri, localName, name, attributes);
            if (name.equalsIgnoreCase(CARD)) {
                this.currentCard = new CardPersoResponse();
            }
        }
    }

}


