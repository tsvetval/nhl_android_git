package ru.alth.nhl.service.storages;

import java.util.List;

/**
 * Created by admin on 07.04.2016.
 */
public interface FileStorage {
    public List<String> listFiles() throws Exception;
    public void writeFile(String fileName, byte[] content) throws Exception;
    public byte[] readFile(String fileName) throws Throwable;
    public void deleteFile(String filename) throws Exception;
    public void renameFile(String oldName,String newName) throws Exception;
    public void setSettings(String url, String folder, String user, String password);
    public void changeFolder(String folder);
    public void disconnect();
}
