package ru.alth.nhl.service;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.RoleType;
import ru.alth.nhl.dao.BankBranchDao;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.PersoTypeDao;
import ru.alth.nhl.handler.CardHandler;
import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.model.PersoType;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.model.security.MlRole;
import ru.ml.core.services.AccessService;

import java.util.List;

/**
 * Сервис для создания новых карт
 */
public class CardCreateService {

    @Inject
    private PersoTypeDao persoTypeDao;
    @Inject
    private BankBranchDao bankBranchDao;
    @Inject
    private CardDao cardDao;
    @Inject
    private AccessService accessService;

    private static final Logger log = LoggerFactory.getLogger(CardCreateService.class);

    /**
     * Обновление статуса карты после сохранения оператором приёма заявлений или главным администратором
     */
    public void changeCardStatus(Card card){
        log.debug("Changing card status...");
        List<MlRole> roles = accessService.getUserRoles();
        for (MlRole role : roles) {
            if ((role.getRoleType().equals(RoleType.REGISTRY_OPERATOR) || role.getRoleType().equals(RoleType.MAIN_ADMIN_ROLE)) && card.getPersoType() != null) {
                if (card.getPersoType().getCardType().equals(PersoType.CardType.NOT_BANK)){
                    card.setStatus(Card.CardStatus.READY_TO_PERSONALIZE);
                }
                if (card.getPersoType().getCardType().equals(PersoType.CardType.BANK)){
                    card.setStatus(Card.CardStatus.READY_TO_BANK_PERSONALIZE);
                }
                log.debug("cardStatus changed to " + card.getStatus().name());
            }
        }
    }

    /**
     * Создаем карту и пишем в БД
     * */
    public void storeNewCard(Holder holder, String info, Long persoTypeId, String bankBranchId){
        Card card = new Card();
        PersoType persoType = persoTypeDao.findById(persoTypeId, PersoType.class);
        card.setPersoType(persoType);
        changeCardStatus(card);
        card.setInfo(info);
        card.setCardStatusNhl(Card.CardStatusNhl.ACTIVE);
        if(!bankBranchId.isEmpty()){
            BankBranch bankBranch = bankBranchDao.findById(Long.valueOf(bankBranchId), BankBranch.class);
            card.setBankBranch(bankBranch);
        }
        card.setHolder(holder);
        cardDao.persistTransactionalWithoutSecurityCheck(card);
    }
}
