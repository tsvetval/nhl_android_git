package ru.alth.nhl.service.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.reports.exportReports.ExportBankNHLReport;
import ru.alth.nhl.service.reports.exportReports.ExportBankReport;
import ru.alth.nhl.service.reports.exportReports.ExportNHLReport;
import ru.alth.nhl.service.reports.exportReports.ExportReport;
import ru.alth.nhl.service.reports.importReports.ImportBankCardReport;
import ru.alth.nhl.service.reports.importReports.ImportNotBankCardReport;
import ru.alth.nhl.service.reports.importReports.ImportReport;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ReportStrategy {
    private static final Logger log = LoggerFactory.getLogger(ReportStrategy.class);

    public static ExportReport getExportReport(List<Card> cardList){
        ExportReport report;
        switch (cardList.get(0).getStatus()){
            case READY_TO_PERSONALIZE:
                List<Card> notBankCards = new ArrayList<>();
                List<Card> bankCards = new ArrayList<>();
                for(Card card: cardList){
                    if(card.getPANHashNumber() == null){
                        notBankCards.add(card);
                    } else {
                        bankCards.add(card);
                    }
                }
                if(!notBankCards.isEmpty()){
                    log.debug("Creating report for NHL personalize not bank cards.");
                    report = GuiceConfigSingleton.inject(ExportNHLReport.class);
                    report.setCardsForReport(notBankCards);
                    break;
                }
                if(!bankCards.isEmpty()){
                    log.debug("Creating report for NHL personalize bank cards.");
                    report = GuiceConfigSingleton.inject(ExportBankNHLReport.class);
                    report.setCardsForReport(bankCards);
                    break;
                }
            case READY_TO_BANK_PERSONALIZE:
                log.debug("Creating report for bank personalize.");
                report = GuiceConfigSingleton.inject(ExportBankReport.class);
                report.setCardsForReport(cardList);
                break;
            default:
                throw new MlApplicationException("Wrong card status.");
        }
        return report;
    }

    public static ImportReport getImportReport(PersoType type){
        ImportReport report;
        switch (type.getCardType()){
            case BANK:
                log.debug("Checking bank cards");
                report = GuiceConfigSingleton.inject(ImportBankCardReport.class);
                report.setPersoType(type);
                break;
            case NOT_BANK:
                log.debug("Checking not bank cards");
                report = GuiceConfigSingleton.inject(ImportNotBankCardReport.class);
                report.setPersoType(type);
                break;
            default:
                throw new MlApplicationException("Wrong card type.");
        }
        return report;
    }
}