package ru.alth.nhl.service.reports.exportReports;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.PersonalizeServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Отчет для персонализации в НХЛ банковских карт
 */
public class ExportBankNHLReport implements ExportReport {

    private static final Logger log = LoggerFactory.getLogger(ExportBankNHLReport.class);
    private List<Card> cardList;

    @Inject
    private PersonalizeServiceImpl personalizeService;

    @Override
    public void createReportAndChangeStatus() throws Exception {
        log.debug(String.format("Exporting [%s] cards for NHL with PAN.", cardList.size()));
        for (Card card: cardList){
            cardList = new ArrayList<>();
            cardList.add(card);
            personalizeService.importReportAndChangeStatus(cardList, true);
        }
    }

    @Override
    public void setCardsForReport(List<Card> cardList) {
        this.cardList = cardList;
    }
}
