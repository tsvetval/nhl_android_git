package ru.alth.nhl.service;

import ru.alth.nhl.util.crypto.*;
import ru.ml.core.common.exceptions.MlApplicationException;

import javax.smartcardio.CardException;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class CardCommandsService {
    /*    private final String[] ReadKeyArray = {
                "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                "44444444444444444444444444444444",
                "66666666666666666666666666666666",
                "88888888888888888888888888888888",
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        };
        private final String[] WriteKeyArray = {
                "00112233445566778899AABBCCDDEEFF",
                "A0A1A2A3A4A5A6A7B0B1B2B3B4B5B6B7",
                "55555555555555555555555555555555",
                "77777777777777777777777777777777",
                "99999999999999999999999999999999"
        };*/
    private final String[] ReadKeyArray = {
            "5D07291A0B73E5EA2FBCDA924FC4346D",
            "989780E6C88331BFDAF2A2FE4AB50775",
            "8075A2E561C894E0B5F2B59D640BAE37",
            "5DF8A80E23757986583D58A4D9B9E3D3",
            "3107B6BAFE8C4094548C7C794AD98C86"
    };
    private final String[] WriteKeyArray = {
            "49C2B5296EF420A4A229E68AD9E543A4",
            "16F862073104EA8F0801081697CECE6B",
            "49295D9ECB621394A864F880E92C1CBF",
            "DCA43E25BA1AA19254C7802361B9D932",
            "2F3D68EC2C46B9D9D07A437AEA73F47A"
    };


    private final String rootKey = "30313233343536373839414243444546";
    private final String rootDFId = "3F00";
    private final String rootEFId = "2FE0";
    final static String DF_ID = "4F00";

    /**
     * До этой команды должны быть выполнены все необходимые селекты и получено случайное число с карты (Get challenge)
     * а так же прочитана длина файла
     *
     * @return список команд для чтения файла (первые 2 команда служебные далее идут чтения)
     */
    public List<String> getReadFileCommands(String randomCardNum, Long fileNumber, Long fileSize) throws Exception {
        List<String> commandsList = new ArrayList<>();
        String randomTermNum = "EB49DA61821F8F54";
        String key = ReadKeyArray[fileNumber.intValue() - 1];
        byte[] sessionKey = deriveSessionKey(randomCardNum, randomTermNum, key);

        // Формируем команду аутентификации на файле
        Integer keyNumber = fileNumber.byteValue() * 2;
        commandsList.add(authenticateToFileCmd(randomCardNum, randomTermNum, sessionKey, keyNumber.byteValue()));
        //Команда выбора файла
        commandsList.add(selectEFAIDCmd(fileNumber));
        //Команды чтения файла
        commandsList.addAll(generateReadCmdList(fileSize.intValue(), sessionKey));
        return commandsList;
    }

    /**
     * До этой команды должны быть выполнены все необходимые селекты и получено случайное число с карты (Get challenge)
     *
     * @return список команд для чтения файла (первые 2 команда служебные далее идут чтения)
     */
    public List<String> getWriteFileCommands(String randomCardNum, Long fileNumber, String fileData) throws Exception {
        List<String> commandsList = new ArrayList<>();
        String randomTermNum = "EB49DA61821F8F54";
        String key = WriteKeyArray[fileNumber.intValue() - 1];
        byte[] sessionKey = deriveSessionKey(randomCardNum, randomTermNum, key);

        // Формируем команду аутентификации на файле
        Integer keyNumber = fileNumber.byteValue() * 2 - 1;
        commandsList.add(authenticateToFileCmd(randomCardNum, randomTermNum, sessionKey, keyNumber.byteValue()));
        //Команда выбора файла
        commandsList.add(selectEFAIDCmd(fileNumber));
        //Команды записи файла
        commandsList.addAll(generateWriteCmdList(DatatypeConverter.parseHexBinary(fileData), sessionKey));
        return commandsList;
    }

    /**
     * До этой команды должны быть выполнены все необходимые селекты и получено случайное число с карты (Get challenge)
     * selectDFAID(isoDep, rootDFId);
     * selectEFAID(isoDep, rootEFId);
     *
     * @return список команд для чтения файла (первые 2 команда служебные далее идут чтения)
     */
    public List<String> getDeleteCreateFileCommands(String randomCardNum, Long fileNumber, String fileData) throws Exception {
        if (fileData.length() % 2 != 0) {
            throw new MlApplicationException("Длинна файла данных должна быть кратна 2 в hex");
        }

        List<String> commandsList = new ArrayList<>();
        String randomTermNum = "EB49DA61821F8F54";
        String key = rootKey;
        byte[] sessionKey = deriveSessionKey(randomCardNum, randomTermNum, key);

        // Формируем команду аутентификации на файле
        Integer keyNumber = 1;
        commandsList.add(authenticateToFileCmd(randomCardNum, randomTermNum, sessionKey, keyNumber.byteValue()));

        //Команда выбора директории
        commandsList.add(selectDFAID(DF_ID));

        String ef = String.format("%04d", fileNumber);

        // Команда удаления
        commandsList.add(deleteFile(ef, sessionKey));

        // Команда создания
        String conditionalAcKey = String.format("%01X", fileNumber * 2)/*read key number*/ + String.format("%01X", fileNumber * 2 - 1)/*write key number*/;
        commandsList.add(createFile(ef, fileData.length() / 2, conditionalAcKey, sessionKey));

        return commandsList;
    }


    private String selectEFAIDCmd(Long fileNumber) throws Exception {
        String ef = String.format("%04d", fileNumber);
        byte[] efHex = {(byte) (ef.length() / 2)};
        return "80A40000"
                + DatatypeConverter.printHexBinary(efHex)
                + ef;

    }

    public String selectDFAID(String df) throws Exception {
        return "80A40000"
                + String.format("%02X", (df.length() / 2))
                + df;
    }

    private byte[] deriveSessionKey(String randomCardNum, String randomTermNum, String key16) throws SMartChipSMException {
        String diverData = generateDeriveData(randomCardNum, randomTermNum);
        return SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(key16), DatatypeConverter.parseHexBinary(diverData), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
    }


    private String generateDeriveData(String randomCardNum, String randomTermNum) {
        return randomCardNum + randomTermNum;
    }

    private String authenticateToFileCmd(String randomCardNum, String randomTermNum, byte[] sessionKey, byte keyNumber) throws Exception {
        //String randomTermNum = "EB49DA61821F8F54";
        //String diverData = generateDeriveData(randomCardNum, randomTermNum);

        byte[] cryptogrammTerminalBytes = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(randomCardNum), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        String cryptogrammTerminal = DatatypeConverter.printHexBinary(cryptogrammTerminalBytes);

        cryptogrammTerminal = cryptogrammTerminal.substring(0, 8); // : 0DDEBC19
        byte[] keyNumberHex = {keyNumber};
        return "808200" + DatatypeConverter.printHexBinary(keyNumberHex) + "0C" + randomTermNum + cryptogrammTerminal;
    }

    private List<String> generateReadCmdList(int fileSize, byte[] sessionKey) throws SMartChipSMException, IOException, CardException {
        List<String> resultList = new ArrayList<>();

        int blockSize = 192;//192; // 192 -
        String ClaIns = "84B0";
        int offset = 0;

        while (fileSize > offset) {
            int length = 0;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset);

            int cmdLen = length + 4;
            String cmd = ClaIns + p1p2 + "05" + String.format("%02X", cmdLen);

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(sessionKey,
                    DatatypeConverter.parseHexBinary(cmd), EnMacMode.MAC_RETAIL));

            String mac = calcMacData.substring(0, 8);
            cmd = cmd + mac;

            resultList.add(cmd);
            offset += length;
        }
        return resultList;
    }


    public List<String> generateWriteCmdList(byte[] fileData, byte[] sessionKey) throws SMartChipSMException, IOException, CardException {
        List<String> resultList = new ArrayList<>();
        int blockSize = 234;//192; // 192 -
        String ClaIns = "84D6";
        int offset = 0;
        int fileSize = fileData.length;
        int length = 0;

        while (fileSize > offset) {
            if (fileData.length - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileData.length - offset;
            }

            String dataStr = DatatypeConverter.printHexBinary(Arrays.copyOfRange(fileData, offset, offset + length));
            String len = String.format("%02X", 4 + dataStr.length() / 2);

            String p1p2 = String.format("%04X", offset); //offset.ToString("X04");

            String dataForMac = ClaIns + p1p2 + len + dataStr;

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(sessionKey,
                    DatatypeConverter.parseHexBinary(dataForMac), EnMacMode.MAC_RETAIL));


            String mac = calcMacData.substring(0, 8);
            String cmd = ClaIns + p1p2 + len + dataStr + mac;
            resultList.add(cmd);
            offset += length;
        }
        return resultList;
    }


    private String deleteFile(String fileId, byte[] sessionKey) throws Exception {
        Byte P1 = 0x00;
        Byte P2 = 0x00;
        Byte P3 = 0x06;

        String cmdDel = "84E4"
                + String.format("%02X", P1)
                + String.format("%02X", P2)
                + String.format("%02X", P3)
                + fileId;
        String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(sessionKey,
                DatatypeConverter.parseHexBinary(cmdDel), EnMacMode.MAC_RETAIL));

        String macData = calcMacData.substring(0, 8);
        cmdDel += macData;
        return cmdDel;
    }

    private String createFile(String fileId, Integer size, String conditionalAcKey, byte[] sessionKey) throws Exception {
        byte P1 = 0x10; // (0x10 - for Elementary File)
        byte P2 = 0x02; // (0x02 - always)
        byte P3 = 0x0B; // (0x0B - always)

        String fileSize = String.format("%04X", size);//"2C40"; //

        // 00h - удаление возможно
        // 80h - удаление невозможно
        String dfType = "00";  //00h (80h) – бинарный; 01h (81h) - кошелек; 03h (83h) – файл ключей; 10h – директория DF

        // 1. For directory 0x00 always
        // 2. files conditional By PIN
        //    first nibl - read conditional (0	ALW- всегда ; 1 - По предъявлению PIN1 ; 2 - По предъявлению PIN2; 6 - По предъявлению ADM; F - NEV никогда)
        //    second nible - write conditional (0	ALW- всегда ; 1 - По предъявлению PIN1 ; 2 - По предъявлению PIN2; 6 - По предъявлению ADM; F - NEV никогда)
        String conditionalAcPin = "00";

        //string conditionalAcKey = "21";
        String cmd = "84E0";
        //executeCreationCommand("84E0", P1, P2, P3, fileSize, fileId, dfType, conditionalAcPin, conditionalAcKey);
        String cmdCreateDir = "84E0"
                + String.format("%02X", P1)
                + String.format("%02X", P2)
                + String.format("%02X", P3)
                + fileSize
                + fileId
                + dfType
                + conditionalAcPin
                + conditionalAcKey;  // 1 byte 4-7 bits read key number  0-3 write  key number

        String calcMacData = cmdCreateDir;

        String macData = DatatypeConverter.printHexBinary(SMUtil.mac(sessionKey,
                DatatypeConverter.parseHexBinary(calcMacData), EnMacMode.MAC_RETAIL));

        macData = macData.substring(0, 8);

        cmdCreateDir += macData;
        return cmdCreateDir;
    }


}
