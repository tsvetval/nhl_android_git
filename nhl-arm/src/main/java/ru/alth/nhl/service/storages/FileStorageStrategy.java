package ru.alth.nhl.service.storages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.PersoType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;

/**
 * Created by admin on 07.04.2016.
 */
public class FileStorageStrategy {
    private static final Logger log = LoggerFactory.getLogger(FileStorageStrategy.class);

    public static FileStorage getImportStorage(PersoType persoType, boolean isNHLPersonalization){
        FileStorage fileStorage;
        switch (persoType.getStorageType()){
            case FTP:
                fileStorage = GuiceConfigSingleton.inject(FTPFileStorage.class);
                log.debug("Chosen storage is FTPFileStorage");
                break;
            case SFTP:
                fileStorage = GuiceConfigSingleton.inject(SFTPFileStorage.class);
                log.debug("Chosen storage is SFTPFileStorage");
                break;
            default:
                throw new MlApplicationException("Storage type not supported");
        }
        if(isNHLPersonalization){
            fileStorage.setSettings(persoType.getImportFTPURL(),persoType.getImportNHLFTPFolder(),persoType.getImportFTPUser(),persoType.getImportFTPPassword());
        } else {
            fileStorage.setSettings(persoType.getImportFTPURL(),persoType.getImportFTPFolder(),persoType.getImportFTPUser(),persoType.getImportFTPPassword());
        }
        return fileStorage;
    }

    public static FileStorage getExportStorage(PersoType persoType, boolean isNHLPersonalization){
        FileStorage fileStorage;
        switch (persoType.getStorageType()){
            case FTP:
                fileStorage = GuiceConfigSingleton.inject(FTPFileStorage.class);
                log.debug("Chosen storage is FTPFileStorage");
                break;
            case SFTP:
                fileStorage = GuiceConfigSingleton.inject(SFTPFileStorage.class);
                log.debug("Chosen storage is SFTPFileStorage");
                break;
            default:
                throw new MlApplicationException("Storage type not supported");
        }
        if(isNHLPersonalization){
            fileStorage.setSettings(persoType.getExportFTPURL(),persoType.getExportNHLFTPFolder(),persoType.getExportFTPUser(),persoType.getExportFTPPassword());
        } else {
            fileStorage.setSettings(persoType.getExportFTPURL(),persoType.getExportFTPFolder(),persoType.getExportFTPUser(),persoType.getExportFTPPassword());
        }
        return fileStorage;
    }

}
