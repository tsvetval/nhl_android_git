package ru.alth.nhl.service.reports.importReports;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.FTPExchange;
import ru.alth.nhl.service.ImportPersoServiceImpl;

import java.util.List;

/**
 * Реализация импорта отчета небанковских карт
 */
public class ImportNotBankCardReport implements ImportReport {

    private static final Logger log = LoggerFactory.getLogger(ImportNotBankCardReport.class);
    private PersoType type;

    @Inject
    private ImportPersoServiceImpl importPersoService;
    @Inject
    private FTPExchange ftpExchange;

    @Override
    public void importReport(){
        try{
            List<String> fileList = ftpExchange.listFiles(type, false);
            log.debug(String.format("Filelist size is [%s]", fileList.size()));
            for (String filename : fileList) {
                byte[] persoFileData = ftpExchange.readFile(filename, type, false);
                log.debug(String.format("File [%s] size = [%s] bytes", filename, persoFileData.length));
                try {
                    importPersoService.importNotBankReportAndMoveFileToArchive(persoFileData, filename, type, false);
                } catch (Exception e) {
                    log.error(String.format("Error while importing file [%s]", filename), e);
                    //create  Error ExportReport and move file to archive
                    importPersoService.createErrorReport(persoFileData, filename);
                    ftpExchange.moveFileToArchive(filename, type, persoFileData, false);
                }
            }
        } catch (Throwable e) {
            // Skip this error, and continue
            log.error("Fatal ERROR in FTPImportTask on importing not bank report ", e);
        }

    }

    @Override
    public void setPersoType(PersoType type) {
        this.type = type;
    }


}
