package ru.alth.nhl.service;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.dto.HolderDto;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.dao.RegionDao;
import ru.alth.nhl.model.Holder;

import javax.xml.bind.DatatypeConverter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 */
public class HolderService {

    private static final Logger log = LoggerFactory.getLogger(HolderService.class);

    @Inject
    private HolderDao holderDao;
    @Inject
    private RegionDao regionDao;


    public class ValidationException extends Exception{
        public ValidationException(String message) {
            super(message);
        }
    }

    private Enum[] createMandatoryFields = {
            Holder.Fields.photo,
            Holder.Fields.lastname,
            Holder.Fields.firstname,
            Holder.Fields.secondname,
            Holder.Fields.birthdate,
            Holder.Fields.teamname,
            Holder.Fields.vidchlenstva,
            Holder.Fields.statusnhl,
            Holder.Fields.phone,
            Holder.Fields.email,
            Holder.Fields.mailingAddress,
            Holder.Fields.documnum,
            Holder.Fields.documdate,
            Holder.Fields.residency,
            Holder.Fields.external_id

            /*Holder.Fields.idnhlnumber*/
    };


    private class EvaluateAndValidate<T, R> {
        private List<T> validationValues = new ArrayList<>();
        private Function<T, R> evaluator = o -> (R)o;

        public EvaluateAndValidate(Function<T, R> evaluator) {
            this.evaluator = evaluator;
        }

        public EvaluateAndValidate(T... values) {
            this.validationValues = Arrays.asList(values);
        }

        public EvaluateAndValidate(List<T> validationValues, Function<T, R> evaluator, Predicate<T> validator) {
            this.validationValues = validationValues;
            this.evaluator = evaluator;
        }

        public R execute(T value) throws ValidationException{
            try {
                if (!validationValues.isEmpty()) {
                    if (!validationValues.contains(value)) {
                        throw new ValidationException("Wrong validation value " + value);
                    }
                }
                return evaluator.apply(value);
            } catch (Throwable e){
                throw new ValidationException("Wrong validation value" + value);
            }
        }
    }


    private Function<String, byte[]> photoFunction = DatatypeConverter::parseBase64Binary;

    private static final DateTimeFormatter middleDateFormat = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneId.systemDefault());

    private Function<String, Date> dateFunction = new Function<String, Date>() {
        @Override
        public Date apply(String s) {
            return Date.from(LocalDate.parse(s, middleDateFormat).atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
    };
    private Function<String,Long> longFunction = new Function<String, Long>() {
        @Override
        public Long apply(String s){
            return Long.valueOf(s);
        }
    };

    Map<String, EvaluateAndValidate> adapterMap = ImmutableMap.<String, EvaluateAndValidate>builder()
            .put(Holder.Fields.photo.name(), new EvaluateAndValidate(photoFunction))
            .put(Holder.Fields.birthdate.name(), new EvaluateAndValidate(dateFunction))
            .put(Holder.Fields.rezus.name(), new EvaluateAndValidate("00","01"))
            .put(Holder.Fields.gruppa.name(), new EvaluateAndValidate("01","02","03","04"))
            .put(Holder.Fields.vidchlenstva.name(), new EvaluateAndValidate("00","01","02", "03"))
            .put(Holder.Fields.statusnhl.name(), new EvaluateAndValidate("00","01"))
            .put(Holder.Fields.protivopakaz.name(), new EvaluateAndValidate("00","01"))
            .put(Holder.Fields.allegry.name(), new EvaluateAndValidate("00","01"))
            .put(Holder.Fields.chronical.name(), new EvaluateAndValidate("00","01"))
            //.put(Holder.Fields.documnum.name(), new EvaluateAndValidate(asas))
            .put(Holder.Fields.documdate.name(), new EvaluateAndValidate(dateFunction))
            //.put(Holder.Fields.drivernum.name(), new EvaluateAndValidate())
            .put(Holder.Fields.driverdate.name(), new EvaluateAndValidate(dateFunction))
            .put(Holder.Fields.matchDate.name(), new EvaluateAndValidate(dateFunction))
            .put(Holder.Fields.residency.name(), new EvaluateAndValidate("PR","PNR"))
            .put(Holder.Fields.external_id.name(), new EvaluateAndValidate(longFunction))

            .build();

    /**
     *
     * @param params
     * @return nhlid
     * @throws ValidationException
     */
    public Holder createHolder(Map<String, String> params) throws ValidationException {
        // check mandatory fields
        for (Enum field : createMandatoryFields){
          if (!params.containsKey(field.name()) || params.get(field.name()).toString().isEmpty()){
              throw new ValidationException(String.format("Missing parameter [%s]", field.name()));
          }
        }


        Holder holder = holderDao.createNewEntity(Holder.class);
        // check non api parameters
        for (String param : params.keySet()){
            Holder.Fields field = null;
            try {
                field =  Holder.Fields.valueOf(param);
            } catch (Throwable e){
                throw new ValidationException(String.format("Wrong parameter [%s]", param));
            }

            if (adapterMap.containsKey(field.name())){
                try {
                    holder.set(field.name(), adapterMap.get(field.name()).execute(params.get(field.name())));
                } catch (ValidationException e) {
                    throw new ValidationException(String.format("Wrong parameter [%s] value [%s]", field.name(), params.get(field.name())));
                }
            } else {
                holder.set(field.name(), params.get(field.name()));
            }
        }

        if (params.containsKey(Holder.Fields.photo.name())){
            holder.set(Holder.Fields.photo.name() + "_filename", "photo.jpg");
        }

        holderDao.persistTransactionalWithoutSecurityCheck(holder);

        return holder;

    }

    public void updateHolder(Holder holder, Map<String, String> params) throws ValidationException{
        for (String param : params.keySet()){
            Holder.Fields field = null;
            try {
                field =  Holder.Fields.valueOf(param);
            } catch (Throwable e){
                throw new ValidationException(String.format("Wrong parameter [%s]", param));
            }

            if (adapterMap.containsKey(field.name())){
                try {
                    holder.set(field.name(), adapterMap.get(field.name()).execute(params.get(field.name())));
                } catch (ValidationException e) {
                    throw new ValidationException(String.format("Wrong parameter [%s] value [%s]", field.name(), params.get(field.name())));
                }
            } else {
                holder.set(field.name(), params.get(field.name()));
            }
        }
        if (params.containsKey(Holder.Fields.photo.name())){
            if (params.get(Holder.Fields.photo.name()) == null || params.get(Holder.Fields.photo.name()).isEmpty()){
                holder.set(Holder.Fields.photo.name() + "_filename", null);
            } else {
                holder.set(Holder.Fields.photo.name() + "_filename", "photo.jpg");
            }
        }

        holderDao.mergeTransactionalWithoutSecurityCheck(holder);
    }

    private static final SimpleDateFormat birthdayDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public void saveHolderViaHolderDto(HolderDto holderDto) throws ParseException {
        Holder holder = holderDao.createNewEntity(Holder.class);
        holder.setLastName(holderDto.getLastName());
        holder.setFirstName(holderDto.getFirstName());
        holder.setSecondName(holderDto.getSecondName());
        holder.setExternalId(Long.valueOf(holderDto.getExternalId()));
        holder.setBirthdate(birthdayDateFormat.parse(holderDto.getBirthdate()));
        holder.setDocNum(holderDto.getDocSer() + " " + holderDto.getDocNum());
        holder.setDataSource(holderDto.getDataSource());
        holder.setResidency(holderDto.getResidency());
        holder.setPhoto(holderDto.getPhoto());
        holder.setPhotoFilename(holderDto.getExternalId()+".jpg");
        if(regionDao.findRegionByName(holderDto.getRegion()) != null){
            holder.setRegion(regionDao.findRegionByName(holderDto.getRegion()));
        }
        holderDao.persistTransactionalWithoutSecurityCheck(holder);
    }
}
