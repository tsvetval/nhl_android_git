package ru.alth.nhl.service;

import com.google.inject.Inject;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 *
 */
public class CardCheckService {

    @Inject
    private CardDao cardDao;

    public ValidationResult checkCardStatus(Card card, String hash1, String hash2, String hash3, String hash4){
        if (card == null || card.getHolder() == null){
            return new ValidationResult(false, "У карты отсутствует держатель", "WARN");
        }

        ValidationResult result = null;

        // Проверяем активность карты
        if (!Card.CardStatus.ACTIVE.equals(card.getStatus())){
            result = new  ValidationResult(false, "Карта не активна", "WARN");
        }

        LocalDate startMonthDateTime = LocalDate.now().withDayOfMonth(1);

        // Срок действия карты истек
        if (card.getExpireDate() == null ||
           LocalDateTime.ofInstant(Instant.ofEpochMilli(card.getExpireDate().getTime()), ZoneId.systemDefault())
                   .toLocalDate().isBefore(startMonthDateTime)){
            result = new ValidationResult(false, "Срок действия карты истек", "WARN");
        }

        if (result != null) {
            result.setHashesvalidation(hash1.equals(card.getHashFile1()), hash2.equals(card.getHashFile2()),
                    hash3.equals(card.getHashFile3()), hash4.equals(card.getHashFile4()));
        } else  {
            //Проверяем несовпадение хешей
            if(!hash1.equals(card.getHashFile1())){
                card.setStatus(Card.CardStatus.BLOCKED);
                cardDao.persistTransactionalWithoutSecurityCheck(card, false, false);
                result = new ValidationResult(false, "Предъявлена поддельная карта!", "WARN");
            } else if (card.getFlagASCI() == null || !card.getFlagASCI()) {
                result = new ValidationResult(false, true, hash2.equals(card.getHashFile2()),
                        false, hash4.equals(card.getHashFile4()), "Карте требуется обновление", "INFO");
            } else if (!hash2.equals(card.getHashFile2())
                    || !hash3.equals(card.getHashFile3())
                    || !hash4.equals(card.getHashFile4())){
                result = new ValidationResult(false, true, hash2.equals(card.getHashFile2()),
                        hash3.equals(card.getHashFile3()), hash4.equals(card.getHashFile4()), "Карте требуется обновление", "INFO");
            } else {
                result = new ValidationResult(true, true, true, true, true, null, null);
            }
        }

        return result;
    }


    public class ValidationResult{
        private String failDescription;
        private Boolean hash1Valid;
        private Boolean hash2Valid;
        private Boolean hash3Valid;
        private Boolean hash4Valid;

        private String errorCode;
        private  Boolean validationResult;

        public ValidationResult() {
        }

        public ValidationResult(Boolean validationResult, String failDescription, String errorCode) {
            this.validationResult = validationResult;
            this.failDescription = failDescription;
            this.errorCode = errorCode;
        }

        public ValidationResult(Boolean validationResult, Boolean hash1Valid, Boolean hash2Valid, Boolean hash3Valid, Boolean hash4Valid, String failDescription, String errorCode) {
            this.validationResult = validationResult;
            this.hash1Valid = hash1Valid;
            this.hash2Valid = hash2Valid;
            this.hash3Valid = hash3Valid;
            this.hash4Valid = hash4Valid;
            this.failDescription = failDescription;
            this.errorCode = errorCode;
        }

        public void setHashesvalidation(Boolean hash1Valid, Boolean hash2Valid, Boolean hash3Valid, Boolean hash4Valid) {
            this.hash1Valid = hash1Valid;
            this.hash2Valid = hash2Valid;
            this.hash3Valid = hash3Valid;
            this.hash4Valid = hash4Valid;
        }

        public String getFailDescription() {
            return failDescription;
        }

        public Boolean getHash1Valid() {
            return hash1Valid;
        }

        public Boolean getHash2Valid() {
            return hash2Valid;
        }

        public Boolean getHash3Valid() {
            return hash3Valid;
        }

        public Boolean getHash4Valid() {
            return hash4Valid;
        }

        public Boolean getValidationResult() {
            return validationResult;
        }
    }

    public void changeFlagASCI(Card card){
        card.setFlagASCI(true);
        cardDao.persistTransactionalWithoutSecurityCheck(card, false, false);
    }
}
