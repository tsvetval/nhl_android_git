package ru.alth.nhl.service.reports.exportReports;

import ru.alth.nhl.model.Card;

import java.util.List;

/**
 *
 */
public interface ExportReport {
    void createReportAndChangeStatus() throws Exception;
    void setCardsForReport(List<Card> cardList);
}
