package ru.alth.nhl.service;

import au.com.bytecode.opencsv.CSVWriter;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.ReportDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.util.CustomCSVWriter;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.holders.MetaDataHolder;
import ru.ml.core.model.MlDynamicEntityImpl;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class PersonalizeServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(PersonalizeServiceImpl.class);
    @Inject
    private CommonDao commonDao;
    @Inject
    private ReportDao reportDao;
    @Inject
    private CardDao cardDao;
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    FTPExchange ftpExchange;

    private static final DateTimeFormatter fullDateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.systemDefault());

    public byte[] createReportFile(List<Card> cardList){
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        CustomCSVWriter writer = null;
        String separator = "#$";
        String noSeparator = "";
        char escapechar = CSVWriter.NO_ESCAPE_CHARACTER;
        String lineEnd = "";
        try {
            if(cardList.get(0).getStatus().equals(Card.CardStatus.READY_TO_PERSONALIZE)){
                // Проводим экспорт объектов
                writer = new CustomCSVWriter(new OutputStreamWriter(result, Charset.forName("windows-1251")), separator, escapechar, lineEnd);
                String[] firstLineWithoutPAN = new String[1];
                String[] firstLineWithPAN =  new String[1];
                if(cardList.get(0).getPersoType().getOSType().equals(PersoType.OSType.SCONE)){
                    firstLineWithoutPAN [0] = "Card_NHL";
                    firstLineWithPAN [0] = "Card_NHL_PAN";
                } else {
                    firstLineWithoutPAN [0] = "Card_NHL_Social";
                    firstLineWithPAN [0]  = "Card_NHL_Social_PAN";
                }

                if(cardList.get(0).getPANHashNumber() == null){
                    writer.writeNext(firstLineWithoutPAN);
                } else {
                    writer.writeNext(firstLineWithPAN);
                }
                // Записываем в файл всех выбранных держателей
                for (Card card : cardList){   //TODO вставитьразделитель строки
                    writer.writeNext(new String[]{"\r\n"});
                    writer.writeNext(exportCardToCSV(card));
                }
            } else if(cardList.get(0).getStatus().equals(Card.CardStatus.READY_TO_BANK_PERSONALIZE)) {
                // Проводим экспорт объектов
                writer = new CustomCSVWriter(new OutputStreamWriter(result, Charset.forName("windows-1251")), noSeparator, escapechar, lineEnd);

                // Записываем первую строку файла
                writer.writeNext(firstDataLine(cardList.get(0)));
                writer.writeNext(new String[]{"\r\n"});

                // Записываем в файл всех выбранных держателей
                int lineNumber = 2;
                for (Card card : cardList){
                    writer.writeNext(clientDataLine(card, lineNumber));
                    writer.writeNext(new String[]{"\r\n"});
                    lineNumber ++;
                    writer.writeNext(contractDataLine(card, lineNumber));
                    writer.writeNext(new String[]{"\r\n"});
                    lineNumber ++;
                }

                // Записываем последнюю строку файла
                writer.writeNext(lastDataLine(lineNumber, cardList));
            }

        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    log.error("Can't close CSV File");
                }
            }
        }
        return result.toByteArray();
    }


    private String appendWhiteSpaces(int amount){
        StringBuilder spaces = new StringBuilder();
        for(int i = 0; i < amount; i++){
            spaces.append(" ");
        }
        return spaces.toString();
    }

    private String convertFilialNumber(String filialNumber){
        return filialNumber.substring(2,4) + filialNumber.substring(0, 2);
    }

    @Transactional
    public String markCardsAsSendToPersonalizeAndCreateReport(List<Card> cardList, byte[] persoFileData){
        Date currentDate = new Date();
        String reportName = convertReportName(cardList.get(0), currentDate);
        createReport(reportName, persoFileData);
        this.markCardsAsSendToPersonalize(cardList);
        return reportName;
    }

    public void markCardsAsSendToPersonalize(List<Card> cardList){
        for (Card card : cardList){
            if(card.getStatus().equals(Card.CardStatus.READY_TO_PERSONALIZE)){
                card.setStatus(Card.CardStatus.SEND_TO_PERSONALIZATION);
            } else {
                card.setStatus(Card.CardStatus.SEND_TO_BANK_PERSONALIZATION);
            }
            card.setFlagASCI(true);
            commonDao.persistWithoutSecurityCheck(card, false, false);
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public void importReportAndChangeStatus(List<Card> cardList, boolean isNHLPersonalization) throws Exception {
        Date currentDate = new Date();
        try {
            log.debug("Trying to create report...");
            String reportName = convertReportName(cardList.get(0), currentDate);
            byte[] output = createReportFile(cardList);
            log.debug(String.format("Report size is [%s]", output.length));
            createReport(reportName, output);
            ftpExchange.writeFile(reportName, output, cardList.get(0).getPersoType(), isNHLPersonalization);
            markCardsAsSendToPersonalize(cardList);
            log.debug("Report done.");
        } catch (Exception e) {
            log.error("Ошибка записи файла на FTP/SFTP.", e);
            throw e;
        }
    }


    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy-HH-mm-ss-SSS").withZone( ZoneId.systemDefault());


    public void createReport(String reportName, byte[] persoFileData){
        log.debug(String.format("Storing report [%s] to DB", reportName));
        Date currentDate = new Date();

        MlDynamicEntityImpl newObject = commonDao.createNewEntity(metaDataHolder.getEntityClassByName("NHL_PersoTask"));
        newObject.set("file", persoFileData);
        newObject.set("file_filename", reportName);
        newObject.set("createDate", currentDate);
        commonDao.persistWithoutSecurityCheck(newObject);
        log.debug(String.format("Report [%s] has been stored to DB", reportName));
    }

    private String convertReportName(Card card, Date currentDate){
        log.debug("Making name for report...");
        String reportName;
        if(card.getStatus().equals(Card.CardStatus.READY_TO_PERSONALIZE)){
            reportName = "personalize-" + formatter.format( currentDate.toInstant()) + ".req";
            log.debug(String.format("Report name is [%s]", reportName));
            return reportName.toUpperCase();
        } else {
            Calendar now = Calendar.getInstance();
            Calendar previous = Calendar.getInstance();
            // Если в базе имеются предыдущие отчёты
            if(reportDao.getLastBankPersReportDate() != null){
                previous.setTime(reportDao.getLastBankPersReportDate());
                if(reportDao.getCurrentValueFromSequence() == 99){
                    throw new MlApplicationException("Unable to create report with number much than 99");
                }
                if(previous.get(Calendar.DAY_OF_MONTH) != now.get(Calendar.DAY_OF_MONTH)){
                    reportDao.refreshSequence();
                }
            } else {
                reportDao.refreshSequence();
            }

            Long number =  reportDao.getNextValueFromSequence();
            reportName = "A" + convertFilialNumber(getFilial(card)) + "_" + number
                    + "." + String.format("%03d",now.get(Calendar.DAY_OF_YEAR));
            log.debug(String.format("Report name is [%s]", reportName));
            return reportName;
        }
    }

    private String getFilial(Card card){
        if(card.getHolder().getRegion() == null){
            return card.getBankBranch().getFilial();
        } else {
            return card.getHolder().getRegion().getBankBranch().getFilial();
        }
    }


    private static final DateTimeFormatter shortDateFormat = DateTimeFormatter.ofPattern("MMyy").withZone( ZoneId.systemDefault());
    /**
     * Возвращает массив значений для CSV для одной строки
     * @return
     */
    private String [] exportCardToCSV(Card card){
        List<String> result = new ArrayList<>();
        Holder holder = card.getHolder();
        result.add(printStringOrEmpty(card.get(Card.Fields.num.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.lastname.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.firstname.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.secondname.name())));

        result.add(printDateOrEmpty(holder.get(Holder.Fields.birthdate.name())));

        if (card.getExpireDate() != null) {
            result.add(shortDateFormat.format(
                    ( card.getExpireDate()).toInstant())
            );
        } else {
            result.add("");
        }

        result.add(printStringOrEmpty(holder.get(Holder.Fields.rezus.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.gruppa.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.idnhlnumber.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.teamname.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.vidchlenstva.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.statusnhl.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.phone.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.email.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.mailingAddress.name())));
        result.add(printStringOrEmpty(card.get(Card.Fields.cardstatus.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.dopusk.name())));

        String docnum = holder.get(Holder.Fields.documnum.name());
        if (docnum != null){
            docnum = docnum.replaceAll(" ", "");
        }
        result.add(printStringOrEmpty(docnum)
                + printDocumDate(holder.get(Holder.Fields.documdate.name())));
        log.debug(printDocumDate(holder.get(Holder.Fields.documdate.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.drivernum.name()))
                + printDateOrEmpty(holder.get(Holder.Fields.driverdate.name())));
        result.add(printDateOrEmpty(holder.get(Holder.Fields.matchDate.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.matchPlace.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.team.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.playerNumber.name())));
        if(card.get(Card.Fields.flagASCI.name()) != null){
            result.add(printStringOrEmpty(convertFlagASCI(card.get(Card.Fields.flagASCI.name()))));
        } else {
            result.add("01");
        }
        result.add(printStringOrEmpty(holder.get(Holder.Fields.insurcompname.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.insurnum.name())));
        result.add(printDateOrEmpty(holder.get(Holder.Fields.medocmotrdate.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.rezultmedocmotr.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.height.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.weight.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.protivopakaz.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.allegry.name())));
        result.add(printStringOrEmpty(holder.get(Holder.Fields.chronical.name())));
        result.add(printStringOrEmpty(card.getHashFile1()));
        result.add(printStringOrEmpty(card.getHashFile2()));
        result.add(printStringOrEmpty(card.getHashFile3()));
        result.add(printStringOrEmpty(card.getHashFile4()));

        if (holder.get(Holder.Fields.photo.name()) != null){
            result.add(DatatypeConverter.printHexBinary(holder.get(Holder.Fields.photo.name())));
        } else {
            result.add("");
        }

        result.add(printStringOrEmpty(card.getPANHashNumber()));
        return result.toArray(new String[result.size()]);
    }

    private static final DateTimeFormatter middleDateFormat = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneId.systemDefault());

    private String [] firstDataLine(Card card) {
        List<String> result = new ArrayList<>();
        result.add("FH000001APPLICAT  13 ");
        result.add(convertFilialNumber(getFilial(card)));
        result.add("  ");
        LocalDateTime localDateTime = LocalDateTime.now();
        result.add(localDateTime.format(fullDateFormat));
        result.add("00");
        result.add(reportDao.getCurrentValueFromSequence().toString());
        result.add(convertFilialNumber(getFilial(card)));
        result.add("  RNWN");
        result.add(appendWhiteSpaces(2628));
        result.add("*");
        return result.toArray(new String[result.size()]);
    }

    private String [] lastDataLine(int lineNumber, List<Card> cardList ) {
        List<String> result = new ArrayList<>();
        result.add("FT");
        result.add(String.format("%06d", lineNumber));
        result.add(String.format("%06d", cardList.size() * 2));
        result.add(appendWhiteSpaces(2669));
        result.add("*");
        return result.toArray(new String[result.size()]);
    }

    /**
     * Возвращает массив значений строки с данными клиента
     * @return
     */
    private String [] clientDataLine(Card card, int lineNumber){
        List<String> result = new ArrayList<>();
        Holder holder = card.getHolder();
        result.add("RD");
        result.add(String.format("%06d", lineNumber));
        result.add(String.format("%06d", lineNumber));
        result.add(appendWhiteSpaces(4));
        result.add("1");
        result.add("1");
        result.add("00");
        result.add(appendWhiteSpaces(20));
        LocalDateTime localDateTime = LocalDateTime.now();
        result.add(localDateTime.format(middleDateFormat));

        String docnum = holder.getDocNum();
        if(isResident(card)){
            StringBuilder sb = new StringBuilder(docnum);
            sb.insert(2, " ");
            docnum = sb.toString();
        }
        result.add(String.format("%-32s", docnum));
        result.add(String.format("%-6s", holder.getResidency()));
        result.add(String.format("%-32s", holder.getFirstName()));
        result.add(String.format("%-32s", holder.getLastName()));
        result.add(String.format("%-32s", holder.getSecondName()));
        result.add(appendWhiteSpaces(96));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(30));
        result.add(String.format("%-32s", docnum));
        result.add(appendWhiteSpaces(390));
        result.add(String.format("%-24s", holder.getIdNhlNumber()));
        result.add(String.format("%-32s", holder.getIdNhlNumber()));
        result.add(appendWhiteSpaces(44));
        result.add("Y");
        result.add("810");
        result.add("D");
        result.add("00000000000000");
        result.add(appendWhiteSpaces(13));
        result.add(String.format("%-26s", "/" + holder.getEmbossedName().toUpperCase() + "/" + holder.getEmbossedLastName().toUpperCase()));
        result.add(appendWhiteSpaces(170));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(127));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(1157));
        if(isResident(card)){
            result.add(String.format("%-24s", card.getBankBranch().getFilial() + "CBPSDRRP"));
        } else {
            result.add(String.format("%-24s", card.getBankBranch().getFilial() + "CBPSDNRP"));
        }
        result.add(appendWhiteSpaces(219));
        result.add("*");
        return result.toArray(new String[result.size()]);
    }

    /**
     * Возвращает массив значений строки с данными контракта
     * @return
     */
    private String [] contractDataLine(Card card, long lineNumber){
        List<String> result = new ArrayList<>();
        Holder holder = card.getHolder();
        result.add("RD");
        result.add(String.format("%06d", lineNumber));
        result.add(String.format("%06d", lineNumber));
        result.add(appendWhiteSpaces(4));
        result.add("0");
        result.add("2");
        result.add("03");
        result.add(appendWhiteSpaces(10));
        result.add(String.format("%-10s",card.getBankBranch().getABSNumber()));
        LocalDateTime localDateTime = LocalDateTime.now();
        result.add(localDateTime.format(middleDateFormat));

        String docnum = holder.getDocNum();
        if(isResident(card)){
            StringBuilder sb = new StringBuilder(docnum);
            sb.insert(2, " ");
            docnum = sb.toString();
        }
        result.add(String.format("%-32s", docnum));
        result.add(String.format("%-6s", holder.getResidency()));
        result.add(String.format("%-32s", holder.getFirstName()));
        result.add(String.format("%-32s", holder.getLastName()));
        result.add(String.format("%-32s", holder.getSecondName()));
        result.add(appendWhiteSpaces(96));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(30));
        result.add(String.format("%-32s", docnum));
        result.add(appendWhiteSpaces(414));
        result.add(String.format("%-32s", holder.getIdNhlNumber()));
        result.add(String.format("%-32s", holder.getIdNhlNumber()));
        result.add(appendWhiteSpaces(12));
        result.add("N");
        result.add("810");
        result.add("D");
        result.add("00000000000000");
        result.add(appendWhiteSpaces(13));
        result.add(String.format("%-26s", "/" + holder.getEmbossedName().toUpperCase() + "/" + holder.getEmbossedLastName().toUpperCase()));
        result.add(appendWhiteSpaces(130));
        result.add("NCRD");
        result.add(appendWhiteSpaces(36));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(127));
        result.add(String.format("%-32s", holder.getLastName() + holder.getFirstName().charAt(0) + checkSecondName(holder)));
        result.add(appendWhiteSpaces(1157));
        if(isResident(card)){
            result.add(String.format("%-24s", card.getBankBranch().getFilial() + "CBPSDRRPMPCO"));
        } else {
            result.add(String.format("%-24s", card.getBankBranch().getFilial() + "CBPSDNRPMPCO"));
        }
        result.add(appendWhiteSpaces(219));
        result.add("*");
        return result.toArray(new String[result.size()]);
    }


    public byte[] createReportAnswerToBank(List<Card> cardList){
        log.debug("Creating report answer to bank");
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        CustomCSVWriter writer = null;
        String noSeparator = "";
        char escapechar = CSVWriter.NO_ESCAPE_CHARACTER;
        String lineEnd = "";
        try {
            writer = new CustomCSVWriter(new OutputStreamWriter(result, Charset.forName("windows-1251")), noSeparator, escapechar, lineEnd);
            List<String> panHashNumbers = new ArrayList<>();
            for (Card card: cardList){
                panHashNumbers.add(card.getPANHashNumber());
                panHashNumbers.add(";");
            }
            // Записываем в файл все PAN номера персонализированных карт
                writer.writeNext(panHashNumbers.toArray(new String[panHashNumbers.size()]));
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    log.error("Can't close report file");
                }
            }
        }
        log.debug("Report complete");
        return result.toByteArray();
    }


    private String printStringOrEmpty(Object value){
        return value == null ? "" : value.toString();
    }
    private String printHexStringOrEmpty(Object value){
        return value == null ? "" : Long.toHexString((Long)value);
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    private String printDateOrEmpty(Date value){
        return value == null ? "" : dateFormat.format(value);
    }

    private String printDocumDate(Date value){
        return value == null ? "00000000" : dateFormat.format(value);
    }

    private String printPaddingHexStringOrEmpty(Object value) {
        if (value != null) {
            String valueString = Long.toHexString((Long)value);
            if (valueString.length() % 2 == 0) {
                return valueString;
            } else {
                return "0" + valueString;
            }
        }
        return "";
    }

    private boolean isResident(Card card){
        return card.getHolder().getResidency().equals("PR");
    }

    private String checkSecondName(Holder holder) throws MlApplicationException{
        String secondName = holder.getSecondName();
        if(holder.getResidency().equals("PNR") && (holder.getSecondName() == null || holder.getSecondName().equals(""))){
            return "";
        } else if(holder.getResidency().equals("PR") && (holder.getSecondName() == null || holder.getSecondName().equals(""))){
            throw new MlApplicationException(String.format("Resident with ID [%s] has not secondName", holder.getExternalId()));
        } else {
            return String.valueOf(secondName.charAt(0));
        }
    }

    private String convertFlagASCI(boolean flag){
        if(flag){
            return "01";
        } else {
            return "00";
        }
    }
}
