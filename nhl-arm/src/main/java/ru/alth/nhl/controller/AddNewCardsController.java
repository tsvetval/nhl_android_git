package ru.alth.nhl.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.BankBranchDao;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.dao.PersoTypeDao;
import ru.alth.nhl.model.BankBranch;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.service.CardCreateService;
import ru.alth.nhl.service.ValidateHolders;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.filter.FilterBuilder;
import ru.ml.core.filter.result.FilterResult;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.handlers.filter.FilterHelperService;
import ru.ml.web.service.annotations.PageBlockAction;

import java.lang.reflect.Type;

import java.util.List;

/**
 * Контроллер для созданиия карт для списка держателей
 */

@Resource(id = "addNewCardsController", url = "/controller/addnewcards", description = "Mass card adding for holders")
@Controller
public class AddNewCardsController implements MlController {

    private static final Logger log = LoggerFactory.getLogger(AddNewCardsController.class);

    @Inject
    ValidateHolders validateHolders;
    @Inject
    PersoTypeDao persoTypeDao;
    @Inject
    BankBranchDao bankBranchDao;
    @Inject
    HolderDao holderDao;

    @PageBlockAction(action = "getMeta", dataType = MlHttpServletResponse.DataType.JSON)
    public void getMeta(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext context){
        List<PersoType> types = persoTypeDao.getAllPersoTypes();
        List<BankBranch> branches = bankBranchDao.getAllBranches();
        JsonArray dataTypes = new JsonArray();
        types.forEach(type -> {
            JsonObject typeJson = new JsonObject();
            typeJson.add("id", new JsonPrimitive(type.getId()));
            typeJson.add("name", new JsonPrimitive(type.getName()));
            typeJson.add("type", new JsonPrimitive(type.getCardType().toString()));
            dataTypes.add(typeJson);
        });

        JsonArray dataBranches = new JsonArray();
        branches.forEach(branch -> {
            JsonObject branchJson = new JsonObject();
            branchJson.add("id", new JsonPrimitive(branch.getId()));
            branchJson.add("name", new JsonPrimitive(branch.getName()));
            dataBranches.add(branchJson);
        });
        resp.addDataToJson("types", dataTypes);
        resp.addDataToJson("branches", dataBranches);
    }


    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }

    @PageBlockAction(action = "addNewCards", dataType = MlHttpServletResponse.DataType.JSON)
    public void createCards(MlHttpServletRequest req, MlHttpServletResponse resp,  ResourceContext context){
        log.debug("Trying to create new cards...");
        String info = req.getString("additionalData");
        Long persoTypeId = req.getLong("persoTypeId");
        String bankBranchId = req.getString("bankBranchId");

        //Id выбранных холдеров
        List<Long> ids;
        List<Holder> holderList;

        //Если выбрана галка "Всё"
        if(req.hasString("isAllSelected")){
            //Если без использования фильтра
            if(!req.hasString("useFilter")){
                holderList = holderDao.getAllHolders();
            } else {
                FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterHelperService.class).buildByRequest(req);
                FilterResult filterResult = filterBuilder.buildFilterResult();
                holderList = filterResult.getAllRows();
            }
        } else {
            String selectedIds = req.getString("selectedIds");
            Type listType = new TypeToken<List<Long>>() {}.getType();
            ids = new Gson().fromJson(selectedIds, listType);
            holderList = holderDao.findHoldersByIds(ids);
        }
        validateHolders.validateHolder(holderList, info, persoTypeId, bankBranchId);
    }
}
