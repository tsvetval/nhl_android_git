package ru.alth.nhl.controller.card;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.service.CardCheckService;
import ru.alth.nhl.util.TLV;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.dao.CommonDao;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.service.annotations.PageBlockAction;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 *
 */
@Resource(id = "GetCardDataController", url = "/card", description = "get Card Data")
@Controller
public class GetCardDataController  implements MlController {
    private static final Logger log = LoggerFactory.getLogger(GetCardDataController.class);
    @Inject
    private CardDao cardDao;
    @Inject
    protected CardCheckService cardCheckService;


    @PageBlockAction(action = "getCardData", dataType = MlHttpServletResponse.DataType.JSON)
    public void getCardData(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) throws UnsupportedEncodingException {
        // validate parameters
        String randomCardNum = params.getString("file5Data", null);
        if (randomCardNum == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [file5Data]");
        }
        Map<String, String> fileData = TLV.parseTLV(randomCardNum);
        //TODO проверить хеши сразу
        String hash1 = fileData.get("DF51");
        String hash2 = fileData.get("DF52");
        String hash3 = fileData.get("DF53");
        String hash4 = fileData.get("DF54");

        Card card = cardDao.findCardByNum(fileData.get("DF17"));
        if (card == null) {
            throw new MlApplicationException("Данная карта не найдена в системе");
        }


        if (card.getHolder() != null) {
            JsonObject cardDataResult = new JsonObject();
            cardDataResult.add("file1TLVdata", new JsonPrimitive(card.serializeTLVFile1WithOutImage().serialize()));
            cardDataResult.add("file2TLVdata", new JsonPrimitive(card.serializeTLVFile2().serialize()));
            cardDataResult.add("file3TLVdata", new JsonPrimitive(card.serializeTLVFile3().serialize()));
            cardDataResult.add("file4TLVdata", new JsonPrimitive(card.serializeTLVFile4().serialize()));

            if (card.getHolder().get(Holder.Fields.photo.name()) != null){
                cardDataResult.add("photo", new JsonPrimitive(DatatypeConverter.printHexBinary(card.getHolder().get(Holder.Fields.photo.name()))));
            }

            resp.addDataToJson("cardData", cardDataResult);
        } else {
            throw new MlApplicationException("У карты отсутствует держатель");
        }
    }

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw  new MlServerException("not implemented");
    }
}
