package ru.alth.nhl.controller.external;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Holder;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Map;

@Resource(id = "GetHolderIdByDocumnumController", url = "/api/v_1/holder/getByDocumnum", description = "Get Holder`s id by document number Info External API")
@Controller
public class GetHolderIdByDocnumController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(GetHolderIdByDocnumController.class);
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    @Inject
    HolderDao holderDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }
        if (!req.hasString("documnum")) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Missing documnum parameter"));
            return;
        }
        String docNumber = req.getString("documnum");
        Holder holder;

        holder = holderDao.findHolderByDocumNumber(docNumber);
        if(holder == null){
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Holder not found in DB"));
            return;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.add(Holder.Fields.idnhlnumber.name(), nullOrValue(holder.getIdNhlNumber()));
        resp.setJsonData(jsonObject);
    }

    private JsonElement nullOrValue(Object primitive) {
        return primitive == null ? JsonNull.INSTANCE : new JsonPrimitive(primitive.toString());
    }
}
