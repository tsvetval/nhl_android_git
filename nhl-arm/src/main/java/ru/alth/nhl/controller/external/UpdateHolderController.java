package ru.alth.nhl.controller.external;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.service.HolderService;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 */
@Resource(id = "UpdateHolderController", url = "/api/v_1/holder/update", description = "Update Holder External API")
@Controller
public class UpdateHolderController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(UpdateHolderController.class);

    @Inject
    HolderService holderService;
    @Inject
    HolderDao holderDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }

        if (!paramMap.containsKey("idnhlnumber")){
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Missing idnhlnumber parameter"));
            return;
        }
        if(paramMap.containsKey("photo") || paramMap.containsKey("firstname") || paramMap.containsKey("lastname")
                || paramMap.containsKey("secondname") || paramMap.containsKey("birthdate")){
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Full name, birthdate and photo could not be changed"));
            return;
        }
        String nhlNumber = paramMap.get("idnhlnumber");
        Holder holder;
        try {
            holder = holderDao.findHolderByIdNHLNumber(nhlNumber);
        } catch (Throwable e){
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Holder not found in DB"));
            return;
        }
        try {
//            Map<String, Object> paramMap =  req.getRequest().getParameterMap().keySet().stream()
//                    .collect(Collectors.toMap(s -> s, o -> req.getRequest().getParameter(o)));
//

            holderService.updateHolder(holder, paramMap);
            resp.getResponse().setStatus(HttpServletResponse.SC_OK);
        } catch (HolderService.ValidationException e) {
            log.error(e.getMessage(), e);
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive(e.getMessage()));
        }
    }
}