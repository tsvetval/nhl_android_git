package ru.alth.nhl.controller.external;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Holder;
import ru.alth.nhl.service.HolderService;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 */
@Resource(id = "CreateHolderController", url = "/api/v_1/holder/create", description = "Create Holder External API")
@Controller
public class CreateHolderController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(CreateHolderController.class);

    @Inject
    HolderService holderService;


    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }


        // Fill Object with values
        try {
//            Map<String, Object> paramMap = req.getRequest().getParameterMap().keySet().stream()
//                    .collect(Collectors.toMap(s -> s, o -> req.getRequest().getParameter(o)));

            Holder holder = holderService.createHolder(paramMap);
            resp.getResponse().setStatus(HttpServletResponse.SC_OK);
            resp.addDataToJson("id", new JsonPrimitive(holder.getIdNhlNumber()));
        } catch (HolderService.ValidationException e) {
            log.error(e.getMessage(), e);
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive(e.getMessage()));
        }
    }
}
