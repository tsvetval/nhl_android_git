package ru.alth.nhl.controller.card;

import com.google.gson.Gson;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.service.CardCommandsService;
import ru.alth.nhl.util.crypto.*;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.service.annotations.PageBlockAction;

import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Resource(id = "ReadCardController", url = "/card/read", description = "ReadCard Controller")
@Controller
public class ReadCardController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(ReadCardController.class);

    private static final String readKey1 = "5D07291A0B73E5EA2FBCDA924FC4346D";
    private static final String readKey2 = "989780E6C88331BFDAF2A2FE4AB50775";
    private static final String readKey3 = "8075A2E561C894E0B5F2B59D640BAE37";
    private static final String readKey4 = "5DF8A80E23757986583D58A4D9B9E3D3";
    private static final String readKey5 = "3107B6BAFE8C4094548C7C794AD98C86";

    @Inject
    CardCommandsService cardCommandsService;
    @Inject
    private SocialApplet socialApplet;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }

    @PageBlockAction(action = "getReadCommands", dataType = MlHttpServletResponse.DataType.JSON)
    public void getReadCommands(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) {
        // validate parameters
        String randomCardNum = params.getString("randomCardNum", null);
        if (randomCardNum == null){
            throw new MlApplicationException("Не установлен обязательный параметр [randomCardNum]");
        }
        Long fileNumber = params.getLong("fileNumber", null);
        if (fileNumber == null){
            throw new MlApplicationException("Не установлен обязательный параметр [fileNumber]");
        }
        Long fileSize = null;
        try {
            fileSize = Long.parseLong(params.getString("fileSize", null), 16);
        } catch (Throwable e){
            throw new MlApplicationException("Не установлен обязательный параметр [fileSize]");
        }

        // generate commands
        List<String> commandsResult;
        try {
            commandsResult = cardCommandsService.getReadFileCommands(randomCardNum, fileNumber, fileSize);
        } catch (Exception e) {
            log.error("Error while creating Read commands", e);
            throw new MlApplicationException("Ошибка создания команд чтения карты, обратитесь к администратору");
        }

        // response with JSON
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("result", gson.toJsonTree(commandsResult));

    }


    @PageBlockAction(action = "getReadCommandsJavaCard", dataType = MlHttpServletResponse.DataType.JSON)
    public void getReadCommandsJavaCard(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) {
        log.debug("Getting read commands from server");
        // Response initial update
        String respStr = params.getString("respStr", null);
        log.debug("Response initial update " + respStr);
        // Размер файла для чтения
        Long fileSize = Long.parseLong(params.getString("fileSize", null), 16);
        log.debug("FileSize " + fileSize);
        // Response селекта с серийным номером карты
        String selectRes = params.getString("selectRes", null);
        log.debug("Response of main select " + selectRes);
        String randomNumber = params.getString("randNumber", null);
        log.debug("Random card number " + randomNumber);
        String readKeyNumber = params.getString("readKeyNumber", null);
        log.debug("Read key number " + readKeyNumber);
        // Счётчик аутентификаций
        String counter = respStr.substring(2, 6);
        log.debug("Auth amount " + counter);
        List<String> commandsList = new ArrayList<>();

        String extAuthCommand;
        try{
            String cardKey = socialApplet.diverKey(selectRes, readKey5);
            log.debug("Card key " + cardKey);
            // Вычисление сессионного ключа
            byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(cardKey),
                    DatatypeConverter.parseHexBinary(String.format("%-32s", counter).replace(" ", "0")), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
            log.debug("Session key " + DatatypeConverter.printHexBinary(sessionKey));
            extAuthCommand = socialApplet.createExternalAuthenticationCommand(respStr, randomNumber, readKeyNumber, sessionKey);
            log.debug("Command for external authentication " + extAuthCommand);
            // Добавляем команду external update
            commandsList.add(extAuthCommand);
            // Добавляем список команд на чтение файла
            commandsList.addAll(socialApplet.generateReadCmdList(fileSize.intValue(), sessionKey, extAuthCommand.substring(extAuthCommand.length() - 16)));
        } catch (Exception e) {
            log.error("Error while creating Read commands", e);
            throw new MlApplicationException("Ошибка создания команд чтения карты, обратитесь к администратору");
        }

        // response with JSON
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("result", gson.toJsonTree(commandsList));

    }

    private String getReadKey(String keyNumber){
        if(keyNumber.equals("01")) return readKey1;
        if(keyNumber.equals("02")) return readKey2;
        if(keyNumber.equals("03")) return readKey3;
        if(keyNumber.equals("04")) return readKey4;
        if(keyNumber.equals("05")) return readKey5;
        return null;
    }
}
