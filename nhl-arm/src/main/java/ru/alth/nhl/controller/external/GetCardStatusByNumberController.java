package ru.alth.nhl.controller.external;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.Map;


@Resource(id = "GetCardStatusByNumberController", url = "/api/v_1/card", description = "Get card`s status by number External API")
@Controller
public class GetCardStatusByNumberController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(GetCardStatusByNumberController.class);

    @Inject
    private CardDao cardDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }
        if (!req.hasString("num")) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Missing num parameter"));
            return;
        }
        String num = req.getString("num");
        Card card;

        card = cardDao.findCardByNum(num);
        if(card == null){
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Card not found in DB"));
            return;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.add(Card.Fields.status.name(), nullOrValue(card.getStatus().name()));
        resp.setJsonData(jsonObject);
    }

    private JsonElement nullOrValue(Object primitive) {
        return primitive == null ? JsonNull.INSTANCE : new JsonPrimitive(primitive.toString());
    }
}
