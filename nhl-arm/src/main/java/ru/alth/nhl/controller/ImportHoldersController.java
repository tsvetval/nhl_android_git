package ru.alth.nhl.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.common.dto.HolderDto;
import ru.alth.nhl.dao.ReportDao;
import ru.alth.nhl.service.ImportHolders;
import ru.alth.nhl.service.ImportPersoServiceImpl;
import ru.alth.nhl.util.ZipReader;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.prop.Property;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.helper.DownloadHelper;
import ru.ml.web.service.annotations.PageBlockAction;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by i_tovstyonok on 06.04.2016.
 */

@Resource(id = "importHoldersController", url = "/controller/import/holders", description = "Import holders data from file")
@Controller
public class ImportHoldersController implements MlController {

    private static final Logger log = LoggerFactory.getLogger(ImportHoldersController.class);
    private static final String ZIP_PATTERN = "^\\d{14}[_]\\d+(.zip)$";

    @Inject
    private ZipReader zipReader;
    @Inject
    private ImportHolders importHolders;
    @Inject
    private ReportDao reportDao;
    @Inject
    private ImportPersoServiceImpl importPersoServiceImpl;

    @PageBlockAction(action = "processImport")
    public void processImport (MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext context) {

        Long userId = null;
        if (req.getRequest().getSession().getAttribute("user-id") != null) {
            userId = (Long) req.getRequest().getSession().getAttribute("user-id");
        }
        String filename = req.getString("filename");
        String tempPath = String.format("%s/user_%d/%s", Property.getTempDir(), userId, filename);

        String errorsReport;
        String errorReportLink = "";
        String status = "success";
        List<HolderDto> holderDtoList = new ArrayList<>();
        List<String[]> errorsList = new ArrayList<>();
        try {
            HashMap<String, byte[]> unzippedFile =  zipReader.unzipArchive(tempPath);
            if(!importHolders.isReportNameCorrect(filename, ZIP_PATTERN) || unzippedFile.get(filename.substring(0, filename.indexOf("."))) == null){
                throw new MlApplicationException("Выбранный файл не поддерживается системой");
            }
            holderDtoList = importHolders.parseXLSXFile(filename, unzippedFile);
            if(holderDtoList.size() + 1 != Integer.valueOf(filename.substring(filename.indexOf('_') + 1, filename.indexOf('.')))){
                throw new MlApplicationException("Количество записей в файле отличается от указанного в названии");
            }
            errorsList = importHolders.saveHolders(holderDtoList);
            importPersoServiceImpl.createReport(IOUtils.toByteArray(new FileInputStream(tempPath)), filename);
            if(!errorsList.isEmpty()){
                errorsReport = importHolders.writeCSVWithErrorHolders(errorsList);
                status = "partial";
                errorReportLink = DownloadHelper.generateDownloadLink(reportDao.getPersoTaskByName(errorsReport), errorsReport, req.getRequest().getServletContext());
            }
        } catch(IOException e) {
            log.debug(String.format("Unable to import file [%s]", filename), e);
        }

        JsonObject mockup = new JsonObject();
        JsonObject count = new JsonObject();
        count.add("success", new JsonPrimitive(holderDtoList.size() - errorsList.size()));
        count.add("all", new JsonPrimitive(holderDtoList.size()));
        mockup.add("count", count);
        mockup.add("status", new JsonPrimitive(status));
        mockup.add("errorFile", new JsonPrimitive(errorReportLink));
        resp.addDataToJson("result", mockup);
    }

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

    }
}
