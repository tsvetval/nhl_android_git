package ru.alth.nhl.controller.external;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.Holder;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Resource(id = "GetHolderInfoController", url = "/api/v_1/holder", description = "Get Holder Info External API")
@Controller
public class GetHolderInfoController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(GetHolderInfoController.class);
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    @Inject
    HolderDao holderDao;
    @Inject
    CardDao cardDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }
        if (!req.hasString("idnhlnumber")) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Missing idnhlnumber parameter"));
            return;
        }
        String nhlNumber = req.getString("idnhlnumber");
        Holder holder;
        try {
            holder = holderDao.findHolderByIdNHLNumber(nhlNumber);
        } catch (Throwable e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Holder not found in DB"));
            return;
        }
        resp.setJsonData(serializeHolder(holder));
    }

    private JsonElement serializeHolder(Holder holder) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(Holder.Fields.lastname.name(), nullOrValue(holder.getLastName()));
        jsonObject.add(Holder.Fields.firstname.name(), nullOrValue(holder.getFirstName()));
        jsonObject.add(Holder.Fields.secondname.name(), nullOrValue(holder.getSecondName()));
        jsonObject.add(Holder.Fields.birthdate.name(), nullOrDate(holder.getBirthdate()));
        jsonObject.add(Holder.Fields.external_id.name(), nullOrValue(holder.getExternalId()));
        jsonObject.add(Holder.Fields.gruppa.name(), nullOrValue(holder.getGroup()));
        jsonObject.add(Holder.Fields.rezus.name(), nullOrValue(holder.getRezus()));
        jsonObject.add(Holder.Fields.teamname.name(), nullOrValue(holder.getTeamName()));
        jsonObject.add(Holder.Fields.vidchlenstva.name(), nullOrValue(holder.getVidChlenstva()));
        jsonObject.add(Holder.Fields.statusnhl.name(), nullOrValue(holder.getStatusNhl()));
        jsonObject.add(Holder.Fields.phone.name(), nullOrValue(holder.getPhone()));
        jsonObject.add(Holder.Fields.email.name(), nullOrValue(holder.getEmail()));
        jsonObject.add(Holder.Fields.mailingAddress.name(), nullOrValue(holder.getMailingAddress()));
        jsonObject.add(Holder.Fields.residenceAddress.name(), nullOrValue(holder.getResidenceAddress()));
        jsonObject.add(Holder.Fields.sex.name(), nullOrValue(holder.getSex()));
        jsonObject.add(Holder.Fields.birthplace.name(), nullOrValue(holder.getBirthPlace()));
        jsonObject.add(Holder.Fields.documPlace.name(), nullOrValue(holder.getDocumPlace()));
        jsonObject.add(Holder.Fields.subdivisionCode.name(), nullOrValue(holder.getSubdivisionCode()));
        jsonObject.add(Holder.Fields.insurcompname.name(), nullOrValue(holder.getInsurCompname()));
        jsonObject.add(Holder.Fields.insurnum.name(), nullOrValue(holder.getInsurNum()));
        jsonObject.add(Holder.Fields.medocmotrdate.name(), nullOrDate(holder.getMedocmotrDate()));
        jsonObject.add(Holder.Fields.rezultmedocmotr.name(), nullOrValue(holder.getResultMedOsmotr()));
        jsonObject.add(Holder.Fields.height.name(), nullOrValue(holder.getHeight()));
        jsonObject.add(Holder.Fields.weight.name(), nullOrValue(holder.getWeight()));
        jsonObject.add(Holder.Fields.protivopakaz.name(), nullOrValue(holder.getProtivopakaz()));
        jsonObject.add(Holder.Fields.allegry.name(), nullOrValue(holder.getAllegry()));
        jsonObject.add(Holder.Fields.chronical.name(), nullOrValue(holder.getChronical()));
        jsonObject.add(Holder.Fields.residency.name(), nullOrValue(holder.getResidency()));
        jsonObject.add(Holder.Fields.documnum.name(), nullOrValue(holder.getDocNum()));
        jsonObject.add(Holder.Fields.documdate.name(), nullOrDate(holder.getDocumDate()));
        jsonObject.add(Holder.Fields.drivernum.name(), nullOrValue(holder.getDrivernum()));
        jsonObject.add(Holder.Fields.driverdate.name(), nullOrDate(holder.getDriverDate()));
        jsonObject.add(Holder.Fields.matchDate.name(), nullOrDate(holder.getMatchDate()));
        jsonObject.add(Holder.Fields.matchPlace.name(), nullOrValue(holder.getMatchPlace()));
        jsonObject.add(Holder.Fields.team.name(), nullOrValue(holder.getTeamName()));
        jsonObject.add(Holder.Fields.playerNumber.name(), nullOrValue(holder.getPlayerNumber()));
        jsonObject.add("cards", seializeCards(cardDao.findCardsByHolder(holder)));
        return jsonObject;
    }

    private JsonElement seializeCards(List<Card> cards) {
        JsonArray result = new JsonArray();
        for (Card card : cards) {
            JsonObject jsonCard = new JsonObject();
            jsonCard.add("id", nullOrValue(card.getNum()));
            result.add(jsonCard);
        }
        return result;
    }

    private JsonElement nullOrValue(Object primitive) {
        return primitive == null ? JsonNull.INSTANCE : new JsonPrimitive(primitive.toString());
    }

    private JsonElement nullOrDate(Date date) {
        return  date == null ? JsonNull.INSTANCE : new JsonPrimitive(dateFormat.format(date));
    }
}
