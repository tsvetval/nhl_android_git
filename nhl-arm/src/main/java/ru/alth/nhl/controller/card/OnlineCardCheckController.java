package ru.alth.nhl.controller.card;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.CardCheckService;
import ru.alth.nhl.controller.exchange.ExportPersoController;
import ru.alth.nhl.util.TLV;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.helper.DownloadHelper;
import ru.ml.web.service.annotations.PageBlockAction;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Класс проверки онлайн статуса карты
 */
@Resource(id = "checkCardController", url = "/card/check", description = "check card online status")
@Controller
public class OnlineCardCheckController implements MlController {

    private static final Logger log = LoggerFactory.getLogger(OnlineCardCheckController.class);
    @Inject
    private CardDao cardDao;
    @Inject
    protected CardCheckService cardCheckService;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) {
        // Получаем идендификатор карта и хеши файлов на карте
        boolean validationDone;

        String cardId = req.getString("cardId", null);
        String hash1 = req.getString("hash1", null);
        String hash2 = req.getString("hash2", null);
        String hash3 = req.getString("hash3", null);
        String hash4 = req.getString("hash4", null);

        if (cardId == null || hash1 == null || hash2 == null || hash3 == null || hash4 == null) {
            validationDone = false;
        } else {
            // Ищем карту
            Card card = cardDao.findCardByNum(cardId);
            CardCheckService.ValidationResult result = cardCheckService.checkCardStatus(card, hash1, hash2, hash3, hash4);
            validationDone = result.getValidationResult();
        }

        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        if (validationDone) {
            resp.addDataToJson("result", new JsonPrimitive("OK"));
        } else {
            resp.addDataToJson("result", new JsonPrimitive("FAIL"));
        }
    }

    @PageBlockAction(action = "checkCardAndGetHolder", dataType = MlHttpServletResponse.DataType.JSON)
    public void checkCardAndGetHolder(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) throws IOException {
        // validate parameters
        String randomCardNum = params.getString("file5Data", null);
        if (randomCardNum == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [file5Data]");
        }
        Map<String, String> fileData = TLV.parseTLV(randomCardNum);
        String hash1 = fileData.get("DF51");
        String hash2 = fileData.get("DF52");
        String hash3 = fileData.get("DF53");
        String hash4 = fileData.get("DF54");

        Card card = cardDao.findCardByNum(fileData.get("DF17"));
        if (card == null) {
            throw new MlApplicationException("Данная карта не найдена в системе");
        }

        CardCheckService.ValidationResult result = cardCheckService.checkCardStatus(card, hash1, hash2, hash3, hash4);
        card.getHolder();


        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("checkResult", gson.toJsonTree(result));

        if (card.getHolder() != null) {
            JsonObject cardDataResult = new JsonObject();
            cardDataResult.add("holderTitle", new JsonPrimitive(card.getHolder().getTitle()));
            cardDataResult.add("holderDocNum", new JsonPrimitive(card.getHolder().getDocNum()));

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
            cardDataResult.add("holderBirthDate", new JsonPrimitive(card.getHolder().getBirthdate() != null ? dateFormatter.format(card.getHolder().getBirthdate()) : ""));

            byte [] holderPhoto = card.getHolder().getPhoto();
            if (holderPhoto != null) {
                String holderPhotoLink = DownloadHelper.generateDownloadLink(holderPhoto, (String) card.getHolder().getId().toString() + "_" + card.getHolder().get("photo_filename"), params.getRequest().getServletContext());
                if (holderPhotoLink != null) {
                    cardDataResult.add("holderPhoto", new JsonPrimitive(holderPhotoLink));
                }
            }

            cardDataResult.add("cardId", new JsonPrimitive(card.getId()));
            cardDataResult.add("cardStatus", new JsonPrimitive(card.getStatus() != null ? card.getStatus().description : ""));

            resp.addDataToJson("cardData", cardDataResult);
        }
    }

}