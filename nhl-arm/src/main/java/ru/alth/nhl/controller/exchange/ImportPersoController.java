package ru.alth.nhl.controller.exchange;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import ru.alth.nhl.service.ImportPersoServiceImpl;
import ru.alth.nhl.service.ImportResult;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.model.security.MlUser;
import ru.ml.core.services.AccessService;
import ru.ml.prop.Property;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.service.annotations.PageBlockAction;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Контроллер для импорта файлов из центра персонализации
 */
@Resource(id = "importPersoController", url = "/import/perso/doimport", description = "Import Perso Controller")
@Controller
public class ImportPersoController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(ImportPersoController.class);
    private ImportResult result = null;

    @Inject
    ImportPersoServiceImpl importPersoService;
    @Inject
    AccessService accessService;


    @PageBlockAction(action = "doImport", dataType = MlHttpServletResponse.DataType.JSON)
    public void doImport(MlHttpServletRequest params, MlHttpServletResponse resp,  ResourceContext context) {

        log.debug("StartImportFile");
        MlUser user = accessService.getCurrentUser();
        String filename = params.getString("filename");
        final Path tempPath = Paths.get(String.format("%s/user_%d/%s", Property.getTempDir(), user.getId(), filename));

        try {
            byte[] persoFileData = Files.readAllBytes(tempPath);
            log.debug(String.format("file [%s] size = [%s] bytes", filename, persoFileData.length));
            InputStream myInputStream = new ByteArrayInputStream(persoFileData);

            log.debug(String.format("Start import file with path %1$s", tempPath.toString()));
            List<ImportPersoServiceImpl.CardPersoResponse> cardPersoResponseList = importPersoService.parsePersoAnswerXML(new InputSource(myInputStream));
            importPersoService.changeCardStatus(cardPersoResponseList);
            // создаем файл с отчетом
            importPersoService.createReport(persoFileData, tempPath.getFileName().toString());

        } catch (Exception e) {
            log.error(String.format("Error while importing file %1$s", filename));
        }
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(result);
        resp.addDataToJson("result", new JsonPrimitive(json));
    }


    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }
}
