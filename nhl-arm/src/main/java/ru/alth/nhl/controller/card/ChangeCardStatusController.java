package ru.alth.nhl.controller.card;

import com.google.gson.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.model.PersoType;
import ru.alth.nhl.schedule.FTPExportTask;
import ru.alth.nhl.service.reports.exportReports.ExportReport;
import ru.alth.nhl.service.reports.ReportStrategy;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.holders.EnumHolder;
import ru.ml.core.holders.MetaDataHolder;
import ru.ml.core.model.system.MlAttr;
import ru.ml.core.model.system.MlEnum;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.service.annotations.PageBlockAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер для переключения статуса карт
 */

@Resource(id = "changeCardStatusController", url = "/card/changeCardStatus/change", description = "Change card status controller")
@Controller
public class ChangeCardStatusController implements MlController {

    private static final Logger log = LoggerFactory.getLogger(ChangeCardStatusController.class);

    @Inject
    CardDao cardDao;
    @Inject
    EnumHolder enumholder;
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    private FTPExportTask ftpExportTask;

    @PageBlockAction(action = "getStatusList", dataType = MlHttpServletResponse.DataType.JSON)
    public void getStatusList(MlHttpServletRequest params, MlHttpServletResponse resp,  ResourceContext context){
        log.debug("Getting list of cards");
        Card card = cardDao.findById(params.getLong("cardId"), Card.class);
        JsonArray jsonArray = new JsonArray();

        if(card.getStatus().equals(Card.CardStatus.ACTIVE)){
            jsonArray.add(toJson(Card.CardStatus.BLOCKED));
        } else if(card.getStatus().equals(Card.CardStatus.BLOCKED)){
            jsonArray.add(toJson(Card.CardStatus.ACTIVE));
        } else if(card.getStatus().equals(Card.CardStatus.PERSONALIZED)){
            jsonArray.add(toJson(Card.CardStatus.ACTIVE));
        } else if(card.getStatus().equals(Card.CardStatus.PERSONALIZATION_ERROR)){
            jsonArray.add(toJson(Card.CardStatus.READY_TO_PERSONALIZE));
        }

        resp.addDataToJson("currentStatus", new JsonPrimitive(card.getStatus().description));
        resp.addDataToJson("statusList", jsonArray);
    }

    private JsonElement toJson(Card.CardStatus cardStatus) {
        MlAttr mlAttr = metaDataHolder.getAttr("Card", "status");
        JsonObject object = new JsonObject();
        MlEnum status = enumholder.getEnum(mlAttr, cardStatus.name());
        object.add("id", new JsonPrimitive(status.getCode()));
        object.add("name", new JsonPrimitive(status.getTitle()));
        return object;
    }


    @PageBlockAction(action = "change", dataType = MlHttpServletResponse.DataType.JSON)
    public void changeStatus(MlHttpServletRequest params, MlHttpServletResponse resp,  ResourceContext context) throws Exception {
        log.debug("Changing card status...");
        String statusCode = params.getString("statusCode");
        Card card = cardDao.findById(params.getLong("cardId"), Card.class);
        if (card == null){
            throw new MlApplicationException(String.format("Карта с id = [%s] не найдена", params.getLong("cardId")));
        }

        switch (statusCode) {

            case "BLOCKED":
                card.setStatus(Card.CardStatus.BLOCKED);
                break;

            case "PERSONALIZED":
                card.setStatus(Card.CardStatus.PERSONALIZED);
                break;

            case "ACTIVE":
                card.setStatus(Card.CardStatus.ACTIVE);
                break;

            case "READY_TO_PERSONALIZE":
                card.setStatus(Card.CardStatus.READY_TO_PERSONALIZE);
                break;

            default:
                log.error("Статус не выбран");
                throw new MlApplicationException("Статус карты не выбран!");
        }
        cardDao.persistTransactionalWithSecurityCheck(card, false, false);
        log.debug("Changed to "+ card.getStatus());
    }

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }
}
