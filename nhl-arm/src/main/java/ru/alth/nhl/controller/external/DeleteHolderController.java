package ru.alth.nhl.controller.external;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import ru.alth.nhl.dao.CardDao;
import ru.alth.nhl.dao.HolderDao;
import ru.alth.nhl.model.Holder;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.Map;

@Resource(id = "DeleteHolderController", url = "/api/v_1/holder/delete", description = "Delete Holder External API")
@Controller
public class DeleteHolderController implements MlController {
    @Inject
    HolderDao holderDao;
    @Inject
    CardDao cardDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }

        if (!paramMap.containsKey("idnhlnumber")){
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Missing idnhlnumber parameter"));
            return;
        }
        String nhlNumber = paramMap.get("idnhlnumber");
        Holder holder;
        try {
            holder = holderDao.findHolderByIdNHLNumber(nhlNumber);
        } catch (Throwable e){
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Holder not found in DB"));
            return;
        }
        if(!cardDao.findCardsByHolder(holder).isEmpty()){
            resp.getResponse().setStatus(HttpServletResponse.SC_FORBIDDEN);
            resp.addDataToJson("message", new JsonPrimitive("Holder can't be delete because holder have cards."));
            return;
        }
        holderDao.removeTransactional(holder);
        resp.setJsonData(new JsonObject());
    }
}
