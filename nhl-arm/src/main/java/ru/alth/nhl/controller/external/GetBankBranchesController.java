package ru.alth.nhl.controller.external;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import liquibase.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.dao.BankBranchDao;
import ru.alth.nhl.model.BankBranch;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@Resource(id = "GetBankBranchesController", url = "/api/v_1/bank", description = "Get list of bank branches External API")
@Controller
public class GetBankBranchesController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(GetBankBranchesController.class);

    @Inject
    private BankBranchDao bankBranchDao;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        Map<String, String> paramMap;
        try {
            String body = StreamUtil.getStreamContents(req.getRequest().getInputStream());
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            paramMap = new Gson().fromJson(body, type);

        } catch (Exception e) {
            resp.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.addDataToJson("message", new JsonPrimitive("Wrong json body"));
            return;
        }

        List<BankBranch> bankBranchList;

        bankBranchList = bankBranchDao.getAllBranches();

        if(bankBranchList.isEmpty()){
            resp.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            resp.addDataToJson("message", new JsonPrimitive("Bank branches not found in DB"));
            return;
        }

        resp.setJsonData(serializeBankBranches(bankBranchList));
    }

    private JsonElement serializeBankBranches(List<BankBranch> bankBranchList) {
        JsonArray result = new JsonArray();
        for (BankBranch branch : bankBranchList) {
            JsonObject jsonBranch = new JsonObject();
            jsonBranch.add("name", nullOrValue(branch.getName()));
            jsonBranch.add("ABS_number", nullOrValue(branch.getABSNumber()));
            result.add(jsonBranch);
        }
        return result;
    }

    private JsonElement nullOrValue(Object primitive) {
        return primitive == null ? JsonNull.INSTANCE : new JsonPrimitive(primitive.toString());
    }
}
