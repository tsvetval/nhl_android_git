package ru.alth.nhl.controller.exchange;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.PersonalizeServiceImpl;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.dao.CommonDao;
import ru.ml.core.model.util.MlUtil;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.utils.controller.UtilController;
import ru.ml.web.utils.controller.utils.AbstractUtilController;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Контроллер для создания файла персонализации
 */
@Resource(id = "ExportPersoController", url = "/export/personalize", description = "Export to CSV Personalize Data")
@Controller
public class ExportPersoController extends AbstractUtilController implements MlController, UtilController {

    private static final Logger log = LoggerFactory.getLogger(ExportPersoController.class);
    @Inject
    private CommonDao commonDao;
    @Inject
    protected PersonalizeServiceImpl personalizeService;

    @Override
    public void serve(MlHttpServletRequest req, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        // Получаем выбранные объекты
        //TODO обработка ошибки и выдать предупреждение если ничего не выбрано
        List<Card> cardList =  (List<Card>)(List<?>)this.getSelectedList(req);

        // Создаем файл и обновляем держателей
        byte[] output = personalizeService.createReportFile(cardList);
        String reportName = personalizeService.markCardsAsSendToPersonalizeAndCreateReport(cardList, output);

        resp.addDownloadData(output, reportName);

    }


    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlUtil mlUtil, MlHttpServletResponse mlHttpServletResponse) throws IOException, ParseException, DatatypeConfigurationException {
        this.serve(mlHttpServletRequest, mlHttpServletResponse, null);
    }
}
