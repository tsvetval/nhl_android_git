package ru.alth.nhl.controller.card;

import com.google.gson.Gson;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.alth.nhl.model.Card;
import ru.alth.nhl.service.CardCheckService;
import ru.alth.nhl.service.CardCommandsService;
import ru.alth.nhl.util.crypto.EnCipherDirection;
import ru.alth.nhl.util.crypto.EnCipherMode;
import ru.alth.nhl.util.crypto.SMUtil;
import ru.alth.nhl.util.crypto.SocialApplet;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.ml.core.dao.CommonDao;
import ru.ml.web.common.MlHttpServletRequest;
import ru.ml.web.common.MlHttpServletResponse;
import ru.ml.web.service.annotations.PageBlockAction;

import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Resource(id = "WriteCardController", url = "/card/write", description = "WriteCard Controller")
@Controller
public class WriteCardController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(WriteCardController.class);


    private static final String writeKey1 = "49C2B5296EF420A4A229E68AD9E543A4";
    private static final String writeKey2 = "16F862073104EA8F0801081697CECE6B";
    private static final String writeKey3 = "49295D9ECB621394A864F880E92C1CBF";
    private static final String writeKey4 = "DCA43E25BA1AA19254C7802361B9D932";
    private static final String writeKey5 = "2F3D68EC2C46B9D9D07A437AEA73F47A";

    @Inject
    CardCommandsService cardCommandsService;
    @Inject
    private CommonDao commonDao;
    @Inject
    private CardCheckService cardCheckService;
    @Inject
    private SocialApplet socialApplet;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }

    @PageBlockAction(action = "getWriteCommands", dataType = MlHttpServletResponse.DataType.JSON)
    public void getUpdateCardCommands(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) {
        Long cardId = params.getLong("cardId", null);
        if (cardId == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [cardId]");
        }
        String randomCardNum = params.getString("randomCardNum", null);
        if (randomCardNum == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [randomCardNum]");
        }
        Long fileNumber = params.getLong("fileNumber", null);
        if (fileNumber == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [fileNumber]");
        }

        Card card = (Card) commonDao.findById(cardId, Card.class);
        if (card == null){
            throw new MlApplicationException("Карта не найдена в системе cardId = " + cardId);
        }

        // generate commands
        List<String> commandsResult;
        try {
            // Устанавливаем признак ASCI
            if(card.getFlagASCI() == null || !card.getFlagASCI()){
                cardCheckService.changeFlagASCI(card);
            }

            String newFileData = null;
            switch (fileNumber.intValue()) {
                case 2:
                    newFileData = card.serializeTLVFile2().serialize();
                    break;
                case 3:
                    newFileData = card.serializeTLVFile3().serialize();
                    break;
                case 4:
                    newFileData = card.serializeTLVFile4().serialize();
                    break;
                case 5:
                    newFileData = card.serializeTLVFile5().serialize();
                    break;
                default:
                    throw new MlApplicationException("Недопустимое значение параметра [fileNumber] :" + fileNumber);
            }
            commandsResult = cardCommandsService.getWriteFileCommands(randomCardNum, fileNumber, newFileData);
        } catch (Exception e) {
            log.error("Error while creating Write commands", e);
            throw new MlApplicationException("Ошибка создания команд записи карты, обратитесь к администратору");
        }

        // response with JSON
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("result", gson.toJsonTree(commandsResult));

    }

    @PageBlockAction(action = "getDeleteCreateCommands", dataType = MlHttpServletResponse.DataType.JSON)
    public void getDeleteCreateCardCommands(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) {
        Long cardId = params.getLong("cardId", null);
        if (cardId == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [cardId]");
        }
        String randomCardNum = params.getString("randomCardNum", null);
        if (randomCardNum == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [randomCardNum]");
        }
        Long fileNumber = params.getLong("fileNumber", null);
        if (fileNumber == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [fileNumber]");
        }

        Card card = (Card) commonDao.findById(cardId, Card.class);
        if (card == null){
            throw new MlApplicationException("Карта не найдена в системе cardId = " + cardId);
        }

        // generate commands
        List<String> commandsResult;
        try {
            // Устанавливаем признак ASCI
            if(card.getFlagASCI() == null || !card.getFlagASCI()){
                cardCheckService.changeFlagASCI(card);
            }
            String newFileData = null;
            switch (fileNumber.intValue()) {
                case 2:
                    newFileData = card.serializeTLVFile2().serialize();
                    break;
                case 3:
                    newFileData = card.serializeTLVFile3().serialize();
                    break;
                case 4:
                    newFileData = card.serializeTLVFile4().serialize();
                    break;
                default:
                    throw new MlApplicationException("Недопустимое значение параметра [fileNumber] :" + fileNumber);
            }
            commandsResult = cardCommandsService.getDeleteCreateFileCommands(randomCardNum, fileNumber, newFileData);
        } catch (Exception e) {
            log.error("Error while creating Create/Delete commands", e);
            throw new MlApplicationException("Ошибка создания команд создания/удаления записией карты, обратитесь к администратору");
        }

        // response with JSON
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("result", gson.toJsonTree(commandsResult));

    }


    @PageBlockAction(action = "getWriteCommandsJavaCard", dataType = MlHttpServletResponse.DataType.JSON)
    public void getWriteCommandsJavaCard(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext context) {
        Long cardId = params.getLong("cardId", null);
        if (cardId == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [cardId]");
        }
        // Response initial update
        String respStr = params.getString("respStr", null);
        if (respStr == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [respStr]");
        }
        Long fileNumber = params.getLong("fileNumber", null);
        if (fileNumber == null) {
            throw new MlApplicationException("Не установлен обязательный параметр [fileNumber]");
        }

        Card card = (Card) commonDao.findById(cardId, Card.class);
        if (card == null){
            throw new MlApplicationException("Карта не найдена в системе cardId = " + cardId);
        }

        // Response селекта с серийным номером карты
        String selectRes = params.getString("selectRes", null);
        log.debug("Response of main select " + selectRes);
        String randomNumber = params.getString("randNumber", null);
        log.debug("Random card number " + randomNumber);
        // Счётчик аутентификаций
        String counter = respStr.substring(2, 6);
        log.debug("Auth amount " + counter);
        String extAuthCommand;

        // generate commands
        List<String> commandsResult = new ArrayList<>();
        try {
            String newFileData = null;
            // Устанавливаем признак ASCI
            if(card.getFlagASCI() == null || !card.getFlagASCI()){
                cardCheckService.changeFlagASCI(card);
            }
            switch (fileNumber.intValue()) {
                case 2:
                    newFileData = card.serializeTLVFile2().serialize();
                    break;
                case 3:
                    newFileData = card.serializeTLVFile3().serialize();
                    break;
                case 4:
                    newFileData = card.serializeTLVFile4().serialize();
                    break;
                case 5:
                    newFileData = card.serializeTLVFile5().serialize();
                    break;
                default:
                    throw new MlApplicationException("Недопустимое значение параметра [fileNumber] :" + fileNumber);
            }

            String cardKey = socialApplet.diverKey(selectRes, getWriteKey(fileNumber.intValue()));
            log.debug("Card key " + cardKey);
            // Вычисление сессионного ключа
            byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(cardKey),
                    DatatypeConverter.parseHexBinary(String.format("%-32s", counter).replace(" ", "0")), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
            log.debug("Session key " + DatatypeConverter.printHexBinary(sessionKey));
            extAuthCommand = socialApplet.createExternalAuthenticationCommand(respStr, randomNumber, getKeyNumber(fileNumber.intValue()), sessionKey);
            log.debug("Command for external authentication " + extAuthCommand);
            // Добавляем команду external update
            commandsResult.add(extAuthCommand);

            // Добавляем команду на создание файла
            commandsResult.addAll(socialApplet.getWriteFileCommands(fileNumber, newFileData, sessionKey, extAuthCommand.substring(extAuthCommand.length() - 16)));
        } catch (Exception e) {
            log.error("Error while creating Create/Delete commands", e);
            throw new MlApplicationException("Ошибка создания команд создания/удаления записией карты, обратитесь к администратору");
        }

        // response with JSON
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        Gson gson = new Gson();
        resp.addDataToJson("result", gson.toJsonTree(commandsResult));

    }

    private String getWriteKey(int fileNumber){
        if(fileNumber == 1) return writeKey1;
        if(fileNumber == 2) return writeKey2;
        if(fileNumber == 3) return writeKey3;
        if(fileNumber == 4) return writeKey4;
        if(fileNumber == 5) return writeKey5;
        return null;
    }

    private String getKeyNumber(int fileNumber){
        if(fileNumber == 1) return "00";
        if(fileNumber == 2) return "04";
        if(fileNumber == 3) return "06";
        if(fileNumber == 4) return "08";
        if(fileNumber == 5) return "0A";
        return null;
    }
}
