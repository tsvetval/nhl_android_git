<div class="ml-auth">

    <div class="ml-auth-blocks-wrap container" style="background-color: #536D9B">

        <div class="ml-auth__login-title row">
            <div class="col-xs-offset-1 col-xs-23">
                Авторизация в системе
            </div>
        </div>
        <div class="ml-auth__login-name row">
            <input class="ml-auth__login-name-input col-xs-22 col-xs-offset-1" placeholder="Логин" name="login" id="login" value=""/>
        </div>
        <div class="ml-auth__login-password row">
            <input class="ml-auth__login-password-input col-xs-20 col-xs-offset-1" placeholder="Пароль" type="password" name="pass_login" id="password" value=""/>
            <span class="ml-auth__login-password-show col-xs-2 glyphicon glyphicon-eye-open" title="Показать пароль"></span>
        </div>
        <div class="row">
            <div class="col-xs-22 col-xs-offset-1">
                <button type="submit" class="ml-auth__login-button" id="loginButton">Войти в систему</button>
            </div>
        </div>
        <div class="ml-auth__login-delimiter"></div>

    </div>
</div><!-- .ml-auth -->

