/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 10.11.14
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */

define(
    ['log', 'misc', 'backbone',
        'cmligth/page_blocks/print_decl/model/PrintDeclBlockModel',
        'cmligth/page_blocks/print_decl/view/PrintDeclBlockView'],
    function (log, misc, backbone, model, view) {
        console.log("Print Boot");
        return {
            model : model,
            view : view
        };
    });