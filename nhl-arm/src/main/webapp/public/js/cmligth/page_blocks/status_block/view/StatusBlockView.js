/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 30.10.14
 * Time: 17:41
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            initialize:function () {
                console.log("initialize StatusBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {
                    activeClick: function(){
                        var objectId = _this.model.get("objectId");
                        var className = _this.model.get("className");
                        var data = {
                            "className" : className,
                            "objectId" : objectId
                        };
                        _this.model.active(data);
                    },
                    blockClick: function(){
                        var objectId = _this.model.get("objectId");
                        var className = _this.model.get("className");
                        var data = {
                            "className" : className,
                            "objectId" : objectId
                        };
                        _this.model.block(data);
                    },
                    readyToPersonalizedClick: function(){
                        var objectId = _this.model.get("objectId");
                        var className = _this.model.get("className");
                        var data = {
                            "className" : className,
                            "objectId" : objectId
                        };
                        _this.model.readyToPersonalized(data);
                    },
                    notUsedClick: function(){
                        var objectId = _this.model.get("objectId");
                        var className = _this.model.get("className");
                        var data = {
                            "className" : className,
                            "objectId" : objectId
                        };
                        var dialog = new Message({
                            title: "Подтверждение",
                            message: "Вы действительно хотите перевести карту в статус «Уничтожена/утеряна»? Дальнейший перевод карты из этого статуса будет невозможен!",
                            type: 'dialogMessage'
                        });
                        dialog.show({
                            okAction: function () {
                                _this.model.notUsed(data);
                            }
                        });
                    },
                     cancelClick: function(){
                         var objectId = _this.model.get("objectId");
                         var className = _this.model.get("className");
                         var data = {
                             "className" : className,
                             "objectId" : objectId
                         };
                         _this.model.cancel(data);
                    }
                });

            }

        });
        return view;
    });
