/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 30.10.14
 * Time: 17:41
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockModel, Messsage, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                folderId: undefined,
                className: undefined,
                objectId : undefined,
                data: undefined
            },

            initialize: function () {
                console.log("initialize StatusBlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    var folderId = params.folderId;
                    var className = params.className;
                    var objectId = params.objectId;
                    this.set('folderId', folderId);
                    this.set('objectId', objectId);
                    this.set('className', className);
                    this.prompt({
                        folderId:folderId,
                        className: className,
                        objectId: params.objectId
                    });
                });
            },

            prompt: function (params) {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        folderId: params.folderId,
                        className: params.className,
                        objectId: params.objectId
                    }
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.set('className', result.className);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            readyToPersonalized: function (data) {
                var _this = this;
                var options = {
                    action: "readyToPersonalized",
                    data: {
                        className: data.className,
                        objectId: data.objectId
                    }
                };

                var callback = function (result) {
                    _this.closePage();
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject('cancel', result, opener));
                };

                return this.callServerAction(options, callback);

            },

            block: function (data) {
                var _this = this;
                var options = {
                    action: "block",
                    data: {
                        className: data.className,
                        objectId: data.objectId
                    }
                };

                var callback = function (result) {
                    _this.closePage();
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject('cancel', result, opener));
                };

                return this.callServerAction(options, callback);

            },

            notUsed: function (data) {
                var _this = this;
                var options = {
                    action: "notUsed",
                    data: {
                        className: data.className,
                        objectId: data.objectId
                    }
                };

                var callback = function (result) {
                    _this.closePage();
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject('cancel', result, opener));
                };

                return this.callServerAction(options, callback);
            },

            active: function (data) {
                // Переводить в статус Блок все активные карты этого пользователя
                var _this = this;
                var options = {
                    action: "active",
                    data: {
                        className: data.className,
                        objectId: data.objectId
                    }
                };

                var callback = function (result) {
                    _this.closePage();
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject('cancel', result, opener));
                };

                return this.callServerAction(options, callback);
            },

            cancel: function (data) {
                // Отмена операции, возврат к просмотру карты
                var _this = this;
                var options = {
                    action: "cancel",
                    data: {
                        className: data.className,
                        objectId: data.objectId
                    }
                };

                var callback = function (result) {
                    _this.closePage();
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject('cancel', result, opener));
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
