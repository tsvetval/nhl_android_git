/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 10.11.14
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            initialize:function () {
                console.log("initialize PrintBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));

            }

        });
        return view;
    });