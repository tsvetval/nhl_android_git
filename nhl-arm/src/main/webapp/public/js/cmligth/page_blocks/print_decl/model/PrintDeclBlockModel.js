/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 10.11.14
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockModel, Messsage, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                folderId: undefined,
                className: undefined,
                objectId : undefined,
                data: undefined
            },

            initialize: function () {
                console.log("initialize PrintBlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    var folderId = params.folderId;
                    var className = params.className;
                    var objectId = params.objectId;
                    this.set('folderId', folderId);
                    this.set('objectId', objectId);
                    this.set('className', className);
                    this.prompt({
                        folderId:folderId,
                        className: className,
                        objectId: params.objectId
                    });
                });
            },

            prompt: function (params) {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        folderId: params.folderId,
                        className: params.className,
                        objectId: params.objectId
                    }
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.set('className', result.className);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            }
        })
    });
