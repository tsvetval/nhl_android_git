/**
 * Created with IntelliJ IDEA.
 * User: a_yakovlev
 * Date: 30.10.14
 * Time: 17:40
 */
define(
    ['log', 'misc', 'backbone',
        'cmligth/page_blocks/status_block/model/StatusBlockModel',
        'cmligth/page_blocks/status_block/view/StatusBlockView'],
    function (log, misc, backbone, model, view) {
        console.log("Status Boot");
        return {
            model : model,
            view : view
        };
    });
