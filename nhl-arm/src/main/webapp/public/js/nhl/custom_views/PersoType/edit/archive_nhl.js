define(
    ['log', 'misc', 'cms/page_blocks/form_block/object_edit_block/view/EditAttrView', 'text!nhl/custom_views/PersoType/edit/export_nhl_ftp_folder.tpl'],
    function (log, misc, EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "change .attrField": "keyPressed"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                this.cardTypeField = this.model.get('pageBlockModel').get('attrList').findWhere({entityFieldName: 'card_type'});
                this.listenTo(this.cardTypeField, 'change:value', this.render)
            },

            render: function () {
                /* Get card_type field from object and listen changes */
                var _this = this;
                
                if (this.cardTypeField) {

                    var hidden = !this.cardTypeField.get('value') || this.cardTypeField.get('value').code == 'NOT_BANK';
                    _this.model.set('hidden', hidden, {silent: true});
                }


                if (this.isHidden()) {
                    return;
                } 

                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$attrLabel = this.$el.find('.attr-label')
                this.$inputField.val(this.model.get('value'));
                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper()
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (val) {
                if (this.$inputField.val().length == 0) {
                    this.model.set('value', "");
                } else {
                    this.model.set('value', this.$inputField.val());
                }
            }

        });

        return view;
    });
