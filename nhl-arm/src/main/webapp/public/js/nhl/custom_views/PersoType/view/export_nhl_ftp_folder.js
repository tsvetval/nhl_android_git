/**
 * Кастомное представление для отображения поля "Папка экспорта" для объектов класса PersoType
 */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/form_block/view/AttrView', 'text!cms/page_blocks/form_block/object_view_block/templates/attrs/StringAttrView.tpl'],
    function (log, misc, backbone, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            render: function () {
                /* Get card_type field from object and listen changes */
                var cardTypeField = this.model.get('pageBlockModel').get('attrList').findWhere({entityFieldName: 'card_type'});
                if (this.isHidden() || cardTypeField.get('value').code == 'NOT_BANK') {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
            }
        });

        return view;
    });
