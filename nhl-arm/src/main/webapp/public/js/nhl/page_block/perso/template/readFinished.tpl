<style type="text/css">
    .block-header {
        font-size: 2.1em;
        margin-bottom: 20px;
    }

    #update_not_need, #update_need {
        display: none;
        padding-top: 30px;
    }

    .update-status {
        font-size: 1.5em;
    }

</style>

<div class="perso-update-read-finished-template">
    <div class="row">
        <div class="col-xs-12 col-xs-offset-6">
            <h2 class="block-header">
                Обновление карты НХЛ
            </h2>

            <div class="row">
                <div class="col-xs-6">
                    <div class="img-thumbnail">
                        <% if (cardData.holderPhoto) { %>
                        <img src="<%= cardData.holderPhoto %>" class="img-responsive">
                        <% } else { %>
                        <div class="text-warning">Нет фотографии</div>
                        <% } %>
                    </div>
                </div>
                <div class="col-xs-17 col-xs-offset-1">
                    <div class="row">
                        <div class="col-xs-7"><label>ФИО: </label></div>
                        <div class="col-xs-17"><%= cardData.holderTitle %></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7"><label>Номер паспорта: </label></div>
                        <div class="col-xs-17"><%= cardData.holderDocNum%></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7"><label>Дата рождения: </label></div>
                        <div class="col-xs-17"><%= cardData.holderBirthDate %></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7"><label>Статус карты: </label></div>
                        <div class="col-xs-17"><%= cardData.cardStatus %></div>
                    </div>
                    <% if (checkResult.errorCode == 'WARN') { %>
                    <div class="row">
                        <div class="col-xs-7"><label>Состояние: </label></div>
                        <div class="col-xs-17"><span class="label label-danger"><%= checkResult.failDescription %></span></div>
                    </div>
                    <% } %>
                </div>
            </div>

            <div id="update_not_need" class="row">
                <div class="col-xs-12">
                    <h3 class="update-status text-success">Обновление не требуется</h3>
                </div>
                <div class="col-xs-12 text-right">
                    <button id="return_button" class="btn btn-default">Вернуться к чтению карты</button>
                </div>
            </div>

            <div id="update_need" class="row">
                <div class="col-xs-12">
                    <% if (checkResult.errorCode != 'WARN') { %>
                    <h3 class="update-status text-danger">Карте требуется обновление</h3>
                    <% } %>
                </div>
                <div class="col-xs-12 text-right">
                    <div class="btn-group">
                        <% if (checkResult.errorCode != 'WARN') { %>
                        <button id="update_button" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> Обновить</button>
                        <% } %>
                        <button id="return_button" class="btn btn-default">Вернуться к чтению карты</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

