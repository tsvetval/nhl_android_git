define(
    ['log', 'misc', 'backbone', 'cms/events/events',
        'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject'],

    function (log, misc, backbone, cmsEvents, PageBlockModel, NotifyEventObject) {
        var model = PageBlockModel.extend({

            defaults: {
                pageTitle: 'Выбор статуса карты',
                controller: '/card/changeCardStatus/change'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);
                console.log("initialize BlockModel");
            },

            notifyPageBlocks: function (data) {
                this.trigger(cmsEvents.NOTIFY_PAGE_BLOCKS, data)
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.set("objectId", params.objectId);
                    $.when(_this.getStatusList(params.objectId)).then(function(response){
                        _this.set("statusList", response.statusList);
                        _this.set("currentStatus", response.currentStatus);
                        _this.updatePageTitle(_this.get('pageTitle'));
                        _this.trigger(cmsEvents.RENDER_VIEW);
                    });
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            getStatusList: function (objectId){
                var _this = this;
                var options = {
                    url: misc.getContextPath() + this.get('controller'),
                    action: 'getStatusList',
                    data: {
                        cardId: objectId
                    }
                };
                return _this.callServerAction(options);
            },

            changeCardStatus: function (statusCode) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + this.get('controller'),
                    action: 'change',
                    data: {
                        statusCode: statusCode,
                        cardId: this.get('objectId')
                    }
                };

                _this.callServerAction(options).then(function () {

                    var opener = _this.get("pageModel").get("page_opener");
                    if (opener) {
                        if(opener.get("page_opener")) {
                            opener = [opener, opener.get("page_opener")]
                        }
                        _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.REFRESH_PAGE, {}, opener));
                    }

                    _this.closePage();
                });
            },

            cancelEditStatus: function(){
                this.closePage();
            }

        });
        return model;
    });
