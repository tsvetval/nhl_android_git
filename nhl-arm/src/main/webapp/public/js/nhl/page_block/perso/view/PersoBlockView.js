define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!nhl/page_block/perso/template/init.tpl',
        'text!nhl/page_block/perso/template/readFinished.tpl',
        'text!nhl/page_block/perso/template/done.tpl',
        'text!nhl/page_block/perso/template/error.tpl'


    ],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              initTemplate, readFinishedTemplate, doneTemplate, errorTemplate) {

        var view = PageBlockView.extend({

            events: {
                "click #doImport": "importClick",
                "click #return_button": "render"
            },

            initialize: function () {
                this.listenTo(this.model, 'notJava', this.javaNotWork);
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'readCard', this.readCardHandler);
                this.listenTo(this.model, 'renderDone', this.renderDone);
                this.listenTo(this.model, 'renderError', this.renderError);
                this.listenTo(this.model, 'renderCard', this.renderCard);
            },

            javaNotWork: function () {
                alert("В данном браузере не удалось запустить аплет, обратитесь к администратору.");
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(initTemplate, {model: this.model}));
                this.$readerSelect = this.$el.find('#readerSelect');
                var select2Data = [];
                select2Data.push({id: '', text: ''});
                this.model.get('terminalList').forEach(function (text) {
                    select2Data.push({id: text, text: text});
                });

                this.$readerSelect.select2({
                    width: '100%',
                    data: select2Data,
                    placeholder: "Выберите значение"
                });


                //this.$yearField.val(null).trigger("change");
                this.$readerSelect.on("change", function () {
                    _this.model.saveTerminal(_this.$readerSelect.val());
                });

                if ($.cookie('terminal')) {
                    this.$readerSelect.val($.cookie('terminal')).trigger('change')
                }
            },

            readCardHandler: function () {
                this.$('#choose-reader, #card-status').hide();
                this.$('#import-process').show()
            },

            renderCard: function () {
                var _this = this;
                var cardCheckResult = this.model.get('checkResult');
                this.$el.html(_.template(readFinishedTemplate, {cardData: this.model.get('cardData'), checkResult: cardCheckResult}));
                if (cardCheckResult.validationResult){
                    // обновление не требуется
                    this.$el.find('#update_not_need').show();
                } else {
                    // требуется обновление
                    this.$el.find('#update_need').show();
                    this.$el.find('#update_button').click(function() {
                        _this.model.doUpdate()
                    });
                }
            },

            renderError: function (message) {
                this.$el.html(_.template(errorTemplate, {message: message}));
            },

            renderDone: function () {
                var _this = this;
                this.$el.html(_.template(doneTemplate, {cardData: this.model.get('cardData')}/*, {importedCount: this.model.get('importResult').importedCount}*/));
            },

            importClick: function () {
                var _this = this;
                var file = _this.$el.find('#import-file')[0].files[0];
                if (file) {
                    _this.model.importInvestData(file);
                    _this.$el.find('#import-div').hide();
                    _this.$el.find('#import-buttons').hide();
                    _this.$el.find('#delete-buttons').hide();
                    _this.$el.find('#import-process').show();
                } else {
                    // TODO _this.showEshowError("Не задан файл импорта!");
                }
            }


        });
        return view;
    });
