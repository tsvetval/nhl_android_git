/**
 * @name: AddNewCardsModel
 * @package:
 * @project: nhl-all
 * Created by i_tovstyonok on 15.03.2016.
 */

define(
    ['log', 'misc', 'cms/model/PageBlockModel', 'cms/events/events', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, PageBlockModel, Events, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                controller: 'controller/addnewcards',
                pageTitle: 'Добавление карт'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('Initialize AddNewCardsModel');
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */

            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                this.on(Events.RESTORE_PAGE, this.process)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.on(Events.RESTORE_PAGE, this.process);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            process: function () {
                var _this = this;
                this.set('data', this.get('pageModel').get('hashParams'));
                this.updatePageTitle(this.get('pageTitle') || 'Добавление карт');

                $.when(this.callServerAction({
                    action: 'getMeta',
                    url: misc.getContextPath() + '/' + _this.get('controller')
                })).then(function (response) {
                    if (response.types) {
                        _this.get('data').persoTypes = response.types;
                        _this.get('data').branches = response.branches;
                        _this.trigger(Events.RENDER_VIEW)
                    }
                })

            },

            addCardsRequest: function (data) {
                var _this = this;

                /**
                 * В зависимости от типа выборки отправляется либо массив id либо настройки поиска
                 * useFilter может быть либо simple либо extended, в зависимости от этого отправляются разные поля с
                 * настройками фильтра (строка simpleFilter или массив extendedFilterSettings)
                 */
                if (this.get('data').isAllSelected) {
                    data.isAllSelected = true

                    if (this.get('data').useFilter) {
                        switch (this.get('data').useFilter) {
                            case 'simple':
                                if (this.get('data').simpleFilter) {
                                    data.useFilter = this.get('data').useFilter;
                                    data.simpleFilter = this.get('data').simpleFilter
                                }
                                break;
                            case 'extended':
                                if (this.get('data').extendedFilterSettings) {
                                    data.useFilter = this.get('data').useFilter;
                                    data.extendedFilterSettings = this.get('data').extendedFilterSettings
                                }
                                break;
                            default:
                                break;
                        }
                    }

                } else {
                    data.selectedIds = JSON.stringify(this.get('data').selectedIds)
                }

                data.className = this.get('data').className;
                var options = {
                    url: misc.getContextPath() + '/' + this.get('controller'),
                    action: 'addNewCards',
                    data: data
                };


                this.callServerAction(options).then(function (response) {
                    /* Close page on success */
                    var opener = _this.get("pageModel").get("page_opener");

                    _this.notifyPageBlocks(new NotifyEventObject(Events.UNSET_SELECTED_IDS, {update: true}, opener));
                    _this.closePage()
                })


            }
        });

        return model
    }
);