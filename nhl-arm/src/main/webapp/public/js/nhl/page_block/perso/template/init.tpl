<style type="text/css">
    .block-header {
        font-size: 2.1em;
        margin-bottom: 20px;
    }

    #import-buttons {
        text-align: center;
        font-size: 1.4em;
        padding: 20px;
        color: #29949D;
    }

    #import-process .progress-bar {
        width: 100%;
    }
</style>

<div class="perso-update-init-template">
    <div class="row">
        <div class="col-xs-12 col-xs-offset-6 form form-horizontal">
            <h2 class="block-header">
                Обновление карты НХЛ
            </h2>
            <div id="choose-reader" class="form-group">
                <div class="col-xs-8">
                    <label id="import-file-label" for="readerSelect">Выбранный считыватель</label>
                </div>
                <div class="col-xs-16">
                    <select id="readerSelect" class="readerSelect"></select>
                </div>
            </div>
            <div id="card-status" class="form-group">
                <div id="import-buttons">
                    <h3>Вставьте/приложите карту в считыватель</h3>
                </div>
            </div>

            <div id="import-process" class="import-process" style="display: none;">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active">
                        <span>Подождите! Идёт чтение данных карты...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
