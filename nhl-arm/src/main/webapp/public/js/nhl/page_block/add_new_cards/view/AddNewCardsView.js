/**
 * @name: AddNewCardsView
 * @package:
 * @project: nhl-all
 * Created by i_tovstyonok on 15.03.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'select2', 'cms/view/PageBlockView', 'text!nhl/page_block/add_new_cards/template/AddNewCardsTemplate.tpl'],
    function (log, misc, backbone, Events, select2, PageBlockView, Template) {
        var view = PageBlockView.extend({
            events: {
                "click #add-cards-confirm": "addCardsConfirm",
                "click #add-cards-cancel": "closePage"
            },

            initialize: function () {
                log.debug("Initialize AddNewCardsView");
                this.listenTo(this.model, Events.RENDER_VIEW, this.render);
            },

            render: function () {
                var _this = this;
                /* Render view with template engine */
                this.$el.html(_.template(Template, {model: this.model}));

                this.$types = this.$('#perso-type-select');
                this.$branches = this.$('#branch-select');

                this.$types.select2({
                    data: _.map(_this.model.get('data').persoTypes, function (type) {
                        return {id: type.id, text: type.name}
                    }) || [],
                    allowClear: true,
                    placeholder: 'Выбрать тип персонализации'
                });


                this.$branches.select2({
                    data: _.map(_this.model.get('data').branches, function (branch) {
                        return {id: branch.id, text: branch.name}
                    }) || [],
                    allowClear: true,
                    placeholder: 'Выбрать отделение'
                });

                /* Enable or disable confirm button on change perso type */
                this.$types.on('change', function (e) {
                    var typeId = $(this).val();
                    var type = _.findWhere(_this.model.get('data').persoTypes, {id: parseInt(typeId)});
                    if (type) {
                        if (type.type == 'BANK') {
                            _this.$('#branch-select-row').show();
                            _this.$('#add-cards-confirm').prop('disabled', true)

                        } else {
                            _this.$('#branch-select-row').hide();
                            _this.$('#add-cards-confirm').prop('disabled', !$(this).val())
                        }
                    } else {
                        _this.$('#branch-select-row').hide();
                    }

                });

                _this.$branches.on('change', function (e) {
                    _this.$('#add-cards-confirm').prop('disabled', !$(this).val())
                });

                /* Hide select bank branch by default */
                this.$('#branch-select-row').hide();

                /* After render view set block as rendered */
                this.model.set('blockRendered', true);
                return true
            },

            addCardsConfirm: function () {
                var data = {
                    persoTypeId: this.$types.val(),
                    bankBranchId: this.$branches.val(),
                    additionalData: this.$('#additional-data').val()
                };

                this.model.addCardsRequest(data)
            },

            closePage: function () {
                this.model.closePage()
            }
        });
        return view
    }
);
