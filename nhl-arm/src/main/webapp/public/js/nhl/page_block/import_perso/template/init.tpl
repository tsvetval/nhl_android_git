<h2>
    Импорт файла персонализации
</h2>
<div id="import-div">
    <input id="import-file" name="import-file" type="file"/>
    <label id="import-file-label" for="import-file">Нажмите, чтобы выбрать файл для импорта</label>
</div>

<div id="import-buttons">
    <div class="btn btn-primary" id="doImport">Импорт</div>
    <div class="btn btn-default" id="cancelImport">Отмена</div>
</div>

<div id="import-process" class="import-process" style="display: none;">
    Подождите! Идёт импорт данных...
</div>
<div id="import-status" class="import-status"></div>
