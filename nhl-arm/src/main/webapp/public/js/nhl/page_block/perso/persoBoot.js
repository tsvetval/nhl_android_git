/*
 * Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
 * Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
 * */


define(
    ['log', 'misc', 'backbone',
        'nhl/page_block/perso/model/PersoBlockModel',
        'nhl/page_block/perso/view/PersoBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
