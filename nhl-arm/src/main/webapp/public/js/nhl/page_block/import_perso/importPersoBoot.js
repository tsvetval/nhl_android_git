/*
 * Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
 * Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
 * */


define(
    ['log', 'misc', 'backbone',
        'nhl/page_block/import_perso/model/ImportBlockModel',
        'nhl/page_block/import_perso/view/ImportBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
