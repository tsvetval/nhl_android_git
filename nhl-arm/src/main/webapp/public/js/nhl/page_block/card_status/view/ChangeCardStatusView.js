define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!nhl/page_block/card_status/template/init.tpl'],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              initTemplate) {

        var view = PageBlockView.extend({

            events: {
                "click .status-button": "changeStatusClick",
                "click #cancel": "cancel",
                "click #cancel-error": "cancel"
            },

            initialize: function () {
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'renderDone', this.renderDone);
            },

            render: function () {
                var _this = this;
                var currentStatus = _this.model.get("currentStatus");
                this.$el.html(_.template(initTemplate, {
                    model: this.model,
                    classFormatter: function (code) {
                        if (code == 'ACTIVE' || code == 'Активна') return 'success';
                        if (code == 'BLOCKED' || code == 'Блокирована') return 'danger';
                        return 'primary'
                    }
                }));

            },

            changeStatusClick: function (e) {
                var _this = this;
                var statusCode = $(e.target).data('status-id');
                if (statusCode) {
                    this.model.changeCardStatus(statusCode)
                }
            },

            cancel: function() {
                this.model.cancelEditStatus();
            }
        });
        return view;
    });