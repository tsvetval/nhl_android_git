/**
 * @name: AddNewCards
 * @package:
 * @project: nhl-all
 * @javaController:
 * Created by i_tovstyonok on 15.03.2016.
 */

define(
    ['log', 'misc', 'backbone', 'nhl/page_block/add_new_cards/model/AddNewCardsModel', 'nhl/page_block/add_new_cards/view/AddNewCardsView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
