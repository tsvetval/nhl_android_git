<section id="add-new-cards" role="template">
    <div class="row">
        <% if ((model.get('data').selectedIds && model.get('data').selectedIds.length > 0) || model.get('data').isAllSelected) { %>
        <div class="well">
            <h3>Добавление карт для
                <% print(!model.get('data').isAllSelected ? model.get('data').selectedIds.length : '')  %> держателей </h3>
        </div>
        <div class="col-xs-24">
            <div class="row">
                <div class="col-xs-24">
                    <div class="form-group row">
                        <div class="col-xs-24">
                            <label>Тип персонализации</label>
                        </div>
                        <div class="col-xs-24">
                            <select id="perso-type-select" class="form-control"></select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="branch-select-row">
                <div class="col-xs-24">
                    <div class="form-group row">
                        <div class="col-xs-24">
                            <label>Отделение банка</label>
                        </div>
                        <div class="col-xs-24">
                            <select id="branch-select" class="form-control"></select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-24">
                    <div class="form-group row">
                        <div class="col-xs-24">
                            <label>Дополнительные данные</label>
                        </div>
                        <div class="col-xs-24">
                            <textarea id="additional-data" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-24">
                    <div class="form-group row">
                        <div class="col-xs-24">
                            <button id="add-cards-confirm" class="btn btn-success" disabled="">Добавить</button>
                            <button id="add-cards-cancel" class="btn btn-default">Отмена</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% } else { %>
        <h3>Держатели карт не выбраны.</h3>
        <% } %>
    </div>
</section>