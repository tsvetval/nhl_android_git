/**
 * @name: importHoldersBoot
 * @package:
 * @project: nhl-all
 * @javaController:
 * Created by i_tovstyonok on 06.04.2016.
 */

define(
    ['log', 'misc', 'backbone', 'nhl/page_block/import_holders/model/importHoldersModel', 'nhl/page_block/import_holders/view/importHoldersView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
