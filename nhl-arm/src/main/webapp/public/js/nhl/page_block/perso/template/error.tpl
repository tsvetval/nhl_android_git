<style type="text/css">
    .block-header {
        font-size: 2.1em;
        margin-bottom: 20px;
    }

    #update_not_need, #update_need {
        display: none;
        padding-top: 30px;
    }

    .update-status {
        font-size: 1.5em;
    }

</style>

<div class="perso-update-error-template">
    <div class="row">
        <div class="col-xs-12 col-xs-offset-6">
            <h2 class="block-header">
                Обновление карты НХЛ
            </h2>

            <div style="padding-top: 30px;">
                <div class="col-xs-12">
                    <h3 class="update-status text-danger">Произошла ошибка</h3>
                    <div id="error-message"><%= message %></div>
                </div>
                <div class="col-xs-12 text-right">
                    <button id="return_button" class="btn btn-default">Вернуться к чтению карты</button>
                </div>
            </div>

        </div>
    </div>
</div>

