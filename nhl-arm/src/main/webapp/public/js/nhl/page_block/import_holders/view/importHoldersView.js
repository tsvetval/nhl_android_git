/**
 * @name: importHolderView
 * @package:
 * @project: nhl-all
 * Created by i_tovstyonok on 06.04.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/events/events', 'text!nhl/page_block/import_holders/template/importHoldersTemplate.tpl',
        'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockView, Events, Template, NotifyEventObject) {
        var view = PageBlockView.extend({
            events: {
                "click #upload-import-holders-file": "uploadImportFile",
                "click #close-import-holders, #cancel-import-holders": "closeImportPage"
            },

            initialize: function () {
                log.debug("Initialize importHolderView");
                this.listenTo(this.model, Events.RENDER_VIEW, this.render);
            },

            render: function () {
                /* Render view with template engine */
                this.$el.html(_.template(Template, {model: this.model}));
                this.$('#import-result').hide();
                this.listenTo(this.model, 'upload:progress', this.updateUploadProgress);
                this.listenTo(this.model, 'upload:complete', this.uploadCompleteHandler);
                this.listenTo(this.model, 'import:start', this.importStartHandler);
                this.listenTo(this.model, 'import:complete', this.importCompleteHandler);
                this.inputFileHandler();

                /* Hide steps and progress bars */
                this.$('#progress-block').hide();
                this.$('#import-start').hide();

                /* After render view set block as rendered */
                this.model.set('blockRendered', true);
                return true
            },

            inputFileHandler: function () {
                var _this = this;
                this.$('#import-holders-file').on('change', function (e) {
                    var file = e.target.files[0];
                    _this.$('#upload-import-holders-file').prop('disabled', !file)

                    if (file) {
                        $(e.target).parent().removeClass('has-error')
                    }
                })
            },

            uploadImportFile: function () {
                var file = this.$('#import-holders-file')[0].files[0];
                if (file) {
                    this.$('#input-block, #input-controls').hide();
                    this.$('#progress-block').show();
                    this.model.uploadImportFile(file)
                } else {
                    this.$('#import-holders-file').parent().addClass('has-error')
                }
            },

            updateUploadProgress: function (percent) {
                this.$('#progress-block .progress-bar').css('width', percent + '%');
            },

            uploadCompleteHandler: function () {
                this.$('#progress-block').hide()
            },

            importStartHandler: function () {
                this.$('#import-holders-form').hide();
                this.$('#import-start').show();
            },

            importCompleteHandler: function (result) {
                if (result && result.count && result.status) {
                    this.$('#import-start').hide();
                    this.$('#result-error-panel').hide();
                    this.$('#import-result').show();

                    this.$('#result-count-success').text(result.count.success);
                    this.$('#result-count-all').text(result.count.all);

                    if (result.status == 'partial') {
                        this.$('#result-info-panel').addClass('bg-warning');

                        if (result.errorFile) {
                            this.$('#result-error-panel').show();
                            this.$('#result-error-link').prop('href', result.errorFile)
                        }
                    }

                    if (result.status == 'success') {
                        this.$('#result-info-panel').addClass('bg-success');
                        this.$('#result-error-panel').hide();
                    }
                } else {
                    this.model.displayErrorMessage("Ошибка приложения", "Получен неверный ответ от сервера.")
                }

            },

            closeImportPage: function (e) {
                if ($(e.target).prop('id') == 'close-import-holders') {
                    var opener = this.model.get("pageModel").get("page_opener");
                    this.model.notifyPageBlocks(new NotifyEventObject(Events.REFRESH_PAGE, {}, opener));
                }
                this.model.closePage()
            }
        });
        return view
    }
);
