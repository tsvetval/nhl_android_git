/**
 * @name: importHoldersModel
 * @package:
 * @project: nhl-all
 * Created by i_tovstyonok on 06.04.2016.
 */

define(
    ['log', 'misc', 'cms/model/PageBlockModel', 'cms/events/events'],
    function (log, misc, PageBlockModel, Events) {
        var model = PageBlockModel.extend({
            defaults: {
                controller: '/controller/import/holders',
                pageTitle: 'Импорт данных о держателях'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('Initialize importHoldersModel')
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */

            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                this.on(Events.RESTORE_PAGE, this.process)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.off(Events.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            process: function (params) {
                this.updatePageTitle(this.get('pageTitle'));

                this.trigger(Events.RENDER_VIEW)
            },

            uploadImportFile: function (file) {
                var _this = this;
                var data = new FormData();
                data.append(file.name, file);

                $.ajax({
                    url: misc.getContextPath() + '/upload',
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        /* Magic progress bar builder */
                        xhr.upload.addEventListener("progress", function(e){
                            if (e.lengthComputable) {
                                var percentComplete = Math.floor((e.loaded / e.total) * 100);
                                _this.trigger('upload:progress', percentComplete)
                            }
                        }, false);

                        return xhr
                    },
                    method: 'post',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        _this.processImport(file.name)
                    },
                    error: function (xhr) {
                        console.info('Upload fail', xhr)
                    },
                    complete: function () {
                        _this.trigger('upload:complete')
                    }
                })
            },

            processImport: function (filename) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + this.get('controller'),
                    action: 'processImport',
                    data: {
                        filename: filename
                    }
                };

                this.trigger('import:start');
                this.callServerAction(options).then(function (response) {
                    if (response.result) {
                        _this.trigger('import:complete', response.result)
                    } else {
                        _this.displayErrorMessage("Ошибка приложения", "Получен неверный ответ от сервера.")
                    }

                }).fail(function () {
                    /*Do something when world gonna burn */
                    _this.trigger(Events.RENDER_VIEW)
                })
            }


        });

        return model
    }
);