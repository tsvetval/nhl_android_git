define(
    [
        'misc'
        , 'log'
        , 'backbone'
        , 'deployJava'
        , 'jquery_cookie'
        , 'jquery'
    ],
    function (misc, log, backbone, deployJava, q, $) {

        var CardReaderWrapper = backbone.Model.extend({
            defaults: {
                appletInitialized : false,
                waitInsertIntervalId : undefined
            },

            /*Проводит инициализацию аплета возвращает differed true - если инициализирован*/
            initApplet: function () {
                log.debug("Start initApplet");
                var _this = this;
                var def = new $.Deferred();
                /*
                 Initialize applet
                 */
                var appletParams = {
                    jnlp_href: "http://" + location.host + misc.getContextPath() + "/public/jar/card_reader.jnlp?version=1",
                    fontSize: 16,
                    device: 'cardreader.0',
                    java_arguments: '-Djava.net.preferIPv4Stack=true'
                };
                var appletVersion = '1.1';

                deployJava.writeAppletTag({
                    id: 'CardReader',
                    width: 1,
                    height: 1
                }, appletParams);


                this.cardReader = window.CardReader;

                var count = 0;

                function tryInitApplet() {
                    log.debug("Try initApplet");
                    try {
                        var terminalList = _this.cardReader.getTerminalList(); //this call throw exception if applet is not inited yet
                        _this.set('appletInitialized', true);
                        console.log("Applet inited!");
                        def.resolve(true);
                    } catch (e) {
                        log.debug("TryInitApplet fail count = " + count);
                        count++;
                        if (count > 20) {
                            log.debug("Fail init applet notJava count = " + count);
                            _this.trigger('notJava', {});
                            def.resolve(false);
                        } else {
                            setTimeout(tryInitApplet, 500);
                        }
                    }

                };
                setTimeout(tryInitApplet, 4000);

                return def.promise();
            },

            waitForCardInsert : function(){
                log.debug("Start waitForCardInsert");
                var _this = this;
                var def = new $.Deferred();
                var intervalID = setInterval(function () {
                    if (_this.cardPresentChecker()){
                        log.debug("Card inserted");
                        clearInterval(intervalID);
                        _this.trigger('cardInserted', {});
                        def.resolve();
                    }
                }, 200);
                this.set("waitInsertIntervalId", intervalID);

                return def.promise();
            },


            cardPresentChecker: function (cardReader) {
                if (this.cardReader.isCardPresent()) {
                    this.set("cardPresent", true);
                } else {
                    this.set("cardPresent", false);
                }
                return this.get("cardPresent");

            },

            /*
             Wrapped CardReader functions
             */

            getTerminalList: function () {
                log.debug("getTerminalList");
                var terminalList = this.cardReader.getTerminalList();
                var result = [];
                for (var i = 0; i < terminalList.length; i++) {
                    result.push(terminalList[i]);
                }
                return result;
            },

            setTerminal: function (name) {
                if (!name) throw new Error("Terminal name has bad value: " + name);
                log.debug("setTerminal name = " + name);
                return this.cardReader.setTerminal(name);
            },

            runAPDU: function (apdu) {
                var runApduResult = this.cardReader.runAPDU(apdu);
                if (runApduResult.sw) {
                    return {
                        sw: runApduResult.sw,
                        response: runApduResult.response
                    };
                }
                else {
                    log.debug("ERROR Could not execute apdu: " + runApduResult.message);
                    throw new Error("Could not execute apdu: " + runApduResult.message);
                }
            },

            getTerminal: function () {
                return this.cardReader.getTerminal();
            }

        });
        return CardReaderWrapper;

    });
