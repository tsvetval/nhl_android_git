<section id="change-card-status" role="template">
    <style>
        .status-info {
            padding: 8px 12px;
            font-weight: normal;
            font-size: 14px;
        }
    </style>
    <div class="row">
        <div class="col-xs-24 well">
            <span class="status-info label label-<%= classFormatter(model.get('currentStatus')) %>" style="">
                Текущий статус карты: <span id="curStatus"><%= model.get("currentStatus") %></span>
            </span>
            <% if (model.get('statusList').length > 0) { %>

            <span class="" style="margin-left: 30px;"><label>Изменить статус карты на:</label> </span>
            <span class="btn-group" style="margin-left: 10px;">
            <% _.each(model.get('statusList'), function (status) {%>
                <button class="btn btn-<%= classFormatter(status.id) %> status-button" data-status-id="<%= status.id %>"><%= status.name %></button>
            <% }) %>
            </span>
            <% } else { %>
            <span class="label label-danger status-info" style="margin-left: 30px;" id="error-message">
                Для данной карты ручное изменение статуса запрещено.
            </span>
            <% } %>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-24">
            <div class="btn btn-default" id="cancel-error">Отмена</div>
        </div>
    </div>
</section>