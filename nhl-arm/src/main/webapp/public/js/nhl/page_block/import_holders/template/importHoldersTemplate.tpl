<style type="text/css">
    .block-header {
        font-size: 2.1em;
        margin-bottom: 20px;
    }
    
    /* For Firefox only */
    @-moz-document url-prefix() {
        input[type="file"].form-control {
            height: auto;
        }
    }
</style>
<section id="import-holders-template" role="template">
    <div class="row" id="import-holders-form">
        <div class="col-xs-24 well">
            <h2 class="block-header">Загрузка файла с данными о держателях</h2>
            <div class="form-group col-xs-24" id="input-block">
                <div class="col-xs-12">
                    <input type="file" class="form-control" id="import-holders-file" accept=".zip">
                </div>
            </div>
            <div class="form-group col-xs-24" id="progress-block">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active"></div>
                </div>
            </div>
            <div class="form-group col-xs-24" id="input-controls">
                <div class="col-xs-12 btn-group">
                    <button class="btn btn-success" id="upload-import-holders-file" disabled="true">Импортировать</button>
                    <button class="btn btn-primary" id="cancel-import-holders">Отмена</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="import-start">
        <div class="col-xs-24 well">
            <h2 class="block-header">Импортируются данные о держателях...</h2>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" style="width: 100%"></div>
            </div>
        </div>
    </div>
    <div class="row" id="import-result">
        <div class="col-xs-24 well">
            <div class="row">
                <h2 class="block-header">Результаты импорта:</h2>
                <div class="col-xs-16 panel-body" id="result-info-panel">
                    <p>Импорт <span id="result-count-success"></span> держателей из <span id="result-count-all"></span> выполнен успешно</p>
                </div>
                <div class="col-xs-8 panel-body bg-danger" id="result-error-panel">
                    <div>
                        <p>Файл с ошибочными данными: <a href="#" id="result-error-link">Скачать</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-24">
            <button class="btn btn-success" id="close-import-holders">Вернуться к списку держателей</button>
        </div>
    </div>
</section>