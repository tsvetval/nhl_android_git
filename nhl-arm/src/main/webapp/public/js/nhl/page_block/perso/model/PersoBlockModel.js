define(
    [
        'jquery',
        'log',
        'misc',
        'backbone',
        'cms/events/events',
        'cms/model/PageBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'jquery_cookie',
        'nhl/page_block/perso/model/CardReaderWrapper',
        'underscore',
        'select2'
    ],
    function ($, log, misc, backbone, cmsEvents, PageBlockModel, NotifyEventObject, jquery_cookie, CardReaderWrapper, _, select2) {
        var model = PageBlockModel.extend({
            defaults: {
                cardReader: undefined, // Текущий объект CardReaderWrapper
                terminalList: undefined, // Список терминалов
                selectedTerminal: undefined,  // Выбранный терминал

                /*AFETR CARD CHECK*/
                checkResult: undefined,
                cardData: undefined,
                errorMessage: {
                    WRITE: 'Ошибка записи на карту, повторите попытку',
                    READ: 'Ошибка чтения карты, повторите попытку',
                    SERVER_FAIL: 'Ошибка при обращении к серверу.',
                    NHL: 'НХЛ приложение отсутствует на карте'
                }
            },

            initialize: function () {
                model.__super__.initialize.call(this);
                var _this = this;
                console.log("initialize BlockModel (ContentBlock)");

                var cardReaderWrapper = new CardReaderWrapper();
                _this.set('cardReader', cardReaderWrapper);

            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.restorePageListener();
                });
                // wait for card


            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },


            restorePageListener: function () {
                var _this = this;

                $.when(_this.get("cardReader").initApplet()).then(function (appletInitialized) {
                        log.debug("After appletInitialized");
                        if (appletInitialized) {
                            _this.set("terminalList", _this.get("cardReader").getTerminalList());
                            if ($.cookie('terminal')) {
                                _this.set("selectedTerminal", $.cookie('terminal'));
                                _this.get('cardReader').setTerminal($.cookie('terminal'));
                                _this.startWaitingForCard();
                            }
                            _this.trigger(cmsEvents.RENDER_VIEW);
                            _this.updatePageTitle('Обновление карт')
                        } else {
                            _this.trigger('notJava');
                        }
                    }
                );
            },

            startWaitingForCard: function () {
                var _this = this;
                // Ожидаем когда вставят карту
                log.debug("startWaitingForCard, stopListening cardInsert on  " + _this.get("cardReader"));
                this.stopListening(_this.get("cardReader"), 'cardInserted');
                log.debug("startListening cardInsert on  " + _this.get("cardReader"));
                this.listenToOnce(_this.get("cardReader"), 'cardInserted', function () {
                    this.trigger('readCard');
                    // Таймаут для контактной карты она не успевает инициализироваться нужно примерно 400 мс
                    setTimeout(function(){
                        try {
                            _this.verifyCardType('read')
                        } catch (e) {
                            console.error("Error reading card: " + e.message)
                            _this.trigger('renderError', _this.get('errorMessage').READ);
                        }
                    }, 400);
                });
                _this.get("cardReader").waitForCardInsert();
                //$.when(_this.get("cardReader").waitForCardInsert()).then(function () {
                //    alert('Card present');
                //});
            },


            saveTerminal: function (terminalName) {
                log.debug("saveTerminal name = " + terminalName);

                var _this = this;
                var cardReader = _this.get('cardReader');
                $.cookie('terminal', terminalName, {expires: 7});
                var cardReaderWrapper = _this.get('cardReader');
                cardReaderWrapper.setTerminal(terminalName);

                _this.startWaitingForCard();
            },

            verifyCardType: function (action) {
                var _this = this;
                //selectInstanceAID
                log.debug("Select AID for JavaCard" + '00A40400' + '0F' + 'F7496E706173536F6369616C417001');
                var res = _this.get('cardReader').runAPDU('00A40400' + '0F' + 'F7496E706173536F6369616C417001');
                if (_this.checkTypeOfCard(res)) {
                    switch (action){
                        case 'read':
                            _this.readJavaCard(res.response);
                            break;
                        case 'update':
                            _this.updateJavaCard(res.response);
                            break;
                        default:
                            log.debug('Empty action parameter')
                    }
                    return;
                }


                log.debug("Select AID for Scon" + '00A40400' + '08' + 'F700000432900001');
                res = _this.get('cardReader').runAPDU('00A40400' + '08' + 'F700000432900001');
                if (_this.checkTypeOfCard(res)) {
                    switch (action){
                        case 'read':
                            _this.readSconCard();
                            break;
                        case 'update':
                            _this.updateSconCard();
                            break;
                        default:
                            log.debug('Empty action parameter')
                    }
                    return;
                }

                this.trigger('renderError', _this.get('errorMessage').NHL);

            },

            readJavaCard: function(selectRes){
                var _this = this;

                // Select 5 File
                log.debug("Select 5 file " + '80A4000504');
                var res = _this.get('cardReader').runAPDU('80A4000504');
                log.debug('Response: ' + res.response + 'SW' + res.sw);
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }
                var fileSize = res.response.substring(0, 4);
                var readKeyNumber = res.response.substring(4,6);

                // Initialize update
                var randNumber = _this.getRandomNumber();
                log.debug("Initialize update " + '805000' + readKeyNumber + '08' + randNumber);
                res = _this.get('cardReader').runAPDU('805000' + readKeyNumber + '08' + randNumber);
                console.log(res.response + ' ' + res.sw);
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }

                var updateRes = res.response;

                _this.getReadCommandsFormServerForJavaCard(updateRes, randNumber, selectRes, readKeyNumber, fileSize).then(function (result) {
                    // Execute server commands
                    var readData = '';
                    if (result.result) {
                        result.result.forEach(function (cmd) {
                            try {
                                log.debug("Run cmd" + cmd);
                                res = _this.get('cardReader').runAPDU(cmd);
                                log.debug('Response = ' + res.response + ' SW = ' + res.sw);
                            } catch (e) {
                                _this.trigger('renderError', e.message);
                            }

                            if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                                log.debug('Response = ' + res.response + ' SW = ' + res.sw);
                            }
                            if (cmd.substring(0, 4) == '84B0') {
                                readData += res.response.substring(0, res.response.length - 8);
                            }

                        });

                        log.debug("File5 DATA = " + readData);
                        // callServer for card data
                        _this.checkUserCard(readData).then(function (result) {
                            if (!result.checkResult || !result.cardData) {
                                _this.trigger('renderError', result.checkResult.failDescription)
                            } else {
                                _this.set('checkResult', result.checkResult);
                                _this.set('cardData', result.cardData);
                                _this.trigger('renderCard');

                            }
                        });
                    } else {
                        _this.trigger('renderError', _this.get('errorMessage').SERVER_FAIL)
                    }

                }).fail(function () {
                    //TODO
                    _this.trigger('renderError', _this.get('errorMessage').READ)
                })
                
            },

            readSconCard: function(){
                var _this = this;
                // select DFAID
                log.debug("Select DF " + '80A40000' + '02' + '4F00');
                var res = _this.get('cardReader').runAPDU('80A40000' + '02' + '4F00');
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }
                // select EF
                log.debug("Select EF " + '80A40000' + '02' + '0005');
                res = _this.get('cardReader').runAPDU('80A40000' + '02' + '0005');
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }
                // parse result
                var fileSize = res.response.substring(4, 8);

                // select EF 3EF0
                log.debug("Select EF " + '80A40000' + '02' + '3EF0');
                res = _this.get('cardReader').runAPDU('80A40000' + '02' + '3EF0');
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }
                // get chalange
                log.debug("Select EF " + '8084000008');
                res = _this.get('cardReader').runAPDU('8084000008');
                if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                    return;
                }
                var cardRandomNumber = res.response;
                // ask server for read commands
                _this.getReadCommandsFormServer(fileSize, cardRandomNumber, 5).then(function (result) {
                    // Execute server commands
                    var readData = '';
                    if (result.result) {
                        result.result.forEach(function (cmd) {
                            try {
                                log.debug("Run cmd " + cmd);
                                res = _this.get('cardReader').runAPDU(cmd);
                            } catch (e) {
                                _this.trigger('renderError', e.message);
                            }

                            if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                                log.debug('Response = ' + res.response + ' SW = ' + res.sw);
                            }
                            if (cmd.substring(0, 4) == '84B0') {
                                readData += res.response.substring(0, res.response.length - 8);
                            }

                        });
                        log.debug("File5 DATA = " + readData);
                        // callServer for card data
                        _this.checkUserCard(readData).then(function (result) {
                            if (!result.checkResult || !result.cardData) {
                                _this.trigger('renderError', result.checkResult.failDescription)
                            } else {
                                _this.set('checkResult', result.checkResult);
                                _this.set('cardData', result.cardData);
                                _this.trigger('renderCard');

                            }
                        });
                    } else {
                        _this.trigger('renderError', _this.get('errorMessage').SERVER_FAIL)
                    }

                }).fail(function () {
                    //TODO
                    _this.trigger('renderError', _this.get('errorMessage').READ)
                })
            },

            doUpdate: function () {
                var _this = this;
                log.debug("Selecting update type");
                _this.verifyCardType('update');
            },

            updateJavaCard: function (selectRes) {
                var _this = this;
                $.when(_this.updateJavaFile(2, selectRes))
                    .then(function () {return _this.updateJavaFile(3, selectRes)})
                    .then(function () {return _this.updateJavaFile(4, selectRes)})
                    .then(function () {
                        if (_this.get('checkResult')) {
                            //Если были обновления то обновляем файл 5
                            return _this.updateJavaFile(5, selectRes)
                        }
                    }).then(function () {
                    _this.trigger('renderDone')
                });/*.fail(function (xhr) {
                 _this.trigger('renderError', xhr.statusText)
                 });*/
            },

            updateJavaFile: function (fileNumber, selectRes) {
                log.debug("Prepare update file " + fileNumber);
                var _this = this;
                var def = new $.Deferred();
                var checkResult = _this.get('checkResult');
                var cardData = _this.get('cardData');
                if (!checkResult['hash' + fileNumber + 'Valid']) {
                        // Генерируем случайное число
                        var randNumber = _this.getRandomNumber();
                        var updateRes = _this.prepareWriteFileJavaCard(randNumber ,fileNumber);
                        if (!updateRes) {
                            return def.reject();
                        }
                        $.when(_this.writeFileJavaCard(fileNumber, updateRes, cardData.cardId, selectRes, randNumber))
                            .then(function(){
                                def.resolve();
                            })
                            .fail(function (xhr) {
                                def.reject();
                            });

                } else {
                    def.resolve();
                }
                return def.promise();
            },

            prepareWriteFileJavaCard: function (randNumber, fileNumber) {
                var _this = this;
                log.debug('prepareWriteFileJavaCard');
                var keyNumber ='';
                switch (fileNumber){
                    case 2:
                        keyNumber = '04';
                        break;
                    case 3:
                        keyNumber = '06';
                        break;
                    case 4:
                        keyNumber = '08';
                        break;
                    case 5:
                        keyNumber = '0A';
                        break;
                }
                
                try {
                    // Initialize update
                    log.debug("Initialize update " + '805000' + keyNumber + '08' + randNumber);
                    var res = _this.get('cardReader').runAPDU('805000' + keyNumber + '08' + randNumber);
                    console.log(res.response + ' ' + res.sw);
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return;
                    }
                    return res.response;
                } catch (e) {
                    _this.trigger('renderError', _this.get('errorMessage').READ)
                }

            },

            writeFileJavaCard: function (fileNumber, updateRes, cardId, selectRes, randNumber) {
                return this._updateFileDataJavaCard(fileNumber, updateRes, cardId, "getWriteCommandsJavaCard", selectRes, randNumber);
            },

            _updateFileDataJavaCard: function (fileNumber, respStr, cardId, action, selectRes, randNumber) {
                log.debug("Call server for Commands " + action);

                var _this = this;
                var def = new $.Deferred();
                var options = {
                    url: misc.getContextPath() + "/card/write",
                    action: action,
                    ml_request: true,
                    data: {
                        respStr: respStr,
                        selectRes: selectRes,
                        randNumber: randNumber,
                        fileNumber: fileNumber,
                        cardId: cardId
                    },
                    onFail: function (res) {
                        if (res.error) {
                            _this.trigger('renderError', res.message)
                        }
                    }
                };
                $.when(_this.callServerAction(options)).then(function (result) {
                    // Execute server commands
                    if (result.result) {
                        result.result.forEach(function (cmd) {
                            try {
                                log.debug("Execute command " + cmd);
                                var res = _this.get('cardReader').runAPDU(cmd);
                            } catch (e) {
                                console.error("Error update card: " + e.message)
                                _this.trigger('renderError', _this.get('errorMessage').WRITE);
                            }

                            if (!_this.checkSW9000Result(res, _this.get('errorMessage').WRITE)) {
                                def.reject({statusText: _this.get('errorMessage').WRITE});
                                return false;
                            }

                        });
                        def.resolve();

                    } else {
                        _this.trigger('renderError', _this.get('errorMessage').SERVER_FAIL)
                    }
                });

                return def.promise();
            },


            updateSconCard: function () {
                var _this = this;
                $.when(_this.updateSconFile(2))
                    .then(function () {return _this.updateSconFile(3)})
                    .then(function () {return _this.updateSconFile(4)})
                    .then(function () {
                        if (_this.get('checkResult')) {
                            //Если были обновления то обновляем файл 5
                            return _this.updateSconFile(5)
                        }
                    }).then(function () {
                    _this.trigger('renderDone')
                });/*.fail(function (xhr) {
                 _this.trigger('renderError', xhr.statusText)
                 });*/
            },

            updateSconFile: function (fileNumber) {
                log.debug("Prepare update file " + fileNumber);
                var _this = this;
                var def = new $.Deferred();
                var checkResult = _this.get('checkResult');
                var cardData = _this.get('cardData');
                if (!checkResult['hash' + fileNumber + 'Valid']) {
                    if (fileNumber != 5){
                        // Для всех файлов кроме 5 удаляем и создаем заново файлы
                        var randomCardNum = _this.prepareDeleteCreateFileSconCard();
                        if (!randomCardNum) {
                            return def.reject();
                        }
                        $.when(_this.deleteAndCreateFile(fileNumber, randomCardNum, cardData.cardId))
                            .then(function () {
                                randomCardNum = _this.prepareWriteFile();
                                if (!randomCardNum) {
                                    return def.reject();
                                }
                                return _this.updateFileData(fileNumber, randomCardNum, cardData.cardId);
                            })
                            .then(function(){
                                def.resolve();
                            })
                            .fail(function (xhr) {
                                def.reject();
                            });
                    } else {
                        // 5 файл просто обновляем
                        randomCardNum = _this.prepareWriteFile();
                        if (!randomCardNum) {
                            return def.reject();
                        }
                        $.when(_this.updateFileData(fileNumber, randomCardNum, cardData.cardId))
                            .then(function(){
                                def.resolve();
                            })
                            .fail(function(){def.reject();});
                    }
                } else {
                    def.resolve();
                }
                return def.promise();
            },

            deleteAndCreateFile: function (fileNumber, randomCardNum, cardId) {
                return this._updateFileData(fileNumber, randomCardNum, cardId, "getDeleteCreateCommands");
            },
            updateFileData: function (fileNumber, randomCardNum, cardId) {
                return this._updateFileData(fileNumber, randomCardNum, cardId, "getWriteCommands");
            },

            _updateFileData: function (fileNumber, randomCardNum, cardId, action) {
                log.debug("Call server for Commands " + action);

                var _this = this;
                var def = new $.Deferred();
                var options = {
                    url: misc.getContextPath() + "/card/write",
                    action: action,
                    ml_request: true,
                    data: {
                        randomCardNum: randomCardNum,
                        fileNumber: fileNumber,
                        cardId: cardId
                    },
                    onFail: function (res) {
                        if (res.error) {
                            _this.trigger('renderError', res.message)
                        }
                    }
                };
                $.when(_this.callServerAction(options)).then(function (result) {
                    // Execute server commands
                    if (result.result) {
                        result.result.forEach(function (cmd) {
                            try {
                                log.debug("Execute command " + cmd);
                                var res = _this.get('cardReader').runAPDU(cmd);
                            } catch (e) {
                                console.error("Error update card: " + e.message)
                                _this.trigger('renderError', _this.get('errorMessage').WRITE);
                            }

                            if (!_this.checkSW9000Result(res, _this.get('errorMessage').WRITE)) {
                                def.reject({statusText: _this.get('errorMessage').WRITE});
                                return false;
                            }

                        });
                        def.resolve();

                    } else {
                        _this.trigger('renderError', _this.get('errorMessage').SERVER_FAIL)
                    }
                });

                return def.promise();
            },


            prepareDeleteCreateFileSconCard: function () {
                var _this = this;
                log.debug('prepareDeleteCreateFileSconCard');

                try {
                    // select DFAID
                    log.debug("Select DF CMD : " + '80A40000' + '02' + '3F00');
                    res = _this.get('cardReader').runAPDU('80A40000' + '02' + '3F00');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    log.debug("Select EF CMD : " + '80A40000' + '02' + '2FE0');
                    // select EF 2FE0
                    res = _this.get('cardReader').runAPDU('80A40000' + '02' + '2FE0');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    // get chalange
                    log.debug("GET Challenge : " + '8084000008');
                    res = _this.get('cardReader').runAPDU('8084000008');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    return res.response;
                } catch (e) {
                    _this.trigger('renderError', _this.get('errorMessage').READ)
                }

            },

            prepareWriteFile: function () {
                var _this = this;
                log.debug('prepareWriteFile');
                //selectInstanceAID
                log.debug("Select AID CMD : " + '00A40400' + '08' + 'F700000432900001');

                try {
                    var res = _this.get('cardReader').runAPDU('00A40400' + '08' + 'F700000432900001');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').NHL)) {
                        return false;
                    }
                    // select DFAID
                    log.debug("Select DF CMD : " + '80A40000' + '02' + '4F00');
                    res = _this.get('cardReader').runAPDU('80A40000' + '02' + '4F00');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    // select EF 3EF0
                    log.debug("Select EF CMD : " + '80A40000' + '02' + '3EF0');
                    res = _this.get('cardReader').runAPDU('80A40000' + '02' + '3EF0');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    // get chalange
                    log.debug("GET Challenge : " + '8084000008');
                    res = _this.get('cardReader').runAPDU('8084000008');
                    if (!_this.checkSW9000Result(res, _this.get('errorMessage').READ)) {
                        return false;
                    }
                    return res.response;
                } catch (e) {
                    _this.trigger('renderError', _this.get('errorMessage').READ)
                }

            },


            checkUserCard: function (file5Data) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + "/card/check",
                    action: 'checkCardAndGetHolder',
                    ml_request: true,
                    data: {
                        file5Data: file5Data

                    },
                    onFail: function (responseData) {
                        _this.trigger('renderError', responseData.message)
                    }
                };
                return _this.callServerAction(options);
            },


            checkSW9000Result: function (res, errorMessage) {
                if (!res || !res.sw) return false;

                log.debug('CMD result = ' + res.sw);
                if (res.sw != '9000') {
                    this.trigger('renderError', errorMessage);
                    return false;
                }
                return true;
            },

            checkTypeOfCard: function(res){
                return res.sw == '9000';
            },

            getReadCommandsFormServer: function (fileSize, randomCardNum, fileNumber) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + "/card/read",
                    action: 'getReadCommands',
                    ml_request: true,
                    data: {
                        randomCardNum: randomCardNum,
                        fileNumber: fileNumber,
                        fileSize: fileSize
                    }
                };
                return _this.callServerAction(options);
            },

            getReadCommandsFormServerForJavaCard: function (res, randomNumber, selectRes, readKeyNumber, fileSize) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + "/card/read",
                    action: 'getReadCommandsJavaCard',
                    ml_request: true,
                    data: {
                        respStr: res,
                        selectRes: selectRes,
                        randNumber: randomNumber,
                        readKeyNumber: readKeyNumber,
                        fileSize: fileSize
                    }
                };
                return _this.callServerAction(options);
            },

            getRandomNumber: function(){
                var randNumber = '';
                for(var i = 0; i < 8; i++){
                    randNumber += Math.round(Math.random() * 255).toString(16).toUpperCase();
                }
                if(randNumber.length % 2 == 0){
                    return randNumber;
                } else{
                    return 0 + randNumber;
                }

            }
        });
        return model;
    });
