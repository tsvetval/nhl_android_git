/**
 * Created by Sergei on 19.02.2016.
 */


define(
    ['log', 'misc', 'backbone',
        'nhl/page_block/card_status/model/ChangeCardStatusModel',
        'nhl/page_block/card_status/view/ChangeCardStatusView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });