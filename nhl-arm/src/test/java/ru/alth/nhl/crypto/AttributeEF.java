package ru.alth.nhl.crypto;

/**
 * Created by User on 24.11.2015.
 */
public class AttributeEF {

    String fileID;
    String fileSize;
    String fileAccessory;
    String fileAcByPin;
    String fileAcByKey;
    String fileType;


    public AttributeEF(String selectResponse) {
        if (selectResponse.length() != 16)
            throw new RuntimeException("Select Respone to EF must be 8 bytes length");

        fileID = selectResponse.substring(0, 4);
        fileSize = selectResponse.substring(4, 8);
        fileAccessory = selectResponse.substring(8, 10);
        fileAcByPin = selectResponse.substring(10, 12);
        fileAcByKey = selectResponse.substring(12, 14);

        fileType = selectResponse.substring(14, 16);

    }


    public int getFileSizeDec() {
        return Integer.parseInt(fileSize, 16);
    }
}
