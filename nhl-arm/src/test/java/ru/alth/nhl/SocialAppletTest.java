package ru.alth.nhl;

import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.util.StringBuilders;
import org.codehaus.groovy.runtime.StringBufferWriter;
import org.codehaus.groovy.runtime.powerassert.SourceText;
import ru.alth.nhl.util.crypto.*;

import javax.smartcardio.CardException;
import javax.xml.bind.DatatypeConverter;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 *
 */
public class SocialAppletTest {

    public static void main(String[] args) throws Exception {
//        auth();
//        diverKey();
//        generateReadCmdList();
//        getWriteFileCommands();
//        test3DESWithIV();
//        getRandomNumber();
//        externalAuthentication();

//        String data ="DF210A00000000000000000305DF2205C7E5EDE8F2DF230100DF240100DF250F2833343529203336332D34362D3433DF261764666764666764666764664064666764666766672E7275DF2700DF5220E81F58CFEDF00A6BF6F3C55D16A4A698708207F470332C4765C9E5F426177E9235CD60348BAC521B0BC5F9441237";
//        Map<String, String> res = parseTLV(data);
//        System.out.println(res);
        bytesCounter();
    }

    public static void bytesCounter() throws UnsupportedEncodingException {
        String word = "Трамвай №46";
        String asciWord = DatatypeConverter.printHexBinary(word.getBytes("UTF-8"));
        System.out.println("asciWord = " + asciWord);
        System.out.println(asciWord.length()/2);
    }

    public static void externalAuthentication() throws SMartChipSMException, IOException {
        String inputCommand = "010001D26C34DD43071658739CEC0EC1209000";
        String randomNumber = "EB49DA61821F8F54";
        String sessionKey = "BD5A8F4301AC9E68B3A99391AB0B9DD2";
        String fileNumber ="05";

        // Счётчик аутентификаций
        String counter = inputCommand.substring(2, 6);

        // Случайное число, генерируемое картой
        String challenge = inputCommand.substring(6, 18);

        // Формирование блока Dhost
        String dHost = counter + challenge + randomNumber + "8000000000000000";

        byte [] dcrHost = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(sessionKey), DatatypeConverter.parseHexBinary(dHost), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);

        String outputCrypt = DatatypeConverter.printHexBinary(dcrHost)
                .substring(DatatypeConverter.printHexBinary(dcrHost).length() - 16, DatatypeConverter.printHexBinary(dcrHost).length());
        // Команда APDU External Authentication
        String cmd = "808200" + fileNumber + "08" + outputCrypt;
        System.out.println(cmd);
    }

    public static void getRandomNumber(){
        String randNumber = "";
        for(int i = 0; i < 8; i++){
            randNumber += String.format("%02x", Math.round(Math.random() * 255)).toUpperCase();
        }
        if(randNumber.length() % 2 == 0){
            System.out.println(randNumber);
        } else{
            System.out.println(0 + randNumber);
        }
    }

    public static void auth() throws SMartChipSMException {
        String inputCommand = "010001D26C34DD43071658739CEC0EC120";
        System.out.println("Команда, полученная от карты = " + inputCommand);
        String randomNumber = "EB49DA61821F8F54";
        System.out.println("Случайное число, использованное при initialize update = " + randomNumber);
        String keyValue = "00112233445566778899AABBCCDDEEFF";

        // Счётчик аутентификаций
        String counter = inputCommand.substring(2, 6);
        System.out.println("Счётчик аутентификаций = " + counter);
        // Случайное число, генерируемое картой
        String challenge = inputCommand.substring(6, 18);
        System.out.println("Случайное число, генерируемое картой = " + challenge);

        // Криптограмма карты
        String crypt = inputCommand.substring(18, inputCommand.length());

        System.out.println("Ключ  = " + keyValue);
        System.out.println("DIV = " + String.format("%-32s", counter).replace(" ", "0"));

        byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(keyValue),
                DatatypeConverter.parseHexBinary(String.format("%-32s", counter).replace(" ", "0")), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        System.out.println("Сессионный ключ (алгоритм 3DES2) = " + DatatypeConverter.printHexBinary(sessionKey));
        // Формирование блока Dhost
        String dHost = counter + challenge + "EB49DA61821F8F54" + "8000000000000000";
        System.out.println("Dhost = " + dHost);

        byte [] dcrHost = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(dHost), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        System.out.println("DCRhost (алгоритм 3DES2)= " + DatatypeConverter.printHexBinary(dcrHost));
        String crypto = DatatypeConverter.printHexBinary(dcrHost)
                .substring(DatatypeConverter.printHexBinary(dcrHost).length() - 16, DatatypeConverter.printHexBinary(dcrHost).length());
        System.out.println("Криптограмма хоста - последние 8 байт = " + crypto);
        System.out.println("Команда External Authenticate = " + "8082000508" + crypto);
    }

    public static void diverKey() throws SMartChipSMException {
        String selectRes = "6F24840FF7496E706173536F6369616C417001A511BF0C0EDF110B0000000000000000000017";
        String serialNum = selectRes.substring(selectRes.length() - 16);
        System.out.println(serialNum);
        System.out.println(inverse(serialNum));
        String keyDerivationData = serialNum + inverse(serialNum);
        String originalKey = "404142434445464748494A4B4C4D4E4F";
        byte [] cardKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(originalKey),
                DatatypeConverter.parseHexBinary(keyDerivationData), EnCipherMode.ECB, EnCipherDirection.ENCRYPT_MODE);
        String crypto = DatatypeConverter.printHexBinary(cardKey);
        System.out.println(crypto);


    }

    private static String inverse(String string){
        Map<Character, Character> map = new HashMap<>();
        map.put('0', 'F');
        map.put('1', 'E');
        map.put('2', 'D');
        map.put('3', 'C');
        map.put('4', 'B');
        map.put('5', 'A');
        map.put('6', '9');
        map.put('7', '8');
        map.put('8', '7');
        map.put('9', '6');
        map.put('A', '5');
        map.put('B', '4');
        map.put('C', '3');
        map.put('D', '2');
        map.put('E', '1');
        map.put('F', '0');
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < string.length(); i++){
            output.append(map.get(string.charAt(i)));
        }
        return output.toString();
    }


    public static void generateReadCmdList() throws SMartChipSMException, IOException, CardException {
        List<String> resultList = new ArrayList<>();

        int blockSize = 192;
        String ClaIns = "84B0";
        int offset = 0;

        String sessionKey = "46A75707FF58B6E3F89D4C327DCEF8CB";
        String hostCrypto = "FB5DE58446FF73DA";

        int fileSize = 16;

        while (fileSize > offset) {
            int length;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset);
            int cmdLen = length;
            String cmd1 = ClaIns + p1p2 + String.format("%02X", cmdLen) + "800000";
            System.out.println(cmd1);
            String cmd = ClaIns + p1p2 + "09" + String.format("%02X", cmdLen);
            System.out.println(cmd);
            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(cmd1), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE, DatatypeConverter.parseHexBinary(hostCrypto)));
            System.out.println(calcMacData);
//            String mac = calcMacData.substring(0, 8);
            cmd = cmd + calcMacData;
            resultList.add(cmd);
            offset += length;
        }
    }

    public static void getWriteFileCommands() throws Exception {
        Long fileNumber = 5L;
        String fileData = "DF210A00000000000000000303DF2200DF2300DF2400DF2500DF2600DF2700DF52203CBBF7F697C85BB07641CE09F6F795FDD789B2418749FBFE7160ABB154318434";
        String sessionKey = "07B39EF738EF2C80761F86323505837C";
        String hostCrypto = "250750F38B012ABA";
        List<String> resultList = new ArrayList<>();
//        int blockSize = 234;//192; // 192 -
        int blockSize = 192;
        String ClaIns = "84D0";
        int offset = 0;
        int fileSize = fileData.length();
        int length = 0;
        byte [] data = DatatypeConverter.parseHexBinary(fileData);
        StringBuilder stringBuilder = new StringBuilder(fileData);
        while (fileSize > offset) {
            if (fileData.length() - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileData.length() - offset;
            }

//            String dataStr = DatatypeConverter.printHexBinary(Arrays.copyOfRange(data, offset, offset + length));
            String dataStr = stringBuilder.substring(offset, offset + length);
            String dataForEncrypt = dataStr + "80";

            System.out.println("DataStr " + dataForEncrypt);

            while (dataForEncrypt.length() % 16 != 0){
                System.out.println(dataForEncrypt.length());
                dataForEncrypt += "00";
            }

            System.out.println(dataForEncrypt.length());

            String encryptedData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(dataForEncrypt), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE));
            System.out.println("EncryptedData " + encryptedData);

            while ((10 + dataStr.length()) % 16 != 0){
                System.out.println(dataStr.length());
                dataStr += "00";
            }

            String len = String.format("%02X", dataStr.length() / 2);
            System.out.println("Len " + len);

            String p1p2 = String.format("%04X", offset); //offset.ToString("X04");
            System.out.println("p1p2 " + p1p2);

            String dataForMac = ClaIns + p1p2 + len + dataStr;

            System.out.println("dataForMac " + dataForMac);

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(dataForMac), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE, DatatypeConverter.parseHexBinary(hostCrypto)));
            System.out.println("CalcMacData " + calcMacData);

            String blockLength = String.format("%02X", (encryptedData.length() + calcMacData.length()) / 2);
            System.out.println("BlockLength " + blockLength);

            String cmd = ClaIns + p1p2 + blockLength + encryptedData + calcMacData;
            System.out.println("Cmd " + cmd);

            resultList.add(cmd);
            offset += length;
        }
    }



    public static void test3DESWithIV() throws SMartChipSMException {
        String dataForMac = "84D0000042DF210A00000000000000000303DF2200DF2300DF2400DF2500DF2600DF2700DF52203CBBF7F697C85BB07641CE09F6F795FDD789B2418749FBFE7160ABB15431843480";
        String sessionKey = "7C083A5D9E79BBE89BB407D7715C7C42";
        String hostCrypto = "464C5988FD13E2B8";
        String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(DatatypeConverter.parseHexBinary(sessionKey),
                DatatypeConverter.parseHexBinary(dataForMac), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE, DatatypeConverter.parseHexBinary(hostCrypto)));
        System.out.println("CalcMacData " + calcMacData);
        System.out.println("CalcMacData " + calcMacData.substring(calcMacData.length() - 16));
    }


    public static  Map<String, String> parseTLV(String data) {

        Map<String, String> resultTags = new TreeMap<String, String>();
        String tag = "";
        int len = 0;
        String value = "";
        boolean flag;

        System.out.println((String.format("Parsing TLV String: %1$s ...", data)));

        for (int i = 0; i < data.length(); ) {
            flag = false;

            tag = data.substring(i, i + 2);
            if ((Integer.parseInt(tag, 16) & 0x20) == 0x20) flag = true;

            i += 2;
            if ((Integer.parseInt(tag, 16) & 0x1F) == 0x1F)
                for (; ; ) {
                    tag += data.substring(i, i + 2);
                    i += 2;
                    if ((Integer.parseInt(tag, 16) & 0x80) != 0x80) break;
                }
            try {
                len = Integer.parseInt(data.substring(i, i + 2), 16);
                i += 2;
            } catch (Exception e){
                break;
            }
            if ((len & 0x80) == 0x80) {
                int tmp = (len & 0x7F) * 2;
                try{
                    len = Integer.parseInt(data.substring(i, i + tmp), 16);
                    i += tmp;
                } catch (Exception e){
                    break;
                }

            }

            len *= 2;
            try {
                value = data.substring(i, i + len);
            } catch (Exception e){
                break;
            }

            i += len;
            if (flag == true) {
                Map<String, String> tmpTags = parseTLV(value);
                for (Map.Entry<String, String> entry : tmpTags.entrySet())
                    resultTags.put(entry.getKey(), entry.getValue());
            } else {
                resultTags.put(tag, value);
                System.out.println((String.format("Found new tag: '%1$s', value: '%2$s'", tag, value)));
            }
        }

        return resultTags;
    }
}
