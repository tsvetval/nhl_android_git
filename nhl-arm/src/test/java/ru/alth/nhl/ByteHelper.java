package ru.alth.nhl;

/**
 * Created by d_litovchenko on 14.01.15.
 */
public class ByteHelper {

    public static String binaryToHexString(byte[] bytes) {
        if(bytes == null){
            return "null";
        }
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToBinary(String str) {
        try {
            String tmp = str.replaceAll(" ", "").toUpperCase();
            if ((tmp.length() % 2) != 0) tmp += "0";

            int count = tmp.length() / 2;

            byte[] res = new byte[count];
            for (int i = 0; i < count; i++) {
                res[i] = (byte) (int) Integer.decode("0x" + tmp.substring(2 * i, 2 * i + 2));
            }
            return res;
        } catch (Exception ex) {
        }
        return new byte[]{};
    }
}
