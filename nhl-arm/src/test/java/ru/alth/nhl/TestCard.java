package ru.alth.nhl;

import ru.alth.nhl.crypto.*;
import ru.alth.nhl.util.TLV;
import ru.alth.nhl.util.crypto.*;

import javax.smartcardio.*;
import javax.xml.bind.DatatypeConverter;
import java.util.Date;
import java.util.Map;

/**
 * Created by User on 24.11.2015.
 */
public class TestCard {



    //    static {
//        try {
//            System.setProperty("sun.security.smartcardio.t0GetResponse", "false");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    final static String InstanceAID = "F700000432900001";
    final static String DF_ID = "4F00";


    public static void main(String[] arg) {
        CardTerminals terminals = TerminalFactory.getDefault().terminals();
        boolean doWhile = true;

        while (doWhile) {
            try {
                for (CardTerminal cardTerminal : terminals.list()) {
                    if (!doWhile) {
                        break;
                    }
                    Card card = null;
                    if (cardTerminal.isCardPresent()) {
                        card = cardTerminal.connect("*");
                        ATR atr = card.getATR();
                        System.out.println("Card CONNECT " + card.getProtocol());
                        System.out.println("Card ATR " + DatatypeConverter.printHexBinary(atr.getBytes()));
                        System.out.println("Card hist ATR " + DatatypeConverter.printHexBinary(atr.getHistoricalBytes()));

                        CardChannel cardChannel = card.getBasicChannel();
                        long startTime = new Date().getTime();
                        dataReadTest(cardChannel);
                        System.out.println("******************************Assert RESULTS");
                        System.out.println("Write and Read - OK");
                        System.out.println(String.format("Total time : [%s] ms", new Date().getTime() - startTime));

                        doWhile = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                doWhile = false;
            }
        }
    }

    public static void dataReadTest(CardChannel cardChannel) throws Exception {
        //Пытемся сделать селект сконовского приложения

        selectInstanceAID(cardChannel);
        //CardData cardData = SconReaderHelper.readCardData(cardChannel);
        System.out.println(String.format("***********************Finish Read[%s] ", 1));

        SconWriteHalper.writeTestCardData(cardChannel);


        if (1==1){
            return;
        }







        selectInstanceAID(cardChannel);
        selectDFAID(cardChannel, DF_ID);

        long startTime = new Date().getTime();
        System.out.println(String.format("***********************Start Test Read File [%s] ", 1));
        selectEFAID(cardChannel, "3EF0");
        byte keyNumber = 2;
        String sessionKey = authenticateToFile(cardChannel, "5D07291A0B73E5EA2FBCDA924FC4346D", keyNumber);
        AttributeEF attr = selectEFAID(cardChannel, "0001");

        System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        String fileData = readBinarySec(cardChannel, attr.getFileSizeDec(), sessionKey);
        System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",1, fileData));



        Map<String, String> fileResult =  TLV.parseTLV(fileData);
        System.out.println(String.format("Last Name [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF12")))));
        System.out.println(String.format("First Name [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF13")))));
        System.out.println(String.format("Second Name [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF14")))));
        System.out.println(String.format("Date Birth [%s]", fileResult.get("DF15")));
        System.out.println(String.format("Exp Date [%s]", fileResult.get("DF16")));
        System.out.println(String.format("CardId [%s]", fileResult.get("DF17")));
        System.out.println(String.format("Blood resus [%s] 00 - negativ 01 - positive", fileResult.get("DF18")));
        System.out.println(String.format("Blood group [%s]", fileResult.get("DF19")));

        System.out.println(String.format("***********************End Test Read File [%s] time : [%s] ms", 1, new Date().getTime() - startTime));

        // Read file 0002
        startTime = new Date().getTime();
        System.out.println(String.format("***********************Start Test Read File [%s] ", 2));
        selectEFAID(cardChannel, "3EF0");
        keyNumber = 4;
        sessionKey = authenticateToFile(cardChannel, "989780E6C88331BFDAF2A2FE4AB50775", keyNumber);
        attr = selectEFAID(cardChannel, "0002");

        System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        fileData = readBinarySec(cardChannel, attr.getFileSizeDec(), sessionKey);
        System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",2, fileData));

        fileResult =  TLV.parseTLV(fileData);
        System.out.println(String.format("Team name [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF21")))));
        System.out.println(String.format("Men Status [%s]", fileResult.get("DF22")));
        System.out.println(String.format("Phone number [%s]", fileResult.get("DF23")));
        System.out.println(String.format("Email [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF24")))));

        System.out.println(String.format("***********************End Test Read File [%s] time : [%s] ms", 2, new Date().getTime() - startTime));

        // Read file 0003
        startTime = new Date().getTime();
        System.out.println(String.format("***********************Start Test Read File [%s] ", 3));

        selectEFAID(cardChannel, "3EF0");
        keyNumber = 6;
        sessionKey = authenticateToFile(cardChannel, "8075A2E561C894E0B5F2B59D640BAE37", keyNumber);
        attr = selectEFAID(cardChannel, "0003");

        System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        fileData = readBinarySec(cardChannel, attr.getFileSizeDec(), sessionKey);
        System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",3, fileData));

        fileResult =  TLV.parseTLV(fileData);
        System.out.println(String.format("Insurance company [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF31")))));
        System.out.println(String.format("Insurance number [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF32")))));
        System.out.println(String.format("med explore date [%s]", fileResult.get("DF33")));
        System.out.println(String.format("med explore Result [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF34")))));
        System.out.println(String.format("Height [%s]", fileResult.get("DF35")));
        System.out.println(String.format("Weight [%s]", fileResult.get("DF36")));
        System.out.println(String.format("Protivopokazania [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF37")))));
        System.out.println(String.format("Allergia [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF38")))));
        System.out.println(String.format("Ills [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF38")))));

        System.out.println(String.format("***********************End Test Read File [%s] time : [%s] ms", 3, new Date().getTime() - startTime));


        // Read file 0004
        startTime = new Date().getTime();
        System.out.println(String.format("***********************Start Test Read File [%s] ", 4));
        selectEFAID(cardChannel, "3EF0");
        keyNumber = 8;
        sessionKey = authenticateToFile(cardChannel, "5DF8A80E23757986583D58A4D9B9E3D3", keyNumber);
        attr = selectEFAID(cardChannel, "0004");

        System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        fileData = readBinarySec(cardChannel, attr.getFileSizeDec(), sessionKey);
        System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",4, fileData));

        fileResult =  TLV.parseTLV(fileData);
        System.out.println(String.format("Dopusk na match [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF41")))));
        System.out.println(String.format("passport [%s]", fileResult.get("DF42")));
        System.out.println(String.format("prava [%s]", new String(DatatypeConverter.parseHexBinary(fileResult.get("DF43")))));

        System.out.println(String.format("***********************End Test Read File [%s] time : [%s] ms", 4, new Date().getTime() - startTime));


        // Read file 0005
        startTime = new Date().getTime();
        System.out.println(String.format("***********************Start Test Read File [%s] ", 5));
        selectEFAID(cardChannel, "3EF0");
        keyNumber = 10;
        sessionKey = authenticateToFile(cardChannel, "3107B6BAFE8C4094548C7C794AD98C86", keyNumber);
        attr = selectEFAID(cardChannel, "0005");

        System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        fileData = readBinarySec(cardChannel, attr.getFileSizeDec(), sessionKey);
        System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",4, fileData));

        fileResult =  TLV.parseTLV(fileData);
        System.out.println(String.format("HASH [%s]", fileResult.get("DF51")));

        System.out.println(String.format("***********************End Test Read File [%s] time : [%s] ms", 5, new Date().getTime() - startTime));



        System.out.println(String.format("Finish Test success [%s]","OK"));

    }


    public static void selectInstanceAID(CardChannel cardChannel) throws Exception {
        // SelectAID  07A0000000041010
        String cmd = "00A40400"
                + byteToHexString((byte) ((InstanceAID).length() / 2))
                + InstanceAID;
        System.out.println(String.format("Select Instance comand : [%s]", cmd));

        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd
        )));
        System.out.println("SelectAID " + byteToHexString((byte) response.getSW1()) + " "
                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));


        // Map<String, String> SelectAIDTlvResult = TLV.parseTLV(binaryToHexString(response.getData()));
        // System.out.println("PDOL  = " + SelectAIDTlvResult.get("9F38"));


//        SelectResult result = new SelectResult();
//        result.setTags(SelectAIDTlvResult);
//
//        return result;

    }

    public static void selectDFAID(CardChannel cardChannel, String df) throws Exception {
        String cmd = "80A40000"
                + byteToHexString((byte) (df.length() / 2))
                + df;
        System.out.println(String.format("Select DF [%s] comand : [%s]", df, cmd));

        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd
        )));
        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));


//        dirId = selectResponse.Substring(0, 4);
//        maxFileSize = selectResponse.Substring(4, 4);
//        dirAttribute = selectResponse.Substring(8, 2);
//        countSubDF = selectResponse.Substring(10, 2);
//        countEF = selectResponse.Substring(12, 2);
//        counterPin1 = selectResponse.Substring(14, 2);
//        counterPin2 = selectResponse.Substring(16, 2);


    }

    public static AttributeEF selectEFAID(CardChannel cardChannel, String ef) throws Exception {
        String cmd = "80A40000"
                + byteToHexString((byte) (ef.length() / 2))
                + ef;
        System.out.println(String.format("Select EF [%s] comand : [%s]", ef, cmd));

        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd
        )));
        System.out.println("SelectEF " + byteToHexString((byte) response.getSW1()) + " "
                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
        return new AttributeEF(DatatypeConverter.printHexBinary(response.getData()));

    }

    public static String authenticateToFile(CardChannel cardChannel, String key16, byte keyNumber) throws Exception {
        String cmd = "8084000008";
        System.out.println(String.format("Get challenge cmd : [%s]", cmd));
        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd)));
        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));

        String randomCardNum = DatatypeConverter.printHexBinary(response.getData());
        String randomTermNum = "EB49DA61821F8F54";
        String diverData = randomCardNum + randomTermNum;

        byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(key16), DatatypeConverter.parseHexBinary(diverData), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        byte[] cryptogrammTerminalBytes = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(randomCardNum), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        String cryptogrammTerminal = DatatypeConverter.printHexBinary(cryptogrammTerminalBytes);

        cryptogrammTerminal = cryptogrammTerminal.substring(0, 8); // : 0DDEBC19

        cmd = "808200" + byteToHexString(keyNumber) + "0C" + randomTermNum + cryptogrammTerminal; // 01 -
        System.out.println(String.format("Start Auth command : [%s]", cmd));
        response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd)));
        System.out.println("Start Auth " + byteToHexString((byte) response.getSW1()) + " "
                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));

        return DatatypeConverter.printHexBinary(sessionKey);

    }

    public static String byteToHexString(byte inputByte) {
        return DatatypeConverter.printHexBinary(new byte[]{inputByte});
    }


    public static String readBinarySec(CardChannel cardChannel, int fileSize, String sessionKey) throws SMartChipSMException, CardException {
        int blockSize = 192;//192; // 192 -
        String ClaIns = "84B0"      ;
        int offset = 0;

        StringBuilder sb = new StringBuilder();

        while (fileSize > offset) {
            int length = 0;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset); //offset.ToString("X04");

            int cmdLen = length + 4;

            String cmd = ClaIns + p1p2 + "05" + String.format("%02X", cmdLen);// cmdLen.ToString("X2");

            //Padder padder = new Padder();
            //padder.AddRange(StringConvert.HexStringToByteArray(cmd));

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(cmd), EnMacMode.MAC_RETAIL));

//                    StringConvert.ByteArrayToHexString(SupportDllCrypto.CalculateRetailMac(
//                    padder.ToPaddedArrayDpfs(),
//                    StringConvert.HexStringToByteArray(sessionKey)));

            String mac = calcMacData.substring(0, 8);
            cmd = cmd + mac;
            //String readAnsw = sendApdu(cmd + mac, "Read BinarySec");
            System.out.println(String.format("Read command : [%s]", cmd));
            ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd)));
            System.out.println("Read command result " + byteToHexString((byte) response.getSW1()) + " "
                    + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
            String readAnsw = DatatypeConverter.printHexBinary(response.getData());

            if (readAnsw.length() / 2 != cmdLen)
                throw new RuntimeException("Read card error wrong number of read bytes");

            String readData = readAnsw.substring(0, readAnsw.length() - 8);
            String readMac = readAnsw.substring(readAnsw.length() - 8, readAnsw.length());

//            padder = new Padder();
//            padder.AddRange(StringConvert.HexStringToByteArray(readData));

            String calcReadMac = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(readData), EnMacMode.MAC_RETAIL));

//                    StringConvert.ByteArrayToHexString(SupportDllCrypto.CalculateRetailMac(
//                    padder.ToPaddedArrayDpfs(),
//                    StringConvert.HexStringToByteArray(sessionKey)));


            if (!calcReadMac.substring(0, 8).equals(readMac))
                throw new RuntimeException("Read Binary Sec error. readedMAC is not equal calculated read Mac");

            sb.append(readData);
            offset += length;
        }

        return sb.toString();
    }



}
