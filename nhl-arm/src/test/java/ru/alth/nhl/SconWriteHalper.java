package ru.alth.nhl;

import ru.alth.nhl.crypto.*;
import ru.alth.nhl.util.crypto.*;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

/**
 *
 */
public class SconWriteHalper {
/*    <key value="00112233445566778899AABBCCDDEEFF" number="01" />
    <key value="FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" number="02" />
    <key value="A0A1A2A3A4A5A6A7B0B1B2B3B4B5B6B7" number="03" />
    <key value="44444444444444444444444444444444" number="04" />
    <key value="55555555555555555555555555555555" number="05" />
    <key value="66666666666666666666666666666666" number="06" />
    <key value="77777777777777777777777777777777" number="07" />
    <key value="88888888888888888888888888888888" number="08" />
    <key value="99999999999999999999999999999999" number="09" />
    <key value="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" number="0A" />*/

    private static final String rootKey = "30313233343536373839414243444546";
    private static final String rootDFId = "3F00";
    private static final String rootEFId = "2FE0";

    final static String DF_ID = "4F00";

    public static final CardData writeTestCardData(CardChannel isoDep) throws Exception {
        deleteFileAndCreateFile(isoDep);

        CardData cardData = new CardData();

        selectDFAID(isoDep, DF_ID);
        //writeFile1(cardData, isoDep);
        writeFile2(cardData, isoDep);
       // readFile3(cardData, isoDep);
        //(cardData, isoDep);
        return cardData;
    }

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd.MM.yyyy");

    private static void deleteFileAndCreateFile(CardChannel isoDep) throws Exception {
        // Авторизуемся на рутовой директории
        selectDFAID(isoDep, rootDFId);
        selectEFAID(isoDep, rootEFId);
        String sessionKey = authenticateToFile(isoDep, rootKey, (byte)1);
        // Выбираем директорию
        selectDFAID(isoDep, DF_ID);
        //AttributeEF attr = selectEFAID(isoDep, "0002");
        //selectEFAID(isoDep, "3EF0");
        // удаляем файл 2
       // TODO  Select(ef.ID) ???
        deleteFile(isoDep, "0002", sessionKey);

        String conditionalAcKey  = String.format("%01X", 4)/*read key number*/ + String.format("%01X", 3)/*write key number*/;
        createFile(isoDep, "0002", 170, conditionalAcKey,  sessionKey);

    }

    private static void deleteFile(CardChannel isoDep, String fileId, String sessionKey) throws Exception {
        Byte P1 = 0x00;
        Byte P2 = 0x00;
        Byte P3 = 0x06;

        String cmdDel = "84E4"
                + String.format("%02X", P1)
                + String.format("%02X", P2)
                + String.format("%02X", P3)
                + fileId;
        String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                DatatypeConverter.parseHexBinary(cmdDel), EnMacMode.MAC_RETAIL));

        String macData = calcMacData.substring(0, 8);
        cmdDel += macData;

        System.out.println(String.format("Delete command : [%s]", cmdDel));
        CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmdDel))).getBytes());

        System.out.println(String.format("Delete answer : [%s]", response.toString()));
    }

    private static void createFile(CardChannel isoDep, String fileId, Integer fs, String conditionalAcKey,  String sessionKey) throws Exception {
        byte P1 = 0x10; // (0x10 - for Elementary File)
        byte P2 = 0x02; // (0x02 - always)
        byte P3 = 0x0B; // (0x0B - always)

        String fileSize = String.format("%04X", fs);//"2C40"; //

        // 00h - удаление возможно
        // 80h - удаление невозможно
        String dfType = "00";  //00h (80h) – бинарный; 01h (81h) - кошелек; 03h (83h) – файл ключей; 10h – директория DF

        // 1. For directory 0x00 always
        // 2. files conditional By PIN
        //    first nibl - read conditional (0	ALW- всегда ; 1 - По предъявлению PIN1 ; 2 - По предъявлению PIN2; 6 - По предъявлению ADM; F - NEV никогда)
        //    second nible - write conditional (0	ALW- всегда ; 1 - По предъявлению PIN1 ; 2 - По предъявлению PIN2; 6 - По предъявлению ADM; F - NEV никогда)
        String conditionalAcPin = "00";

        //string conditionalAcKey = "21";
        String cmd = "84E0";
        //executeCreationCommand("84E0", P1, P2, P3, fileSize, fileId, dfType, conditionalAcPin, conditionalAcKey);
        String cmdCreateDir = "84E0"
                + String.format("%02X", P1)
                + String.format("%02X", P2)
                + String.format("%02X", P3)
                + fileSize
                + fileId
                + dfType
                + conditionalAcPin
                + conditionalAcKey;  // 1 byte 4-7 bits read key number  0-3 write  key number

        String calcMacData = cmdCreateDir;

        String macData = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                DatatypeConverter.parseHexBinary(calcMacData), EnMacMode.MAC_RETAIL));

        macData = macData.substring(0, 8);

        cmdCreateDir += macData;

        System.out.println(String.format("Create command : [%s]", cmdCreateDir));
        CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmdCreateDir))).getBytes());

        System.out.println(String.format("Create answer : [%s]", response.toString()));
    }



    private static void writeFile1(CardData cardData, CardChannel isoDep) throws Exception {
        String result = "";
        byte[] fileData = DatatypeConverter.parseHexBinary(result);
        writeFileData(isoDep, (byte) 1, "00112233445566778899AABBCCDDEEFF", "0001", fileData);



/*        Map<String, String> fileResult = TLV.parseTLV(fileData);

        cardData.setFirstName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF13")), "cp1251"));
        cardData.setLastName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF12")), "cp1251"));
        cardData.setSecondName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF14")), "cp1251"));
        cardData.setPhoto(DatatypeConverter.parseHexBinary(fileResult.get("DF11")));


        String date = fileResult.get("DF15");
        if (date != null && !date.isEmpty()) {
            cardData.setBirthDate(simpleDateFormat2.format(simpleDateFormat.parse(date)));
        }
        cardData.setExpirationDate(fileResult.get("DF16"));

        cardData.setCardId(fileResult.get("DF17"));
        cardData.setBloodResus(fileResult.get("DF18"));
        cardData.setBloodGroup(fileResult.get("DF19"));*/
    }

    private static void writeFile2(CardData cardData, CardChannel isoDep) throws Exception {
        String result = "DF2109CBEEEAEEECEEF2E8E2DF220109DF23102B3728393136293435332D35352D3132DF2410626F6C656C614079616E6465782E7275";
        byte[] fileData = DatatypeConverter.parseHexBinary(result);
        writeFileData(isoDep, (byte) 3, "A0A1A2A3A4A5A6A7B0B1B2B3B4B5B6B7", "0002", fileData);

       /* Map<String, String> fileResult = TLV.parseTLV(fileData);

        cardData.setIdNhlNumber(fileResult.get("DF21"));
        cardData.setTeamName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF22")), "cp1251"));

        cardData.setMemberNhlStatus(fileResult.get("DF23"));
        cardData.setActiveStatus(fileResult.get("DF24"));
        cardData.setPhoneNumber(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF25")), "cp1251"));
        cardData.setEmail(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF26")), "cp1251"));*/
    }


    private static void writeFile3(CardData cardData, CardChannel isoDep) throws Exception {
        byte[] fileData = new byte[0];

        writeFileData(isoDep, (byte) 5, "55555555555555555555555555555555", "0003", fileData);
/*        Map<String, String> fileResult = TLV.parseTLV(fileData);

        cardData.setCardNhlStatus(fileResult.get("DF31"));
        cardData.setPassOnMatch(fileResult.get("DF32"));
        cardData.setPassport(fileResult.get("DF33"));
        cardData.setPrava(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF34")), "cp1251"));*/
    }


    private static void writeFile4(CardData cardData, CardChannel isoDep) throws Exception {
        byte[] fileData = new byte[0];

        writeFileData(isoDep, (byte) 7, "77777777777777777777777777777777", "0004", fileData);
        /*System.out.println(String.format("***********************File4 Data: [%s] ", fileData));

        Map<String, String> fileResult = TLV.parseTLV(fileData);

        cardData.setInsuranceCompany(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF41")), "cp1251"));
        cardData.setInsuranceNumber(fileResult.get("DF42"));

        String date = fileResult.get("DF43");
        if (date != null && !date.isEmpty()) {
            cardData.setMedExploreDate(simpleDateFormat2.format(simpleDateFormat.parse(date)));
        }

        cardData.setMedExploreResult(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF44")), "cp1251"));
        if (fileResult.get("DF45") != null && !fileResult.get("DF45").isEmpty()) {
            cardData.setHeight(Integer.parseInt(fileResult.get("DF45")));
        }
        if (fileResult.get("DF46") != null && !fileResult.get("DF46").isEmpty()) {
            cardData.setWeight(Integer.parseInt(fileResult.get("DF46")));
        }
        if (fileResult.get("DF47") != null)
            cardData.setProtivopokazania(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF47")), "cp1251"));
        if (fileResult.get("DF48") != null)
            cardData.setAllergia(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF48")), "cp1251"));
        if (fileResult.get("DF49") != null)
            cardData.setIlls(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF49")), "cp1251"));*/

    }

    private static void writeFileData(CardChannel isoDep, Byte keyNumber, String keyValue, String ef, byte[] fileData) throws Exception {
        //System.out.println(String.format("***********************Start Test Read File [%s] ", 1));
        selectEFAID(isoDep, "3EF0");
        String sessionKey = authenticateToFile(isoDep, keyValue, keyNumber);
        AttributeEF attr = selectEFAID(isoDep, ef);

        //System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        writeBinarySec(isoDep, fileData, sessionKey);
        ///System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",1, fileData));
    }


    public static void selectDFAID(CardChannel isoDep, String df) throws Exception {
        String cmd = "80A40000"
                + DatatypeConverter.byteToHexString((byte) (df.length() / 2))
                + df;
        System.out.println(String.format("Select DF [%s] comand : [%s]", df, cmd));
        CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd))).getBytes());
        System.out.println(String.format("Select DF : [%s]", response.toString()));

//        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd
//        )));
//        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getBytes()));
        //        dirId = selectResponse.Substring(0, 4);
//        maxFileSize = selectResponse.Substring(4, 4);
//        dirAttribute = selectResponse.Substring(8, 2);
//        countSubDF = selectResponse.Substring(10, 2);
//        countEF = selectResponse.Substring(12, 2);
//        counterPin1 = selectResponse.Substring(14, 2);
//        counterPin2 = selectResponse.Substring(16, 2);
    }

    public static AttributeEF selectEFAID(CardChannel isoDep, String ef) throws Exception {
        String cmd = "80A40000"
                + DatatypeConverter.byteToHexString((byte) (ef.length() / 2))
                + ef;
        System.out.println(String.format("Select EF [%s] comand : [%s]", ef, cmd));
        CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd))).getBytes());
        if (!response.isSW9000()) {
            throw new RuntimeException("Select EF wrong answer" + response.getSW());
        }
        System.out.println(String.format("Select EF Result [%s] ", response.toString()));
//        System.out.println("SelectEF " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getBytes()));
        return new AttributeEF(response.getResponseData());

    }

    public static String authenticateToFile(CardChannel isoDep, String key16, byte keyNumber) throws Exception {
        String cmd = "8084000008";
        System.out.println(String.format("Get challenge cmd : [%s]", cmd));
        CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd))).getBytes());
//        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getBytes()));

        String randomCardNum = response.getResponseData();
        String randomTermNum = "EB49DA61821F8F54";
        String diverData = randomCardNum + randomTermNum;

        byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(key16), DatatypeConverter.parseHexBinary(diverData), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        byte[] cryptogrammTerminalBytes = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(randomCardNum), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        String cryptogrammTerminal = DatatypeConverter.printHexBinary(cryptogrammTerminalBytes);

        cryptogrammTerminal = cryptogrammTerminal.substring(0, 8); // : 0DDEBC19

        cmd = "808200" + DatatypeConverter.byteToHexString(keyNumber) + "0C" + randomTermNum + cryptogrammTerminal; // 01 - ����� �����
        System.out.println(String.format("Start Auth command : [%s]", cmd));
        response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd))).getBytes());
//        System.out.println("Start Auth " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
        System.out.println(String.format("Auth result : [%s]", response));
        return DatatypeConverter.printHexBinary(sessionKey);

    }

    public static void writeBinarySec(CardChannel isoDep, byte[] fileData, String sessionKey) throws SMartChipSMException, IOException, CardException {
        int blockSize = 192;//192; // 192 -
        String ClaIns = "84D6";
        int offset = 0;
        int fileSize = fileData.length;
        int length = 0;

        while (fileSize > offset) {
            if (fileData.length - offset > blockSize) {
                length = blockSize;
            }
            else {
                length = fileData.length - offset;
            }

            String dataStr = DatatypeConverter.printHexBinary(Arrays.copyOfRange(fileData, offset, offset + length));
            String len = String.format("%02X", 4 + dataStr.length() / 2);

            String p1p2 = String.format("%04X", offset); //offset.ToString("X04");

            String dataForMac = ClaIns + p1p2 + len + dataStr;

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(dataForMac), EnMacMode.MAC_RETAIL));


            String mac = calcMacData.substring(0, 8);
            String cmd = ClaIns + p1p2 + len + dataStr + mac;

            System.out.println(String.format("Write command : [%s]", cmd));
            CData response = new CData(isoDep.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd))).getBytes());

            String readAnsw = response.getResponseData();
            System.out.println(String.format("Write answer : [%s]", response.toString()));
            offset += length;
        }

    }

}
