package ru.alth.applet.util;


public class ByteHelper {
    public static String hex(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String hex(int i) {
        if(i <= 0xFF) return String.format("%02X", i);
        else if(i <= 0xFFFF) return String.format("%04X", i);
        else if(i <= 0xFFFFFF) return String.format("%06X", i);
        else return String.format("%08X", i);
    }

    public static byte[] bytes(String s) {
        try {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i + 1), 16));
            }
            return data;
        } catch (Exception e) {
            throw new RuntimeException("Couldn't convert following string to byte array: '" + s + "'", e);
        }
    }

    public static byte[] bytes(int i)
    {
        int j = (Integer.toHexString(i).length() + 1) / 2;
        byte abyte0[] = new byte[j];
        for(int k = 0; k < j; k++)
            abyte0[k] = (byte)(i >>> 8 * (j - 1 - k) & 0xff);

        return abyte0;
    }


}
