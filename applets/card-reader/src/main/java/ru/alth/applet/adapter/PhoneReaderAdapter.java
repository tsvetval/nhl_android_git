package ru.alth.applet.adapter;

import jmtp.*;
import ru.alth.applet.dto.ApduResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 01.02.2016.
 */
public class PhoneReaderAdapter implements ReaderAdapter {
    private String phoneName;
    private PortableDeviceFolderObject pstFolder;
    private PortableDevice device;
    private File tmpDir;

    @Override
    public List<String> getAvailableTerminalNames() {
        List<String> result = new ArrayList<>();
        PortableDeviceManager manager = new PortableDeviceManager();
        for (PortableDevice device : manager.getDevices()) {
            device.open();
            for (PortableDeviceObject object : device.getRootObjects()) {
                // If the object is a storage object
                if (object instanceof PortableDeviceStorageObject) {
                    PortableDeviceStorageObject storage = (PortableDeviceStorageObject) object;
                    System.out.println(storage + "************");
                    for (PortableDeviceObject o2 : storage.getChildObjects()) {
                        if (o2.getOriginalFileName().equals("PST")) {
                            try {
                                PortableDeviceFolderObject pstFolder = (PortableDeviceFolderObject) o2;
                                for (PortableDeviceObject o : pstFolder.getChildObjects()) {
                                    if (o.getOriginalFileName().equals("enable.nfc")) {
                                        result.add(device.getModel());
                                    }
                                }
                            } catch (ClassCastException e) {
                                System.out.println("PST not folder");
                            }
                        }
                    }
                }
            }
            device.close();
        }
        return result;
    }

    @Override
    public void stop() {

    }

    @Override
    public void setTerminalName(String name) {
        System.out.println("SET PHONE " + name);
        this.phoneName = name;
    }

    @Override
    public boolean isCardPresent() {
        if (tmpDir == null) {
            String tmpDirName = System.getProperty("java.io.tmpdir");
            tmpDir = new File(tmpDirName + "/PST");
            if (!tmpDir.exists()) {
                tmpDir.mkdir();
            }
        }
        if (phoneName != null) {
            if (pstFolder == null) {
                findPstFolder();
            }
            if (pstFolder != null) {
                for (PortableDeviceObject o : pstFolder.getChildObjects()) {
                    if (o.getOriginalFileName().equals("card.insert")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public ApduResult runAPDU(String cmd) {
        try {
            String tmpDirName = System.getProperty("java.io.tmpdir");
            System.out.println("tmpdir = " + tmpDirName + "   apdu = " + cmd);
            File tmpDir = new File(tmpDirName + "/PST");
            if (!tmpDir.exists()) {
                tmpDir.mkdir();
            }
            BigInteger a = new BigInteger("12345");
            String number = String.valueOf(System.currentTimeMillis());
            File file = new File(tmpDir, "apdu_" + number);
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(cmd.getBytes());
            outputStream.close();
            pstFolder.addAudioObject(file, "artist", "album", a);
            ApduResult apduResult = waitAnswer(number, tmpDir);
            return apduResult;
        } catch (Exception e) {
            ApduResult apduResult = new ApduResult();
            apduResult.message = "Error execute command " + e.toString();
            return apduResult;
        }
    }

    private void findPstFolder() {
        PortableDeviceManager manager = new PortableDeviceManager();
        for (PortableDevice device : manager.getDevices()) {
            device.open();
            if (device.getModel().equals(phoneName)) {
                for (PortableDeviceObject object : device.getRootObjects()) {
                    // If the object is a storage object
                    if (object instanceof PortableDeviceStorageObject) {
                        PortableDeviceStorageObject storage = (PortableDeviceStorageObject) object;
                        for (PortableDeviceObject o2 : storage.getChildObjects()) {
                            if (o2.getOriginalFileName().equals("PST")) {
                                System.out.println("PST folder found ======= ");
                                try {
                                    pstFolder = (PortableDeviceFolderObject) o2;
                                    this.device = device;
                                    return;
                                } catch (ClassCastException e) {
                                    System.out.println("PSt not folder");
                                }
                            }
                        }
                    }
                }
            }
            device.close();
        }
    }

    private ApduResult waitAnswer(String number, File tmpDir) {
        long l = Long.parseLong(number) + (60 * 1000);

        if (pstFolder != null) {
            while (true) {
                System.out.println("number = " + number + "   ---------------------------  try find");
                try {
                    for (PortableDeviceObject o : pstFolder.getChildObjects()) {

                        if (("answer_" + number).equals(o.getOriginalFileName())) {
                            PortableDeviceToHostImpl32 portableDeviceToHostImpl32 = new PortableDeviceToHostImpl32();
                            portableDeviceToHostImpl32.copyFromPortableDeviceToHost(o.getID(), tmpDir.getAbsolutePath(), device);
                            File file = new File(tmpDir, o.getOriginalFileName());
                            FileInputStream inputStream = new FileInputStream(file);
                            byte[] buf = new byte[(int) file.length()];
                            inputStream.read(buf);
                            System.out.println("RESULT = " + new String(buf) + " ------------------- ");
                            String answer = new String(buf);
                            o.delete();
                            ApduResult apduResult = new ApduResult();
                            apduResult.sw = answer.split(":")[0];
                            String responce = answer.split(":")[1];
                            apduResult.response = "null".equals(responce) ? "" : responce;
                            return apduResult;
                        } else if ("error".equals(o.getOriginalFileName())) {
                            PortableDeviceToHostImpl32 portableDeviceToHostImpl32 = new PortableDeviceToHostImpl32();
                            portableDeviceToHostImpl32.copyFromPortableDeviceToHost(o.getID(), tmpDir.getAbsolutePath(), device);
                            File file = new File(tmpDir, o.getOriginalFileName());
                            FileInputStream inputStream = new FileInputStream(file);
                            byte[] buf = new byte[(int) file.length()];
                            inputStream.read(buf);
                            o.delete();
                            throw new RuntimeException(new String(buf));
                        }
                    }
                    Thread.sleep(100);
                    if (l < System.currentTimeMillis()) {
                        throw new RuntimeException("Timeout");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        } else {
            throw new RuntimeException("pstFolder is null");
        }
    }
}
