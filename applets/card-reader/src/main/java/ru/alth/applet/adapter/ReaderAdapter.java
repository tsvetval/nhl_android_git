package ru.alth.applet.adapter;

import ru.alth.applet.dto.ApduResult;

import java.util.List;

/**
 * Created by admin on 29.01.2016.
 */
public interface ReaderAdapter {

    List<String> getAvailableTerminalNames();
    void stop();
    void setTerminalName(String name);
    boolean isCardPresent();

    ApduResult runAPDU(String cmd);
}
