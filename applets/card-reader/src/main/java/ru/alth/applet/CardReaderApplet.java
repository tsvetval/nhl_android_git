package ru.alth.applet;

import ru.alth.applet.adapter.CardReaderAdapter;
import ru.alth.applet.adapter.PhoneReaderAdapter;
import ru.alth.applet.adapter.ReaderAdapter;
import ru.alth.applet.dto.ApduResult;

import java.applet.Applet;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CardReaderApplet extends Applet {

    static {
        try {
            System.setProperty("sun.security.smartcardio.t0GetResponse", "false");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/applet/best_practices.html
    private static final long serialVersionUID = 5146989229965823975L;

    private static Logger log = Logger.getLogger(Applet.class.getName());
    List<ReaderAdapter> adapters;
    ReaderAdapter selectedAdapter;

    @Override
    public void init() {
        super.init();
        boolean enableMtpLibs = true;
        System.out.println("Try load MTP libs");
        try {
            // Fetch params
            if ("32".equals(System.getProperty("sun.arch.data.model"))) {
                System.loadLibrary("jmtp_32");
            } else {
                System.loadLibrary("jmtp_64");
            }
        } catch (UnsatisfiedLinkError e) {
            System.out.println("MTP libs not found");
            enableMtpLibs = false;
        }

        adapters = new ArrayList<>();
        adapters.add(new CardReaderAdapter());
        if(enableMtpLibs) {
            adapters.add(new PhoneReaderAdapter());
        }
    }

    public static void main(String a[]) {
        CardReaderApplet cardReaderApplet = new CardReaderApplet();
        cardReaderApplet.init();
        for (String s : cardReaderApplet.getTerminalList()) {
            System.out.println(s);
        }
        cardReaderApplet.setTerminal("HTC Desire 500 dual sim");
        System.out.println(cardReaderApplet.isCardPresent());

        if(cardReaderApplet.isCardPresent()) {
            ApduResult apduResult = cardReaderApplet.runAPDU("00A404000EF0495472616E73706F7274494401FF");
            System.out.println(apduResult.sw + " " + apduResult.message + " " + apduResult.response);
        }
    }

    public String[] getTerminalList() {
        return doPrivileged(new PrivilegedAction<String[]>() {
            @Override
            public String[] run() {
                List<String> readers = new ArrayList<>();
                for (ReaderAdapter adapter : adapters) {
                    readers.addAll(adapter.getAvailableTerminalNames());
                }
                return readers.toArray(new String[readers.size()]);
            }
        });

    }

    public Boolean isApplet(){
        return true;
    }

    public void setTerminal(final String name) {
        System.out.println("Card reader, setTerminal: " + name);
        AccessController.doPrivileged(new PrivilegedAction<Boolean>() {
            @Override
            public Boolean run() {
                // Find appropriate adapter for specified reader
                ReaderAdapter newAdapter = null;
                for (ReaderAdapter adapter : adapters) {
                    System.out.println("try adapter is = " + adapter.getClass());
                    if (adapter.getAvailableTerminalNames().contains(name)) {
                        newAdapter = adapter;
                        break;
                    }
                }

                System.out.println("Select adapter is = " + newAdapter.getClass());
                if (newAdapter == null) {
                    throw new IllegalArgumentException("Couldn't find adapter for reader: " + name);
                } else if (newAdapter == selectedAdapter) {
                    log.info("Selected terminal which already running (only set set handler and terminal name)");
                } else {
                    // Stop old adapter (if it was been started), and start new one
                    if (selectedAdapter != null) {
                        log.info("Stop old terminal");
                        selectedAdapter.stop();
                    }
                    selectedAdapter = newAdapter;
                    selectedAdapter.setTerminalName(name);
                    log.info("Selected and started new terminal: " + name);
                }
                return true;
            }
        });
    }

    public boolean isCardPresent() {
        return doPrivileged(new PrivilegedAction<Boolean>() {
            @Override
            public Boolean run() {
                if (selectedAdapter != null) {
                    return selectedAdapter.isCardPresent();
                }
                return false;
            }
        });
    }


    public ApduResult runAPDU(final String cmd) {
        return doPrivileged(new PrivilegedAction<ApduResult>() {
            @Override
            public ApduResult run() {
                if (selectedAdapter != null) {
                    try {

                        return selectedAdapter.runAPDU(cmd);
                    } catch (Exception e) {
                        throw new RuntimeException("Couldn't execute APDU", e);
                    }
                } else {
                    throw new RuntimeException("Terminal is not selected!");
                }
            }
        });
    }

    public String readMifareUID(final String AID) throws Exception {
        return doPrivileged(new PrivilegedAction<String>() {
            @Override
            public String run() {
                if (selectedAdapter != null) {
                    try {
                        return MifareInfoReader.readMifareUID(selectedAdapter, AID);
                    } catch (Exception e) {
                        throw new RuntimeException("Error while read MChip Pan", e);
                    }
                } else {
                    throw new RuntimeException("Terminal is not selected!");
                }
            }
        });
    }

    public String readMifareKDD(final String AID) throws Exception {
        return doPrivileged(new PrivilegedAction<String>() {
            @Override
            public String run() {
                if (selectedAdapter != null) {
                    try {
                        return MifareInfoReader.readMifareKDD(selectedAdapter, AID);
                    } catch (Exception e) {
                        throw new RuntimeException("Error while read MChip Pan", e);
                    }
                } else {
                    throw new RuntimeException("Terminal is not selected!");
                }
            }
        });
    }


    private <T> T doPrivileged(final PrivilegedAction<T> action) {
        return AccessController.doPrivileged(new PrivilegedAction<T>() {
            @Override
            public T run() {
                try {
                    return action.run();
                } catch (Throwable e) {
                    log.log(Level.SEVERE, "Error while performing privileged action", e);
                    return null;
                }
            }
        });
    }

}

