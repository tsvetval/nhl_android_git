/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.alth.applet.util;

/**
 *
 * @author v_favstov
 */
public class StringConvert
{
    public static String TrimRight(String s, int ch)
    {
        int i = s.indexOf(ch);
        return i == -1 ? s : s.substring(0, i);
    }

    public static byte[] HexStringToByteArray(String HexString) throws Exception
    {
        String s = HexString.trim();
        s = s.replace(" ", "");
        if ((s.length() % 2) == 1)
            throw new Exception("String has a odd length");

        int len = s.length() / 2;

        byte[] arr = new byte[len];

        for (int i = 0; i < len; i++)
        {
            int index = i * 2;
            arr[i] = (byte)(Integer.parseInt(s.substring(index, index + 2), 16) & 0xFF);
        }
        return arr;
    }

    /// <summary>
    /// �������������� ����������������� ������ � ����� ����� (int) 
    /// </summary>
    /// <param name="HexString">
    /// ��������� ������������� ����������������� ����� ������ �����
    /// (������� ����� ������������ �������). ������ ����� ��������� �������.
    /// </param>
    /// <returns>����� �����</returns>
    public static int HexStringToIntBigEndian(String HexString)
    {
        String s = HexString.trim();
        s = s.replace(" ", "");
        return Integer.parseInt(s, 16);
    }

    /// <summary>
    /// ���������, ������� �� ������ �� ����������������� ���� � ������ �� ����� 2
    /// </summary>
    /// <param name="data">������</param>
    /// <returns>true - �������, ����� false</returns>
    public static boolean IsHexString(String data)
    {
        if((data.length() == 0) || (data.length() % 2 != 0))
            return false;

        for(int i = 0 ; i < data.length() ; i++)
        {
            if(!StringConvert.IsHexDigit(data.charAt(i)))
                return false;
        }
        return true;
    }

    private static boolean IsHexDigit(char ch)
    {
        return ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f'));
    }

    public static String ByteArrayToHexString(byte[] hex)
    {
        if (hex == null)
            return new String();
        return ByteArrayToHexString(hex, 0, hex.length);
    }

    /// <summary>
    /// �������������� ������� ������ � ������. ������ ���� �������� ������� ������������� �  
    /// ��� �������������������������� ������� (0x6F -> "6F").
    /// </summary>
    /// <param name="hex">�������� ������</param>
    /// <param name="offset">�������� � ������� hex</param>
    /// <param name="len">���������� ����, ���������� � �������� ������</param>
    /// <returns>c�������� ������������� �������� �������</returns>
    public static String ByteArrayToHexString(byte[] hex, int offset, int len)
    {
        StringBuilder s = new StringBuilder("");
        if (hex == null)
            return s.toString();

        if (offset<0 || len<0 || (offset + len) > hex.length)
            throw new IndexOutOfBoundsException("Position is out of range");
        for (int i = 0; i < len; i++)
        {
            s.append(String.format("%02X", hex[offset+i]));
            //s.AppendFormat("{0:X2}", hex[offset+i]);
        }

        return s.toString();
    }
}
