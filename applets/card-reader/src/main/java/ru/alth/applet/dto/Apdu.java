package ru.alth.applet.dto;

import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;

/**
 * Created by Administrator on 02.06.14.
 */
public class Apdu {
    //private static final int APDU_HAVE_LD = 1;
    private static final int APDU_HAVE_LE = 2;
    private static final int TYPE_0 = 0;  // CLA INS P1 P2
    private static final int TYPE_1 = 1;  // CLA INS P1 P2 LD Data
    private static final int TYPE_2 = 2;  // CLA INS P1 P2 LE
    private static final int TYPE_3 = 3;  // CLA INS P1 P2 LD Data LE

    private int cla;
    private int ins;
    private int p1;
    private int p2;
    private byte[] data;
    private int le;
    private int type;

    public Apdu(String apdu) throws CardException
    {
        byte[] cmd = DatatypeConverter.parseHexBinary(apdu);
        if (cmd.length < 4)
            throw new CardException("Wrong APDU length");

        cla = extract(cmd, 0);
        ins = extract(cmd, 1);
        p1 = extract(cmd, 2);
        p2 = extract(cmd, 3);
        data = null;
        type = TYPE_0;

        if (cmd.length > 5)
        {
            int ld = extract(cmd, 4);
            data = new byte[ld];
            System.arraycopy(cmd, 5, data, 0, ld);

            if (cmd.length > 5 + ld)
            {
                le = extract(cmd, 5 + ld);
                type = TYPE_3;
            }
            else
                type = TYPE_1;
        } else if (cmd.length == 5) {
            le = extract(cmd, 4);
            type = TYPE_2;
        }
    }

    private static int extract(byte[] bytes, int index)
    {
        // Лучи ненависти яве с ее отсутсвующим типом usigned byte
        return (bytes[index] >= 0 ? bytes[index] : 256 + bytes[index]);
    }

    public CommandAPDU getAPDU() throws CardException
    {
        /*
            Я отказался от использования разных конструкторов класса CommandAPDU, чтобы избежать обрезания байта LE
         */

       /*
        CommandAPDU res = null;
        switch (type)
        {
            case TYPE_0 : res = new CommandAPDU(cla, ins, p1, p2, data);           break;
            case TYPE_1 : res = new CommandAPDU(cla, ins, p1, p2, data);     break;
            case TYPE_2 : res = new CommandAPDU(cla, ins, p1, p2, data);       break;
            case TYPE_3 : res = new CommandAPDU(cla, ins, p1, p2, data); break;
            default:
                throw new CardException("Wrong APDU type");
        }
        return res;
        */
        byte[] bytes;
        if(data==null || data.length==0) {
            bytes = new byte[]{(byte) cla, (byte) ins, (byte) p1, (byte) p2, (byte) le};
        }
        else {
            bytes = new byte[]{(byte) cla, (byte) ins, (byte) p1, (byte) p2, (byte) data.length};
            bytes = Arrays.copyOf(bytes, bytes.length + data.length + 1); // +1 for Le
            System.arraycopy(data,0,bytes,bytes.length - data.length - 1, data.length);
            bytes[bytes.length-1] = (byte) le;
        }
        return new CommandAPDU(bytes);
    }

    public void setLe(int le)
    {
        this.le = le;
        type = type | APDU_HAVE_LE;
    }

    public boolean haveLe() {
        return (type & APDU_HAVE_LE)!=0;
    }

    public int getType() {
        return type;
    }
}
