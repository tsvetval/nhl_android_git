package ru.alth.applet;


import ru.alth.applet.adapter.ReaderAdapter;
import ru.alth.applet.dto.ApduResult;
import ru.alth.applet.util.StringConvert;
import ru.alth.applet.util.Tlv;

import java.util.Random;

import static ru.alth.applet.util.ByteHelper.hex;

public class MifareInfoReader {

    public static String readMifareKDD(ReaderAdapter adapter, String AID) throws Exception{
        System.out.println("read mifare kdd. adapter = "+adapter.getClass()+" aid = "+AID);
        ApduResult apduResult = adapter.runAPDU("80CA00CF00");//String.format("00A40400%02x%s", AID.length()/2,  AID));
        System.out.println("read mifare kdd. sw = "+apduResult.sw+" resp = "+apduResult.response+" mess = "+apduResult.message);
        String kdd = apduResult.response;//Tlv.GetValue(apduResult.response, "CF");
        return kdd;
    }

    private static String tryReadMifareUID(ReaderAdapter readerAdapter, String apdu) throws Exception {
        ApduResult apduAnswer = readerAdapter.runAPDU(apdu);
        try {
            if (apduAnswer.sw.equals("9000") && apduAnswer.response!=null && !apduAnswer.response.equals("")) {
                return apduAnswer.response;
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
        return null;
    }

    public static String readMifareUID(ReaderAdapter readerAdapter, String aid) throws Exception {
        String answer;

        // Select AID
        String selectApplication = "00A40400" + hex(new byte[]{(byte) (aid.length() / 2)}) + aid+"FF";
        ApduResult selectAnswer = readerAdapter.runAPDU(selectApplication);
        if(!selectAnswer.sw.equals("9000")) {
            throw new RuntimeException("Couldn't select AID '" + aid + "' ("+selectAnswer.sw+")");
        }

        // Tag D2 (UID) contained in select AID response for MGT cards in contact mode
        if(selectAnswer.response!=null && !selectAnswer.response.equals("")) {
            String d2 = Tlv.GetValue(selectAnswer.response, "D2");
            if(d2!=null) return d2;
        }

        /*
            Try different commands for read UID
         */
        answer = tryReadMifareUID(readerAdapter, "80DF0000080B54570745FE3AE7"); // Works only for card, personalized in PST, not for SKM cards
        if(answer!=null) return answer.substring(0, 8);

        answer = tryReadMifareUID(readerAdapter, "00C0000006"); // MGT cards, contact interface only
        if(answer!=null) {
            // Answer for 00C0000006 looks like response to select AID, use the same parsing
            String d2 = Tlv.GetValue(answer, "D2");
            if(d2!=null) return d2;
        }

        answer = tryReadMifareUID(readerAdapter, "80CA00D200"); // MGT cards, contactless interface only
        if(answer!=null) {
            String d2 = Tlv.GetValue(answer, "D2");
            if(d2!=null) return d2;
        }

        answer = tryReadMifareUID(readerAdapter, "FFCA000000");
        if(answer!=null) return answer;

        return null;
    }

    private static String padding(String dataForPadding) {
        dataForPadding += "80";
        while ((dataForPadding.length() / 2) % 8 != 0) {
            dataForPadding += "00";
        }
        return dataForPadding;
    }


    public static String getRandom(int size)
    {
        Random generator = new Random();
        byte[] result = new byte[size];
        generator.nextBytes(result);
        return StringConvert.ByteArrayToHexString(result);
    }
}
