package ru.alth.applet.adapter;

import ru.alth.applet.dto.Apdu;
import ru.alth.applet.dto.ApduResult;

import javax.smartcardio.*;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class CardReaderAdapter implements ReaderAdapter {

    private String selectedTerminal;
    private CardTerminal cardTerminal;
    private CardChannel cardChannel;

    @Override
    public List<String> getAvailableTerminalNames() {
        List<String> result = new ArrayList<>();
        try {
            CardTerminals terminals = TerminalFactory.getDefault().terminals();
            for (int i = 0; i < terminals.list().size(); i++) {
                CardTerminal terminal = terminals.list().get(i);
                result.add(terminal.getName());
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return result;
    }

    @Override
    public void stop() {

    }

    @Override
    public void setTerminalName(String name) {
        selectedTerminal = name;
    }

    @Override
    public boolean isCardPresent() {
        if (cardTerminal == null) {
            cardTerminal = TerminalFactory.getDefault().terminals().getTerminal(selectedTerminal);
        }
        try {
            if(cardTerminal.isCardPresent()){
                cardChannel = cardTerminal.connect("*").getBasicChannel();
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return false;
    }

    @Override
    public ApduResult runAPDU(String cmd) {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        ApduResult result = new ApduResult();
        if (cardChannel != null) {
            try {
                Apdu apdu = new Apdu(cmd);
                CommandAPDU capdu;
                capdu = apdu.getAPDU();
                System.out.println("Run apdu (simple reader): " + DatatypeConverter.printHexBinary(capdu.getBytes()));
                ResponseAPDU res = this.cardChannel.transmit(capdu);

                if (res.getSW1() == 0x6C) {
                    apdu.setLe(res.getSW2());
                    capdu = apdu.getAPDU();
                    System.out.println("(SW1=6C,SW2=" + res.getSW2() + ") Run apdu (simple reader): " + DatatypeConverter.printHexBinary(capdu.getBytes()));
                    res = this.cardChannel.transmit(capdu);
                }

                ByteArrayOutputStream data = new ByteArrayOutputStream();
                if (res.getSW1() == 0x61) {
                    data.write(res.getData());
                    apdu = new Apdu("00C00000");
                    apdu.setLe(res.getSW2());
                    capdu = apdu.getAPDU();
                    System.out.println("(SW1=61,SW2=" + res.getSW2() + ") Run apdu (simple reader): " + DatatypeConverter.printHexBinary(capdu.getBytes()));
                    System.out.println("before command");
                    res = this.cardChannel.transmit(capdu);
                    System.out.println("after command");
                }
                data.write(res.getData());

                result.message = "Success";
                result.sw = DatatypeConverter.printHexBinary(new byte[]{(byte) res.getSW1(), (byte) res.getSW2()});
                result.response = DatatypeConverter.printHexBinary(data.toByteArray());
            } catch (CardException e) {
                e.printStackTrace();
                result.message = "CardException: " + e.getMessage();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                result.message = "Wrong command: " + e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                result.message = "Unknown exception: " + e.getMessage();
            }
        } else
            result.message = "Card not connected";

        System.out.println("result.sw: " + result.sw);
        System.out.println("result.response: " + result.response);
        System.out.println("result.message: " + result.message);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        return result;
    }
}
