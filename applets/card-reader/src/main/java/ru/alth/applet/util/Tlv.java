/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.alth.applet.util;

/**
 *
 * @author v_favstov
 */
public class Tlv
{
    public static String GetValue(String TLV, String tag) throws Exception
    {
        byte[] bTLV = StringConvert.HexStringToByteArray(TLV);
        byte[] bTag = StringConvert.HexStringToByteArray(tag);
        byte[] ret = GetValue(bTLV, 0, bTLV.length, bTag);
        if ( ret != null )
            return StringConvert.ByteArrayToHexString(ret);
        return null;
    }

    public static byte[] GetValue(byte[] TLV, int offset, int len, byte[] tag)
    {
        int posTag, tagLen, valueLen, posLen, posValue, i;
        len += offset;
        for ( posTag = offset ; posTag < len ; )
        {
            if ( (TLV[posTag] & (byte)0x1F) == 0x1F )
            {
                tagLen = 2;
                for ( ; posTag + tagLen - 1 < len ; tagLen++ )
                {
                    if ( ((TLV[posTag + tagLen - 1] & 0xFF) & (byte)0x80) == 0 )
                        break;
                }
            }
            else
                tagLen = 1;
            posLen = posTag + tagLen;
            if ( ((TLV[posLen] & 0xFF) & (byte)0x80) == 0 )
            {
                valueLen = TLV[posLen];
                posValue = posLen + 1;
            }
            else
            {
                int LenLen = TLV[posLen] & 0x7F;
                if ( LenLen > 4 )
                    return null;
                for ( i = 0, valueLen = 0 ; i < LenLen ; i++ )
                {
                    valueLen <<= 4;
                    valueLen += (TLV[posLen + 1 + i] & 0x000000FF);
                }
                posValue = posLen + 1 + LenLen;
            }
            if ( tag.length == tagLen && tag[0] == TLV[posTag])// && Util.memcmp(tag, 0, TLV, posTag, tagLen) == 0 )
            {
                byte[] ret = new byte[valueLen];
                System.arraycopy(TLV, posValue, ret, 0, valueLen);
                return ret;
            }
            if ( ((TLV[posTag] & 0xFF) & (byte)0x20) != 0 )
            {
                byte[] ret = GetValue(TLV, posValue, valueLen, tag);
                if ( ret != null )
                    return ret;
            }
            posTag = posValue + valueLen;
        }
        return null;
    }

    public static String ExtractTag(String TLV) throws Exception
    {
        byte[] bTLV = StringConvert.HexStringToByteArray(TLV);
        int tagLen;
        if ( ((bTLV[0] & 0xFF) & (byte)0x1F) == 0x1F )
        {
            for ( tagLen = 2; tagLen - 1 < bTLV.length ; tagLen++ )
            {
                byte b = (byte)(TLV.charAt(tagLen - 1) & 0xFF);
                if((b & (byte)(0x80 & 0xFF)) == 0 )
                    break;
            }
        }
        else
            tagLen = 1;
        return StringConvert.ByteArrayToHexString(bTLV, 0, tagLen);
    }
}
