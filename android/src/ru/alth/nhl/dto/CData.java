package ru.alth.nhl.dto;


import ru.alth.nhl.helpers.ByteHelper;

/**
 * Вспомогательный класс 
 * для работы с бинарными данными
 * @author a_gorshenin
 */
public class CData implements CDataInteface
{
	/** Бинарные данные */
	private byte[] mData; 
	
	/** Конструктор по умолчанию - создание пустого массива */
	public CData()
	{
		mData = new byte[0];
	}
	
	/** Конструктор по умолчанию - создание массива заданного размера */
	protected CData(int size)
	{
		mData = new byte[size];
	}

	/** Конструктор из строки */
	public CData(String data)
	{
		this.load(data);
	}
	
	/** Конструктор копирования */
	public CData(CData data)
	{
		this.load(data.mData);
	}

	public CData(byte[] data)
	{
		this.load(data);
	}

	/** Конструктор склеивания */
	public CData(CDataInteface... a)
	{
		int size = 0;
		for (CDataInteface data : a)
			size += data.bytesLength();

		this.mData = new byte[size];
		int off = 0;
		
		for (CDataInteface data : a)
		{
			System.arraycopy(data.toBytes(), 0, this.mData, off, data.bytesLength());
			off += data.bytesLength();
		}
	}

	protected void load(byte[] data)
	{
		if (data == null)
			mData = new byte[0];
		else 
			mData = data.clone();
	}

	protected void load(String data)
	{
		if (data != null)
			mData = ByteHelper.hexStringToBinary(data);
		else
			mData = new byte[0];
	}
	
	/** Приведение к строке */
	public String toString()
	{
		return ByteHelper.binaryToHexString(mData);
	}
	
	/** содержимое в виде массива байт */
	public byte[] data()
	{ 
		return mData; 
	}
	
	/** Размер */
    public int size()
    { 
    	return mData.length; 
    }

	/** Проверка на пустоту */
	public boolean isEmpty()
	{ 
		return mData.length == 0; 
	}
	
	/** Сравнение */
	public boolean equals(String data)
	{
		data = data.toUpperCase();
		return data.equals(this.toString());
	}
	
	public boolean equals(CData data)
	{
		if (this.mData.length != data.mData.length)
			return false;
		for (int i = 0; i < this.mData.length; i++)
			if (this.mData[i] != data.mData[i])
				return false;
		return true;
	}

	public boolean equals(byte[] data)
	{
		if (data == null)
			return false;
		if (this.mData.length != data.length)
			return false;
		for (int i = 0; i < this.mData.length; i++)
			if (this.mData[i] != data[i])
				return false;
		return true;
	}

	/** Вернуть byte */
	public int at(int index)
	{
		// Лучи ненависти яве с ее отсутсвующим типом usigned byte
		return (mData[index] >= 0 ? mData[index] : 256 + mData[index]);
	}

	/** Первые len байтов */
	public CData first(int len)
	{
		return this.sub(0, len);
	}

	/** Последние len байтов */
	public CData last(int len)
	{
		return this.sub(this.size() - len, len);
	}
	
	/** Вырезать часть длиной len начиная со start */
	public CData sub(int start, int len)
	{
		int st = start;
		int ln = len;
		int sz = this.size();
		if (st > sz)
			st = sz;
		if (st + ln > sz)
			ln = sz - st;

		CData result = new CData(ln);
		if (ln > 0)
			System.arraycopy(this.mData, st, result.mData, 0, ln);

		return result;
	}

	/** Установить значение байта */
	public void setByte(int number, int value)
	{
		if (number < mData.length)
			mData[number] = (byte)value;
	}
	
	/** Конвертировать строку в массив байтов */
	static public byte[] convertString(String data)
	{
		return ByteHelper.hexStringToBinary(data);
	}

	public int bytesLength() {
		return this.size();
	}

	public byte[] toBytes() {
		return this.data();
	}

	public String getResponseData(){
		String result = ByteHelper.binaryToHexString(mData);

		return result.substring(0, result.length() - 4);
	}

	public String getSW(){
		String result = ByteHelper.binaryToHexString(mData);
		return result.substring(result.length() - 4, result.length());
	}

	public boolean isSW9000(){
		return  "9000".equals(getSW());

	}
	
}
