package ru.alth.nhl.dto;

public interface CDataInteface {
	public byte[] toBytes();
	public int bytesLength();
}
