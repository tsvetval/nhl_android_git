package ru.alth.nhl.dto;

import java.util.ArrayList;
import java.util.List;


/**
 * Класс для описания APDU комманд
 * Структура APDU:
 * Заголовок Head(4 байта) : CLA INS P1 P2
 * Данный    Data : Ld (DATA)
 * Длина ожидаемых данных : Le
 * Варианты APDU
 * Head Ld (DATA) Le
 * Head Ld (DATA)
 * Head Le
 */
public class APDU implements CDataInteface {
    // Константы для создания ожидаемых ответов для APDU комманд
    static public final int NORMAL = 1;
    static public final int INACTIVE = 2;
    static public final int NOT_FOUND = 4;
    static public final int BAD_CONDITIONS = 8;
    static public final int SECURITY_CONDITION_NOT_STATISFIED = 16;

    private CData header;  // Заголовок
    private CData data;    // Данные (null, если нет)
    private Integer le;    // Длина ожидаемого ответа (null, если нет)
    private int type;      // Ожидаемые результаты

    /**
     * Команды без посылки и приема данных
     *
     * @param header - команда
     * @param type   - ожидаемый ответ
     */
    public APDU(String header, int type) {
        this.header = new CData(checkHeader(header));
        this.data = null;
        this.le = null;
        this.type = type;
    }

    /**
     * Команда с данными
     *
     * @param header - команда
     * @param data   - данные
     * @param type   - ожидаемый ответ
     */
    public APDU(String header, String data, int type) {
        this.header = new CData(checkHeader(header));
        this.data = new CData(data);
        this.le = null;
        this.type = type;
    }

    /**
     * Команда с данными
     *
     * @param header - команда
     * @param data   - данные
     * @param type   - ожидаемый ответ
     */
    public APDU(String header, CData data, int type) {
        this.header = new CData(checkHeader(header));
        this.data = new CData(data);
        this.le = null;
        this.type = type;
    }

    /**
     * Команда с данными
     *
     * @param header - команда
     * @param data   - данные
     * @param type   - ожидаемый ответ
     */
    public APDU(CData header, CData data, int type) {
        this.header = new CData(checkHeader(header));
        this.data = new CData(data);
        this.le = null;
        this.type = type;
    }

    /**
     * Команда с ожидаемым ответом
     *
     * @param header - команда
     * @param length - длина ожидаемых данных
     * @param type   - ожидаемый ответ
     */
    public APDU(String header, int length, int type) {
        this.header = new CData(checkHeader(header));
        this.data = null;
        this.le = length;
        this.type = type;
    }

    /**
     * Команда с данными и ожидаемым ответом
     *
     * @param header - команда
     * @param data   - данные
     * @param length - длина ожидаемых данных
     * @param type   - ожидаемый ответ
     */
    public APDU(String header, String data, int length, int type) {
        this.header = new CData(checkHeader(header));
        this.data = new CData(data);
        this.le = length;
        this.type = type;
    }

    /**
     * Конструктор копирования
     *
     * @param apdu - команда
     */
    public APDU(APDU apdu) {
        this.header = new CData(apdu.header);
        this.data = (apdu.data == null) ? null : new CData(apdu.data);
        this.le = (apdu.le == null) ? null : apdu.le;
        this.type = apdu.type;
    }

    private String checkHeader(String cmd) {
        if (cmd.length() != 8)
            throw new RuntimeException("Invalid length of APDU header");
        return cmd;
    }

    private CData checkHeader(CData cmd) {
        if (cmd.size() != 4)
            throw new RuntimeException("Invalid length of APDU header");
        return cmd;
    }

    /**
     * Заголовок команды
     */
    public CData header() {
        return this.header;
    }

    /**
     * Данные команды
     */
    public CData data() {
        return (this.data == null) ? new CData() : this.data;
    }

    /**
     * Тип команды
     */
    public int type() {
        return this.type;
    }

    /**
     * Длина запрашиваемых данных
     */
    public CData le() {
        if (this.le == null)
            return new CData();
        byte[] bt = new byte[1];
        bt[0] = this.le.byteValue();
        return new CData(bt);
    }

    public boolean containsData() {
        return (this.data != null);
    }

    public boolean containsLe() {
        return (this.le != null);
    }

    public int getCLA() {
        return this.header.at(0);
    }

    public void setCLA(int cla) {
        this.header.setByte(0, cla);
    }

    public void setP1(int p1) {
        this.header.setByte(2, p1);
    }

    public void setP2(int p2) {
        this.header.setByte(3, p2);
    }

    public void setData(CData data) {
        this.data = (data == null) ? null : new CData(data);
    }

    public void setLE(Integer length) {
        this.le = (length == null) ? null : length;
    }

    static public boolean checkResult(CData result, int type) {
        List<byte[]> res = APDU.allowedResultsForType(type);
        for (int i = 0; i < res.size(); i++)
            if (result.equals(res.get(i)))
                return true;
        return false;
    }

    static private List<byte[]> allowedResultsForType(int type) {
        List<byte[]> results = new ArrayList<byte[]>();
        if ((type & NORMAL) != 0)
            results.add(CData.convertString("9000"));
        if ((type & INACTIVE) != 0)
            results.add(CData.convertString("6283"));
        if ((type & BAD_CONDITIONS) != 0)
            results.add(CData.convertString("6985"));
        if ((type & NOT_FOUND) != 0) {
            results.add(CData.convertString("6A86"));
            results.add(CData.convertString("6A88"));
            results.add(CData.convertString("6200"));
            results.add(CData.convertString("6A82")); //Номер ключа не найден
        }
        if ((type & SECURITY_CONDITION_NOT_STATISFIED) != 0)
            results.add(CData.convertString("6982"));
        return results;
    }

    public List<byte[]> allowedResults() {
        return APDU.allowedResultsForType(this.type);
    }

    @Override
    public int bytesLength() {
        int size = this.header.size();

        if (this.data != null)
            size += this.data.size() + 1;
        if (this.le != null)
            size++;

        return size;
    }

    @Override
    public byte[] toBytes() {
        int size = this.bytesLength();
        byte[] result = new byte[size];

        System.arraycopy(this.header.data(), 0, result, 0, 4);

        if (this.data != null) {
            result[4] = (byte) this.data.size();
            System.arraycopy(this.data.data(), 0, result, 5, this.data.size());
        }
        if (this.le != null)
            result[result.length - 1] = this.le.byteValue();

        return result;
    }

    public CData toCData() {
        return new CData(this.toBytes());
    }

    @Override
    public String toString() {
        return this.toCData().toString();
    }
}
