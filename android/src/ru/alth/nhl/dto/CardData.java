package ru.alth.nhl.dto;

import ru.alth.nhl.task.checkcard.OnlineCardStatus;

import java.io.Serializable;

/**
 */
public class CardData implements Serializable {
    private String cardReadError = null;

    private String kdd;
    private String firstName;
    private String lastName;
    private String secondName;
    private String birthDate;
    private byte[] photo;


    private String expirationDate;
    private String cardId;
    private String bloodResus;
    private String bloodGroup;

    private String teamName;
    private String memberNhlStatus;
    private String activeStatus;
    private String phoneNumber;
    private String email;
    private String mailingAddress;


    private String cardNhlStatus;
    private String insuranceCompany;
    private String insuranceNumber;
    private String medExploreDate;
    private String medExploreResult;
    private String height;
    private String weight;
    private String protivopokazania;
    private String allergia;
    private String ills;


    private String passOnMatch;
    private String passport;
    private String prava;
    private String matchDate;
    private String matchPlace;
    private String matchTeam;
    private String playerNumber;



    private String idNhlNumber;

    OnlineCardStatus onlineCardStatus = null;

    String file1Hash;
    String file2Hash;
    String file3Hash;
    String file4Hash;


    private boolean _readedByOnline;

    private String serverUrl;

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getKdd() {
        return kdd;
    }

    public void setKdd(String kdd) {
        this.kdd = kdd;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getCardReadError() {
        return cardReadError;
    }

    public void setCardReadError(String cardReadError) {
        this.cardReadError = cardReadError;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBloodResus() {
        return bloodResus;
    }

    public void setBloodResus(String bloodResus) {
        this.bloodResus = bloodResus;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getMemberNhlStatus() {
        return memberNhlStatus;
    }

    public void setMemberNhlStatus(String memberNhlStatus) {
        this.memberNhlStatus = memberNhlStatus;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getCardNhlStatus() {
        return cardNhlStatus;
    }

    public void setCardNhlStatus(String cardNhlStatus) {
        this.cardNhlStatus = cardNhlStatus;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getMedExploreDate() {
        return medExploreDate;
    }

    public void setMedExploreDate(String medExploreDate) {
        this.medExploreDate = medExploreDate;
    }

    public String getMedExploreResult() {
        return medExploreResult;
    }

    public void setMedExploreResult(String medExploreResult) {
        this.medExploreResult = medExploreResult;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getProtivopokazania() {
        return protivopokazania;
    }

    public void setProtivopokazania(String protivopokazania) {
        this.protivopokazania = protivopokazania;
    }

    public String getAllergia() {
        return allergia;
    }

    public void setAllergia(String allergia) {
        this.allergia = allergia;
    }

    public String getIlls() {
        return ills;
    }

    public void setIlls(String ills) {
        this.ills = ills;
    }

    public String getPassOnMatch() {
        return passOnMatch;
    }

    public void setPassOnMatch(String passOnMatch) {
        this.passOnMatch = passOnMatch;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPrava() {
        return prava;
    }

    public void setPrava(String prava) {
        this.prava = prava;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchPlace() {
        return matchPlace;
    }

    public void setMatchPlace(String matchPlace) {
        this.matchPlace = matchPlace;
    }

    public String getMatchTeam() {
        return matchTeam;
    }

    public void setMatchTeam(String matchTeam) {
        this.matchTeam = matchTeam;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public OnlineCardStatus getOnlineCardStatus() {
        return onlineCardStatus;
    }

    public void setOnlineCardStatus(OnlineCardStatus onlineCardStatus) {
        this.onlineCardStatus = onlineCardStatus;
    }

    public String getIdNhlNumber() {
        return idNhlNumber;
    }

    public void setIdNhlNumber(String idNhlNumber) {
        this.idNhlNumber = idNhlNumber;
    }

    public boolean is_readedByOnline() {
        return _readedByOnline;
    }

    public void set_readedByOnline(boolean _readedByOnline) {
        this._readedByOnline = _readedByOnline;
    }

    @Override
    public String toString() {
        return "CardData{" +
                "kdd='" + kdd + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }


    public String getFile1Hash() {
        return file1Hash;
    }

    public void setFile1Hash(String file1Hash) {
        this.file1Hash = file1Hash;
    }

    public String getFile2Hash() {
        return file2Hash;
    }

    public void setFile2Hash(String file2Hash) {
        this.file2Hash = file2Hash;
    }

    public String getFile3Hash() {
        return file3Hash;
    }

    public void setFile3Hash(String file3Hash) {
        this.file3Hash = file3Hash;
    }

    public String getFile4Hash() {
        return file4Hash;
    }

    public void setFile4Hash(String file4Hash) {
        this.file4Hash = file4Hash;
    }

    public String printBlood() {
       String  result = "";
       if (getBloodGroup() != null && !getBloodGroup().isEmpty()){
           result += Integer.parseInt(getBloodGroup());
       }
       if (getBloodResus() != null && !getBloodResus().isEmpty()){
           result += "00".equals(getBloodResus()) ? "-" : "+";
       }
       return result;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
