package ru.alth.nhl.dto;

import ru.alth.nhl.helpers.ByteHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Вспомогательный класс
 * для работы с бинарными данными упакованными в TLV формат
 *
 * @author a_gorshenin
 */

public class TLVData implements CDataInteface {
    private int tag;
    private CData data;


    //todo: move in some another place
    public static Map<String, String> parseMap(String data) {
        Map<String, String> resultTags = new HashMap<String, String>();
        boolean flag; // Constructed data object
        String tag = "";
        String value = "";
        int len = 0;
        for (int i = 0; i < data.length(); ) {
            flag = false;
            // tag
            tag = data.substring(i, i + 2);
            if ((Integer.parseInt(tag, 16) & 0x20) == 0x20) {
                flag = true;
            }
            i += 2;
            if ((Integer.parseInt(tag, 16) & 0x1F) == 0x1F) {
                for (; ; ) {
                    tag += data.substring(i, i + 2);
                    i += 2;
                    if ((Integer.parseInt(tag, 16) & 0x80) != 0x80) {
                        break;
                    }
                }
            }

            // len
            if ((i + 2) <= data.length())
                len = Integer.parseInt(data.substring(i, i + 2), 16);
            i += 2;
            if ((len & 0x80) == 0x80) {
                int tmp = (len & 0x7F) * 2;
                len = Integer.parseInt(data.substring(i, i + tmp), 16);
                i += tmp;
            }
            len *= 2;

            // value
            if ((i + len) <= data.length())
                value = data.substring(i, i + len);
            i += len;
            if (flag == true && tag != "71" && tag != "72") {  // TODO: !!! special function needs for tags 71, 72 !!!
                Map<String, String> tmpTags = parseMap(value);

                for (String key : tmpTags.keySet()) {
                    resultTags.put(key, tmpTags.get(key));
                }

            } else {
                resultTags.put(tag, value);
            }
        }

        return resultTags;
    }

    private boolean isLongTag() {
        return ((this.tag & 0x1F00) == 0x1F00);
    }

    private byte hiTagByte() {
        return (byte) ((this.tag & 0xFF00) >> 8);
    }

    private byte lowTagByte() {
        return (byte) (this.tag & 0xFF);
    }

    /**
     * Конструктор по умолчанию - создание пустого тега
     */
    public TLVData(int tag) {
        this.tag = tag;
        this.data = new CData();
    }

    /**
     * Конструктор из строки
     */
    public TLVData(int tag, String data) {
        this.tag = tag;
        this.data = new CData(data);
    }

    /**
     * Parse string as TLV structure. String should contains only TLV and length field of parsed TLV
     * should correspond with string length. More info:
     * <a href='http://www.openscdp.org/scripts/tutorial/emv/TLV.html'>http://www.openscdp.org/scripts/tutorial/emv/TLV.html</a>
     *
     * @param tlv TLV string
     * @return parsed TLV structure
     * @throws IllegalArgumentException if string length is not corresponding to length field
     */
    public static TLVData parse(String tlv) {
        try {
            byte[] bytes = ByteHelper.hexStringToBinary(tlv);
            int i = 0;

            // Read tag
            int tag;
            {
                byte firstByte = bytes[i];
                i++;
                if ((firstByte & 31) == 31) { // XXX11111 (31) - there is a 2nd byte with tag value
                    byte secondByte = bytes[i];
                    i++;
                    tag = (firstByte << 8) | secondByte;
                } else {
                    tag = firstByte;
                }
            }


            // Read length
            int length;
            {
                byte firstByte = bytes[i];
                i++;
                // If byte has a pattern 1XXXXXXX,  means that there are more bytes in length field
                if (firstByte > 127) {
                    length = 0;
                    // In this case first byte means length of TLV length field
                    for (int j = 0; j < firstByte; j++) {
                        byte nextByte = bytes[i];
                        i++;
                        length = (length << 8) | nextByte;
                    }
                } else {
                    length = firstByte;
                }
            }

            // Read data
            int dataLength = bytes.length - i;
            if (dataLength != length)
                throw new IllegalArgumentException("Rest of data is bigger than a length parameter. Use special function parseList to parse list of tags, if you need");
            byte[] data = new byte[dataLength];
            System.arraycopy(bytes, i, data, 0, data.length);

            return new TLVData(tag, new CData(data));

        } catch (Exception e) {
            throw new IllegalArgumentException("Could not parse tlv structure from string '" + tlv + "'", e);
        }
    }

    /**
     * Parse list of TLV data. When first TLV parsed from string, and string has more characters, it is parsed again.
     *
     * @param tlv list of TLV data (concatenation of some TLV serialized structures)
     * @return list of parsed TLV structures
     */
    public static List<TLVData> parseList(String tlv) {
        try {
            ArrayList<TLVData> result = new ArrayList<TLVData>();
            byte[] bytes = ByteHelper.hexStringToBinary(tlv);
            int i = 0;

            while (i < bytes.length) {

                // Read tag
                int tag;
                {
                    int firstByte = (0xFF) & bytes[i];
                    i++;
                    if ((firstByte & 31) == 31) { // XXX11111 (31) - there is a 2nd byte with tag value
                        int secondByte = (0xFF) & bytes[i];
                        i++;
                        tag = (firstByte << 8) | secondByte;
                    } else {
                        tag = firstByte;
                    }
                }

                if (tag == 0) {
                    return result; //todo fix it
                }
                // Read length
                int length;
                {
                    int firstByte = (0xFF) & bytes[i];
                    i++;
                    if (firstByte > 127) {
                        length = 0;
                        for (int j = 0; j < firstByte; j++) {
                            int nextByte = (0xFF) & bytes[i];
                            i++;
                            length = (length << 8) | nextByte;
                        }
                    } else {
                        length = firstByte;
                    }
                }

                // Read data
                byte[] tagData = new byte[length];
                System.arraycopy(bytes, i, tagData, 0, length);
                i += length;

                result.add(new TLVData(tag, new CData(tagData)));
            }

            return result;


        } catch (Exception e) {
            throw new IllegalArgumentException("Could not parse tlv structure from string '" + tlv + "'", e);
        }
    }


    /**
     * Конструктор из CData
     */
    public TLVData(int tag, CData data) {
        this.tag = tag;
        this.data = new CData(data);
    }

    /**
     * Данные в теге
     */
    public CData data() {
        return this.data;
    }

    public int getTag() {
        return tag;
    }

    /**
     * Установить новые данные для тега
     */
    public void setData(CData data) {
        this.data = data;
    }

    /**
     * Признак пустоты тега
     */
    public boolean isEmpty() {
        return this.data.isEmpty();
    }

    /**
     * Длина тега, приведенного к массиву байтов
     */
    @Override
    public int bytesLength() {
        int size = 0;
        if (!this.data.isEmpty()) {
            size += this.data.size();
            if (this.isLongTag())
                size += 3;
            else
                size += 2;
        }
        return size;
    }

    /**
     * Приведение к массиву байтов
     */
    @Override
    public byte[] toBytes() {
        if (this.data.isEmpty())
            return new byte[0];

        // Generate bytes for tag (could be 1 or 2 bytes long)
        byte[] tagBytes = this.isLongTag()
                ? new byte[]{hiTagByte(), lowTagByte()}
                : new byte[]{lowTagByte()};

        // Generate bytes for length
        byte[] lengthBytes;
        {
            int length = this.data.size();
            // If length is less than 127, it's enough one byte to encode it
            if (length < 0x80) {
                lengthBytes = new byte[]{(byte) length};
            }
            // else, use special TLV format to encode length
            else {
                // Calculate byte count in length
                int bytesInLength = 0;
                int tmp = length;
                while (tmp != 0) {
                    bytesInLength++;
                    tmp >>= 8;
                }

                // Create byte array for length. +1 for count byte with of bytes of length
                lengthBytes = new byte[1 + bytesInLength];

                // Copy length per bytes
                int i = bytesInLength;
                tmp = length;
                while (tmp != 0) {
                    lengthBytes[i--] = (byte) tmp;
                    tmp >>= 8;
                }

                // Write count of bytes in length in first byte
                lengthBytes[0] = (byte) (bytesInLength | 0x80);
            }
        }

        // Generate bytes for value
        byte[] valueBytes = this.data.data();


        // Concatenate tag, length and value bytes and return result
        byte[] result = new byte[tagBytes.length + lengthBytes.length + valueBytes.length];


        System.arraycopy(tagBytes, 0, result, 0, tagBytes.length);
        System.arraycopy(lengthBytes, 0, result, tagBytes.length, lengthBytes.length);
        System.arraycopy(valueBytes, 0, result, tagBytes.length + lengthBytes.length, valueBytes.length);

        return result;
    }

    /**
     * Приведение к CData
     */
    public CData toCData() {
        return new CData(this.toBytes());
    }

    /**
     * Приведение к String
     */
    public String toString() {
        return (new CData(this.toBytes())).toString();
    }

    /**
     * Извлечение тега из данных
     */
    static public TLVData getTag(CData data, int tag) {
        TLVData result = new TLVData(tag);
        int index = 0;

        while (index < data.size()) {
            int tg = 0;
            int tgl = 0;
            int ln = 0;
            if ((data.at(index) & 0x1F) == 0x1F)  // Long tag
            {
                tg = (data.at(index) << 8) + (data.at(index + 1));
                tgl = 2;
                ln = data.at(index + 2);
            } else {                              // Short tag
                tg = data.at(index);
                tgl = 1;
                ln = data.at(index + 1);
            }
            if (tg == tag)
                result.setData(data.sub(index + tgl + 1, ln));

            index += (ln + tgl + 1);
        }
        return result;
    }
}
