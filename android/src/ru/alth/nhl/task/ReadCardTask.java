package ru.alth.nhl.task;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import ru.alth.nhl.crypto.TLV;
import ru.alth.nhl.dto.APDU;
import ru.alth.nhl.dto.CData;
import ru.alth.nhl.dto.CardData;
import ru.alth.nhl.dto.TLVData;
import ru.alth.nhl.helpers.AppletReaderHelper;
import ru.alth.nhl.helpers.SconReaderHelper;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ReadCardTask extends AsyncTask<Intent, Void, CardData> {

    public interface TaskListener {
        public void onFinished(CardData result);
    }

    private final TaskListener taskListener;

    public ReadCardTask(TaskListener taskListener) {
        this.taskListener = taskListener;
    }

    @Override
    /**
     * The system calls this to perform work in a worker thread and
     * delivers it the parameters given to AsyncTask.execute()
     */
    protected CardData doInBackground(Intent... intent) {

        boolean useOnline = intent[0].getBooleanExtra("USE_ONLINE", true);
        String serverUrl = intent[0].getStringExtra("server");


        IsoDep isoDep = null;
        CardData cardResult = new CardData();
        try {

            Tag tag = intent[0].getParcelableExtra(NfcAdapter.EXTRA_TAG);

            if (availableIsoDep(tag.getTechList())) {
                //private IsoDep isoDep;
                isoDep = IsoDep.get(tag);
                isoDep.connect();
                isoDep.setTimeout(5000);
                boolean isScon = true;
                try {
                    //Пытемся сделать селект сконовского приложения
                    APDU selectAIDScon = new APDU("00A40400", "F700000432900001", APDU.NORMAL);
                    CData result = new CData(isoDep.transceive(selectAIDScon.toBytes()));
                    isScon = result.toString().endsWith("9000");
                } catch (Throwable e) {
                    isScon = false;
                }
                if (isScon) {
                    // Запускаем сконовский сценарий
                    // Если оналйн режим то считываем только 5 файл и вызываем оналйн сервис
                    if (useOnline) {
                        String file5TlvData = SconReaderHelper.readFile5Alone(isoDep);
                        CardData cardData = readCardDataFromServer(serverUrl, file5TlvData);
                        if (cardData != null) {
                            return cardData;
                        }
                    }
                    // Если не удалось считать оналйн считываем офлайн
                    return SconReaderHelper.readCardData(isoDep);
                } else {
                    APDU selectAID = new APDU("00A40400", "F7496E706173536F6369616C417001", APDU.NORMAL);
                    CData result = new CData(isoDep.transceive(selectAID.toBytes()));
                    boolean isSocialApplet = result.toString().endsWith("9000");
                    // Читаем карту с Social Applet`ом
                    if(isSocialApplet){
                        String serialNum = TLVData.parseMap(result.toString()).get("DF11");
                        if(useOnline){
                            String file5TlvData = AppletReaderHelper.readFile5(isoDep, serialNum.substring(serialNum.length() - 16));
                            CardData cardData = readCardDataFromServer(serverUrl, file5TlvData);
                            if (cardData != null) {
                                return cardData;
                            }
                        }
                        // Если не удалось считать оналйн считываем офлайн
                        return AppletReaderHelper.readCardData(isoDep, serialNum.substring(serialNum.length() - 16));
                    }
                }
                //TODO
                //adapter.disableForegroundDispatch(this);
            }
        } catch (Throwable e) {
            cardResult.setCardReadError(e.getMessage());
            //showWaitForCard();
        } finally {
            try {
                if (isoDep != null) {
                    isoDep.close();
                }
            } catch (Throwable e) {
                //e.printStackTrace();
            }
        }
        return cardResult;
    }

    public final CardData readCardDataFromServer(String url, String file5Data) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        final HttpParams httpParameters = httpClient.getParams();

        HttpConnectionParams.setConnectionTimeout(httpParameters, 7 * 1000);
        HttpConnectionParams.setSoTimeout(httpParameters, 7 * 1000);
        try {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }

            HttpPost httpPost = new HttpPost(url + "/card");
            // Building post parameters, key and value pair
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(1);
            nameValuePair.add(new BasicNameValuePair("file5Data", file5Data));
            nameValuePair.add(new BasicNameValuePair("action", "getCardData"));
            nameValuePair.add(new BasicNameValuePair("ml_request", "true"));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {
                byte[] result = EntityUtils.toByteArray(response.getEntity());
                String resultStr = new String(result, "UTF-8");
                JSONObject jsonResult = new JSONObject(resultStr);
                CardData cardData = null;
                if (jsonResult.has("cardData")) {
                    JSONObject res = jsonResult.getJSONObject("cardData");
                    cardData = new CardData();

                    SconReaderHelper.parseFile1TLV(cardData, TLV.parseTLV(res.getString("file1TLVdata")));
                    SconReaderHelper.parseFile2TLV(cardData, TLV.parseTLV(res.getString("file2TLVdata")));
                    SconReaderHelper.parseFile3TLV(cardData, TLV.parseTLV(res.getString("file3TLVdata")));
                    SconReaderHelper.parseFile4TLV(cardData, TLV.parseTLV(res.getString("file4TLVdata")));
                    String photo = res.getString("photo");
                    cardData.setPhoto(ru.alth.nhl.crypto.DatatypeConverter.parseHexBinary(photo));
                    cardData.set_readedByOnline(true);
                }
                return cardData;
            } else {
                return null;
            }
        } catch (Throwable e) {
            return null;
        }
    }


    @Override
    /**
     * The system calls this to perform work in the UI thread and delivers
     * the result from doInBackground()
     */
    protected void onPostExecute(CardData cardData) {
        if (this.taskListener != null) {
            // And if it is we call the callback function on it.
            this.taskListener.onFinished(cardData);
        }
    }

    private boolean availableIsoDep(String[] techList) {
        for (String tech : techList) {
            if ("android.nfc.tech.IsoDep".equals(tech) /*|| "android.nfc.tech.MifareClassic".equals(tech)*/) {
                return true;
            }
        }
        return false;
    }

}
