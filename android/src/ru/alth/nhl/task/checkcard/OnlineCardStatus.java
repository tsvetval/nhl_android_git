package ru.alth.nhl.task.checkcard;

/**
 *
 */
public enum OnlineCardStatus {
    ONLINE,
    OK,
    SERVICE_NOT_AVAILABLE,
    FAIL
}
