package ru.alth.nhl.task.checkcard;

import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import ru.alth.nhl.dto.CardData;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class CheckCardStatusTask extends AsyncTask<CardData, Void, OnlineCardStatus> {

    public interface TaskListener {
        void onFinished(OnlineCardStatus result);
    }

    private final TaskListener taskListener;

    public CheckCardStatusTask(TaskListener taskListener) {
        this.taskListener = taskListener;
    }

    /**
     */
    protected OnlineCardStatus doInBackground(CardData... args) {

        HttpClient httpClient = new DefaultHttpClient();
        final HttpParams httpParameters = httpClient.getParams();

        HttpConnectionParams.setConnectionTimeout(httpParameters, 7 * 1000);
        HttpConnectionParams.setSoTimeout        (httpParameters, 7 * 1000);
        try {
            CardData cardData = args[0];
            String url = cardData.getServerUrl();
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            HttpPost httpPost = new HttpPost(url + "/card/check");
            // Building post parameters, key and value pair
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(1);
            nameValuePair.add(new BasicNameValuePair("cardId", cardData.getCardId()));
            nameValuePair.add(new BasicNameValuePair("hash1", cardData.getFile1Hash()));
            nameValuePair.add(new BasicNameValuePair("hash2", cardData.getFile2Hash()));
            nameValuePair.add(new BasicNameValuePair("hash3", cardData.getFile3Hash()));
            nameValuePair.add(new BasicNameValuePair("hash4", cardData.getFile4Hash()));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));

            HttpResponse response = httpClient.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {
                byte[] result = EntityUtils.toByteArray(response.getEntity());
                String resultStr = new String(result, "UTF-8");
                JSONObject jsonResult = new JSONObject(resultStr);
                if (jsonResult.getString("result").equals("OK")) {
                    return OnlineCardStatus.OK;
                } else {
                    return OnlineCardStatus.FAIL;
                }

            } else {
                return OnlineCardStatus.SERVICE_NOT_AVAILABLE;
            }
        } catch (Throwable e) {
            return OnlineCardStatus.SERVICE_NOT_AVAILABLE;
        }
    }

    @Override
    protected void onPostExecute(OnlineCardStatus cardStatus) {
        if (this.taskListener != null) {
            this.taskListener.onFinished(cardStatus);
        }
    }
}
