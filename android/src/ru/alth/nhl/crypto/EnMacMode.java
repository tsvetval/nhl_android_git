package ru.alth.nhl.crypto;

/**
 * Created by User on 24.11.2015.
 */
public enum EnMacMode {
    /**
     * MAC in accordance with ISO / IEC 9797-1 algorithm 1
     */
    MAC_FULL_TDES,
    /**
     * MAC in accordance with ISO / IEC 9797-1 algorithm 3
     */
    MAC_RETAIL
}
