package ru.alth.nhl.crypto;

/**
 *
 */
public enum EnCipherDirection {
    ENCRYPT_MODE,
    DECRYPT_MODE
}
