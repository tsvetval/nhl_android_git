package ru.alth.nhl.crypto;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract class for TLV parsing and serialization
 */
public abstract class TLV {

    protected Map<String, String> tags = new java.util.TreeMap<String, String>();
    protected static final Logger log = Logger.getLogger(TLV.class.toString());


    /**
     * Constructor, parsing tlv string
     *
     * @param tlv string to parse
     */
    protected TLV(String tlv) throws Exception {
        try {
            tags = parseTLV(tlv);
        } catch (Exception ex) {
            log.log(Level.SEVERE, "TLV Parse Error", ex);
            throw new Exception("TLV Parse Error");
        }
    }

    /**
     * Constructor, using map of tags
     *
     * @param tags
     */
    protected TLV(Map<String, String> tags) {
        this.tags = tags;
    }

    /**
     * Default Constructor with empty tags
     */
    protected TLV() {
    }

    /**
     * Parse TLV
     *
     * @param data - data, that must be parse
     * @return tags - parse result
     */
    public static Map<String, String> parseTLV(String data) {
        Map<String, String> resultTags = new TreeMap<String, String>();
        String tag = "";
        int len = 0;
        String value = "";
        boolean flag;

        log.fine(String.format("Parsing TLV String: %1$s ...", data));

        for (int i = 0; i < data.length(); ) {
            flag = false;

            tag = data.substring(i, i + 2);
            if ((Integer.parseInt(tag, 16) & 0x20) == 0x20) flag = true;

            i += 2;
            if ((Integer.parseInt(tag, 16) & 0x1F) == 0x1F)
                for (; ; ) {
                    tag += data.substring(i, i + 2);
                    i += 2;
                    if ((Integer.parseInt(tag, 16) & 0x80) != 0x80) break;
                }

                len = Integer.parseInt(data.substring(i, i + 2), 16);
                i += 2;


            if ((len & 0x80) == 0x80) {
                int tmp = (len & 0x7F) * 2;
                    len = Integer.parseInt(data.substring(i, i + tmp), 16);
                    i += tmp;

            }

            len *= 2;
                value = data.substring(i, i + len);

            i += len;
            if (flag == true) {
                Map<String, String> tmpTags = parseTLV(value);
                for (Map.Entry<String, String> entry : tmpTags.entrySet())
                    resultTags.put(entry.getKey(), entry.getValue());
            } else {
                resultTags.put(tag, value);
                log.fine(String.format("Found new tag: '%1$s', value: '%2$s'", tag, value));
            }
        }

        return resultTags;
    }


    /**
     * Convert object to TLV String without it's own tag and length
     *
     * @return tlv string
     */
    public String serialize() {
        String result = "";
        log.fine("Staring TLV Serialization ...");
        for (Map.Entry<String, String> entry : tags.entrySet()) {
            String len = getTagLength(entry.getValue());
            String step = entry.getKey() + len + entry.getValue();
            log.fine(String.format("Serialization step: tag='%1$s'; len='%2$s'; value='%3$s'; result='%4$s'", entry.getKey(), len, entry.getValue(), step));
            result += step;
        }
        log.fine("TLV Serialized: " + result);
        return result;
    }

    /**
     * Convert object to TLV String with it's own tag (provided as parameter) and length
     *
     * @param tag
     * @return tlv string
     */
    public String serialize(String tag) {
        String tlv = serialize();
        return tag + getTagLength(tlv) + tlv;
    }

    /**
     * Calculate tag length in proper format
     *
     * @param value
     * @return Tag length in hex string
     */
    protected String getTagLength(String value) {
        int len = value.length() / 2;
/*
        if (len < 0x80) return String.format("%02X", len);

        byte bytes = 0;
        for (int i = 0x01; i < 0xFF; i++)
            if (len > Math.pow(0xFF, i)) {
                bytes++;
            }   else {
                break;
            }

        String format = "%0" + Integer.toString(bytes * 2) + "X";
        bytes |= 1 << 7;

        String head_byte = String.format("%02X", bytes);
        String length = String.format(format, len);

        log.fine(String.format("Complex TLV Tag length: head_byte='%1$s'; length='%2$s';", head_byte, length));

        return head_byte + length;
*/
        if (len < 0x80) {
            return String.format("%02X", len);
        }

        int bytes = 0;
        String valueLengthLength = String.format("%X", len);
        if (valueLengthLength.length() % 2 == 0) {
            bytes = (valueLengthLength.length() / 2);
        } else {
            bytes = (valueLengthLength.length() / 2) + 1;
        }

        String format = "%0" + Integer.toString(bytes * 2) + "X";
        bytes |= 1 << 7;

        String head_byte = String.format("%02X", bytes);
        String length = String.format(format, len);
        log.fine(String.format("Complex TLV Tag length: head_byte='%1$s'; length='%2$s';", head_byte, length));

        return head_byte + length;

    }

    public static Map<String, String> parseJavaCardTLV(String data) {
        Map<String, String> resultTags = new TreeMap<String, String>();
        String tag = "";
        int len = 0;
        String value = "";
        boolean flag;

        log.fine(String.format("Parsing TLV String: %1$s ...", data));

        for (int i = 0; i < data.length(); ) {
            flag = false;

            tag = data.substring(i, i + 2);
            if ((Integer.parseInt(tag, 16) & 0x20) == 0x20) flag = true;

            i += 2;
            if ((Integer.parseInt(tag, 16) & 0x1F) == 0x1F)
                for (; ; ) {
                    tag += data.substring(i, i + 2);
                    i += 2;
                    if ((Integer.parseInt(tag, 16) & 0x80) != 0x80) break;
                }

            try{
                len = Integer.parseInt(data.substring(i, i + 2), 16);
                i += 2;
            } catch (Exception e){
                break;
            }


            if ((len & 0x80) == 0x80) {
                int tmp = (len & 0x7F) * 2;
                try{
                    len = Integer.parseInt(data.substring(i, i + tmp), 16);
                    i += tmp;
                } catch (Exception e){
                    break;
                }

            }

            len *= 2;
            try {
                value = data.substring(i, i + len);
            } catch (Exception e){
                break;
            }

            i += len;
            if (flag == true) {
                Map<String, String> tmpTags = parseTLV(value);
                for (Map.Entry<String, String> entry : tmpTags.entrySet())
                    resultTags.put(entry.getKey(), entry.getValue());
            } else {
                resultTags.put(tag, value);
                log.fine(String.format("Found new tag: '%1$s', value: '%2$s'", tag, value));
            }
        }

        return resultTags;
    }
}
