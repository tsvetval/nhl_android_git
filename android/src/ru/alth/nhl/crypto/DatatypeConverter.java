package ru.alth.nhl.crypto;

/**
 *
 */
public class DatatypeConverter {
    final protected static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    public static String printHexBinary(byte[] byteArray) {
        char[] hexChars = new char[byteArray.length*2];
        int v;

        for(int j=0; j < byteArray.length; j++) {
            v = byteArray[j] & 0xFF;
            hexChars[j*2] = hexArray[v>>>4];
            hexChars[j*2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    public static byte[] parseHexBinary(String hexString) {
        int len = hexString.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
        }

        return data;
    }


    public static String byteToHexString(byte inputByte) {
        return DatatypeConverter.printHexBinary(new byte[]{inputByte});
    }
}
