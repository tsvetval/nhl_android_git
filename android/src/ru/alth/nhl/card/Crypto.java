package ru.alth.nhl.card;

import java.util.Random;

/**
 * Created by d_litovchenko on 28.09.2015.
 */
public class Crypto {

    public static byte[] getRandom(int size) {
        Random generator = new Random();
        byte[] result = new byte[size];
        generator.nextBytes(result);
        return result;
    }
}
