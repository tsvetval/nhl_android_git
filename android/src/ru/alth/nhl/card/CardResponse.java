package ru.alth.nhl.card;

import ru.alth.nhl.helpers.ByteHelper;

/**
 * Структура ответа карты на APDU-команду. Содержит:
 * sw1, sw2 - коды ответа карты
 * data - данные, полученные от карты в результате выполнения команды
 */
public class CardResponse {
    public byte _sw1, _sw2;
    public byte[] _data;

    public CardResponse(byte sw1, byte sw2, byte[] data) {
        _sw1 = sw1;
        _sw2 = sw2;
        _data = data;
    }

    public CardResponse(byte[] response) {
        _sw1 = response[response.length - 2];
        _sw2 = response[response.length - 1];

        if (response.length > 2) {
            _data = new byte[response.length - 2];
            copyArray(response, 0, _data, 0, response.length - 2);
        }
    }

    public static void copyArray(byte[] src, int offset1, byte[] dst, int offset2, int count) {

        for(int i = 0; i < count; i++) {
            dst[offset2 + i] = src[offset1 + i];
        }
    }

    public boolean isOk() {
        if (_sw1 == (byte)0x90 && _sw2 == 0)
            return true;
        return false;
    }

    public boolean isFail() {
        if (_sw1 != (byte)0x90 || _sw2 != 0)
            return true;
        return false;
    }

    public byte[] getData() {
        return _data;
    }

    @Override
    public String toString() {
        return ByteHelper.binaryToHexString(new byte[]{_sw1, _sw2})+":"+ByteHelper.binaryToHexString(_data);
    }
}
