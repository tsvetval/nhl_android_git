package ru.alth.nhl;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import ru.alth.nhl.adapter.TabsPagerAdapter;
import ru.alth.nhl.dto.CardData;
import ru.alth.nhl.task.checkcard.CheckCardStatusTask;
import ru.alth.nhl.task.checkcard.OnlineCardStatus;

/**
 *
 */
public class ShowResultActivity extends FragmentActivity implements
        ActionBar.TabListener {

    private CardData cardData;

    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    // Tab titles
    private String[] tabs = { "1", "2", "3", "4" };
    final int[] ICONS = new int[] {
            R.drawable.frag1,
            R.drawable.frag2,
            R.drawable.frag3,
            R.drawable.frag4,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.BLACK/*.parseColor("#6E6F73")*/));

        /**
         * Экран всегда включен  http://developer.android.com/intl/ru/reference/android/os/PowerManager.html
         */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //actionBar.setSplitBackgroundDrawable(new ColorDrawable(Color.RED));
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.RED));
        setContentView(R.layout.result_form);

        // Получаем параметры модели
        //Bundle b = getIntent().getExtras();

        this.cardData = (CardData)getIntent().getSerializableExtra("cardData");
        //this.cardData = (CardData)b.get("cardData");
        // Initilization
        viewPager = (ViewPager) findViewById(R.id.pager);


        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mAdapter);

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Adding Tabs
        for (int i =0 ; i < ICONS.length; i++) {
            Drawable icon= ShowResultActivity.this.getResources().getDrawable(ICONS[i]);
            Bitmap bitmap = ((BitmapDrawable) icon).getBitmap();
            icon = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 40, true));
            actionBar.addTab(actionBar.newTab()//.setText(tabs[i])
                    .setIcon(icon)
                    .setTabListener(this));
        }

        /**
         * on swiping the viewpager make respective tab selected
         * */
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);
            }
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        if (!cardData.is_readedByOnline()) {
            // Check card status in background
            new CheckCardStatusTask(new CheckCardStatusTask.TaskListener() {
                public void onFinished(OnlineCardStatus result) {
                    cardData.setOnlineCardStatus(result);
                    ProgressBar pb = (ProgressBar) findViewById(R.id.checkStatusBar);
                    TextView status = (TextView) findViewById(R.id.onlineStatus);

                    if (pb != null && status != null) {
                        pb.setVisibility(View.GONE);
                        switch (result) {
                            case OK:
                                status.setTextColor(Color.GREEN);
                                status.setText("Статус подтвержден");
                                break;
                            case FAIL:
                                status.setTextColor(Color.RED);
                                status.setText("Статус не подтвержден");
                                break;
                            case SERVICE_NOT_AVAILABLE:
                                status.setTextColor(Color.YELLOW);
                                status.setText("Сервис недоступен");
                                break;
                        }
                    }
                }
            }).execute(cardData);
        }
    }


    private void animateHideResult() {
        ViewPager layout = (ViewPager) findViewById(R.id.pager);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_out_left_right);
        hyperspaceJumpAnimation.setDuration(200);
        hyperspaceJumpAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(ShowResultActivity.this, ReadCardActivity.class);
                startActivity(intent);
                finish();
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });

        layout.startAnimation(hyperspaceJumpAnimation);
    }

    /*ANIMATION SECTION*/
    // 2.0 and above
    @Override
    public void onBackPressed() {
        animateHideResult();
    }



    public CardData getCardData() {
        return cardData;
    }

    public void setCardData(CardData cardData) {
        this.cardData = cardData;
    }

    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}