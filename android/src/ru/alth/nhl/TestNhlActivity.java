package ru.alth.nhl;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import ru.alth.nhl.dto.CardData;


/**
 *
 */
public class TestNhlActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = getActionBar();
        actionBar.hide();

        setContentView(R.layout.test);

        ImageView btnPassObject = (ImageView) findViewById(R.id.imageView);
        btnPassObject.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openResult();

            }
        });
    }

    public void openResult(){
        Intent intent = new Intent(TestNhlActivity.this, ShowResultActivity.class);
        CardData cardData =  new CardData();
        cardData.setFirstName("Тестов1");
        cardData.setSecondName("Тест");
        cardData.setSecondName("Тестич");
        intent.putExtra("cardData", cardData);
        startActivity(intent);
        finish();
    }


}
