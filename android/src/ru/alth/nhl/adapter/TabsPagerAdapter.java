package ru.alth.nhl.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import ru.alth.nhl.dto.CardData;
import ru.alth.nhl.fragment.Result1Fragment;
import ru.alth.nhl.fragment.Result2Fragment;
import ru.alth.nhl.fragment.Result3Fragment;
import ru.alth.nhl.fragment.Result4Fragment;

/**
 *
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new Result1Fragment();
            case 1:
                return new Result2Fragment();
            case 2:
                return new Result3Fragment();
            case 3:
                return new Result4Fragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }

}
