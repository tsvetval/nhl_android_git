package ru.alth.nhl;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 *
 */
public class SettingsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        /**
         * Экран всегда включен  http://developer.android.com/intl/ru/reference/android/os/PowerManager.html
         */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.settings);


        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.NHL_PREFERENCES), Context.MODE_WORLD_WRITEABLE);;

        boolean isOnline = sharedPref.getBoolean(getString(R.string.SETTINGS_USE_ONLINE), true);
        CheckBox useOnline = (CheckBox)findViewById(R.id.onlineMode);
        useOnline.setChecked(isOnline);

        String serverText = sharedPref.getString(getString(R.string.SETTINGS_SERVER), "172.16.16.35:8080/nhl-arm/");
        EditText server = (EditText)findViewById(R.id.server);
        server.setText(serverText);

        ImageView btnBack = (ImageView) findViewById(R.id.backAndSave);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveAndBack();
            }
        });
    }


    public void saveAndBack(){
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.NHL_PREFERENCES), Context.MODE_WORLD_WRITEABLE);;
        SharedPreferences.Editor editor = sharedPref.edit();

        CheckBox useOnline = (CheckBox)findViewById(R.id.onlineMode);
        editor.putBoolean(getString(R.string.SETTINGS_USE_ONLINE), useOnline.isChecked());

        EditText server = (EditText)findViewById(R.id.server);
        editor.putString(getString(R.string.SETTINGS_SERVER), server.getText().toString());
        editor.commit();


        LinearLayout layout = (LinearLayout) findViewById(R.id.settingPage);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_out_left_right);
        hyperspaceJumpAnimation.setDuration(200);
        hyperspaceJumpAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(SettingsActivity.this, ReadCardActivity.class);
                startActivity(intent);
                finish();
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });

        layout.startAnimation(hyperspaceJumpAnimation);
    }

    /*ANIMATION SECTION*/
    // 2.0 and above
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        saveAndBack();
    }

}
