package ru.alth.nhl.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 */
public class DialogHelper {
    Activity activity;
    private static DialogHelper dialogHelper;

    private DialogHelper() {
    }

    public static void init(Activity activity){
        if(dialogHelper == null){
            dialogHelper = new DialogHelper();
        }
        dialogHelper.setActivity(activity);
    }

    private void setActivity(Activity activity) {
        this.activity = activity;
    }

    public static DialogHelper getInstance(){
        return dialogHelper;
    }

    public void showAlert(String message, DialogInterface.OnClickListener listener){
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", listener);
        alertDialog.show();
    }
}
