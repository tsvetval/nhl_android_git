package ru.alth.nhl.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.tech.IsoDep;
import android.util.DisplayMetrics;
import ru.alth.nhl.crypto.*;
import ru.alth.nhl.crypto.DatatypeConverter;
import ru.alth.nhl.dto.APDU;
import ru.alth.nhl.dto.CData;
import ru.alth.nhl.dto.CardData;
import ru.alth.nhl.dto.TLVData;

import javax.smartcardio.CardException;
import javax.xml.bind.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.alth.nhl.helpers.SconReaderHelper.*;

/**
 *
 */
public class AppletReaderHelper {

    private static final String readKey1 = "5D07291A0B73E5EA2FBCDA924FC4346D";
    private static final String readKey2 = "989780E6C88331BFDAF2A2FE4AB50775";
    private static final String readKey3 = "8075A2E561C894E0B5F2B59D640BAE37";
    private static final String readKey4 = "5DF8A80E23757986583D58A4D9B9E3D3";
    private static final String readKey5 = "3107B6BAFE8C4094548C7C794AD98C86";

    protected static final int MAX_FILE_SIZE = 232;


    public static final CardData readCardData(IsoDep isoDep, String serialNum) throws Exception {
        CardData cardData = new CardData();
        readFile1(cardData, isoDep, serialNum);
        readFile2(cardData, isoDep, serialNum);
        readFile3(cardData, isoDep, serialNum);
        readFile4(cardData, isoDep, serialNum);
        return cardData;
    }

    private static void readFile1(CardData cardData, IsoDep isoDep, String serialNum) throws Exception {
        String fileData = readFileData(isoDep, serialNum, "01", readKey1);
        Map<String, String> fileResult = TLV.parseJavaCardTLV(fileData);
        parseFile1TLV(cardData, fileResult);
    }

    private static void readFile2(CardData cardData, IsoDep isoDep, String serialNum) throws Exception {
        String fileData = readFileData(isoDep, serialNum, "02", readKey2);
        Map<String, String> fileResult = TLV.parseJavaCardTLV(fileData);
        parseFile2TLV(cardData, fileResult);
    }

    private static void readFile3(CardData cardData, IsoDep isoDep, String serialNum) throws Exception {
        String fileData = readFileData(isoDep, serialNum, "03", readKey3);
        Map<String, String> fileResult = TLV.parseJavaCardTLV(fileData);
        parseFile3TLV(cardData, fileResult);
    }

    private static void readFile4(CardData cardData, IsoDep isoDep, String serialNum) throws Exception {
        String fileData = readFileData(isoDep, serialNum, "04", readKey4);
        Map<String, String> fileResult = TLV.parseJavaCardTLV(fileData);
        parseFile4TLV(cardData, fileResult);
    }

    public static String readFile5(IsoDep isoDep, String serialNum) throws Exception {
        return readFileData(isoDep, serialNum, "05", readKey5);
    }

    public static String readFileData(IsoDep isoDep, String serialNum, String fileNumber, String masterKey) throws Exception {
        List<String> cmdList = new ArrayList<String>();
        String randomNumber = getRandomNumber();

        // Step 1: Select File
        String selectRes = selectFile(isoDep, fileNumber);
        Long fileSize = Long.parseLong(selectRes.substring(0, 4), 16);

        // Step 2: Initialize Update
        String readKeyNumber = selectRes.substring(4, 6);
                String initRes = initializeUpdate(isoDep, readKeyNumber, randomNumber);
        String counter = initRes.substring(2, 6);
        String cardKey = diverKey(serialNum, masterKey);
        byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(cardKey),
                DatatypeConverter.parseHexBinary(String.format("%-32s", counter).replace(" ", "0")), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);

        // Step 3: External Authentication
        String extAuthCmd = externalAuthentication(initRes, randomNumber, sessionKey, readKeyNumber);
        cmdList.add(extAuthCmd);

        // Step 4: Generate commands for reading file
        String hostCrypto = extAuthCmd.substring(extAuthCmd.length() - 16);
        cmdList.addAll(generateReadCmdList(fileSize.intValue(), sessionKey, hostCrypto));
        StringBuilder readRes = new StringBuilder();
        for(String cmd: cmdList){
            String res = new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd))).toString();
            // Delete SW from response
            readRes.append(res.substring(0, res.length() - 4));
        }
        return readRes.toString();
    }


    private static String initializeUpdate(IsoDep isoDep, String keyNumber, String randomNumber) throws IOException {
        String cmd = "805000" + keyNumber + "08" + randomNumber;
        return new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd))).toString();
    }

    private static String selectFile(IsoDep isoDep, String fileNumber) throws IOException {
        String cmd = "80A400" + fileNumber + "04";
        return new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd))).toString();
    }

    private static String externalAuthentication(String inputCommand, String randomNumber, byte [] sessionKey, String readKeyNumber) throws SMartChipSMException, IOException {
        // Счётчик аутентификаций
        String counter = inputCommand.substring(2, 6);

        // Случайное число, генерируемое картой
        String challenge = inputCommand.substring(6, 18);

        // Формирование блока Dhost
        String dHost = counter + challenge + randomNumber + "8000000000000000";

        byte [] dcrHost = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(dHost), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);

        String outputCrypt = DatatypeConverter.printHexBinary(dcrHost).substring(DatatypeConverter.printHexBinary(dcrHost).length() - 16);
        // Команда APDU External Authentication
        return "808200" + readKeyNumber + "08" + outputCrypt;
    }

    private static String getRandomNumber(){
        String randNumber = "";
        for(int i = 0; i < 8; i++){
            randNumber += String.format("%02x", Math.round(Math.random() * 255)).toUpperCase();
        }
        if(randNumber.length() % 2 == 0){
            return randNumber;
        } else{
            return 0 + randNumber;
        }
    }

    private static String diverKey(String serialNum, String masterKey) throws SMartChipSMException {

        String keyDerivationData = serialNum + inverse(serialNum);
        byte [] cardKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(masterKey),
                DatatypeConverter.parseHexBinary(keyDerivationData), EnCipherMode.ECB, EnCipherDirection.ENCRYPT_MODE);
        return DatatypeConverter.printHexBinary(cardKey);
    }

    private static List<String> generateReadCmdList(int fileSize, byte [] sessionKey, String hostCrypto) throws SMartChipSMException, IOException, CardException {
        List<String> resultList = new ArrayList<String>();

        int blockSize = 192;
        String ClaIns = "84B0";
        int offset = 0;
        String iv = hostCrypto;
        while (fileSize > offset) {
            int length;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset);

            int cmdLen = length;
            String dataForMac = ClaIns + p1p2 + String.format("%02X", cmdLen) + "800000";
            String cmd = ClaIns + p1p2 + "09" + String.format("%02X", cmdLen);

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.encDec3DES2WithIV(sessionKey,
                    DatatypeConverter.parseHexBinary(dataForMac), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE, DatatypeConverter.parseHexBinary(iv)));

            cmd = cmd + calcMacData;
            resultList.add(cmd);
            offset += length;
            iv = calcMacData;
        }
        return resultList;
    }

    private static String inverse(String string){
        Map<Character, Character> map = new HashMap<Character, Character>();
        map.put('0', 'F');
        map.put('1', 'E');
        map.put('2', 'D');
        map.put('3', 'C');
        map.put('4', 'B');
        map.put('5', 'A');
        map.put('6', '9');
        map.put('7', '8');
        map.put('8', '7');
        map.put('9', '6');
        map.put('A', '5');
        map.put('B', '4');
        map.put('C', '3');
        map.put('D', '2');
        map.put('E', '1');
        map.put('F', '0');
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < string.length(); i++){
            output.append(map.get(string.charAt(i)));
        }
        return output.toString();
    }
}
