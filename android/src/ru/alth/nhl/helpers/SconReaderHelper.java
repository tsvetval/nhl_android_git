package ru.alth.nhl.helpers;

import android.nfc.tech.IsoDep;
import ru.alth.nhl.crypto.*;
import ru.alth.nhl.dto.CData;
import ru.alth.nhl.dto.CardData;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Класс для чтения scon карт
 */
public class SconReaderHelper {
    private static final String readKey1 = "5D07291A0B73E5EA2FBCDA924FC4346D";
    private static final String readKey2 = "989780E6C88331BFDAF2A2FE4AB50775";
    private static final String readKey3 = "8075A2E561C894E0B5F2B59D640BAE37";
    private static final String readKey4 = "5DF8A80E23757986583D58A4D9B9E3D3";
    private static final String readKey5 = "3107B6BAFE8C4094548C7C794AD98C86";
    final static String DF_ID = "4F00";

    public static final CardData readCardData(IsoDep isoDep) throws Exception {
        CardData cardData = new CardData();
        selectDFAID(isoDep, DF_ID);

        readFile1(cardData, isoDep);
        readFile2(cardData, isoDep);
        readFile3(cardData, isoDep);
        readFile4(cardData, isoDep);
        return cardData;
    }

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd.MM.yyyy");

    private static void readFile1(CardData cardData, IsoDep isoDep) throws Exception {
        String fileData = readFileData(isoDep, (byte) 2, readKey1, "0001");
        Map<String, String> fileResult = TLV.parseTLV(fileData);
        parseFile1TLV(cardData, fileResult);
    }

    public static void parseFile1TLV(CardData cardData, Map<String, String> fileResult) throws UnsupportedEncodingException, ParseException {
        cardData.setLastName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF12")), "cp1251"));
        cardData.setFirstName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF13")), "cp1251"));
        cardData.setSecondName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF14")), "cp1251"));
        if (fileResult.get("DF11") != null) {
            cardData.setPhoto(DatatypeConverter.parseHexBinary(fileResult.get("DF11")));
        }

        String date = fileResult.get("DF15");
        if (date != null && !date.isEmpty()) {
            cardData.setBirthDate(simpleDateFormat2.format(simpleDateFormat.parse(date)));
        }
        cardData.setExpirationDate(fileResult.get("DF16"));

        cardData.setCardId(fileResult.get("DF17"));


        cardData.setFile1Hash(fileResult.get("DF51"));
    }


    private static void readFile2(CardData cardData, IsoDep isoDep) throws Exception {
        String fileData = readFileData(isoDep, (byte) 4, readKey2, "0002");
        Map<String, String> fileResult = TLV.parseTLV(fileData);

        parseFile2TLV(cardData, fileResult);
    }

    public static void parseFile2TLV(CardData cardData, Map<String, String> fileResult) throws UnsupportedEncodingException {
        cardData.setIdNhlNumber(fileResult.get("DF21"));
        cardData.setTeamName(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF22")), "cp1251"));

        cardData.setMemberNhlStatus(fileResult.get("DF23"));
        cardData.setActiveStatus(fileResult.get("DF24"));
        cardData.setPhoneNumber(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF25")), "cp1251"));
        cardData.setEmail(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF26")), "cp1251"));

        cardData.setMailingAddress(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF27")), "cp1251"));
        cardData.setFile2Hash(fileResult.get("DF52"));
    }


    private static void readFile3(CardData cardData, IsoDep isoDep) throws Exception {
        String fileData = readFileData(isoDep, (byte) 6, readKey3, "0003");
        Map<String, String> fileResult = TLV.parseTLV(fileData);
        parseFile3TLV(cardData, fileResult);
    }

    public static void parseFile3TLV(CardData cardData, Map<String, String> fileResult) throws UnsupportedEncodingException, ParseException {
        cardData.setCardNhlStatus(fileResult.get("DF31"));
        cardData.setPassOnMatch(fileResult.get("DF32"));
        if(fileResult.get("DF39") != null){
            cardData.setPassport(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF33")), "cp1251"));
        } else {
            cardData.setPassport(fileResult.get("DF33"));
        }

        cardData.setPrava(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF34")), "cp1251"));

        String date = fileResult.get("DF35");
        if (date != null && !date.isEmpty()) {
            cardData.setMatchDate(simpleDateFormat2.format(simpleDateFormat.parse(date)));
        }
        if (fileResult.get("DF36") != null) {
            cardData.setMatchPlace(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF36")), "cp1251"));
        }
        if (fileResult.get("DF37") != null) {
            cardData.setMatchTeam(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF37")), "cp1251"));
        }
        if(fileResult.get("DF38") != null){
            cardData.setPlayerNumber(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF38")), "cp1251"));
        }
        cardData.setFile3Hash(fileResult.get("DF53"));
    }

    private static void readFile4(CardData cardData, IsoDep isoDep) throws Exception {
        String fileData = readFileData(isoDep, (byte) 8, readKey4, "0004");
        Map<String, String> fileResult = TLV.parseTLV(fileData);

        parseFile4TLV(cardData, fileResult);

    }


    public static String readFile5Alone(IsoDep isoDep) throws Exception {
        selectDFAID(isoDep, DF_ID);
        return readFileData(isoDep, (byte) 10, readKey5, "0005");
    }


    public static void parseFile4TLV(CardData cardData, Map<String, String> fileResult) throws UnsupportedEncodingException, ParseException {
        cardData.setInsuranceCompany(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF41")), "cp1251"));
        cardData.setInsuranceNumber(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF42")), "cp1251"));

        String date = fileResult.get("DF43");
        if (date != null && !date.isEmpty()) {
            cardData.setMedExploreDate(simpleDateFormat2.format(simpleDateFormat.parse(date)));
        }

        cardData.setMedExploreResult(fileResult.get("DF44"));
        if (fileResult.get("DF45") != null && !fileResult.get("DF45").isEmpty()) {
            cardData.setHeight(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF45")), "cp1251"));
        }
        if (fileResult.get("DF46") != null && !fileResult.get("DF46").isEmpty()) {
            cardData.setWeight(new String(DatatypeConverter.parseHexBinary(fileResult.get("DF46")), "cp1251"));
        }
        if (fileResult.get("DF47") != null)
            cardData.setProtivopokazania(fileResult.get("DF47"));
        if (fileResult.get("DF48") != null)
            cardData.setAllergia(fileResult.get("DF48"));
        if (fileResult.get("DF49") != null)
            cardData.setIlls(fileResult.get("DF49"));

        cardData.setBloodResus(fileResult.get("DF18"));
        cardData.setBloodGroup(fileResult.get("DF19"));
        cardData.setFile4Hash(fileResult.get("DF54"));
    }

    private static String readFileData(IsoDep isoDep, Byte keyNumber, String keyValue, String ef) throws Exception {
        //System.out.println(String.format("***********************Start Test Read File [%s] ", 1));
        selectEFAID(isoDep, "3EF0");
        String sessionKey = authenticateToFile(isoDep, keyValue, keyNumber);
        AttributeEF attr = selectEFAID(isoDep, ef);

        //System.out.println(String.format("File size : [%s] bytes", attr.getFileSizeDec()));
        String fileData = readBinarySec(isoDep, attr.getFileSizeDec(), sessionKey);
        return fileData;
        ///System.out.println(String.format("!!!!!!!!!!!!!!!!Data from file [%s] data : %s}",1, fileData));
    }


    public static void selectDFAID(IsoDep isoDep, String df) throws Exception {
        String cmd = "80A40000"
                + DatatypeConverter.byteToHexString((byte) (df.length() / 2))
                + df;
        new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd)));


//        ResponseAPDU response = cardChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(cmd
//        )));
//        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
        //        dirId = selectResponse.Substring(0, 4);
//        maxFileSize = selectResponse.Substring(4, 4);
//        dirAttribute = selectResponse.Substring(8, 2);
//        countSubDF = selectResponse.Substring(10, 2);
//        countEF = selectResponse.Substring(12, 2);
//        counterPin1 = selectResponse.Substring(14, 2);
//        counterPin2 = selectResponse.Substring(16, 2);
    }

    public static AttributeEF selectEFAID(IsoDep isoDep, String ef) throws Exception {
        String cmd = "80A40000"
                + DatatypeConverter.byteToHexString((byte) (ef.length() / 2))
                + ef;
        System.out.println(String.format("Select EF [%s] comand : [%s]", ef, cmd));
        CData response = new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd)));
        if (!response.isSW9000()) {
            throw new RuntimeException("Select EF wrong answer" + response.getSW());
        }
//        System.out.println("SelectEF " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
        return new AttributeEF(response.getResponseData());

    }

    public static String authenticateToFile(IsoDep isoDep, String key16, byte keyNumber) throws Exception {
        String cmd = "8084000008";
        System.out.println(String.format("Get challenge cmd : [%s]", cmd));
        CData response = new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd)));
//        System.out.println("SelectDf " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));

        String randomCardNum = response.getResponseData();
        String randomTermNum = "EB49DA61821F8F54";
        String diverData = randomCardNum + randomTermNum;

        byte[] sessionKey = SMUtil.encDec3DES2(DatatypeConverter.parseHexBinary(key16), DatatypeConverter.parseHexBinary(diverData), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        byte[] cryptogrammTerminalBytes = SMUtil.encDec3DES2(sessionKey, DatatypeConverter.parseHexBinary(randomCardNum), EnCipherMode.CBC, EnCipherDirection.ENCRYPT_MODE);
        String cryptogrammTerminal = DatatypeConverter.printHexBinary(cryptogrammTerminalBytes);

        cryptogrammTerminal = cryptogrammTerminal.substring(0, 8); // : 0DDEBC19

        cmd = "808200" + DatatypeConverter.byteToHexString(keyNumber) + "0C" + randomTermNum + cryptogrammTerminal; // 01 - ����� �����
        System.out.println(String.format("Start Auth command : [%s]", cmd));
        response = new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd)));
//        System.out.println("Start Auth " + byteToHexString((byte) response.getSW1()) + " "
//                + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));

        return DatatypeConverter.printHexBinary(sessionKey);

    }

    public static String readBinarySec(IsoDep isoDep, int fileSize, String sessionKey) throws SMartChipSMException, IOException {
        int blockSize = 192;//192; // 192 - ����� ������� 8 ��� �������� MAC � ������
        String ClaIns = "84B0";
        int offset = 0;

        StringBuilder sb = new StringBuilder();

        while (fileSize > offset) {
            int length = 0;

            if (fileSize - offset > blockSize) {
                length = blockSize;
            } else {
                length = fileSize - offset;

            }

            String p1p2 = String.format("%04X", offset); //offset.ToString("X04");

            int cmdLen = length + 4;

            String cmd = ClaIns + p1p2 + "05" + String.format("%02X", cmdLen);// cmdLen.ToString("X2");

            //Padder padder = new Padder();
            //padder.AddRange(StringConvert.HexStringToByteArray(cmd));

            String calcMacData = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(cmd), EnMacMode.MAC_RETAIL));

//                    StringConvert.ByteArrayToHexString(SupportDllCrypto.CalculateRetailMac(
//                    padder.ToPaddedArrayDpfs(),
//                    StringConvert.HexStringToByteArray(sessionKey)));

            String mac = calcMacData.substring(0, 8);
            cmd = cmd + mac;
            //String readAnsw = sendApdu(cmd + mac, "Read BinarySec");
            System.out.println(String.format("Read command : [%s]", cmd));
            CData response = new CData(isoDep.transceive(DatatypeConverter.parseHexBinary(cmd)));
            ;
//            System.out.println("Read command result " + byteToHexString((byte) response.getSW1()) + " "
//                    + byteToHexString((byte) response.getSW2()) + "; Data = " + DatatypeConverter.printHexBinary(response.getData()));
            String readAnsw = response.getResponseData();

            if (readAnsw.length() / 2 != cmdLen)
                throw new RuntimeException("Read card error wrong number of read bytes");

            String readData = readAnsw.substring(0, readAnsw.length() - 8);
            String readMac = readAnsw.substring(readAnsw.length() - 8, readAnsw.length());

//            padder = new Padder();
//            padder.AddRange(StringConvert.HexStringToByteArray(readData));

            String calcReadMac = DatatypeConverter.printHexBinary(SMUtil.mac(DatatypeConverter.parseHexBinary(sessionKey),
                    DatatypeConverter.parseHexBinary(readData), EnMacMode.MAC_RETAIL));

//                    StringConvert.ByteArrayToHexString(SupportDllCrypto.CalculateRetailMac(
//                    padder.ToPaddedArrayDpfs(),
//                    StringConvert.HexStringToByteArray(sessionKey)));


            if (!calcReadMac.substring(0, 8).equals(readMac))
                throw new RuntimeException("Read Binary Sec error. readedMAC is not equal calculated read Mac");

            sb.append(readData);
            offset += length;
        }

        return sb.toString();
    }
}
