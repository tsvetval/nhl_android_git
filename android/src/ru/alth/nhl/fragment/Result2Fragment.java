package ru.alth.nhl.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ru.alth.nhl.R;
import ru.alth.nhl.ShowResultActivity;
import ru.alth.nhl.dto.CardData;

/**
 *
 */
public class Result2Fragment  extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CardData cardData = ((ShowResultActivity)getActivity()).getCardData();
        View rootView = inflater.inflate(R.layout.result_2_fragment, container, false);

        //TODO HEX	idnhlnumber
        TextView field = (TextView) rootView.findViewById(R.id.team);
        field.setText(field.getText() + " " + cardData.getTeamName());
        //TODO  vidchlenstva
        field = (TextView) rootView.findViewById(R.id.status);
        String status = "";
        if (cardData.getMemberNhlStatus() != null){
            if ("00".equals(cardData.getMemberNhlStatus())) {
                status += "игрок";
            } else  if ("01".equals(cardData.getMemberNhlStatus())) {
                status += "болельщик";
            } else  if ("02".equals(cardData.getMemberNhlStatus())) {
                status += "судья";
            } else  if ("03".equals(cardData.getMemberNhlStatus())) {
                status += "сотрудник НХЛ";
            }

            if ("00".equals(cardData.getActiveStatus())){
                status += "- активен";
            } else {
                status += "- не активен";
            }
        }
        field.setText(field.getText() + " " + status);

        field = (TextView) rootView.findViewById(R.id.phone);
        field.setText(field.getText() + " " + cardData.getPhoneNumber());

        field = (TextView) rootView.findViewById(R.id.email);
        field.setText(field.getText() + " " + cardData.getEmail());

        field = (TextView) rootView.findViewById(R.id.address);
        field.setText(field.getText() + " " + cardData.getMailingAddress());
        return rootView;
    }
}