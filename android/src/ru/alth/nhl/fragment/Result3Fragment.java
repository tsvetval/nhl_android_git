package ru.alth.nhl.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ru.alth.nhl.R;
import ru.alth.nhl.ShowResultActivity;
import ru.alth.nhl.dto.CardData;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 */
public class Result3Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CardData cardData = ((ShowResultActivity) getActivity()).getCardData();
        View rootView = inflater.inflate(R.layout.result_3_fragment, container, false);

        //TODO cardstatus
        TextView field = (TextView) rootView.findViewById(R.id.cardStatus);
        String nhlStatus = "";
        if ("00".equals(cardData.getCardNhlStatus())) {
            nhlStatus = "активна";
        } else {
            nhlStatus = "не активна";
        }
        field.setText(field.getText() + " " + nhlStatus);

        String dopusk = "";
        field = (TextView) rootView.findViewById(R.id.passOnMatch);
        if ("00".equals(cardData.getPassOnMatch())) {
            dopusk = "допущен";
        } else {
            dopusk = "не допущен";
        }

        field.setText(field.getText() + " " + dopusk);

        String passport = "";
        String documdate = "";
        field = (TextView) rootView.findViewById(R.id.passport);
        if (cardData.getPassport() != null) {
            String date;
            String number;
            if(cardData.getPassport().endsWith("00000000")){
                number = cardData.getPassport().substring(0, cardData.getPassport().length() - 8);
            } else {
                number = cardData.getPassport().substring(0, cardData.getPassport().length()-8);
                date = cardData.getPassport().substring(cardData.getPassport().length()-8);
                SimpleDateFormat formatFrom = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat formatTo = new SimpleDateFormat("dd.MM.yyyy");
                try {
                    documdate = formatTo.format(formatFrom.parse(date));
                } catch (ParseException e) {

                }
            }
            passport = number;
        }

        field.setText(field.getText() + " " + passport);
        field = (TextView) rootView.findViewById(R.id.documdate);

        field.setText(field.getText() + " " + documdate);
        //TODO documdate
        field = (TextView) rootView.findViewById(R.id.prava);
        field.setText(field.getText() + " " + cardData.getPrava());
        //TODO driverdate

        field = (TextView) rootView.findViewById(R.id.matchDate);
        field.setText(field.getText() + " " + cardData.getMatchDate());

        field = (TextView) rootView.findViewById(R.id.matchPlace);
        field.setText(field.getText() + " " + cardData.getMatchPlace());

        field = (TextView) rootView.findViewById(R.id.matchTeam);
        field.setText(field.getText() + " " + cardData.getMatchTeam());

        field = (TextView) rootView.findViewById(R.id.playerNumber);
        field.setText(field.getText() + " " + cardData.getPlayerNumber());
        return rootView;
    }
}