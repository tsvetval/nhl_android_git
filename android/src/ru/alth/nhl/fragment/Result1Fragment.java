package ru.alth.nhl.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import ru.alth.nhl.R;
import ru.alth.nhl.ShowResultActivity;
import ru.alth.nhl.dto.CardData;

/**
 *
 */
public class Result1Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.result_1_fragment, container, false);
        CardData cardData = ((ShowResultActivity)getActivity()).getCardData();

        TextView firstName = (TextView) rootView.findViewById(R.id.FirstName);
        firstName.setText(cardData.getFirstName() + " " + cardData.getSecondName());

//            TextView secondName = (TextView) findViewById(R.id.SecondName);
//            secondName.setText(cardData.getSecondName());

        TextView lastName = (TextView) rootView.findViewById(R.id.LastName);
        lastName.setText(cardData.getLastName());

        TextView dateBirth = (TextView) rootView.findViewById(R.id.DateBirth);
        dateBirth.setText(cardData.getBirthDate());
        if (cardData.getPhoto() != null && cardData.getPhoto().length > 0) {
            Bitmap bm = BitmapFactory.decodeByteArray(cardData.getPhoto(), 0, cardData.getPhoto().length);
            //DisplayMetrics dm = new DisplayMetrics();
            //getWindowManager().getDefaultDisplay().getMetrics(dm);
            ImageView imageView = (ImageView) rootView.findViewById(R.id.photo);
            //imageView.setMinimumHeight(dm.heightPixels);
            //imageView.setMinimumWidth(dm.widthPixels);
            imageView.setImageBitmap(bm);
        }

        TextView field = (TextView) rootView.findViewById(R.id.expDate);
        field.setText(field.getText() + " " + cardData.getExpirationDate());


        if (cardData.getOnlineCardStatus() != null) {
            ProgressBar pb = (ProgressBar) rootView.findViewById(R.id.checkStatusBar);
            pb.setVisibility(View.GONE);
            TextView status = (TextView) rootView.findViewById(R.id.onlineStatus);
            switch (cardData.getOnlineCardStatus()) {
                case OK:
                    status.setTextColor(Color.GREEN);
                    status.setText("Статус подтвержден");
                    break;
                case FAIL:
                    status.setTextColor(Color.RED);
                    status.setText("Статус не подтвержден");
                    break;
                case SERVICE_NOT_AVAILABLE:
                    status.setTextColor(Color.YELLOW);
                    status.setText("Сервис недоступен");
                    break;
            }
        }

        if (cardData.is_readedByOnline()){
            ProgressBar pb = (ProgressBar) rootView.findViewById(R.id.checkStatusBar);
            pb.setVisibility(View.GONE);
            TextView status = (TextView) rootView.findViewById(R.id.onlineStatus);
            status.setTextColor(Color.GREEN);
            status.setText("Данные онлайн");
            pb.setVisibility(View.GONE);
        }

        return rootView;
    }
}
