package ru.alth.nhl.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ru.alth.nhl.R;
import ru.alth.nhl.ShowResultActivity;
import ru.alth.nhl.dto.CardData;

/**
 *
 */
public class Result4Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CardData cardData = ((ShowResultActivity)getActivity()).getCardData();
        View rootView = inflater.inflate(R.layout.result_4_fragment, container, false);

        TextView field = (TextView) rootView.findViewById(R.id.insuaranceCompany);
        field.setText(field.getText() + " " + cardData.getInsuranceCompany());

        field = (TextView) rootView.findViewById(R.id.insuaranceNumber);
        field.setText(field.getText() + " " + cardData.getInsuranceNumber());

        field = (TextView) rootView.findViewById(R.id.medExplore);
        field.setText(field.getText() + " " + cardData.getMedExploreDate());

        String result = "";
        field = (TextView) rootView.findViewById(R.id.medExploreResult);
        if ("00".equals(cardData.getMedExploreResult())){
            result = "годен";
        } else {
            result = "не годен";
        }
        field.setText(field.getText() + " " + result);

        field = (TextView) rootView.findViewById(R.id.height);
        field.setText(field.getText() + " " + cardData.getHeight());

        field = (TextView) rootView.findViewById(R.id.weight);
        field.setText(field.getText() + " " + cardData.getWeight());

        field = (TextView) rootView.findViewById(R.id.protivopokazania);
        if ("00".equals(cardData.getProtivopokazania())){
            result = "да";
        } else {
            result = "нет";
        }
        field.setText(field.getText() + " " + result);

        field = (TextView) rootView.findViewById(R.id.allergia);
        if ("00".equals(cardData.getAllergia())){
            result = "да";
        } else {
            result = "нет";
        }
        field.setText(field.getText() + " " + result);

        field = (TextView) rootView.findViewById(R.id.ills);
        if ("00".equals(cardData.getIlls())){
            result = "да";
        } else {
            result = "нет";
        }
        field.setText(field.getText() + " " + result);

        field = (TextView) rootView.findViewById(R.id.blood);
        field.setText(field.getText() + " " + cardData.printBlood());

        return rootView;
    }
}