package ru.alth.nhl;

import android.app.ActionBar;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import ru.alth.nhl.dto.CardData;
import ru.alth.nhl.helpers.DialogHelper;
import ru.alth.nhl.task.ReadCardTask;

/**
 *
 */
public class ReadCardActivity extends Activity {
    enum State {WAIT_FOR_CARD, READING_CARD}

    private NfcAdapter adapter;
    private PendingIntent pendingIntent;

    private static volatile State state = State.WAIT_FOR_CARD;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        /**
         * Экран всегда включен  http://developer.android.com/intl/ru/reference/android/os/PowerManager.html
         */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        NfcManager manager = (NfcManager) getApplicationContext().getSystemService(Context.NFC_SERVICE);
        adapter = manager.getDefaultAdapter();
        pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        showWaitForCard();
    }


    private void showWaitForCard() {
        try {
            adapter.enableForegroundDispatch(this, pendingIntent, null, null);
        } catch (Throwable e) {

        }
        synchronized (ReadCardActivity.class) {
            state = State.WAIT_FOR_CARD;
            setContentView(R.layout.main);
            findViewById(R.id.readingImage).setVisibility(View.INVISIBLE);
            ImageView btnSettings = (ImageView) findViewById(R.id.settings);
            btnSettings.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    synchronized (ReadCardActivity.class) {
                        if (state.equals(State.WAIT_FOR_CARD)) {
                            showSettings();
                        }
                    }
                }
            });
        }
    }

    public void showResult(CardData cardData) {
        synchronized (ReadCardActivity.class) {
            if (cardData.getCardReadError() != null) {
                TextView logRes = (TextView) findViewById(R.id.padCard);
                logRes.setText(cardData.getCardReadError());
                //TODO comment to show result error
                showWaitForCard();
                return;
            }
            Intent intent = new Intent(ReadCardActivity.this, ShowResultActivity.class);
            Bundle b = new Bundle();
            intent.putExtra("cardData", cardData);
            startActivity(intent);
            finish();



/*
            ImageView btnPassObject = (ImageView) findViewById(R.id.goHomeButton);
            btnPassObject.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    animateHideResult();

                }
            });
*/

        }

    }


    public void showSettings() {
        synchronized (ReadCardActivity.class) {
            Intent intent = new Intent(ReadCardActivity.this, SettingsActivity.class);
            //Bundle b = new Bundle();
            //intent.putExtra("cardData", cardData);
            startActivity(intent);
            finish();
        }

    }


    protected void onResume() {
        try {
            super.onResume();
           /* if (adapter == null) {
                adapter = getAdapter();
            }*/
            adapter.enableForegroundDispatch(this, pendingIntent, null, null);
            //popupInsertCard();
        } catch (Throwable e) {
            // ((TextView) findViewById(R.id.temTextView)).append("!onResume  state = " + state.name());
            DialogHelper.getInstance().showAlert("NFC Error! " + e.toString(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.exit(0);
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //TODO
        adapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (state.equals(State.WAIT_FOR_CARD)) {
            synchronized (ReadCardActivity.class) {
                if (state.equals(State.WAIT_FOR_CARD)) {
                    state = State.READING_CARD;
                    findViewById(R.id.readingImage).setVisibility(View.VISIBLE);

                    SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.NHL_PREFERENCES), Context.MODE_WORLD_WRITEABLE);
                    boolean isOnline = sharedPref.getBoolean(getString(R.string.SETTINGS_USE_ONLINE), true);
                    final String serverText = sharedPref.getString(getString(R.string.SETTINGS_SERVER), "172.16.16.35:8080/nhl-arm/");
                    intent.putExtra(getString(R.string.SETTINGS_USE_ONLINE), isOnline);
                    intent.putExtra(getString(R.string.SETTINGS_SERVER), serverText);
                    // Запускаем задачу чтения карты
                    new ReadCardTask(new ReadCardTask.TaskListener() {
                        public void onFinished(CardData cardData) {
                            if (cardData != null) {
                                cardData.setServerUrl(serverText);
                                showResult(cardData);
                            } else {
                                showWaitForCard();
                            }
                        }
                    }).execute(intent);
                }
            }
        }
    }


    /*ANIMATION SECTION*/
    // 2.0 and above
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
